package fi.sls.ingest.controller;

import fi.sls.ingest.exception.UserNotFoundException;
import fi.sls.ingest.repository.UserRepository;
import fi.sls.ingest.representation.User;
import fi.sls.ingest.security.AuthenticationFacade;
import fi.sls.ingest.security.jwt.UserPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.Optional;

@BasePathAwareController
@RestController
@Slf4j
public class UserController {

    /**
     * Reference to source of currently authenticated user
     */
    AuthenticationFacade authenticationFacade;

    /**
     * Repository from which to load user details
     */
    UserRepository userRepository;

    @Autowired
    public UserController (
            AuthenticationFacade authenticationFacade,
            UserRepository userRepository
    ){
        this.authenticationFacade = authenticationFacade;
        this.userRepository = userRepository;
    }

    /**
     * Provides detailed info on the authenticated user
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/user/me")
    public User me() {
        log.debug("Received user/me request for authenticated user");

        Authentication currentAuth = authenticationFacade.getAuthentication();
        User u = ((UserPrincipal) currentAuth.getPrincipal()).getUser();

        // ensure by loading data from repository
        Optional<User> user = userRepository.findById(u.getId());

        return user.orElseThrow(() -> new UserNotFoundException("User entity not found for authenticated user"));
    }
}
