package fi.sls.ingest.fits.model.fieldresolver.identification;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;

import java.util.Optional;

public class FitsIdentityMetadataFormatVersion extends AbstractFieldValueResolver {

    public FitsIdentityMetadataFormatVersion(FieldValueResolver resolver) {
        super("formatVersion", resolver);
    }

    public FitsIdentityMetadataFormatVersion(FitsResult fitsResult) {
        super("formatVersion", fitsResult);
    }

    public FitsIdentityMetadataFormatVersion(FitsResult fitsResult, FieldValueResolver resolver) {
        super("formatVersion", fitsResult, resolver);
    }

    @Override
    public Optional<String> getValue() {

        if(!fitsResult.getFitsResult().getIdentification().getIdentity().isEmpty() &&
                !fitsResult.getFitsResult().getIdentification().getIdentity().get(0).getVersion().isEmpty()
        ){
            String rtn = fitsResult.getFitsResult().getIdentification().getIdentity().get(0).getVersion().get(0).getValue();

            if(rtn != null && !rtn.isEmpty()){
                return Optional.of(rtn);
            }
        }

        return executeNextResolver();
    }
}
