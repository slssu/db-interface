package fi.sls.ingest.fits.model.fieldresolver.audio;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.*;
import java.util.Iterator;
import java.util.Optional;

public class JHoveWAVEMetadata extends AbstractFieldValueResolver {

    public final static String PROP_AVERAGE_BYTES_PER_SECOND = "AverageBytesPerSecond";

    public JHoveWAVEMetadata(String fieldName, FieldValueResolver resolver) {
        super(fieldName, resolver);
    }

    public JHoveWAVEMetadata(String fieldName, FitsResult fitsResult) {
        super(fieldName, fitsResult);
    }

    public JHoveWAVEMetadata(String fieldName, FitsResult fitsResult, FieldValueResolver resolver) {
        super(fieldName, fitsResult, resolver);
    }

    @Override
    public Optional<String> getValue() {
        XPath xPath = XPathFactory.newInstance().newXPath();

        try {

            xPath.setNamespaceContext(new JHoveWAVENamespaceContext(fitsResult.getXmlDocument()));

            String expStr = "/fits:fits/fits:toolOutput/fits:tool[@name = 'Jhove']//properties/property/values/property[name='AverageBytesPerSecond']/values/value";

            XPathExpression expression = xPath.compile(expStr);

            NodeList nodes = (NodeList) expression.evaluate(fitsResult.getXmlDocument(), XPathConstants.NODESET);
            if(nodes.getLength() > 0){
                Node n = nodes.item(0);
                String val = n.getTextContent();

                if(val != null && !val.isEmpty()){

                    // field contains byte value, but we want bit so multiplication heaven
                    Long multiplied = Long.parseLong(val) * 8;

                    return Optional.of(multiplied.toString());
                }
            }

            return executeNextResolver();
        } catch (XPathExpressionException e) {
            return executeNextResolver();
        }
    }


    class JHoveWAVENamespaceContext implements NamespaceContext{

        private Document sourceDocument;

        public JHoveWAVENamespaceContext(Document document){
            sourceDocument = document;
        }

        @Override
        public String getNamespaceURI(String prefix) {
            if (prefix.equals(XMLConstants.DEFAULT_NS_PREFIX)) {
                return sourceDocument.lookupNamespaceURI(null);
            } else if(prefix.equals("fits")) {
                return "http://hul.harvard.edu/ois/xml/ns/fits/fits_output";
            } else {
                return sourceDocument.lookupNamespaceURI(prefix);
            }
        }

        @Override
        public String getPrefix(String namespaceURI) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Iterator<String> getPrefixes(String namespaceURI) {
            throw new UnsupportedOperationException();
        }
    }
}
