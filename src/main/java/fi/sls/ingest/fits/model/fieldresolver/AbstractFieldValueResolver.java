package fi.sls.ingest.fits.model.fieldresolver;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.generated.Fits;
import org.w3c.dom.Element;

import java.util.Optional;

public abstract class AbstractFieldValueResolver implements FieldValueResolver {

    protected FitsResult fitsResult;
    protected String fieldName;
    protected FieldValueResolver resolver;

    /**
     * Set to true to disable the next resolver that would be called
     * when executeNextResolver is called.
     *
     * This is mainly intended to temporarily disable the next resolver in subclasses
     * that may need to alter the timing for when the resolver is called.
     *
     */
    protected boolean disableNextResolver = false;

    public AbstractFieldValueResolver(String fieldName, FieldValueResolver resolver){
        this(fieldName, resolver.getFitsResult(), resolver);
    }

    public AbstractFieldValueResolver(String fieldName, FitsResult fitsResult){
        this.fieldName = fieldName;
        this.fitsResult = fitsResult;
    }

    /**
     *
     * @param fitsResult
     * @param resolver
     */
    public AbstractFieldValueResolver(String fieldName, FitsResult fitsResult, FieldValueResolver resolver){
        this(fieldName, fitsResult);
        this.resolver = resolver;
    }

    @Override
    public FitsResult getFitsResult() {
        return fitsResult;
    }


    /**
     * Helper for calling the next resolver in the composition chain
     *
     * It is up to subclasses implementing the getValue method to
     * make sure they call this method to ensure the class maintains
     * the chain.
     *
     * @return
     */
    protected Optional<String> executeNextResolver(){
        // This resolver couldn't find a value.
        // Try the next resolver, if one has been chained
        if(!disableNextResolver && resolver != null){
            return resolver.getValue();
        } else {
            return Optional.empty();
        }
    }

    /**
     * Helper for fetching the result structure from a specific tool output, if available
     * on the FITS result XML
     *
     * @param generatedResult
     * @param name
     * @return
     */
    protected Element getToolResultByName(Fits generatedResult, String name){

        if(generatedResult.getToolOutput() != null){
            Optional<Element> element = generatedResult.getToolOutput().getAny().stream().filter(
                    e -> e.getAttribute("name").equalsIgnoreCase(name)
            ).findFirst();

            if(element.isPresent()){
                return element.get();
            }
        }

        return null;
    }
}
