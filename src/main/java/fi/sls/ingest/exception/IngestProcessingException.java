package fi.sls.ingest.exception;

/**
 * Exception thrown if something goes wrong during the ingest process.
 *
 * This is an unchecked exception, so we expect the caller to know to handle it if they want to.
 */
public class IngestProcessingException extends RuntimeException {
    public IngestProcessingException(String exception){
        super(exception);
    }
    public IngestProcessingException(Exception e){
        super(e);
    }
}
