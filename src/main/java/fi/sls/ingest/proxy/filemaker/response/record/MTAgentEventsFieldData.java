package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import fi.sls.ingest.proxy.filemaker.request.RecordPayload;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MTAgentEventsFieldData implements RecordPayload {

    // ID (nummer in FileMaker)
    @JsonProperty("nummer")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Long entityNumber;

    @JsonProperty("date_create")
    @JsonFormat(pattern = "MM/dd/yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    LocalDate dateCreate;

    @JsonProperty("date_modify")
    @JsonFormat(pattern = "MM/dd/yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    LocalDate dateModify;

    @JsonProperty("creator")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String creator;

    @JsonProperty("modifier")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String modifier;

    @JsonProperty("ag_nr")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Long agentNr;

    @JsonProperty("ev_nr")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Long eventNr;
}
