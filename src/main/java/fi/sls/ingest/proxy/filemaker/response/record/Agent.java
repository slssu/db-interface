package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import lombok.Data;
import lombok.Getter;

/**
 * Entity representing API responses for values in the "agents" table/view in FileMaker.
 */
@Data
public class Agent extends FileMakerResponseEntity {

    @Getter
    @JsonIgnore
    private String layoutName = "ingestapi_agents";

    AgentFieldData fieldData;

    public Agent(){
        this.fieldData = new AgentFieldData();
    }

    @Override
    public FindQueryRequest createFindQueryRequest() {
        FindQueryRequest queryRequest = new FindQueryRequest("name", "="+getFieldData().getName());
        queryRequest.addToExistingQueryItem(0, "version", getFieldData().getVersion());

        return queryRequest;
    }

    @Override
    public UpdateRequest createUpdateRequest() {
        return null;
    }

    @Override
    public CreateRequest createRecordRequest() {
        return null;
    }
}
