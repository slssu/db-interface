package fi.sls.ingest.migration.report;

import fi.sls.ingest.migration.MigrationResult;
import org.apache.poi.ss.usermodel.*;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public abstract class XLSXReportBase {

    MigrationResult result;

    CellStyle headerCellStyle;

    Map<String, CellStyle> cellStyleMap = new HashMap<>();

    public XLSXReportBase(MigrationResult result){
        this.result = result;
    }

    /**
     * Process the migrationResult and generate report Workbook
     *
     * @return
     */
    public abstract Workbook generate();

    protected void createCellWithValue(Row row, int column, String value, short fgColor){
        createCellWithValue(row, column, value, fgColor, IndexedColors.BLACK.getIndex(), false);
    }

    protected void createCellWithValue(Row row, int column, String value, short fgColor, boolean setWrapText){
        createCellWithValue(row, column, value, fgColor, IndexedColors.BLACK.getIndex(), setWrapText);
    }

    protected void createCellWithValue(Row row, int column, String value, short fgColor, short fontColor, boolean setWrapText){

        String styleKey = Short.toString(fgColor)
                .concat("_").concat(Short.toString(fontColor))
                .concat("_").concat(Boolean.toString(setWrapText));

        CellStyle applyStyle;

        // ensure we don't create new cell styles for each cell, as there is a limit to nr of styles in a worksheet
        if(cellStyleMap.get(styleKey) != null){
            applyStyle = cellStyleMap.get(styleKey);
        } else {
            Font font = row.getSheet().getWorkbook().createFont();
            font.setColor(fontColor);

            applyStyle = row.getSheet().getWorkbook().createCellStyle();
            applyStyle.setVerticalAlignment(VerticalAlignment.TOP);
            applyStyle.setWrapText(setWrapText);
            applyStyle.setFont(font);
            applyStyle.setFillForegroundColor(fgColor);
            applyStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            cellStyleMap.put(styleKey, applyStyle);
        }

        Cell cell = row.createCell(column);
        cell.setCellStyle(applyStyle);
        cell.setCellValue(value);
    }

    protected int createParametersInfoBlock(Sheet sheet, int rowPos){
        rowPos = createCellKeyValRow(sheet, rowPos,"Samling", result.getCollectionName() + " " + result.getArchiveNr());
        rowPos =  createCellKeyValRow(sheet, rowPos,"Testkörning (inga värden ändras)", result.getIsDryRun() ? "Ja" : "Nej");

        return rowPos;
    }

    protected int createCellKeyValRow(Sheet sheet, int rowPos, String label, String value){

        Row row = sheet.createRow(rowPos++);

        Cell labelCell = row.createCell(0);
        labelCell.setCellStyle(getHeaderStyle(sheet.getWorkbook()));
        labelCell.setCellValue(label);

        Cell valueCell = row.createCell(1);
        valueCell.setCellValue(value);

        return rowPos;
    }

    protected CellStyle getHeaderStyle(Workbook workbook){

        if(headerCellStyle != null){
            return headerCellStyle;
        }

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        headerCellStyle.setFillBackgroundColor(IndexedColors.LIGHT_ORANGE.getIndex());

        return headerCellStyle;
    }

    protected void resizeColumns(int length, Sheet sheet){
        // Resize all columns to fit the content size
        for(int i = 0; i < length; i++) {
            sheet.autoSizeColumn(i);
        }
    }

    protected String getLocalizedDateTime(Instant instant){
        if(instant != null){
            DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME
                    .withLocale( Locale.forLanguageTag("FI") )
                    .withZone( ZoneId.of("Europe/Helsinki"));
            return formatter.format(instant);
        } else {
            return "";
        }
    }
}
