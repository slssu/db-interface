package fi.sls.ingest.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class RestRepositoryConfig implements RepositoryRestConfigurer {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        // allow cors requests from our local nodejs dev server on all paths
        config.getCorsRegistry()
                .addMapping("/**")
                .allowedOrigins(
                        "http://localhost",
                        "http://localhost:3000",
                        "http://10.0.32.124",
                        "https://10.0.32.124",
                        "http://ingest.sls.fi",
                        "https://ingest.sls.fi",
                        "http://ingest01.sls.fi",
                        "https://ingest01.sls.fi",
                        "http://ingest02.sls.fi",
                        "https://ingest02.sls.fi"
                )
                .allowedMethods("PUT", "POST", "GET", "OPTIONS", "DELETE", "PATCH");
    }
}
