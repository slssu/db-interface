package fi.sls.ingest.actuator.health;

import fi.sls.ingest.proxy.slsfileapi.SLSFileAPI;
import fi.sls.ingest.proxy.slsfileapi.exception.FileAPIException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FileAPIHealthCheckTest {

    SLSFileAPI service;
    FileAPIHealthCheck testable;

    @BeforeEach
    void setUp(){
        service = mock(SLSFileAPI.class);

        testable = new FileAPIHealthCheck(service);
    }

    @Test
    void health_returnsUpForServiceOkResponse() {

        when(service.healthcheck()).thenReturn("OK");

        Health response = testable.health();

        assertEquals(Status.UP, response.getStatus());
        assertNotNull(response.getDetails().get("response_time_ms"));
    }

    @Test
    void health_returnsDownForServiceNullResponse() {

        when(service.healthcheck()).thenReturn(null);

        Health response = testable.health();

        assertEquals(Status.DOWN, response.getStatus());
        assertNotNull(response.getDetails().get("error"));
        assertEquals("Could not determine state of service", response.getDetails().get("error"));

    }

    @Test
    void health_returnsDownForFileAPIException() {
        FileAPIException e = new FileAPIException("failed");

        when(service.healthcheck()).thenThrow(e);

        Health response = testable.health();

        assertEquals(Status.DOWN, response.getStatus());
        assertNotNull(response.getDetails().get("error"));
        assertEquals("failed", response.getDetails().get("error"));
    }
}