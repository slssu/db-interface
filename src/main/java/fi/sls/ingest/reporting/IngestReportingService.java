package fi.sls.ingest.reporting;

import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.repository.UserRepository;
import fi.sls.ingest.representation.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Service
public class IngestReportingService {

    IngestItemRepository dataSource;
    UserRepository userRepository;

    @Autowired
    public IngestReportingService(IngestItemRepository dataSource, UserRepository userRepository){
        this.dataSource = dataSource;
        this.userRepository = userRepository;
    }

    public IngestSummaryReport getSummaryReport(LocalDate fromDate, LocalDate toDate){
        IngestSummaryReport report = new IngestSummaryReport(fromDate, toDate);

        // add count for overall (only archived + complete, in case some items in different states are in queue)
        IngestItemSummaryReport totals = new IngestItemSummaryReport();
        totals.setLegend("total");
        totals.setCount(dataSource.countByProcessingStatusIn(Arrays.asList(ProcessingStatus.COMPLETE, ProcessingStatus.ARCHIVED)).orElse(null));
        totals.setTotalBytesProcessed(dataSource.getTotalBytesProcessed().orElse(null));
        report.setItemSummary(totals);

        // fetch users
        Iterable<User> users = userRepository.findAll();
        Iterator<User> userIterator = users.iterator();

        // for each user, add sub-report
        List<IngestItemSummaryReport> userBreakdown = new ArrayList<>();

        while(userIterator.hasNext()){
            User u = userIterator.next();

            IngestItemSummaryReport userReport = new IngestItemSummaryReport(u.getDisplayName(),
                    123L,
                    123123213L
            );

            userBreakdown.add(userReport);
        }


        report.setItemSummaryByUser(userBreakdown);

        return report;
    }

    /**
     * Generate report on staging items for the given user
     *
     * @param user
     * @return
     */
    public StagingItemsReport getStagingItemReport(User user) {
        StagingItemsReport report = new StagingItemsReport();

        report.setItems(dataSource.findByProcessingStatusInAndCreatedByIdIn(
                Arrays.asList(ProcessingStatus.STAGING),
                Arrays.asList(user.getId())
        ));



        return report;
    }
}
