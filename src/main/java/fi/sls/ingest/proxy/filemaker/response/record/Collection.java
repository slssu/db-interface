package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import lombok.Data;
import lombok.Getter;

/**
 * Entity representing API responses for values in the "Nya samlingsvyn" table/view in FileMaker.
 */
@Data
public class Collection extends FileMakerResponseEntity {

    @Getter
    @JsonIgnore
    private String layoutName = "ingestapi_samlingar";

    CollectionFieldData fieldData;

    public Collection(){
        this.fieldData = new CollectionFieldData();
    }

    @Override
    public FindQueryRequest createFindQueryRequest(){
        FindQueryRequest queryRequest = new FindQueryRequest( "arkivetsNr", "="+getFieldData().getArchiveNumber());
        queryRequest.addToExistingQueryItem(0, "slsArkiv", "="+getFieldData().getCollectionName());

        if(getFieldData().getArchiveNumberSuffix()!= null){
            queryRequest.addToExistingQueryItem(0, "arkivetsNr_subsignum", "="+getFieldData().getArchiveNumberSuffix());
        }

        return queryRequest;
    }

    @Override
    public UpdateRequest createUpdateRequest() {
        return new UpdateRequest(getFieldData());
    }

    @Override
    public CreateRequest createRecordRequest() {
        return new CreateRequest(getFieldData());
    }
}
