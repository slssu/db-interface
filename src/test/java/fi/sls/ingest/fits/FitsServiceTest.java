package fi.sls.ingest.fits;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FitsServiceTest {

    FitsService testable;

    @BeforeEach
    void setUp(){
    }

    @Test
    void getMetadataPathForFile() {
        assertEquals("myfile.jpg.fits.xml", FitsService.getMetadataPathForFile("myfile.jpg"));
    }

    @Test
    void generateApiURL() {
    }
}