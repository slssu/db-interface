package fi.sls.ingest.migration.generator.fieldstrategy;

import fi.sls.ingest.proxy.filemaker.response.record.CollectionFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.DigitalObjectFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntityFieldData;

/**
 * Interface to be implemented by generators that are used to extract values for specific fields
 *
 * e.g. to generate the value for metaData.archiveCatalogNr for the OTA 112 collection
 *
 */
public interface FieldGeneratorStrategy<T, V> {

    String getSourceObjectMethodName();
    T generate(V source);
}
