package fi.sls.ingest.reporting.xlsx;

import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.migration.DigitalObjectMigrationResult;
import fi.sls.ingest.reporting.StagingItemsReport;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.HashMap;
import java.util.Map;

public class XLSXStagingItemsReport {

    Map<String, CellStyle> cellStyleMap = new HashMap<>();
    CellStyle headerCellStyle;

    protected StagingItemsReport source;

    private static String[] columns = {
            "filename",
            "entity_identifier",
            "identifier",
            "dc_identifier"
    };

    public XLSXStagingItemsReport(StagingItemsReport source){
        this.source = source;
    }

    public Workbook generate() {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("StagingFiler");

        // row position that will be written to next
        int rowPos = 0;

        // Create header row
        Row headerRow = sheet.createRow(rowPos++);
        for (int i = 0; i < columns.length; i++) {
            Cell headerCell = headerRow.createCell(i);
            headerCell.setCellStyle(getHeaderStyle(workbook));
            headerCell.setCellValue(columns[i]);
        }

        // populate with item results
        for (IngestProcessingItem item : source.getItems()) {

            Row row = sheet.createRow(rowPos++);

            createCellWithValue(row, 0, item.getFilenameMetaData().getFilename());
            createCellWithValue(row, 1, item.getFilenameMetaData().getEntityIdentifier());
            createCellWithValue(row, 2, item.getFilenameMetaData().getDigitalObjectIdentifier());
            createCellWithValue(row, 3, item.getFilenameMetaData().getDcIdentifier());

        }

        resizeColumns(columns.length, sheet);

        return workbook;
    }

    protected void createCellWithValue(Row row, int column, String value){
        createCellWithValue(row, column, value, IndexedColors.BLACK.getIndex(), false);
    }

    protected void createCellWithValue(Row row, int column, String value, short fontColor, boolean setWrapText){

        String styleKey = Short.toString(fontColor)
                .concat("_").concat(Boolean.toString(setWrapText));

        CellStyle applyStyle;

        // ensure we don't create new cell styles for each cell, as there is a limit to nr of styles in a worksheet
        if(cellStyleMap.get(styleKey) != null){
            applyStyle = cellStyleMap.get(styleKey);
        } else {
            Font font = row.getSheet().getWorkbook().createFont();
            font.setColor(fontColor);

            applyStyle = row.getSheet().getWorkbook().createCellStyle();
            applyStyle.setVerticalAlignment(VerticalAlignment.TOP);
            applyStyle.setWrapText(setWrapText);
            applyStyle.setFont(font);
            applyStyle.setFillPattern(FillPatternType.NO_FILL);

            cellStyleMap.put(styleKey, applyStyle);
        }

        Cell cell = row.createCell(column);
        cell.setCellStyle(applyStyle);
        cell.setCellValue(value);
    }

    protected CellStyle getHeaderStyle(Workbook workbook){

        if(headerCellStyle != null){
            return headerCellStyle;
        }

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        headerCellStyle.setFillBackgroundColor(IndexedColors.LIGHT_ORANGE.getIndex());

        return headerCellStyle;
    }

    protected void resizeColumns(int length, Sheet sheet){
        // Resize all columns to fit the content size
        for(int i = 0; i < length; i++) {
            sheet.autoSizeColumn(i);
        }
    }


}
