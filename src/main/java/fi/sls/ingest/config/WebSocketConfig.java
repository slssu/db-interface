package fi.sls.ingest.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.broker.BrokerAvailabilityEvent;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
@EnableWebSocket
@Slf4j
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer, ApplicationListener<BrokerAvailabilityEvent> {

    @Value("${spring.rabbitmq.host}")
    protected String rabbitHost;

    @Value("${spring.rabbitmq.username}")
    protected String rabbitUsername;

    @Value("${spring.rabbitmq.password}")
    private String rabbitPassword;


    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws").setAllowedOrigins("*");
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app");
        //registry.enableSimpleBroker("/topic");


        log.info("Going to start relay broker on host {}", rabbitHost);

        registry
            .enableStompBrokerRelay("/topic", "/queue")
            .setAutoStartup(true)
            .setRelayHost(rabbitHost)
            .setRelayPort(61613)
            .setClientLogin(rabbitUsername)
            .setClientPasscode(rabbitPassword)
            .setSystemLogin(rabbitUsername)
            .setSystemPasscode(rabbitPassword)
            .setVirtualHost("/");
    }

    @Override
    public void onApplicationEvent(BrokerAvailabilityEvent brokerAvailabilityEvent) {
        log.info("WebSocketMessageBroker available: {}", brokerAvailabilityEvent.isBrokerAvailable());
    }
}


