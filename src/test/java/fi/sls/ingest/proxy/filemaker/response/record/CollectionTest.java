package fi.sls.ingest.proxy.filemaker.response.record;

import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CollectionTest {

    Collection testable;

    @BeforeEach
    void setUp(){
        testable = new Collection();
        testable.setFieldData(mock(CollectionFieldData.class));
    }

    @Test
    void layoutName_isCorrect(){
        assertEquals("ingestapi_samlingar", testable.getLayoutName());
    }

    @Test
    void createUpdateRequest(){
        UpdateRequest rtn = testable.createUpdateRequest();
        assertEquals(testable.getFieldData(), rtn.getFieldData());
    }

    @Test
    void createRecordRequest(){
        CreateRequest rtn = testable.createRecordRequest();
        assertEquals(testable.getFieldData(), rtn.getFieldData());
    }

    @Test
    void createFindQueryRequest_queryWithOutSubSignum() {

        when(testable.getFieldData().getCollectionName()).thenReturn("SLS");
        when(testable.getFieldData().getArchiveNumber()).thenReturn(123L);

        FindQueryRequest rtn = testable.createFindQueryRequest();

        assertEquals("=SLS", rtn.getQuery().get(0).get("slsArkiv"));
        assertEquals("=123", rtn.getQuery().get(0).get("arkivetsNr"));
    }

    @Test
    void createFindQueryRequest_queryHAvingSubSignum() {

        when(testable.getFieldData().getCollectionName()).thenReturn("Tjänstearkivet");
        when(testable.getFieldData().getArchiveNumber()).thenReturn(999L);
        when(testable.getFieldData().getArchiveNumberSuffix()).thenReturn("12");

        FindQueryRequest rtn = testable.createFindQueryRequest();

        assertEquals("=Tjänstearkivet", rtn.getQuery().get(0).get("slsArkiv"));
        assertEquals("=999", rtn.getQuery().get(0).get("arkivetsNr"));
        assertEquals("=12", rtn.getQuery().get(0).get("arkivetsNr_subsignum"));
    }
}