# Add access_files column on user
ALTER TABLE `digital_object_migration_result` ADD COLUMN `original_access_files` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL AFTER `filename_meta_data`;
ALTER TABLE `digital_object_migration_result` ADD COLUMN `migrated_access_files` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL AFTER `filename_meta_data`;