package fi.sls.ingest.representation.file;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ObjectCategoryTest {


    @Test
    void constructor_populatesAllArgs(){
        ObjectCategory testable = new ObjectCategory("myfilekey","myarchivesig", "mycategorylabel", "mysubcategory", "aDC2Type");

        assertEquals("myfilekey", testable.getFileKey());
        assertEquals("myarchivesig", testable.getArchiveSignumKey());
        assertEquals("mycategorylabel", testable.getCategoryLabel());
        assertEquals("mysubcategory", testable.getSubCategory());
        assertEquals("aDC2Type", testable.getDc2Type());

    }

}