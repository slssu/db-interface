package fi.sls.ingest.fits.model;

import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.audio.FitsAudioMetadata;
import fi.sls.ingest.fits.model.fieldresolver.audio.JHoveWAVEMetadata;
import fi.sls.ingest.fits.model.fieldresolver.tool.MediaInfoMetadata;
import fi.sls.ingest.fits.model.generated.Fits;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.TimeZone;

/**
 * Represents the FITS analysis result for a file that has been identified as audio data
 */
public class FitsAudioResult extends FitsResult {
    public FitsAudioResult(Fits generatedResult) {
        super(generatedResult);
    }
    public FitsAudioResult(Fits generatedResult, String rawResult) {
        super(generatedResult, rawResult);
    }

    /**
     * Target field in FileMaker: teknisk_data -> ljud_samplingFrequency
     *
     * @return
     */
    public Integer getSampleRate(){
        FieldValueResolver parser = new FitsAudioMetadata(
                FitsAudioMetadata.FIELD_SAMPLE_RATE,
                this,
                new MediaInfoMetadata(MediaInfoMetadata.FIELD_SAMPLING_RATE, MediaInfoMetadata.TRACK_TYPE_AUDIO, this)
        );
        return parseIntegerOrNull(parser);
    }


    /**
     * Target field in FileMaker: teknisk_data -> ljud_numChannels
     *
     * @return
     */
    public Integer getChannels() {
        FieldValueResolver parser = new FitsAudioMetadata(
                FitsAudioMetadata.FIELD_CHANNELS,
                this,
                new MediaInfoMetadata(
                        MediaInfoMetadata.FIELD_CHANNELS,
                        MediaInfoMetadata.TRACK_TYPE_AUDIO,
                        this
                )
        );

        return parseIntegerOrNull(parser);
    }

    /**
     * Target field in FileMaker: teknisk_data -> ljud_bitsPerSample
     *
     * @return
     */
    public Integer getBitDepth() {
        FieldValueResolver parser = new FitsAudioMetadata(
                FitsAudioMetadata.FIELD_BIT_DEPTH,
                this,
                new MediaInfoMetadata(
                        MediaInfoMetadata.FIELD_BIT_DEPTH,
                        MediaInfoMetadata.TRACK_TYPE_AUDIO,
                        this
                )
        );

        return parseIntegerOrNull(parser);
    }

    /**
     * Target field in FileMaker: teknisk_data -> ljud_codecName
     *
     * @return
     */
    public String getAudioDataEncoding() {
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_CODEC_FAMILY,
                MediaInfoMetadata.TRACK_TYPE_AUDIO,
                new FitsAudioMetadata(
                    FitsAudioMetadata.FIELD_AUDIO_DATA_ENCODING,
                    this,
                    new MediaInfoMetadata(
                            MediaInfoMetadata.FIELD_FORMAT,
                            MediaInfoMetadata.TRACK_TYPE_AUDIO,
                            this
                    )
                )
        );

        return parseStringOrNull(parser);
    }

    /**
     * Attempts to resolve the duration of the audio using field resolvers
     *
     * @return
     */
    public Long getAudioDuration(){
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_DURATION,
                MediaInfoMetadata.TRACK_TYPE_AUDIO,
                this);

        return parseLongOrNull(parser);
    }

    /**
     * Target field in FileMaker: teknisk_data -> duration
     *
     * format is: HH:mm:ss.SSS
     *
     * @return
     */
    public String getFormattedDuration(){

        // first try to use resolvers for exact value
        if(getAudioDuration() != null){
            return convertMsToDateFormat(getAudioDuration());
        }
        // calculate if we can't find a match using resolvers
        else if(getNumSamples() != null && getSampleRate() != null){
            Long numSam = getNumSamples();
            Integer samRa = getSampleRate();
            BigDecimal durationInMs = BigDecimal.valueOf(getNumSamples()).divide(BigDecimal.valueOf(getSampleRate()/1000), 0, RoundingMode.HALF_DOWN);

            return convertMsToDateFormat(durationInMs.longValue());
        } else {
            return null;
        }
    }

    protected String convertMsToDateFormat(Long durationInMs){
        Date date = Date.from(Instant.ofEpochMilli(durationInMs));
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        return dateFormat.format(date);
    }

    /**
     * Nr of samples needed for calculating a formatted duration string
     * @return
     */
    public Long getNumSamples() {
        FieldValueResolver parser = new FitsAudioMetadata(
                FitsAudioMetadata.FIELD_NUM_SAMPLES,
                this
        );

        return parseLongOrNull(parser);
    }


    /**
     * Target field in FileMaker: teknisk_data -> ljud_dataRate
     *
     * @return
     */
    public Long getBitRate(){
        FieldValueResolver parser = new JHoveWAVEMetadata(
                JHoveWAVEMetadata.PROP_AVERAGE_BYTES_PER_SECOND,
                this,
                new MediaInfoMetadata(
                        MediaInfoMetadata.FIELD_BIT_RATE,
                        MediaInfoMetadata.TRACK_TYPE_AUDIO,
                        this
                )
        );

        Long bitRate = parseLongOrNull(parser);

        if(bitRate == null
                && getBitRateMode() != null
                && getBitRateMode().equalsIgnoreCase("CBR")
                && getSampleRate() != null
                && getBitDepth() != null
                && getNumAudioStreams() != null
        ){
            bitRate = Long.valueOf(getSampleRate() * getBitDepth() * getChannels());
        }

        return bitRate;
    }

    /**
     * Target field in FileMaker: teknisk_data -> ljud_dataRateMode
     *
     * @return
     */
    public String getBitRateMode() {
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_BIT_RATE_MODE,
                MediaInfoMetadata.TRACK_TYPE_AUDIO,
                this
        );

        return parseStringOrNull(parser);

    }

    /**
     * Target field in FileMaker: teknisk_data -> ljud_codecId
     *
     * @return
     */
    public String getAudioCodecId() {
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_CODEC_ID,
                MediaInfoMetadata.TRACK_TYPE_AUDIO,
                this
        );

        return parseStringOrNull(parser);

    }

    /**
     * Target field in FileMaker: teknisk_data -> ljud_numStreams
     *
     * @return
     */
    public Integer getNumAudioStreams(){
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_COUNT_OF_STREAMS,
                MediaInfoMetadata.TRACK_TYPE_AUDIO,
                this
        );

        return parseIntegerOrNull(parser);
    }

}
