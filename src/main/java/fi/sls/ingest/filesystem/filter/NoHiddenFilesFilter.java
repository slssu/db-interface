package fi.sls.ingest.filesystem.filter;

import java.io.File;
import java.io.FileFilter;

/**
 * FileFilter that removes any files that are marked as hidden by the file system
 */
public class NoHiddenFilesFilter implements FileFilter {

    @Override
    public boolean accept(File file) {
        return !file.isHidden();
    }
}
