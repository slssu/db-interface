package fi.sls.ingest.ws;

import fi.sls.ingest.actuator.metrics.ProcessingMetricsService;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.representation.projection.IngestProcessingItemMinimal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class SocketMessengerTest {

    SocketMessenger testable;
    SimpMessagingTemplate mockTemplate;
    ProjectionFactory projectionFactory;
    ProcessingMetricsService processingMetricsService;

    @BeforeEach
    void setUp() {

        mockTemplate = Mockito.mock(SimpMessagingTemplate.class);
        projectionFactory = Mockito.mock(ProjectionFactory.class);
        processingMetricsService = Mockito.mock(ProcessingMetricsService.class);

        testable = new SocketMessenger(mockTemplate, projectionFactory, processingMetricsService);
    }

    @Test
    void classConstantsAreSet() {
        // if you change the default, remember to change the value in any client code as well!
        assertEquals("/topic/public", SocketMessenger.DEFAULT_DESTINATION);
        assertEquals("api", SocketMessenger.DEFAULT_SENDER);
    }


    @Test
    void broadcast_canBroadCastIngestProcessingItemToDefaultChannel() {
        IngestProcessingItem mockItem = Mockito.mock(IngestProcessingItem.class);
        IngestProcessingItemMinimal mockMinimal = Mockito.mock(IngestProcessingItemMinimal.class);

        MetricsMessage mockMetrics = Mockito.mock(MetricsMessage.class);

        when(projectionFactory.createProjection(IngestProcessingItemMinimal.class, mockItem)).thenReturn(mockMinimal);
        when(processingMetricsService.loadMetrics()).thenReturn(mockMetrics);

        ArgumentCaptor<String> destinationCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<IngestStatusMessage> messageCaptor = ArgumentCaptor.forClass(IngestStatusMessage.class);

        testable.broadcast(mockItem);

        verify(mockTemplate, times(2)).convertAndSend(destinationCaptor.capture(), messageCaptor.capture());

        List<IngestStatusMessage> capturedMessages = messageCaptor.getAllValues();

        assertEquals(SocketMessenger.DEFAULT_DESTINATION, destinationCaptor.getValue());
        assertEquals("api", capturedMessages.get(0).getSender());
        assertEquals(mockMinimal, capturedMessages.get(0).getContent());

        assertEquals("api", capturedMessages.get(1).getSender());
        assertEquals(mockMetrics, capturedMessages.get(1).getContent());
    }
}