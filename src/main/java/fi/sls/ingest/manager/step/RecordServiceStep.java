package fi.sls.ingest.manager.step;


import fi.sls.ingest.proxy.filemaker.service.RecordService;

public abstract class RecordServiceStep implements IngestStep{

    protected RecordService service;

    public RecordServiceStep(RecordService service) {
        this.service = service;
    }
}
