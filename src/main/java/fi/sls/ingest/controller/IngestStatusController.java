package fi.sls.ingest.controller;

import fi.sls.ingest.ws.IngestStatusMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Controller
@Slf4j
public class IngestStatusController {

    @MessageMapping("/status.sendMessage")
    @SendTo("/topic/public")
    public IngestStatusMessage sendMessage(@Payload IngestStatusMessage message){
        log.debug("broadcasting message: {}", kv("ingest_status_message_type", message.getType()), kv("ingest_status_message", message));
        return message;
    }

    @MessageMapping("/status.addUser")
    @SendTo("/topic/public")
    public IngestStatusMessage addUser(@Payload IngestStatusMessage message, SimpMessageHeaderAccessor headerAccessor){

        if(message.getSender() != null){
            headerAccessor.getSessionAttributes().  put("username", message.getSender());
            log.info("User added: {} ", kv("user", message.getSender()));
        } else {
            log.debug("No sender value in message, could not add username");
        }

        return message;
    }
}
