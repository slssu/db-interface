package fi.sls.ingest.proxy.filemaker.response;

public interface FileMakerResponseInterface {
    Object getResponse();
}
