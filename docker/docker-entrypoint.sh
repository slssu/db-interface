#!/bin/bash

# function Copied from MariaDB official image 2019-11-01
#
# usage: file_env VAR [DEFAULT]
#    ie: file_env 'XYZ_DB_PASSWORD' 'example'
# (will allow for "$XYZ_DB_PASSWORD_FILE" to fill in the value of
#  "$XYZ_DB_PASSWORD" from a file, especially for Docker's secrets feature)
file_env() {
	local var="$1"
	local fileVar="${var}_FILE"
	local def="${2:-}"
	if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
		echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
		exit 1
	fi
	local val="$def"
	if [ "${!var:-}" ]; then
		val="${!var}"
	elif [ "${!fileVar:-}" ]; then
		val="$(< "${!fileVar}")"
	fi
	export "$var"="$val"
	unset "$fileVar"
}

# Setup all env vars either from original env variable or _FILE
# suffixed env for docker secrets support
file_env 'SLS_FILEMAKER_USERNAME'
file_env 'SLS_FILEMAKER_PASSWORD'
file_env 'SLS_INGEST_API_KEY'
file_env 'SLS_INGEST_DB_USERNAME'
file_env 'SLS_INGEST_DB_PASSWORD'
file_env 'SLS_INGEST_LDAP_USERNAME'
file_env 'SLS_INGEST_LDAP_PASSWORD'
file_env 'RABBITMQ_DEFAULT_USER'
file_env 'RABBITMQ_DEFAULT_PASS'

# Execute original command
exec "$@"