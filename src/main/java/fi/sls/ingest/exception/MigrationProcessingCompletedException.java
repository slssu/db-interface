package fi.sls.ingest.exception;

/**
 * Exception thrown if an item is attempted to be processed but has been marked as complete, indicating that
 * it was already processed
 *
 * This is an unchecked exception, so we expect the caller to know to handle it if they want to.
 */

public class MigrationProcessingCompletedException extends Throwable {
    public MigrationProcessingCompletedException(String format) {
        super(format);
    }
}
