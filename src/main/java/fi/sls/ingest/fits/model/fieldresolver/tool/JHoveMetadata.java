package fi.sls.ingest.fits.model.fieldresolver.tool;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Optional;

public class JHoveMetadata extends AbstractFieldValueResolver {

    public final static String FIELD_LAST_MODIFIED = "lastModified";
    public final static String FIELD_VERSION = "version";

    public JHoveMetadata(String fieldName, FieldValueResolver resolver) {
        super(fieldName, resolver);
    }

    public JHoveMetadata(String fieldName, FitsResult fitsResult) {
        super(fieldName, fitsResult);
    }

    public JHoveMetadata(String fieldName, FitsResult fitsResult, FieldValueResolver resolver) {
        super(fieldName, fitsResult, resolver);
    }

    @Override
    public Optional<String> getValue() {

        XPath xPath = XPathFactory.newInstance().newXPath();

        try {

            xPath.setNamespaceContext(new JHoveNamespaceContext(fitsResult.getXmlDocument()));

            String expStr = "/fits:fits/fits:toolOutput/fits:tool[@name = 'Jhove']/repInfo/" + fieldName;

            XPathExpression expression = xPath.compile(expStr);

            NodeList nodes = (NodeList) expression.evaluate(fitsResult.getXmlDocument(), XPathConstants.NODESET);
            if(nodes.getLength() > 0){
                Node n = nodes.item(0);
                String val = n.getTextContent();

                if(val != null && !val.isEmpty()){
                    return Optional.of(processValue(val));
                }
            }

            return executeNextResolver();

        } catch (XPathExpressionException e) {
            return executeNextResolver();
        }
    }

    /**
     * Helper for additional processing of a result value before it is returned
     * @param val
     * @return
     */
    protected String processValue(String val){
        switch(fieldName){
            case FIELD_LAST_MODIFIED:
                return LocalDateTime.parse(val, DateTimeFormatter.ISO_OFFSET_DATE_TIME).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            default:
                return val;
        }
    }

    class JHoveNamespaceContext implements NamespaceContext {

        private Document sourceDocument;

        public JHoveNamespaceContext(Document document){
            sourceDocument = document;
        }

        @Override
        public String getNamespaceURI(String prefix) {
            if (prefix.equals(XMLConstants.DEFAULT_NS_PREFIX)) {
                return sourceDocument.lookupNamespaceURI(null);
            } else if(prefix.equals("fits")) {
                return "http://hul.harvard.edu/ois/xml/ns/fits/fits_output";
            } else if(prefix.equals("mix")){
                return "http://www.loc.gov/mix/v20";
            } else {
                return sourceDocument.lookupNamespaceURI(prefix);
            }
        }

        @Override
        public String getPrefix(String namespaceURI) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Iterator<String> getPrefixes(String namespaceURI) {
            throw new UnsupportedOperationException();
        }
    }
}
