package fi.sls.ingest.proxy.slsfileapi;

import fi.sls.ingest.fits.FitsService;
import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.manager.IngestProcessingItem;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotBlank;
import java.io.File;

/**
 * Represents the request body structure for starting the
 * process of moving and converting a file using the SLS File API
 *
 * See also <a href="https://bitbucket.org/slssu/file_api">https://bitbucket.org/slssu/file_api</a>
 *
 * Example request body
 * <code>
 *     {
 *         "path": "ingest/foo/bar/file_name.tif",
 *         "mimeType": "image/tiff",
 *         "metadataPath": "ingest/foo/bar/file_name.tif.fits.xml",
 *         "token": "eyJzdWIiOiJ7XCJpZFwiOjEsXCJ1c2VybmFtZVwiOlwiSW5nZXN0b3JOZXN0b",
 *         "callbackUrl": "https://whatever.tld/move_completed/file_id"
 *     }
 * </code>
 *
 */
@Data
public class FileAPIMoveRequestBody extends FileAPIRequestBody {
    /**
     * The path to the file to process, relative to the projects mount on the server
     */
    @NotBlank
    private String path;

    /**
     * The mimetype for the file as defined by FITS
     */
    @NotBlank
    private String mimeType;

    /**
     * Path to the XML file that the FITS tool generated, relative to the projects mount on the server
     */
    @NotBlank
    private String metadataPath;

    /**
     * Checksum (md5) calculated by FITS is sent to File API for verification after copy
     */
    @NotBlank
    private String checksum;

    /**
     * Creates a FileAPIRequestBody based on the given IngestProcessingItem, FitsResult and callbackUrl
     *
     * @param item
     * @param fitsResult
     * @param callbackUrl
     * @return
     */
    public static FileAPIMoveRequestBody createFrom(IngestProcessingItem item, FitsResult fitsResult, String callbackUrl){
        FileAPIMoveRequestBody rtn = new FileAPIMoveRequestBody();
        rtn.setCallbackUrl(callbackUrl);
        rtn.setPath(StringUtils.trimLeadingCharacter(item.getItem().getFitsPath(), File.separatorChar));
        rtn.setMetadataPath(FitsService.getMetadataPathForFile(item.getItem().getFitsPath()));
        rtn.setMimeType(fitsResult.getIdentityMimetype());
        rtn.setToken(item.getToken());
        rtn.setChecksum(item.getMd5ChecksumBefore());

        return rtn;
    }

    /**
     * Creates a FileAPIRequestBody based on the given IngestProcessingItem and callbackUrl
     * @param item
     * @param callbackUrl
     * @return
     */
    public static FileAPIMoveRequestBody createFrom(IngestProcessingItem item, String callbackUrl){
        FileAPIMoveRequestBody rtn = new FileAPIMoveRequestBody();
        rtn.setCallbackUrl(callbackUrl);
        rtn.setPath(StringUtils.trimLeadingCharacter(item.getItem().getFitsPath(), File.separatorChar));
        rtn.setToken(item.getToken());

        return rtn;
    }

    /**
     * Creates a FileAPIRequestBody based on the given FileAPIResponseBody and callbackUrl
     *
     * @param responseBody
     * @param callbackUrl
     * @return
     */
    public static FileAPIMoveRequestBody createFrom(FileAPIResponseBody responseBody, String callbackUrl){
        FileAPIMoveRequestBody rtn = new FileAPIMoveRequestBody();
        rtn.setCallbackUrl(callbackUrl);
        rtn.setPath(responseBody.getPath());
        rtn.setToken(responseBody.getToken());

        return rtn;

    }
}
