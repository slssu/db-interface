package fi.sls.ingest.ws;

import fi.sls.ingest.actuator.metrics.ProcessingMetricsService;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.migration.DigitalObjectMigrationResult;
import fi.sls.ingest.representation.projection.DigitalObjectMigrationResultMinimal;
import fi.sls.ingest.representation.projection.IngestProcessingItemMinimal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * Provides methods for broadcasting updates to an IngestItem to the given destinations
 *
 */
@Service
@Slf4j
public class SocketMessenger {

    public static final String DEFAULT_DESTINATION = "/topic/public";
    public static final String DEFAULT_SENDER = "api";

    /**
     * WebSocket template to use for sending messages
     */
    private SimpMessagingTemplate webSocket;

    /**
     * Reference to a projectionFactory instance for modifying the response structure for message.
     */
    ProjectionFactory projectionFactory;

    ProcessingMetricsService processingMetricsService;

    /**
     * Create an instance of this class with the given template as the socket used for sending
     *
     * @param webSocket
     */
    public SocketMessenger(
            SimpMessagingTemplate webSocket,
            ProjectionFactory projectionFactory,
            ProcessingMetricsService processingMetricsService
    ){
        this.webSocket = webSocket;
        this.projectionFactory = projectionFactory;
        this.processingMetricsService = processingMetricsService;
    }

    /**
     * Send the content of the given IngestProcessingItem to the default destination using the default sender.
     *
     * @param item
     */
    public void broadcast(IngestProcessingItem item){
        log.info("Broadcasting update for IngestProcessingItem: {} {}",
                kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                kv("ingest_item_status", item.getProcessingStatus()),
                kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
        );
        // convert item to minimal projection
        IngestProcessingItemMinimal minimalItem = projectionFactory.createProjection(IngestProcessingItemMinimal.class, item);

        webSocket.convertAndSend(DEFAULT_DESTINATION, new IngestStatusMessage(DEFAULT_SENDER, minimalItem));

        broadcastAggregateStats();
    }

    /**
     * Send the content of the given DigitalObjectMigrationResult to the migrate destination using the default sender
     * @param item
     */
    public void broadcast(DigitalObjectMigrationResult item) {
        log.info("Broadcasting update for DigitalObjectMigrationResult: {} {}",
                kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                kv("migration_item_status", item.getProcessingStatus())
        );
        // convert item to minimal projection (only includes data the UI cares about)
        DigitalObjectMigrationResultMinimal minimalItem = projectionFactory.createProjection(DigitalObjectMigrationResultMinimal.class, item);

        webSocket.convertAndSend(DEFAULT_DESTINATION, new IngestStatusMessage(DEFAULT_SENDER, minimalItem));

    }


    public void broadcastAggregateStats(){

        MetricsMessage message = processingMetricsService.loadMetrics();

        log.debug("Broadcasting update for MetricsMessage: {}",
                kv("metrics_message", message)
        );

        webSocket.convertAndSend(DEFAULT_DESTINATION, new IngestStatusMessage(IngestStatusMessage.MessageType.METRIC, DEFAULT_SENDER, message));
    }
}
