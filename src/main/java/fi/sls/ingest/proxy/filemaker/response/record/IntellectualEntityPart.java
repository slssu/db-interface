package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import lombok.Data;
import lombok.Getter;

@Data
public class IntellectualEntityPart extends FileMakerResponseEntity {


    @JsonIgnore
    @Getter
    private String layoutName = "ingestapi_intellectualEntityParts";

    IntellectualEntityPartFieldData fieldData;

    public IntellectualEntityPart(){
        this.fieldData = new IntellectualEntityPartFieldData();
    }

    public IntellectualEntityPart(String title, String orderNr, String filePath){
        this();

        getFieldData().setTitle(title);
        getFieldData().setOrderNr(orderNr);
        getFieldData().setFilePath(filePath);

    }

    @Override
    public FindQueryRequest createFindQueryRequest() {
        return new FindQueryRequest("nummer", "="+getFieldData().getEntityNumber());
    }

    @Override
    public UpdateRequest createUpdateRequest() {
        return new UpdateRequest(getFieldData());
    }

    @Override
    public CreateRequest createRecordRequest() {
        return new CreateRequest(getFieldData());
    }
}
