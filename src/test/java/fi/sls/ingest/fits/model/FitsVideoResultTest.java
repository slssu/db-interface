package fi.sls.ingest.fits.model;

import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class FitsVideoResultTest extends FitsResultTestsBase {

    String videoWithMeta = "metadata-extractor-output/video/slsa_1390_av_018_001.mkv.fits.xml";
    String videoNoMeta = "metadata-extractor-output/video/slsa_1390_av_018_001.mkv.fits_no_meta.xml";
    String videoWithMetaMp4 = "metadata-extractor-output/video/ta_2019_av_005_001.mp4.fits.xml";


    @Test
    void formattedDuration_returnsResultIfAvailable() throws IOException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta));

        assertEquals(String.valueOf("00:45:26.200"), testable.getFormattedDuration());
    }

    @Test
    void formattedDuration_returnsNullIfNotAvailable() throws IOException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta));

        assertNull(testable.getFormattedDuration());
    }

    @Test
    void avBitRate_returnsResultIfAvailable() throws IOException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta));

        assertEquals(Long.valueOf(68539228), testable.getAvBitRate());
    }

    @Test
    void avBitRate_returnsNullIfNotAvailable() throws IOException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta));

        assertNull(testable.getAvBitRate());
    }

//    @Test
//    void getAvBitRateMode_returnsResultIfAvailable() throws IOException {
//        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta));
//
//        assertEquals(Long.valueOf(68539228), testable.getAvBitRateMode());
//    }

    @Test
    void getAvBitRateMode_returnsNullIfNotAvailable() throws IOException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta));

        assertNull(testable.getAvBitRateMode());
    }

    @Test
    void avBitDepth_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals(Integer.valueOf(10), testable.getAvBitDepth());
    }

    @Test
    void avBitDepth_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getAvBitDepth());
    }

    @Test
    void bitDepth_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals(Integer.valueOf(32), testable.getBitDepth());
    }

    @Test
    void bitDepth_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getBitDepth());
    }

    @Test
    void standard_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals("PAL", testable.getBroadcastStandard());
    }

    @Test
    void standard_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getBroadcastStandard());
    }

    @Test
    void width_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals(Integer.valueOf(720), testable.getVideoWidth());
    }

    @Test
    void width_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getVideoWidth());
    }

    @Test
    void height_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals(Integer.valueOf(576), testable.getVideoHeight());
    }

    @Test
    void height_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getVideoHeight());
    }

    @Test
    void frameRate_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals("25.000", testable.getFrameRate());
    }

    @Test
    void frameRate_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getFrameRate());
    }

    @Test
    void frameRateMode_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals("CFR", testable.getFrameRateMode());
    }

    @Test
    void frameRateMode_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getFrameRateMode());
    }


    @Test
    void bitRateMode_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMetaMp4), null, loadExampleDocument(videoWithMetaMp4));

        assertEquals("CBR", testable.getBitRateMode());
    }

    @Test
    void bitRateMode_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getBitRateMode());
    }

    @Test
    void bitRate_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMetaMp4), null, loadExampleDocument(videoWithMetaMp4));

        assertEquals(Long.valueOf(339498), testable.getBitRate());
    }

    @Test
    void bitRate_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getBitRate());
    }


    @Test
    void avSampling_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals("4:2:2", testable.getAvSampling());
    }

    @Test
    void avSampling_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getAvSampling());
    }

    @Test
    void pixelAspectRatio_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals("1,000", testable.getPixelAspectRatio());
    }

    @Test
    void pixelAspectRatio_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getPixelAspectRatio());
    }

    @Test
    void displayAspectRatio_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals("5:4", testable.getDisplayAspectRatio());
    }

    @Test
    void displayAspectRatio_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getDisplayAspectRatio());
    }

    @Test
    void numVideoStreams_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals(Integer.valueOf(1), testable.getNumVideoStreams());
    }

    @Test
    void numVideoStreams_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getNumVideoStreams());
    }

    @Test
    void numAudioStreams_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals(Integer.valueOf(1), testable.getNumAudioStreams());
    }

    @Test
    void numAudioStreams_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getNumAudioStreams());
    }

    @Test
    void codecName_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals("FFV1", testable.getCodecName());
    }

    @Test
    void codecName_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getCodecName());
    }

    @Test
    void codecInfo_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMetaMp4), null, loadExampleDocument(videoWithMetaMp4));

        assertEquals("Advanced Video Codec", testable.getCodecInfo());
    }

    @Test
    void codecInfo_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getCodecInfo());
    }

    @Test
    void codecId_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals("V_MS/VFW/FOURCC / FFV1", testable.getCodecId());
    }

    @Test
    void codecId_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getCodecId());
    }

    @Test
    void frameCount_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals(Long.valueOf(68155), testable.getFrameCount());
    }

    @Test
    void frameCount_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getFrameCount());
    }

    @Test
    void scanningFormat_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMetaMp4), null, loadExampleDocument(videoWithMeta));

        assertEquals("Progressive", testable.getScanningFormat());
    }

    @Test
    void scanningFormat_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getScanningFormat());
    }

    @Test
    void sampleRate_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals(Integer.valueOf(48000), testable.getSampleRate());
    }

    @Test
    void sampleRate_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getSampleRate());
    }

    @Test
    void channels_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals(Integer.valueOf(2), testable.getChannels());
    }

    @Test
    void channels_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getChannels());
    }

    @Test
    void audioDataEncoding_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta), null, loadExampleDocument(videoWithMeta));

        assertEquals("PCM", testable.getAudioDataEncoding());
    }

    @Test
    void audioCodecName_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoNoMeta), null, loadExampleDocument(videoNoMeta));

        assertNull(testable.getAudioDataEncoding());
    }
}