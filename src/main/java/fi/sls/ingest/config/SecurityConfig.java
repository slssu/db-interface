package fi.sls.ingest.config;

import fi.sls.ingest.repository.RoleRepository;
import fi.sls.ingest.repository.UserRepository;
import fi.sls.ingest.representation.Role;
import fi.sls.ingest.representation.User;
import fi.sls.ingest.security.CustomUserDetailsService;
import fi.sls.ingest.security.jwt.JwtAuthenticationEntryPoint;
import fi.sls.ingest.security.jwt.JwtAuthenticationFilter;
import fi.sls.ingest.security.ldap.CustomLdapUserDetails;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.security.ldap.authentication.LdapAuthenticationProvider;
import org.springframework.security.ldap.authentication.LdapAuthenticator;
import org.springframework.security.ldap.ppolicy.PasswordPolicyAwareContextSource;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.ldap.userdetails.*;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.nio.ByteBuffer;
import java.util.*;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${sls.ldap.host}")
    protected String ldapHost;

    @Value("${sls.ldap.userDn}")
    protected String userDn;

    @Value("${sls.ldap.userDnPassword}")
    protected String userDnPassword;

    @Value("${sls.ldap.groupSearchBase}")
    protected String groupSearchBase;

    @Value("${sls.ldap.userSearchBase}")
    protected String userSearchBase;

    /**
     * Service for fetching user information from the database
     */
    CustomUserDetailsService customUserDetailsService;

    /**
     * Reference to repository used to load locally stored user info
     */
    UserRepository userRepository;

    /**
     * Reference to repository containing user roles stored locally.
     */
    RoleRepository roleRepository;

    /**
     * Service for logging unauthorized access using jwt token
     */
    JwtAuthenticationEntryPoint unauthorizedHandler;

    /**
     * Encoder used for encrypting passwords in the service
     */
    PasswordEncoder passwordEncoder;

    @Value("${spring.data.rest.basePath}")
    String apiBasePath;

    public SecurityConfig(
            JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint,
            CustomUserDetailsService customUserDetailsService,
            UserRepository userRepository,
            RoleRepository roleRepository,
            PasswordEncoder passwordEncoder
            ){
        this.unauthorizedHandler = jwtAuthenticationEntryPoint;
        this.customUserDetailsService = customUserDetailsService;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .cors()
            .and()
            .csrf()
                .disable()
            .exceptionHandling()
            .authenticationEntryPoint(unauthorizedHandler)
            .and()
            .authorizeRequests()

            // allow anonymous to to static resources
            .antMatchers("/",
                    "/favicon.ico",
                    "/**/*.png",
                    "/**/*.gif",
                    "/**/*.svg",
                    "/**/*.jpg",
                    "/**/*.html",
                    "/**/*.css",
                    "/**/*.js")
            .permitAll()

            // allow anonymous websockets freely for now
            // maybe setup here: https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html#websocket-stomp-authentication-token-based
            // or here: https://www.baeldung.com/spring-security-websockets
            .antMatchers("/ws")
            .permitAll()

            // allow anonymous to auth path
            .antMatchers(apiBasePath+"/auth/**")
            .permitAll()

            // allow anonymous access to heath status of service
            .antMatchers("/actuator/health")
            .permitAll()

            // block the rest
            .anyRequest()
            .authenticated()
            ;

        // Add custom jwt filter
        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter(){
        return new JwtAuthenticationFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .authenticationProvider(ldapAuthenticationProvider())
                ;
    }

    @Bean
    public LdapAuthenticationProvider ldapAuthenticationProvider() throws Exception {
        LdapAuthenticationProvider ldapAuthenticationProvider = new LdapAuthenticationProvider(ldapAuthenticator(), ldapAuthoritiesPopulator());
        ldapAuthenticationProvider.setUserDetailsContextMapper(userDetailsContextMapper());
        return ldapAuthenticationProvider;
    }

    protected LdapAuthoritiesPopulator ldapAuthoritiesPopulator() throws Exception {
        return new DefaultLdapAuthoritiesPopulator(ldapContextSource(), groupSearchBase);
    }

    @Bean
    public LdapAuthenticator ldapAuthenticator() throws Exception {
        BindAuthenticator authenticator = new BindAuthenticator(ldapContextSource());
        authenticator.setUserSearch(new FilterBasedLdapUserSearch(userSearchBase, "(sAMAccountName={0})", ldapContextSource()));
        return authenticator;
    }

    @Bean
    public LdapContextSource ldapContextSource() throws Exception {
        PasswordPolicyAwareContextSource contextSource = new PasswordPolicyAwareContextSource(ldapHost);
        contextSource.setUserDn(userDn);
        contextSource.setPassword(userDnPassword);

        // Tell ldap connection to retrieve AD field
        // "objectGUID" as binary. Otherwise it will be
        // retrieved as a String, thus, modifying the byte[] array
        final Map<String, Object> envProps = new HashMap<>();
        envProps.put("java.naming.ldap.attributes.binary","objectGUID");
        contextSource.setBaseEnvironmentProperties(envProps);

        return contextSource;
    }

    @Bean
    public UserDetailsContextMapper userDetailsContextMapper() {
        return new LdapUserDetailsMapper() {
            @Override
            public UserDetails mapUserFromContext(DirContextOperations ctx, String username, Collection<? extends GrantedAuthority> authorities) {
                LdapUserDetails details = (LdapUserDetails) super.mapUserFromContext(ctx, username, authorities);
                CustomLdapUserDetails rtn = new CustomLdapUserDetails(details);

                rtn.setDisplayName(ctx.getStringAttribute("displayName"));

                byte[] guidBytes = (byte[]) ctx.getObjectAttribute("objectGUID");
                rtn.setObjectGUID(getGuidFromByteArray(guidBytes));

                Optional<User> u = userRepository.findByUuid(rtn.getObjectGUID());

                List<Role> roles = verifyAuthorityToRole(rtn.getAuthorities());

                User user;
                if(u.isPresent()){
                    // update user info
                    user = u.get();
                } else {
                    // create new user info
                    user = new User();
                    // set a random password so users cannot log in through other auth methods (e.g. basic auth) using AD user
                    user.setPassword(passwordEncoder.encode(RandomStringUtils.randomAlphanumeric(32)));
                    user.setUuid(rtn.getObjectGUID());
                }
                // always update names and roles for user
                user.setUsername(rtn.getUsername());
                user.setDisplayName(rtn.getDisplayName());
                user.setRoles(roles);

                userRepository.save(user);
                return customUserDetailsService.loadUserById(user.getId());
            }
        };
    }

    public static String getGuidFromByteArray(byte[] bytes)
    {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        UUID uuid = new UUID(bb.getLong(), bb.getLong());
        return uuid.toString();
    }

    /**
     * Copies roles from the ldap user details to the local database
     * @param authorities
     * @return
     */
    protected List<Role> verifyAuthorityToRole(Collection<? extends GrantedAuthority> authorities) {
        List<Role> roles = new ArrayList<>();

        authorities.stream().forEach((a) -> {
            Optional<Role> r = roleRepository.findByLabel(a.getAuthority());
            if(r.isEmpty()) {
                Role nr = new Role();
                nr.setLabel(a.getAuthority().toUpperCase());
                nr = roleRepository.save(nr);
                roles.add(nr);
            } else {
                roles.add(r.get());
            }
        });

        return roles;
    }
}
