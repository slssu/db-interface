package fi.sls.ingest.proxy.filemaker.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CreateRequest {

    protected RecordPayload fieldData;

    public CreateRequest(RecordPayload fieldData) {
        this.fieldData = fieldData;
    }
}
