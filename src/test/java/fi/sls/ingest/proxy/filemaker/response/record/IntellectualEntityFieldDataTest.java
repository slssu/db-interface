package fi.sls.ingest.proxy.filemaker.response.record;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.representation.file.ObjectCategory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class IntellectualEntityFieldDataTest {

    IntellectualEntityFieldData testable;

    @BeforeEach
    void setUp() {
        testable = new IntellectualEntityFieldData();
    }

    @Test
    void populateFromMetadataResults() {

        FitsResult fitsResult = mock(FitsResult.class);
        ObjectCategory objectCategory = mock(ObjectCategory.class);
        when(objectCategory.getCategoryLabel()).thenReturn("categoryLabel");
        when(objectCategory.getSubCategory()).thenReturn("subCategory");

        FilenameMetaData filenameMetaData = mock(FilenameMetaData.class);
        when(filenameMetaData.getCategory()).thenReturn(objectCategory);
        when(filenameMetaData.getArchiveSignum()).thenReturn("dcsource");
        when(filenameMetaData.getCollectionDescriptionOrName()).thenReturn("dcPublisher2");
        when(filenameMetaData.getArchiveCollectionName()).thenReturn("dcTermsIsPartOf");
        when(filenameMetaData.getEntityIdentifier()).thenReturn("entityIdentifier");

        testable.populateFromMetadataResults(filenameMetaData, fitsResult);

        assertEquals("categoryLabel", testable.getDcType());
        assertEquals("subCategory", testable.getDcType2());
        assertEquals("dcPublisher2", testable.getDcPublisher2());
        assertEquals("dcTermsIsPartOf", testable.getDcTermsIsPartOf());
        assertEquals("dcsource", testable.getDcSource());
        assertEquals("entityIdentifier", testable.getEntityIdentifier());
    }
}