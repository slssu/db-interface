package db.migration;

import fi.sls.ingest.representation.converter.StringCompressionConverter;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Convert the raw_fits_result column to be compressed bytes instead of uncompressed.
 *
 * Saves plenty of space + improves network load times since we don't need to process the raw result in the database
 *
 * migrate rows 0-15000 because open source flyway does not support streaming of migration,
 * and data amounts are too large to allow smooth opeartion without
 */
public class V4__RawFitsResultColumnCompress extends BaseJavaMigration {

    @Override
    public Integer getChecksum() {
        // implement this if needed to detect changes
        return super.getChecksum();
    }

    @Override
    public boolean canExecuteInTransaction(){
        return false;
    }

    @Override
    public void migrate(Context context) throws Exception {

        int offset = 0;
        int pageSize = 20;
        int maxRows = 15000;

        runMigrationWithParams(context, pageSize, offset, maxRows);
    }

    protected void runMigrationWithParams(Context context, int pageSize, int offset, int maxRows) throws SQLException {
        StringCompressionConverter converter = new StringCompressionConverter();


        // figure out total nr of posts to convert
        JdbcTemplate jdbcTemplate = new JdbcTemplate(new SingleConnectionDataSource(context.getConnection(), true));
        Map<String, Object> rowCount = jdbcTemplate.queryForMap("SELECT count(id) as numRows FROM ingest_processing_item");

        long numRows = (long) rowCount.get("numRows");

        // limit nr of rows to process in one migration so we don't run into memory issues on server
        if(numRows > maxRows){
            numRows = maxRows;
        }

        System.out.println(String.format("Processing %d rows in ingest_processing_item table", numRows));

        DataSource ds = new SingleConnectionDataSource(context.getConnection(), true);
        Connection conn = ds.getConnection();
        conn.setAutoCommit(false);

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(ds);

        do{

            System.out.println(String.format("Processing offset %d, with pageSize %d", offset, pageSize));

            List<Map<String, Object>> resultSet = namedParameterJdbcTemplate.queryForList("SELECT id, raw_fits_result FROM ingest_processing_item ORDER BY id LIMIT :offset, :limit",
                    new MapSqlParameterSource()
                            .addValue("offset", offset)
                            .addValue("limit", pageSize)
            );

            System.out.println(String.format("Found %d rows in ingest_processing_item table to process in batch", resultSet.size()));

            Iterator<Map<String, Object>> iter = resultSet.iterator();

            String updateSql = "UPDATE ingest_processing_item SET raw_fits_result=:rawResult WHERE id=:id";

            List<RowObject> updateList = new ArrayList<>();

            while(iter.hasNext()){
                Map<String, Object> row = iter.next();

                long rowId = (long) row.get("id");
                byte[] byteResult = (byte[]) row.get("raw_fits_result");

                // run through converter to ensure value is a compressed byte array
                String stringResult = converter.convertToEntityAttribute(byteResult);
                byte[] reConverted = converter.convertToDatabaseColumn(stringResult);

                RowObject ro = new RowObject(rowId, reConverted);
                updateList.add(ro);
            }

            SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(updateList);

            namedParameterJdbcTemplate.batchUpdate(updateSql, batch);

            conn.commit();
            System.out.println(String.format("Batch offset %d, with pageSize %d commited", offset, pageSize));

            offset += pageSize;

        } while(offset < numRows);
    }

    protected class RowObject {

        public RowObject(long id, byte[] rawResult){
            this.id = id;
            this.rawResult = rawResult;
        }

        public long id;
        public byte[] rawResult;

        public long getId(){
            return id;
        }

        public byte[] getRawResult(){
            return rawResult;
        }

    }
}
