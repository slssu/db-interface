package fi.sls.ingest.fits.model;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class FitsImageResultTest extends FitsResultTestsBase{

    String jpgWithMeta = "metadata-extractor-output/image/fmi_485.1_foto_002_001.jpg.fits.xml";
    String jpgWithNoMeta = "metadata-extractor-output/image/fmi_485.1_foto_002_001.jpg.fits_missing.xml";
    String tiffWithMeta = "metadata-extractor-output/image/fmi_484.1_foto_001_001.tif.fits.xml";
    String tiffWithNoMeta = "metadata-extractor-output/image/fmi_483.1_foto_001_001.tif.fits_no_meta.xml";


    @Test
    void getWidth_returnsResultIfAvailable() throws IOException {
        FitsImageResult testable = (FitsImageResult) FitsResult.createFromFits(loadExampleXML(jpgWithMeta));
        assertEquals(Integer.valueOf(1920), testable.getImageWidth());
    }

    @Test
    void getWidth_returnsNullIfNotAvailable() throws IOException{
        FitsImageResult testable = (FitsImageResult) FitsResult.createFromFits(loadExampleXML(jpgWithNoMeta));
        assertNull(testable.getImageWidth());
    }


    @Test
    void getHeight_returnsResultIfAvailable() throws IOException{
        FitsImageResult testable = (FitsImageResult) FitsResult.createFromFits(loadExampleXML(jpgWithMeta));
        assertEquals(Integer.valueOf(1277), testable.getImageHeight());
    }

    @Test
    void getHeight_returnsNullIfNotAvailable() throws IOException{
        FitsImageResult testable = (FitsImageResult) FitsResult.createFromFits(loadExampleXML(jpgWithNoMeta));
        assertNull(testable.getImageHeight());
    }

    @Test
    void getXSamplingFrequency_returnsResultIfAvailable() throws IOException{
        FitsImageResult testable = (FitsImageResult) FitsResult.createFromFits(loadExampleXML(jpgWithMeta));
        assertEquals(Integer.valueOf(150), testable.getXSamplingFrequency());
    }

    @Test
    void getXSamplingFrequency_returnsNullIfNotAvailable() throws IOException{
        FitsImageResult testable = (FitsImageResult) FitsResult.createFromFits(loadExampleXML(jpgWithNoMeta));
        assertNull(testable.getXSamplingFrequency());
    }

    @Test
    void getBitsPerSample_returnsResultIfAvailable() throws IOException{
        FitsImageResult testable = (FitsImageResult) FitsResult.createFromFits(loadExampleXML(tiffWithMeta));
        assertEquals("8 8 8 8 8 8", testable.getBitsPerSample());
    }

    @Test
    void getBitsPerSample_returnsNullIfNotAvailable() throws IOException{
        FitsImageResult testable = (FitsImageResult) FitsResult.createFromFits(loadExampleXML(tiffWithNoMeta));
        assertNull(testable.getBitsPerSample());
    }

    @Test
    void getBitDepth_returnsResultIfAvailable() throws IOException{
        FitsImageResult testable = (FitsImageResult) FitsResult.createFromFits(loadExampleXML(tiffWithMeta));
        assertEquals(Integer.valueOf(8), testable.getBithDepth());
    }

    @Test
    void getColorSpace_returnsResultIfAvailable() throws IOException{
        FitsImageResult testable = (FitsImageResult) FitsResult.createFromFits(loadExampleXML(tiffWithMeta));
        assertEquals("RGB", testable.getColorSpace());
    }

    @Test
    void getColorSpace_returnsNullIfNotAvailable() throws IOException{
        FitsImageResult testable = (FitsImageResult) FitsResult.createFromFits(loadExampleXML(tiffWithNoMeta));
        assertNull(testable.getColorSpace());
    }

    @Test
    void getIccProfileName_returnsResultIfAvailable() throws IOException{
        FitsImageResult testable = (FitsImageResult) FitsResult.createFromFits(loadExampleXML(tiffWithMeta));
        assertEquals("sRGB IEC61966-2.1", testable.getIccProfileName());
    }

    @Test
    void getIccProfileName_returnsNullIfNotAvailable() throws IOException{
        FitsImageResult testable = (FitsImageResult) FitsResult.createFromFits(loadExampleXML(tiffWithNoMeta));
        assertNull(testable.getIccProfileName());
    }
}