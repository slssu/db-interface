package fi.sls.ingest.proxy.filemaker;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

@Service
public class AuthTokenStore {

    @Getter
    @Setter
    protected AuthToken authToken;

}
