package fi.sls.ingest.migration.report;

import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.migration.DigitalObjectMigrationResult;
import fi.sls.ingest.migration.MigrationResult;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Generates a XLSX report of a migration which does not contain the
 * transient fields of a DigitalObjectMigrationResult row.
 *
 */
public class XLSXMigrationStatusReport extends XLSXReportBase {

    private static String[] columns = {
            "intellectualEntities::recordId",
            "digitalObjects::recordId",
            "digitalObjects::dc_identifier (original)",
            "digitalObjects::dc_identifier (migrated)",
            "accessFiles::filePaths (original)",
            "accessFiles::filePaths (migrated)",
            "migrering lyckas",
            "tidpunkt",
            "felmeddelande"
    };

    public XLSXMigrationStatusReport(MigrationResult result){
        super(result);
    }

    @Override
    public Workbook generate(){
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Processerade filer");

        // row position that will be written to next
        int rowPos = 0;

        rowPos = createParametersInfoBlock(sheet, rowPos);

        // add some padding between info block and rows
        rowPos += 2;

        // Create header row
        Row headerRow = sheet.createRow(rowPos++);
        for(int i = 0; i < columns.length; i++) {
            Cell headerCell = headerRow.createCell(i);
            headerCell.setCellStyle(getHeaderStyle(workbook));
            headerCell.setCellValue(columns[i]);
        }

        // populate with item results
        for(DigitalObjectMigrationResult itemResult : result.getItemResults()) {

            Row row = sheet.createRow(rowPos++);

            createCellWithValue(row, 0, itemResult.getIntellectualEntityRecordId().toString(), IndexedColors.WHITE.getIndex());

            if(itemResult.getDigitalObjectRecordId() != null){
                createCellWithValue(row, 1, itemResult.getDigitalObjectRecordId().toString(), IndexedColors.WHITE.getIndex());
            }

            if(itemResult.getOriginalDcIdentifier() != null && !itemResult.getOriginalDcIdentifier().isEmpty()){
                createCellWithValue(row, 2, itemResult.getOriginalDcIdentifier(), IndexedColors.LIGHT_GREEN.getIndex());
            }
            if(itemResult.getMigratedDcIdentifier() != null && !itemResult.getMigratedDcIdentifier().isEmpty()) {
                createCellWithValue(row, 3, itemResult.getMigratedDcIdentifier(), IndexedColors.LIGHT_GREEN.getIndex());
            }

            // list changed paths of access files in single cell
            String originalAccessFiles = "";
            for(AccessFile f : itemResult.getOriginalAccessFiles()){
                originalAccessFiles = originalAccessFiles.concat(f.getPath()).concat(",\n");
            }
            createCellWithValue(row, 4, originalAccessFiles, IndexedColors.WHITE.getIndex(), true);

            String migratedAccessFiles = "";
            for(AccessFile f : itemResult.getMigratedAccessFiles()){
                migratedAccessFiles = migratedAccessFiles.concat(f.getPath()).concat(",\n");
            }
            createCellWithValue(row, 5, migratedAccessFiles, IndexedColors.LIGHT_GREEN.getIndex(), true);

            if(itemResult.getProcessingStatus().equals(ProcessingStatus.COMPLETE)){
                createCellWithValue(row, 6, "Ja", IndexedColors.LIGHT_GREEN.getIndex());
                createCellWithValue(row, 7, getLocalizedDateTime(itemResult.getProcessingCompletedAt()), IndexedColors.LIGHT_GREEN.getIndex());
            }
            else if(itemResult.getProcessingStatus().equals(ProcessingStatus.DRY_RUN_MIGRATION_FILE_API)) {
                createCellWithValue(row, 6, "OK fram till File-API anrop", IndexedColors.LIGHT_YELLOW.getIndex());
            }
            else if(itemResult.getProcessingStatus().equals(ProcessingStatus.MIGRATION_REJECTED)) {
                createCellWithValue(row, 6, "Avvisad", IndexedColors.PALE_BLUE.getIndex());
                createCellWithValue(row, 7, getLocalizedDateTime(itemResult.getProcessingCompletedAt()), IndexedColors.PALE_BLUE.getIndex());
                createCellWithValue(row, 8, itemResult.getError(), IndexedColors.PALE_BLUE.getIndex());
            }
            else {
                createCellWithValue(row, 6, "Nej", IndexedColors.LIGHT_ORANGE.getIndex());
                createCellWithValue(row, 8, itemResult.getError(), IndexedColors.LIGHT_ORANGE.getIndex());
            }
        }

        resizeColumns(columns.length, sheet);

        return workbook;
    }
}
