package fi.sls.ingest.manager;

import fi.sls.ingest.exception.IngestFilenameException;
import fi.sls.ingest.parser.FilenameMetaParser;
import fi.sls.ingest.parser.IngestFilename;
import fi.sls.ingest.parser.StrictIngestFilename;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.rest.IngestFile;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Service for renaming a file that follows the loose valid naming convention to
 * the strict naming convention
 */
@Service
@Slf4j
public class IngestFileRenamer {

    Validator validator;
    FilenameMetaParser filenameMetaParser;

    @Autowired
    public IngestFileRenamer(Validator validator, FilenameMetaParser filenameMetaParser){
        this.validator = validator;
        this.filenameMetaParser = filenameMetaParser;
    }

    public File rename(IngestItem ingestFile) throws IOException {

        String filename = ingestFile.getName();

        // test if file passes the loose validation, which means we can do something with it if necessary
        filenameMetaParser.parseWithRelaxedValidation(filename);

        // got here without parser throwing exception, test strict to see if we need to do anything
        Set<ConstraintViolation<StrictIngestFilename>> constraintViolationsStrict = validator.validate(new StrictIngestFilename(filename));
        if(!constraintViolationsStrict.isEmpty()){
            // filename needs to be changed, this is on its way to get ugly but...

            // .tif
            String suffix = filename.substring(filename.lastIndexOf("."));

            // everything up to .tif
            String fnNoSuffix = filename.substring(0, filename.lastIndexOf("."));

            String[] splits = fnNoSuffix.split("_");

            int matchedInts = 0;
            // the number groups we want to modify start from index 3 the earliest
            for(int i = 3; i < splits.length; i++){
                String p = splits[i];
                if(p.matches("[0-9]{1,5}") && matchedInts == 0){
                    // first group should be 5 digits
                    splits[i] = String.format("%05d", Integer.parseInt(splits[i]));
                    matchedInts++;
                } else if(p.matches("[0-9]{1,5}") && matchedInts == 1){
                    // second group should be 4 digits
                    splits[i] = String.format("%04d", Integer.parseInt(splits[i]));
                    matchedInts++;
                } else if(p.matches("[0-9]{1,5}") && matchedInts == 2){
                    // third group should be 2 digits
                    splits[i] = String.format("%02d", Integer.parseInt(splits[i]));
                    matchedInts++;
                }
            }

            String joined = String.join("_", splits);
            // test the new name again to see if we need to add a default version nr
            FilenameMetaData metaData = filenameMetaParser.parseWithRelaxedValidation(joined.concat(suffix));
            if(metaData.isDefaultVersionNr()){
                // find the position where archiveSign and pageNr have been set,
                // add the version nr after
                List<String> parts = new ArrayList<>();
                for(int i = 0; i < 3; i++){
                    // add existing parts that will not change
                    parts.add(i, splits[i]);
                }
                int splitLen = splits.length;
                for(int i = 3; i < splitLen; i++){
                    parts.add(splits[i]);
                    if(splits[i-1].matches("[0-9]{5}") && splits[i].matches("[0-9]{4}")){
                        // add default version nr
                        parts.add(i+1, "01");
                        //splitLen++;
                    }
                }
                joined = String.join("_", parts);
            }

            File origFile = ingestFile.getFileCopy();
            File newFile = new File(origFile.getParent(), joined.concat(suffix));

            FileUtils.moveFile(origFile, newFile);

            return newFile;
        } else {
            // filename is already ok, just return a copy
            return ingestFile.getFileCopy();
        }
    }

}
