package fi.sls.ingest.manager;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

class MD5ComparisonItemTest {

    @Test
    void md5ChecksumMatch() {

        MD5ComparisonItem testable = Mockito.mock(MD5ComparisonItem.class);
        when(testable.md5ChecksumMatch()).thenCallRealMethod();

        when(testable.getMd5ChecksumAfter()).thenReturn(null);
        when(testable.getMd5ChecksumBefore()).thenReturn(null);
        assertFalse(testable.md5ChecksumMatch());

        when(testable.getMd5ChecksumAfter()).thenReturn(null);
        when(testable.getMd5ChecksumBefore()).thenReturn("thisisnot32chars");
        assertFalse(testable.md5ChecksumMatch());

        when(testable.getMd5ChecksumAfter()).thenReturn("thisisnot32charseither");
        when(testable.getMd5ChecksumBefore()).thenReturn(null);
        assertFalse(testable.md5ChecksumMatch());

        when(testable.getMd5ChecksumAfter()).thenReturn("thisisnot32charseither");
        when(testable.getMd5ChecksumBefore()).thenReturn("1234567890abcdefghij123456789032");
        assertFalse(testable.md5ChecksumMatch());

        when(testable.getMd5ChecksumAfter()).thenReturn("1234567890abcdefghij1234567890no");
        when(testable.getMd5ChecksumBefore()).thenReturn("thisisnot32chars");
        assertFalse(testable.md5ChecksumMatch());

        when(testable.getMd5ChecksumAfter()).thenReturn("1234567890abcdefghij1234567890no");
        when(testable.getMd5ChecksumBefore()).thenReturn("1234567890abcdefghij123456789032");
        assertFalse(testable.md5ChecksumMatch());

        when(testable.getMd5ChecksumBefore()).thenReturn("1234567890abcdefghij123456789032");
        when(testable.getMd5ChecksumAfter()).thenReturn("1234567890abcdefghij123456789032");
        assertTrue(testable.md5ChecksumMatch());
    }
}