package fi.sls.ingest.security;

/**
 * Implementing classes are usable for verification in the URLTokenAuthFilter
 */
public interface TokenAuthenticable {

    /**
     * Return the token string for the item
     *
     * @return
     */
    String getToken();
}
