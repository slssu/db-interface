package fi.sls.ingest.exception;

/**
 * Exception thrown if the filename provided to the ingest process does not follow the agreed upon conventions.
 */
public class IngestFilenameException extends IllegalArgumentException {

    public IngestFilenameException(String message){
        super(message);
    }
}
