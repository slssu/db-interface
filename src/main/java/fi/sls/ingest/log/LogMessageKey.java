package fi.sls.ingest.log;

/**
 * A class containing key strings used for identifying parts in a log message.
 *
 * E.g. the key INGEST_ITEM_TOKEN should be used when logging the token string for an IngestProcessingItem
 */
public final class LogMessageKey {

    private LogMessageKey(){}

    /**
     * Key for logging incoming request payloads
     */
    public static final String REQUEST_BODY = "request_body";

    /**
     * Key for logging file path string of IngestProcessingItem
     */
    public static final String FILE_PATH = "file_path";


    /**
     * Key for logging token string of IngestProcessingItem
     */
    public static final String INGEST_ITEM_TOKEN = "ingest_item_token";

    /**
     * Key for logging LoggableIngestProcessingItem items
     */
    public static final String INGEST_PROCESSING_ITEM = "ingest_processing_item";


    /**
     * Key for logging FilenameMetaData items
     */
    public static final String FILENAME_METADATA = "filename_metadata";


    /**
     * Key for logging token string of MigrationItem
     */
    public static final String MIGRATION_ITEM_TOKEN = "migration_item_token";
    /**
     * Key for logging MigrationResultItems items
     */
    public static final String MIGRATION_PROCESSING_ITEM = "migration_processing_item";


}
