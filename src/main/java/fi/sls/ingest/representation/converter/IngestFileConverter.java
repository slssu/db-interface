package fi.sls.ingest.representation.converter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.manager.IngestDigitizationProfile;
import fi.sls.ingest.rest.IngestFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.AttributeConverter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

@Slf4j
public class IngestFileConverter extends JsonStringConverter implements AttributeConverter<IngestFile, byte[]> {
    public IngestFileConverter(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public byte[] convertToDatabaseColumn(IngestFile ingestFile) {
        String rtnJson = null;

        if(ingestFile == null){
            return null;
        } else {
            try{
                rtnJson = objectMapper.writeValueAsString(ingestFile);
                return rtnJson.getBytes();
            } catch (JsonProcessingException e) {
                log.error("Error converting from IngestFile to JSON: {}", e.getMessage());
            }
            return null;
        }
    }

    @Override
    public IngestFile convertToEntityAttribute(byte[] jsonData) {
        if(jsonData == null){
            return null;
        } else {
            IngestFile rtnItem = null;
            try{
                rtnItem = objectMapper.readValue(jsonData, IngestFile.class);
            } catch (JsonParseException e){
                // try deserializing
                try {
                    ByteArrayInputStream bos = new ByteArrayInputStream(jsonData);
                    ObjectInputStream in = new ObjectInputStream(bos);
                    rtnItem = (IngestFile) in.readObject();
                } catch (IOException | ClassNotFoundException ex) {
                    log.error("Error deserializing to IngestFile from byte array: {}", e.getMessage());
                }
            } catch (IOException e) {
                log.error("Error converting to IngestFile from JSON: {}", e.getMessage());
            }
            return rtnItem;
        }

    }
}
