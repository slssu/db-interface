package fi.sls.ingest.queue.rabbitmq.queue;

import fi.sls.ingest.queue.rabbitmq.queue.IngestItemPostProcessingQueue;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IngestItemPostProcessingQueueTest {

    @Test
    void constructor_createsQueueWithCorrectName(){
        IngestItemPostProcessingQueue testable = new IngestItemPostProcessingQueue("test");

        assertEquals("test", testable.getName());
    }
}