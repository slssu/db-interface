package fi.sls.ingest.representation.converter;

import fi.sls.ingest.manager.IngestProcessingItem;
import org.junit.jupiter.api.BeforeEach;

class IngestProcessingItemConverterTest extends JsonStringConverterTestBase<IngestProcessingItem> {

    @BeforeEach
    void setUp() {
        testable = new IngestProcessingItemConverter(objectMapper);
    }
}