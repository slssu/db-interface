package fi.sls.ingest.queue.rabbitmq.sender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.exception.MigrationProcessingException;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.migration.DigitalObjectMigrationResult;
import fi.sls.ingest.queue.rabbitmq.queue.MigrationItemPostProcessingQueue;
import fi.sls.ingest.repository.DigitalObjectMigrationResultRepository;
import fi.sls.ingest.repository.PagingAndSortingTokenStorageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Service
@Slf4j
public class MigrationItemSender {

    /**
     * Template to use for sending the item to the queue
     */
    RabbitTemplate template;

    /**
     * The queue to which we send the item
     */
    Queue queue;

    /**
     * JSON mapper for converting the processing item to a json string
     */
    ObjectMapper objectMapper;

    /**
     * Storage for storing the queue item so we can poll the status of it in client apps.
     */
    PagingAndSortingTokenStorageRepository tokenRepository;

    @Autowired
    public MigrationItemSender(
            RabbitTemplate template,
            MigrationItemPostProcessingQueue queue,
            ObjectMapper objectMapper,
            DigitalObjectMigrationResultRepository tokenRepository){
        this.template = template;
        this.queue = queue;
        this.objectMapper = objectMapper;
        this.tokenRepository = tokenRepository;
    }

    public DigitalObjectMigrationResult send(DigitalObjectMigrationResult item) throws MigrationProcessingException
    {
        // check that an item with pathHash is not already in queue
        Optional<DigitalObjectMigrationResult> storedItem = tokenRepository.findByToken(item.getToken());

        if(storedItem.isEmpty()){
            throw new MigrationProcessingException(String.format("Could not find stored item with token: %s", item.getToken()));
        }

        log.info("Sending item with token to queue: {} {}",
                kv("item_token", item.getToken()),
                kv("queue_name", queue.getName()),
                kv(LogMessageKey.MIGRATION_PROCESSING_ITEM, item)
        );
        String msg = null;
        try {
            // convert the item to a json string for storage in queue
            msg = objectMapper.writeValueAsString(item);
        } catch (JsonProcessingException e) {
            log.error("Failed to write json string: {}", e.getMessage(), kv("exception", e));
            throw new MigrationProcessingException(e.getMessage());
        }

        // use the stored item to be sure we have latest info
        item = storedItem.get();
        item.setProcessingStatus(ProcessingStatus.MIGRATION_POST_PROCESSING);
        tokenRepository.save(item);

        // Rabbit MQ can't be polled for an item without removing the item from the queue, so this is fire and forget :(
        // This is why we store the item in a store as well
        template.convertAndSend(queue.getName(), msg);

        return item;
    }

}
