package fi.sls.ingest.actuator.info;


import fi.sls.ingest.config.FileMakerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Adds information about the Filemaker configuration currently in use in our application to the /actuator/info endpoint
 */
@Component
public class FilemakerInfoContributor implements InfoContributor {

    FileMakerConfig config;

    @Autowired
    public FilemakerInfoContributor(FileMakerConfig config){
        this.config = config;
    }

    @Override
    public void contribute(Info.Builder builder) {

        Map<String, String> details = new HashMap<>();

        details.put("url", config.getUrl());
        details.put("database", config.getDatabase());
        details.put("agentName", config.getAgentName());
        details.put("agentVersion", config.getAgentVersion());

        builder.withDetail("filemaker", details);
    }
}
