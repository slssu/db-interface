package db.migration;

import org.flywaydb.core.api.migration.Context;

public class V8__RawFitsResultColumnCompress extends V4__RawFitsResultColumnCompress {

    @Override
    public void migrate(Context context) throws Exception {

        int offset = 4 * 14900;
        int pageSize = 20;
        int maxRows = 5 * 15000;

        runMigrationWithParams(context, pageSize, offset, maxRows);
    }
}
