package fi.sls.ingest.proxy.filemaker.response.record;


import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.manager.IngestDigitizationProfile;
import fi.sls.ingest.manager.MD5ComparisonItem;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.representation.file.FilenameMetaData;
import lombok.Data;
import lombok.Getter;

/**
 * Entity representing API responses for values in the "teknisk_data" table/view in FileMaker.
 */
@Data
public class DigitalObject extends FileMakerResponseEntity {

    @Getter
    @JsonIgnore
    private String layoutName = "ingestapi_digitalObjects";

    DigitalObjectFieldData fieldData;

    public DigitalObject(){
        this.fieldData = new DigitalObjectFieldData();
    }

    /**
     * Utility method that calls the same method on the fieldData object
     *
     * @param filenameMetaData
     * @param fitsResult
     */
    public void populateFromMetadataResults(FilenameMetaData filenameMetaData, FitsResult fitsResult, IngestDigitizationProfile digitizationProfile, MD5ComparisonItem md5Comparison){
        this.fieldData.populateFromMetadataResults(filenameMetaData, fitsResult, digitizationProfile, md5Comparison);
    }

    /**
     * Pass through method to generate the necessary eventId for a given premise event type
     * @param eventType
     * @return
     */
    public String getPremiseEventIdForType(String eventType){
        return fieldData.generatePremiseEventIdFor(eventType);
    }

    /**
     * Pass through method to generate the necessary eventId for a given premise event type
     * @param eventType
     * @return
     */
    public String getPremiseEventIdForType(String eventType, String eventDate){
        return fieldData.generatePremiseEventIdFor(eventType, eventDate);
    }

    @Override
    public FindQueryRequest createFindQueryRequest() {
        return new FindQueryRequest("identifier", "="+getFieldData().getIdentifier());
    }

    @Override
    public UpdateRequest createUpdateRequest() {
        return new UpdateRequest(getFieldData());
    }

    @Override
    public CreateRequest createRecordRequest() {
        return new CreateRequest(getFieldData());
    }
}
