package fi.sls.ingest.actuator.health;

import fi.sls.ingest.proxy.filemaker.AuthToken;
import fi.sls.ingest.proxy.filemaker.service.AuthenticationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

class FilemakerHealthCheckTest {

    FilemakerHealthCheck testable;
    AuthenticationService service;

    @BeforeEach
    void setUp(){
        service = mock(AuthenticationService.class);

        testable = new FilemakerHealthCheck(service);
    }

    @Test
    void health_returnsUpForServiceOkResponse() {

        AuthToken token = mock(AuthToken.class);
        when(token.getToken()).thenReturn("sometoken");

        when(service.authenticate(true)).thenReturn(token);

        Health response = testable.health();

        assertEquals(Status.UP, response.getStatus());
        assertNotNull(response.getDetails().get("response_time_ms"));
    }

    @Test
    void health_callsLogoutOnAuthServiceAfterOkRequest(){
        AuthToken token = mock(AuthToken.class);
        when(token.getToken()).thenReturn("sometoken");

        when(service.authenticate(true)).thenReturn(token);

        Health response = testable.health();

        assertEquals(Status.UP, response.getStatus());
        verify(service, times(1)).logout();
    }

    @Test
    void health_returnsDownForServiceNullToken() {
        AuthToken token = mock(AuthToken.class);
        when(token.getToken()).thenReturn(null);

        when(service.authenticate(true)).thenReturn(token);

        Health response = testable.health();

        assertEquals(Status.DOWN, response.getStatus());
        assertNotNull(response.getDetails().get("error"));
        assertEquals("Could not determine state of service", response.getDetails().get("error"));

    }

    @Test
    void health_returnsDownForServiceNullResponse() {

        when(service.authenticate(true)).thenReturn(null);

        Health response = testable.health();

        assertEquals(Status.DOWN, response.getStatus());
        assertNotNull(response.getDetails().get("error"));
        assertEquals("Could not determine state of service", response.getDetails().get("error"));

    }

    @Test
    void health_returnsDownForException() {
        HttpClientErrorException e = new HttpClientErrorException(HttpStatus.UNAUTHORIZED);

        when(service.authenticate(true)).thenThrow(e);

        Health response = testable.health();

        assertEquals(Status.DOWN, response.getStatus());
        assertNotNull(response.getDetails().get("error"));
        assertEquals("401 UNAUTHORIZED", response.getDetails().get("error"));
    }
}