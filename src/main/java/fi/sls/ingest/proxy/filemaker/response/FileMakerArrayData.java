package fi.sls.ingest.proxy.filemaker.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileMakerArrayData<T> {

    private List<T> data = new ArrayList<>();
}
