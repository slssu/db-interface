package fi.sls.ingest.representation.projection;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import fi.sls.ingest.migration.DigitalObjectMigrationResult;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntityFieldData;
import org.springframework.data.rest.core.config.Projection;

@Projection(name="minimal", types={IntellectualEntityFieldData.class})
@JsonPropertyOrder(alphabetic=true)
public interface IntellectualEntityFieldDataMinimal {

    String getEntityIdentifier();
}
