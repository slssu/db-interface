package fi.sls.ingest.security.filter;

import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.repository.DigitalObjectMigrationResultRepository;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.security.CustomUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class URLTokenAuthFilterTest {

    URLTokenAuthFilter testable;

    CustomUserDetailsService customUserDetailsService;
    IngestItemRepository ingestItemRepository;
    DigitalObjectMigrationResultRepository digitalObjectMigrationResultRepository;


    @BeforeEach
    void setUp() {

        customUserDetailsService = mock(CustomUserDetailsService.class);
        ingestItemRepository = mock(IngestItemRepository.class);
        digitalObjectMigrationResultRepository = mock(DigitalObjectMigrationResultRepository.class);

        testable = new URLTokenAuthFilter(customUserDetailsService, ingestItemRepository, digitalObjectMigrationResultRepository);
    }

    @Test
    void doFilterInternal_logsExceptionAndCallsNextFilter() throws ServletException, IOException {

        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getParameter("token")).thenThrow(RuntimeException.class);

        testable.doFilterInternal(request, response, filterChain);

        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    void doFilterInternal_onlyCallsNextFilterIfNoTokenParam() throws ServletException, IOException {

        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getParameter("token")).thenReturn(null);

        testable.doFilterInternal(request, response, filterChain);

        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    void doFilterInternal_onlyCallsNextFilterIfTokenParamDidNotMatchProcessingItem() throws ServletException, IOException {

        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getParameter("token")).thenReturn("mynonexistingtoken");
        when(request.getRequestURI()).thenReturn("ingest/continue");
        when(ingestItemRepository.findByToken("mynonexistingtoken")).thenReturn(Optional.empty());

        testable.doFilterInternal(request, response, filterChain);

        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    void doFilterInternal_onlyCallsNextFilterIfTokenParamDidNotMatchMigrationItem() throws ServletException, IOException {

        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getParameter("token")).thenReturn("mynonexistingtoken");
        when(request.getRequestURI()).thenReturn("migration/continue");
        when(digitalObjectMigrationResultRepository.findByToken("mynonexistingtoken")).thenReturn(Optional.empty());

        testable.doFilterInternal(request, response, filterChain);

        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    void doFilterInternal_authenticatesAsUserIdOneIfTokenMatches() throws ServletException, IOException {

        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);
        IngestProcessingItem mockItem = mock(IngestProcessingItem.class);

        UserDetails mockUserDetails = mock(UserDetails.class);

        when(request.getParameter("token")).thenReturn("matchingtoken");
        when(request.getRequestURI()).thenReturn("ingest/continue");
        when(ingestItemRepository.findByToken("matchingtoken")).thenReturn(Optional.of(mockItem));


        when(customUserDetailsService.loadUserById(1L)).thenReturn(mockUserDetails);

        testable.doFilterInternal(request, response, filterChain);

        verify(filterChain, times(1)).doFilter(request, response);
        verify(customUserDetailsService).loadUserById(1L);
    }

}