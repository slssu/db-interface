package fi.sls.ingest.migration;

import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.proxy.filemaker.response.record.CollectionFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.DigitalObject;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntityFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.SemlaEventFieldData;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import fi.sls.ingest.proxy.slsfileapi.FileAPIMigrateResponseBody;
import fi.sls.ingest.representation.User;
import fi.sls.ingest.representation.audit.DateAudit;
import fi.sls.ingest.representation.converter.AccessFileArrayConverter;
import fi.sls.ingest.representation.converter.FileAPIMigrateResponseBodyConverter;
import fi.sls.ingest.representation.converter.FilenameMetaDataConverter;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.security.TokenAuthenticable;
import lombok.Data;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains original value + new values for the target DigitalObject
 */
@Data
@Entity
public class DigitalObjectMigrationResult extends DateAudit implements TokenAuthenticable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private User createdBy;


    @Column(length = 64)
    private String originalChecksum;

    /**
     * Name of the top level collection that was processed
     */
    protected String collectionName;

    /**
     * The collection number (archiveNr) that was processed
     */
    protected Long archiveNr;

    /**
     * Unique token for this migration row.
     *
     * Used as callback token when sending the item to the File API.
     */
    @Column(unique = true)
    private String token;

    /**
     * The current processing status of this item
     */
    private ProcessingStatus processingStatus;

    /**
     * The url that the File API should send it's results back to
     */
    @Column(length = 512)
    private String fileAPICallbackURL;

    /**
     * The response body that was returned from the File API after this item was sent to the queue
     */
    @Lob
    @Convert(converter = FileAPIMigrateResponseBodyConverter.class)
    private FileAPIMigrateResponseBody fileAPIResponse;

    /**
     * The filename metadata generated based on the new expected filename
     */
    @Lob
    @Convert(converter = FilenameMetaDataConverter.class)
    private FilenameMetaData filenameMetaData;

    @Lob
    @Convert(converter = AccessFileArrayConverter.class)
    protected List<AccessFile> originalAccessFiles = new ArrayList<>();

    @Lob
    @Convert(converter = AccessFileArrayConverter.class)
    protected List<AccessFile> migratedAccessFiles = new ArrayList<>();

    /**
     * Timestamp for when this item was sent to the first processor after the staging area
     */
    private Instant processingStartedAt;

    /**
     * Timestamp for when this item has been marked as COMPLETE, ie has been completely processed
     */
    private Instant processingCompletedAt;

    /**
     * Marks if the item was part of a dry run migration
     */
    protected boolean dryRun = false;

    /**
     * If processing ends in an error, this field should contain reason for failure on this item
     */
    @Lob
    protected String error;

    /**
     * FM internal record id of the intellectualEntity entity that the digitalObject relates to
     */
    protected Long intellectualEntityRecordId;

    /**
     * FM internal record id of the digitalObject entity for which we will update the dc_identifier
     */
    protected Long digitalObjectRecordId;

    /**
     * The original dc_identifier value
     */
    protected String originalDcIdentifier;

    /**
     * The new dc_identifier value to store
     */
    protected String migratedDcIdentifier;

    /*--- Helper fields that we use to confirm all went well ---*/

    /**
     * The data for the Collection
     */
    @Transient
    protected CollectionFieldData collectionFieldData;

    /**
     * The data for the IntellectualEntity
     */
    @Transient
    protected IntellectualEntityFieldData intellectualEntityFieldData;
    /**
     * The data for the SemlaEvent that should link the
     * original DigitalObject to the IntellectualEntity
     */
    @Transient
    protected SemlaEventFieldData semlaEventFieldData;
    /**
     * The original data that was fetched from the database
     */
    @Transient
    protected DigitalObject original;

    /**
     * The new data that will be stored back to the database
     */
    @Transient
    protected DigitalObject migrated;

    @Transient
    public String getOriginalMimeType(){
        if(original != null && original.getFieldData() != null){
            return original.getFieldData().getFileTypeMime();
        } else {
            return null;
        }
    }

    /**
     * Returns true if the item has been marked as completed.
     *
     * @return
     */
    public boolean isCompleted() {
        return processingCompletedAt != null;
    }
}
