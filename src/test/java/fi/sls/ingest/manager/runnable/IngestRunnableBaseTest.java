package fi.sls.ingest.manager.runnable;

import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.SocketMessenger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class IngestRunnableBaseTest {

    IngestRunnableBase testable;
    IngestItemRepository mockRepository;
    SocketMessenger mockMessenger;

    @BeforeEach
    void setUp(){

        mockRepository = Mockito.mock(IngestItemRepository.class);
        mockMessenger = Mockito.mock(SocketMessenger.class);


        testable = new IngestRunnableBase(mockRepository, mockMessenger) {
            @Override
            protected void setProcessingItemStatusAndSend(ProcessingStatus status, IngestProcessingItem item) {
                super.setProcessingItemStatusAndSend(status, item);
            }
        };
    }

    @Test
    void setProcessingItemStatusAndSend() {
        IngestProcessingItem mockItem = Mockito.mock(IngestProcessingItem.class);
        when(mockItem.getProcessingStatus()).thenCallRealMethod();
        doCallRealMethod().when(mockItem).setProcessingStatus(any(ProcessingStatus.class));

        ArgumentCaptor<ProcessingStatus> statusCaptor = ArgumentCaptor.forClass(ProcessingStatus.class);
        ArgumentCaptor<IngestProcessingItem> itemCaptor = ArgumentCaptor.forClass(IngestProcessingItem.class);

        testable.setProcessingItemStatusAndSend(ProcessingStatus.REJECTED, mockItem);

        verify(mockRepository).save(itemCaptor.capture());
        verify(mockMessenger).broadcast(itemCaptor.capture());

        assertEquals(ProcessingStatus.REJECTED, itemCaptor.getValue().getProcessingStatus());
    }
}