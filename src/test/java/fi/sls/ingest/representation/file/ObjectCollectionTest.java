package fi.sls.ingest.representation.file;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ObjectCollectionTest {


    @Test
    void constructor_setsFileKeyAndLabel(){
        ObjectCollection testable = new ObjectCollection("mykey", "mylabel");

        assertEquals("mykey", testable.getFileKey());
        assertEquals("mylabel", testable.getLabel());
    }
}