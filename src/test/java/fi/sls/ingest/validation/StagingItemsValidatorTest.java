package fi.sls.ingest.validation;

import fi.sls.ingest.manager.IngestItem;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.IngestProcessingItemFlag;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.representation.User;
import fi.sls.ingest.representation.file.FilenameMetaData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class StagingItemsValidatorTest {

    StagingItemsValidator testable;
    IngestItemRepository ingestItemRepository;
    List<IngestProcessingItemFlag> itemFlags;

    List<ProcessingStatus> checkStatuses;


    @BeforeEach
    void setUp() {

        ingestItemRepository = mock(IngestItemRepository.class);
        itemFlags = new ArrayList<>();

        checkStatuses = Arrays.asList(
                ProcessingStatus.STAGING,
                ProcessingStatus.STAGING_REJECTED,
                ProcessingStatus.PROCESSING_FILENAME_METADATA_STAGING,
                ProcessingStatus.STAGING_REJECTED,
                ProcessingStatus.PRE_STAGING,
                ProcessingStatus.POST_STAGING);

        Page queue  = createMockQueue();
        when(ingestItemRepository.findByProcessingStatusIn(checkStatuses, Pageable.unpaged())).thenReturn(queue);

        testable = new StagingItemsValidator(ingestItemRepository);
    }


//    @Test
//    void validate_addsErrorIfSamePathItemInQueue() {
//
//        IngestProcessingItem newProcessingItem = createMockItem("/this/path/already/exists/fmi_485.1_foto_002_001.jpg", "sls:entity.idetifier");
//        testable.validate(newProcessingItem);
//
//        assertEquals(1, newProcessingItem.getErrors().size());
//
//        assertEquals(IngestProcessingItemFlag.Severity.ERROR, newProcessingItem.getErrors().get(0).getSeverity());
//    }

    @Test
    void validate_addsErrorIfIdentificatorIsSame(){
        IngestProcessingItem newProcessingItem = createMockItem("/suffix/is/different/fmi_666.1_foto_002_001.DNG", "sls:existing.idetifier");

        testable.validate(newProcessingItem);

        assertEquals(1, newProcessingItem.getErrors().size());

        assertEquals(IngestProcessingItemFlag.Severity.ERROR, newProcessingItem.getErrors().get(0).getSeverity());

    }

    @Test
    void validate_onlyValidatesLimitedStatuses(){
        IngestProcessingItem newProcessingItem = createMockItem("/suffix/is/different/fmi_666.1_foto_002_001.DNG", "sls:existing.idetifier", "sls",ProcessingStatus.STAGING,  false);
        testable.validate(newProcessingItem);
        verify(ingestItemRepository).findByProcessingStatusIn(checkStatuses, Pageable.unpaged());
    }

    @Test
    void validate_onlyComparesIdentifierIfMetadataExists(){
        IngestProcessingItem newProcessingItem = createMockItem("/suffix/is/different/fmi_666.1_foto_002_001.DNG", "sls:existing.idetifier", "sls",ProcessingStatus.STAGING,  false);
        testable.validate(newProcessingItem);

        assertEquals(0, newProcessingItem.getErrors().size());
    }

    @Test
    void validate_allowsOver10kItems(){
        IngestProcessingItem newProcessingItem = createMockItem("/suffix/is/different/fmi_666.1_foto_12345_001.DNG", "sls:existing.idetifier", "sls",ProcessingStatus.STAGING,  false);
        testable.validate(newProcessingItem);

        assertEquals(0, newProcessingItem.getErrors().size());

        newProcessingItem = createMockItem("/suffix/is/different/slsa_1150_foto_00279_001.tif", "sls:existing.idetifier", "sls",ProcessingStatus.STAGING,  false);
        testable.validate(newProcessingItem);

        assertEquals(0, newProcessingItem.getErrors().size());
    }

    @Test
    void validate_addsWarningIfItemDoesNotBelongToSameCollectionAsExistingStaging(){
        IngestProcessingItem newProcessingItem = createMockItem("/suffix/is/different/fmi_666.1_foto_002_001.DNG", "another:collection.idetifier", "another", ProcessingStatus.STAGING, true, 12L);

//        doCallRealMethod().when(newProcessingItem).addErrorFlag(any());
//        doCallRealMethod().when(newProcessingItem).setErrors(any());
//        doCallRealMethod().when(newProcessingItem).getErrors();
        newProcessingItem.setErrors(new ArrayList<>());

        testable.validate(newProcessingItem);

        assertEquals(1, newProcessingItem.getErrors().size());

        assertEquals(IngestProcessingItemFlag.Severity.WARNING, newProcessingItem.getErrors().get(0).getSeverity());
    }


    // --------------------------------------------------
    // utility methods below
    // --------------------------------------------------
    private Page<IngestProcessingItem> createMockQueue() {
        ArrayList<IngestProcessingItem> queue = new ArrayList<>();

        queue.add(createMockItem("/this/path/already/exists/fmi_485.1_foto_002_001.jpg", "sls:entity.idetifier"));
        queue.add(createMockItem("/suffix/is/different/fmi_666.1_foto_002_001.jpg", "sls:existing.idetifier"));
        queue.add(createMockItem("/suffix/is/different/fmi_999.1_foto_002_001.jpg", "sls:another.idetifier", "sls", ProcessingStatus.STAGING, false));

        return new PageImpl<IngestProcessingItem>(queue);
    }

    private IngestProcessingItem createMockItem(String fitsPath, String entityIdentifier){
        return createMockItem(fitsPath, entityIdentifier,"sls", ProcessingStatus.STAGING, true);
    }

    private IngestProcessingItem createMockItem(String fitsPath, String entityIdentifier, String collection, ProcessingStatus status, boolean includeMetadata){
        return createMockItem(fitsPath, entityIdentifier,collection, status, includeMetadata, null);
    }

    private IngestProcessingItem createMockItem(String fitsPath, String entityIdentifier, String collectionKey, ProcessingStatus processingStatus, boolean includeMetadata, Long createdBy){
        IngestItem ingestItem = mock(IngestItem.class);
        when(ingestItem.getFitsPath()).thenReturn(fitsPath);

        IngestProcessingItem processingItem = new IngestProcessingItem();
        processingItem.setItem(ingestItem);
        processingItem.setErrors(itemFlags);
        processingItem.setProcessingStatus(processingStatus);
        User mockUser = mock(User.class);
        if(createdBy != null){
            mockUser.setId(createdBy);
        } else {
            mockUser.setId(12L);
        }
        processingItem.setCreatedBy(mockUser);

//        when(processingItem.getItem()).thenReturn(ingestItem);
//        when(processingItem.getErrors()).thenReturn(itemFlags);
//        when(processingItem.getProcessingStatus()).thenReturn(processingStatus);

        if(includeMetadata){
            FilenameMetaData metaData = mock(FilenameMetaData.class);
            when(metaData.getDigitalObjectIdentifierNoSuffix()).thenReturn(entityIdentifier);
            when(metaData.getCollectionKey()).thenReturn(collectionKey);

            processingItem.setFilenameMetaData(metaData);
            //when(processingItem.getFilenameMetaData()).thenReturn(metaData);
        }

        return processingItem;
    }

}