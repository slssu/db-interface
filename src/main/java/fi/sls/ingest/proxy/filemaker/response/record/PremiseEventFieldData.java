package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.sls.ingest.proxy.filemaker.request.RecordPayload;
import lombok.Data;

@Data
public class PremiseEventFieldData implements RecordPayload {

    @JsonProperty("nummer")
    Long entityNumber;

    private String eventIdentifierValue;
    private String eventType;
    private String eventDateTime;

    @JsonProperty("digiprofil_id")
    private String digitizationProfileId;

    private String eventOutcomeDetailNote;

}
