package fi.sls.ingest.fits.model;

import fi.sls.ingest.fits.model.generated.Fits;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FitsResultTestsBase {

    Jaxb2Marshaller jaxb2Marshaller;

    public FitsResultTestsBase(){
        jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setPackagesToScan("fi.sls.ingest.fits.model.generated");
    }

    /**
     * Helper to load a example XML file to test parsing
     * @param testXmlPath Full path to a file in the resources/metadata-extractor-output folder
     * @return
     * @throws IOException
     */
    protected Fits loadExampleXML(String testXmlPath) throws IOException {

        Fits fitsResult = (Fits) jaxb2Marshaller.unmarshal(new StreamSource(getReaderForPath(testXmlPath)));

        return fitsResult;
    }

    protected Document loadExampleDocument(String testXmlPath) throws IOException, ParserConfigurationException, SAXException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);
        DocumentBuilder builder = builderFactory.newDocumentBuilder();

        InputSource inputXml = new InputSource(getReaderForPath(testXmlPath));
        Document xmlDocument = builder.parse(inputXml);

        return xmlDocument;
    }

    protected BufferedReader getReaderForPath(String testXmlPath) throws IOException {
        File testXML = new ClassPathResource(testXmlPath).getFile();
        BufferedReader reader = Files.newBufferedReader(testXML.toPath());
        return reader;
    }
}
