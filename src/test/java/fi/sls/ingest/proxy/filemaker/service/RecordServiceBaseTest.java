package fi.sls.ingest.proxy.filemaker.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import fi.sls.ingest.config.FileMakerConfig;
import fi.sls.ingest.proxy.filemaker.AuthToken;
import fi.sls.ingest.proxy.filemaker.AuthTokenStore;
import fi.sls.ingest.proxy.filemaker.FileMakerInternalResponseCode;
import fi.sls.ingest.proxy.filemaker.exception.FileMakerAPIException;
import fi.sls.ingest.proxy.filemaker.exception.FileMakerDataMismatchException;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.*;
import fi.sls.ingest.proxy.filemaker.response.record.Agent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RecordServiceBaseTest {

    RecordServiceBase testable;

    RestTemplate restTemplate;
    AuthTokenStore authTokenStore;
    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        authTokenStore = mock(AuthTokenStore.class);
        restTemplate = mock(RestTemplate.class);
        FileMakerConfig fileMakerConfig = mock(FileMakerConfig.class);
        when(fileMakerConfig.getUrl()).thenReturn("http://fake.local");
        when(fileMakerConfig.getDatabase()).thenReturn("fakedb");

        objectMapper = mock(ObjectMapper.class);
        when(objectMapper.getTypeFactory()).thenReturn(TypeFactory.defaultInstance());

        // setup auth flow, handles most cases in class
        AuthToken at = mock(AuthToken.class);
        when(at.getToken()).thenReturn("myauthtoken");
        when(authTokenStore.getAuthToken()).thenReturn(at);

        AuthenticationResponse authResponse = mock(AuthenticationResponse.class);
        when(authResponse.getResponse()).thenReturn(at);

        ResponseEntity<AuthenticationResponse> response = mock(ResponseEntity.class);
        when(response.getBody()).thenReturn(authResponse);
        when(restTemplate.exchange(
                eq("http://fake.local/fakedb/sessions"),
                eq(HttpMethod.POST),
                any(),
                eq(AuthenticationResponse.class))
        ).thenReturn(response);

        testable = new TestImpl(
                authTokenStore,
                restTemplate,
                fileMakerConfig,
                objectMapper
        );
    }

    @Test
    void findRecordByIdentifier_returnsRecord() throws IOException {
        mockAgentFindResponse();

        Agent agent = mock(Agent.class);
        when(agent.getLayoutName()).thenReturn("ingestapi_agents");

        FileMakerArrayData<Agent> fmad = mock(FileMakerArrayData.class);
        when(fmad.getData()).thenReturn(Arrays.asList(agent));
        FileMakerArrayDataResponse<Agent> fmadr = mock(FileMakerArrayDataResponse.class);
        when(fmadr.getResponse()).thenReturn(fmad);

        when(objectMapper.readValue(anyString(), any(JavaType.class))).thenReturn(fmadr);


        Optional<? extends FileMakerResponseEntity> rtn = testable.findRecordByIdentifier(agent);

        assertNotNull(rtn);
        assertTrue(rtn.isPresent());
    }

    @Test
    void findRecordByQuery_returnsRecord() throws IOException {
        mockAgentFindResponse();

        Agent agent = mock(Agent.class);
        when(agent.getLayoutName()).thenReturn("ingestapi_agents");

        FileMakerArrayData<Agent> fmad = mock(FileMakerArrayData.class);
        when(fmad.getData()).thenReturn(Arrays.asList(agent));
        FileMakerArrayDataResponse<Agent> fmadr = mock(FileMakerArrayDataResponse.class);
        when(fmadr.getResponse()).thenReturn(fmad);

        when(objectMapper.readValue(anyString(), any(JavaType.class))).thenReturn(fmadr);

        FindQueryRequest request = mock(FindQueryRequest.class);
        Optional<? extends FileMakerResponseEntity> rtn = testable.findRecordByQuery(agent, request);

        assertNotNull(rtn);
        assertTrue(rtn.isPresent());
    }

    @Test
    void findRecords_returnsOK() throws IOException {
        mockAgentFindResponse();

        Agent agent = mock(Agent.class);
        when(agent.getLayoutName()).thenReturn("ingestapi_agents");

        FileMakerArrayData<Agent> fmad = mock(FileMakerArrayData.class);
        when(fmad.getData()).thenReturn(Arrays.asList(agent));
        FileMakerArrayDataResponse<Agent> fmadr = mock(FileMakerArrayDataResponse.class);
        when(fmadr.getResponse()).thenReturn(fmad);

        when(objectMapper.readValue(anyString(), any(JavaType.class))).thenReturn(fmadr);

        FileMakerArrayDataResponse<?> rtn = testable.findRecords(agent, 10L);

        assertNotNull(rtn);
        assertNotNull(rtn.getResponse());
        assertEquals(1, rtn.getResponse().getData().size());
    }

    @Test
    void createObjectRecord_throwsExceptionIfResponseNotPresent() throws IOException {

        FileMakerResponseEntity rec = mock(FileMakerResponseEntity.class);
        when(rec.createRecordRequest()).thenReturn(mock(CreateRequest.class));
        when(rec.getLayoutName()).thenReturn("ingestapi_agents");

        mockRestTemplateResponse(
                HttpStatus.I_AM_A_TEAPOT,
                HttpMethod.POST,
                null,
                null);

        FileMakerDataMismatchException e = assertThrows(FileMakerDataMismatchException.class, ()-> {
            testable.createObjectRecord(rec);
        });
    }

    @Test
    void createObjectRecord_returnsReFetchedRecordOnOK() throws IOException {
        FileMakerResponseEntity rec = mock(FileMakerResponseEntity.class);
        when(rec.createRecordRequest()).thenReturn(mock(CreateRequest.class));
        when(rec.getLayoutName()).thenReturn("ingestapi_agents");

        mockRestTemplateResponse(
                HttpStatus.OK,
                HttpMethod.POST,
                null,
                "abcdef"
        );

        mockArrayDataResponse(rec);

        mockAgentGetResponse(123L);

        FileMakerResponseEntity rtn = testable.createObjectRecord(rec);

        assertNotNull(rtn);
    }

    @Test
    void updateObjectRecord_returnsReFetchedRecordOnOK() throws IOException {
        FileMakerResponseEntity rec = mock(FileMakerResponseEntity.class);
        when(rec.createUpdateRequest()).thenReturn(mock(UpdateRequest.class));
        when(rec.getLayoutName()).thenReturn("ingestapi_agents");
        when(rec.getRecordId()).thenReturn(123L);

        mockRestTemplateResponse(
                HttpStatus.OK,
                HttpMethod.PATCH,
                123L,
                "agentJson"
        );

        mockArrayDataResponse(rec);

        mockAgentGetResponse(123L);

        FileMakerResponseEntity rtn = testable.updateObjectRecord(rec);

        assertNotNull(rtn);
    }

    @Test
    void updateObjectRecord_throwsExceptionIfResponseNotPresent() throws IOException {

        FileMakerResponseEntity rec = mock(FileMakerResponseEntity.class);
        when(rec.createUpdateRequest()).thenReturn(mock(UpdateRequest.class));
        when(rec.getLayoutName()).thenReturn("ingestapi_agents");
        when(rec.getRecordId()).thenReturn(123L);

        mockRestTemplateResponse(
                HttpStatus.I_AM_A_TEAPOT,
                HttpMethod.PATCH,
                123L,
                null);

        FileMakerDataMismatchException e = assertThrows(FileMakerDataMismatchException.class, ()-> {
            testable.updateObjectRecord(rec);
        });
    }

    @Test
    void typeCastResponse_returnsNullIfWrapperFails() throws IOException {
        Optional<String> str = Optional.empty();
        JavaType type = objectMapper.getTypeFactory().constructParametricType(FileMakerArrayDataResponse.class, str.getClass());
        Optional<? extends FileMakerResponseEntity> rtn = testable.typeCastResponse(type, str);

        assertNotNull(rtn);
        assertTrue(rtn.isEmpty());
    }

    @Test
    void processFileMakerError_throwsExceptionIfNoFilemakerError() throws IOException {
        HttpServerErrorException ex = mock(HttpServerErrorException.class);
        when(ex.getResponseBodyAsString()).thenReturn("abcdef");
        FileMakerErrorResponse err = mock(FileMakerErrorResponse.class);
        when(err.getFirstResponseCode()).thenReturn(Optional.empty());

        when(objectMapper.readValue(anyString(), any(TypeReference.class))).thenReturn(err);

        FileMakerAPIException e = assertThrows(FileMakerAPIException.class, () -> {
            testable.processFileMakerError(ex);
        });
    }

    @Test
    void processFileMakerError_throwsExceptionForNonConvertableError() throws IOException {
        HttpServerErrorException ex = mock(HttpServerErrorException.class);
        when(ex.getResponseBodyAsString()).thenReturn("abcdef");
        FileMakerErrorResponse err = mock(FileMakerErrorResponse.class);

        FileMakerResponseCode code = mock(FileMakerResponseCode.class);
        when(code.getCode()).thenReturn(666);
        when(err.getFirstResponseCode()).thenReturn(Optional.of(code));

        when(objectMapper.readValue(anyString(), any(TypeReference.class))).thenReturn(err);

        FileMakerAPIException e = assertThrows(FileMakerAPIException.class, () -> {
            testable.processFileMakerError(ex);
        });

        assertEquals("java.lang.IllegalArgumentException: Value 666 cannot be converted to an FileMakerInternalResponseCode enum.", e.getMessage());
    }

    @Test
    void processFileMakerError_throwsExceptionForNonHandledErrors() throws IOException {
        HttpServerErrorException ex = mock(HttpServerErrorException.class);
        when(ex.getResponseBodyAsString()).thenReturn("abcdef");
        FileMakerErrorResponse err = mock(FileMakerErrorResponse.class);

        FileMakerResponseCode code = mock(FileMakerResponseCode.class);
        when(code.getCode()).thenReturn(FileMakerInternalResponseCode.VALIDATION_ERROR.getValue());
        when(err.getFirstResponseCode()).thenReturn(Optional.of(code));

        when(objectMapper.readValue(anyString(), any(TypeReference.class))).thenReturn(err);

        FileMakerAPIException e = assertThrows(FileMakerAPIException.class, () -> {
            testable.processFileMakerError(ex);
        });

        assertEquals(FileMakerInternalResponseCode.VALIDATION_ERROR, e.getCode());
    }

    @Test
    void processFileMakerError_returnsEmptyForRecordMissing() throws IOException {
        HttpServerErrorException ex = mock(HttpServerErrorException.class);
        when(ex.getResponseBodyAsString()).thenReturn("abcdef");
        FileMakerErrorResponse err = mock(FileMakerErrorResponse.class);

        FileMakerResponseCode code = mock(FileMakerResponseCode.class);
        when(code.getCode()).thenReturn(FileMakerInternalResponseCode.RECORD_IS_MISSING.getValue());
        when(err.getFirstResponseCode()).thenReturn(Optional.of(code));

        when(objectMapper.readValue(anyString(), any(TypeReference.class))).thenReturn(err);
        Optional<String> rtn = testable.processFileMakerError(ex);
        assertTrue(rtn.isEmpty());

        when(code.getCode()).thenReturn(FileMakerInternalResponseCode.NO_RECORDS_MATCH_REQUEST.getValue());
        rtn = testable.processFileMakerError(ex);
        assertTrue(rtn.isEmpty());
    }

    @Test
    void typeCastResponse_returnsData() throws IOException {
        Agent agent = mock(Agent.class);
        Optional<Agent> str = Optional.of(agent);
        JavaType type = objectMapper.getTypeFactory().constructParametricType(FileMakerArrayDataResponse.class, str.getClass());

        FileMakerArrayData data = mock(FileMakerArrayData.class);
        when(data.getData()).thenReturn(Arrays.asList(str.get()));

        FileMakerArrayDataResponse mockRsp = mock(FileMakerArrayDataResponse.class);
        when(mockRsp.getResponse()).thenReturn(data);
        when(objectMapper.readValue(anyString(), eq(type))).thenReturn(mockRsp);

        Optional<? extends FileMakerResponseEntity> rtn = testable.typeCastResponse(type, Optional.of("abcdef"));

        assertNotNull(rtn);
        assertTrue(rtn.isPresent());
    }


    @Test
    void getResponseWrapper() throws IOException {

        Optional<String> str = Optional.empty();
        JavaType type = objectMapper.getTypeFactory().constructParametricType(FileMakerArrayDataResponse.class, str.getClass());

        FileMakerArrayDataResponse rtn = testable.getResponseWrapper(type, str);

        assertNull(rtn);

        FileMakerArrayDataResponse mockRsp = mock(FileMakerArrayDataResponse.class);
        str = Optional.of("abcdef");
        when(objectMapper.readValue(anyString(), eq(type))).thenReturn(mockRsp);
        rtn = testable.getResponseWrapper(type, str);
        assertNotNull(rtn);
    }

    protected void mockAgentFindResponse() {
        ResponseEntity<String> agentResponse = mock(ResponseEntity.class);
        when(agentResponse.getStatusCode()).thenReturn(HttpStatus.OK);

        when(agentResponse.getBody()).thenReturn("agentJson");
        when(restTemplate.exchange(
                eq("http://fake.local/fakedb/layouts/ingestapi_agents/_find"),
                eq(HttpMethod.POST),
                any(),
                eq(String.class))
        ).thenReturn(agentResponse);
    }

    protected void mockAgentGetResponse(Long recordId) {
        ResponseEntity<String> agentResponse = mock(ResponseEntity.class);
        when(agentResponse.getStatusCode()).thenReturn(HttpStatus.OK);

        when(agentResponse.getBody()).thenReturn("agentJson");
        when(restTemplate.exchange(
                eq("http://fake.local/fakedb/layouts/ingestapi_agents/records/"+recordId),
                eq(HttpMethod.GET),
                any(),
                eq(String.class))
        ).thenReturn(agentResponse);
    }

    protected void mockRestTemplateResponse(HttpStatus responseCode, HttpMethod method, Long recordId, String responseBody) {

        String url = "http://fake.local/fakedb/layouts/ingestapi_agents/records";
        if(recordId != null) {
            url += "/"+recordId;
        }

        ResponseEntity<String> agentResponse = mock(ResponseEntity.class);

        if(responseBody != null) {
            when(agentResponse.getBody()).thenReturn(responseBody);
        }

        when(agentResponse.getStatusCode()).thenReturn(responseCode);
        when(restTemplate.exchange(
                eq(url),
                eq(method),
                any(),
                eq(String.class))
        ).thenReturn(agentResponse);
    }

    protected void mockArrayDataResponse(FileMakerResponseEntity rec) throws JsonProcessingException {
        FileMakerCreateRecordResponse createResp = mock(FileMakerCreateRecordResponse.class);
        FileMakerResponseCode code = mock(FileMakerResponseCode.class);
        when(code.getCode()).thenReturn(FileMakerInternalResponseCode.OK.getValue());
        when(createResp.getFirstResponseCode()).thenReturn(Optional.of(code));
        FileMakerRecordId recId = mock(FileMakerRecordId.class);
        when(recId.getRecordId()).thenReturn(123L);
        when(createResp.getResponse()).thenReturn(recId);

        when(objectMapper.readValue(anyString(), eq(FileMakerCreateRecordResponse.class))).thenReturn(createResp);

        FileMakerArrayDataResponse arrResp = mock(FileMakerArrayDataResponse.class);
        when(arrResp.getFirstResponseCode()).thenReturn(Optional.of(code));
        FileMakerArrayData data = mock(FileMakerArrayData.class);
        when(data.getData()).thenReturn(Arrays.asList(mock(Agent.class)));
        when(arrResp.getResponse()).thenReturn(data);

        JavaType type = objectMapper.getTypeFactory().constructParametricType(FileMakerArrayDataResponse.class, rec.getClass());
        when(objectMapper.readValue(eq("agentJson"), eq(type))).thenReturn(arrResp);
    }
}

class TestImpl extends RecordServiceBase{
    public TestImpl(AuthTokenStore authTokenStore, RestTemplate restTemplate, FileMakerConfig fileMakerConfig, ObjectMapper objectMapper) {
        super(authTokenStore, restTemplate, fileMakerConfig, objectMapper);
    }
}