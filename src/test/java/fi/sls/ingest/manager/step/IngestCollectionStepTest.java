package fi.sls.ingest.manager.step;

import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.Collection;
import fi.sls.ingest.proxy.filemaker.response.record.CollectionFieldData;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import fi.sls.ingest.representation.file.FilenameMetaData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class IngestCollectionStepTest {

    IngestCollectionStep testable;
    RecordService service;

    Collection fmRtn;
    FilenameMetaData metaData;

    @BeforeEach
    void setUp() {

        service = mock(RecordService.class);

        testable = new IngestCollectionStep(service);

        CollectionFieldData fmcRtn = mock(CollectionFieldData.class);
        when(fmcRtn.getCollectionName()).thenReturn("Tjänstearkivet");
        when(fmcRtn.getArchiveNumber()).thenReturn(123L);
        when(fmcRtn.getArchiveNumberSuffix()).thenReturn("66");

        fmRtn = mock(Collection.class);
        when(fmRtn.getFieldData()).thenReturn(fmcRtn);

        metaData = mock(FilenameMetaData.class);
        when(metaData.getCollectionDescriptionOrName()).thenReturn("Tjänstearkivet");
        when(metaData.getArchiveNrDigit()).thenReturn(123L);
        when(metaData.getArchiveNrSuffix()).thenReturn("66");

    }

    @Test
    void executeStep_returnsExistingIfRecordFound() throws IOException {

        ArgumentCaptor<Collection> serviceCaptor = ArgumentCaptor.forClass(Collection.class);

        Optional<FileMakerResponseEntity> serviceRtn = Optional.of(fmRtn);
        doReturn(serviceRtn).when(service).findRecordByIdentifier(serviceCaptor.capture());

        Collection rtn = testable.executeStep(metaData);

        // verify parameters for service call
        verify(service).findRecordByIdentifier(serviceCaptor.capture());
        assertEquals("Tjänstearkivet", serviceCaptor.getValue().getFieldData().getCollectionName());
        assertEquals(Long.valueOf(123), serviceCaptor.getValue().getFieldData().getArchiveNumber());
        assertEquals("66", serviceCaptor.getValue().getFieldData().getArchiveNumberSuffix());

        // verify values of return object
        assertEquals("Tjänstearkivet", rtn.getFieldData().getCollectionName());
        assertEquals(Long.valueOf(123), rtn.getFieldData().getArchiveNumber());
        assertEquals("66", rtn.getFieldData().getArchiveNumberSuffix());
    }

    @Test
    void executeStep_createsNewIfRecordNotFound() throws IOException {

        ArgumentCaptor<Collection> serviceCaptor = ArgumentCaptor.forClass(Collection.class);

        Optional<FileMakerResponseEntity> serviceRtn = Optional.empty();
        doReturn(serviceRtn).when(service).findRecordByIdentifier(serviceCaptor.capture());
        doReturn(fmRtn).when(service).createObjectRecord(serviceCaptor.capture());

        Collection rtn = testable.executeStep(metaData);

        // verify parameters for service call
        verify(service).findRecordByIdentifier(serviceCaptor.capture());
        assertEquals("Tjänstearkivet", serviceCaptor.getValue().getFieldData().getCollectionName());
        assertEquals(Long.valueOf(123), serviceCaptor.getValue().getFieldData().getArchiveNumber());
        assertEquals("66", serviceCaptor.getValue().getFieldData().getArchiveNumberSuffix());

        // verify values of return object
        assertEquals("Tjänstearkivet", rtn.getFieldData().getCollectionName());
        assertEquals(Long.valueOf(123), rtn.getFieldData().getArchiveNumber());
        assertEquals("66", rtn.getFieldData().getArchiveNumberSuffix());
    }

//    @Test
//    void executeStep_returnsNullIfServiceException() throws IOException {
//
//        when(service.findRecordByIdentifier(any())).thenThrow(IOException.class);
//        Collection rtn = testable.executeStep(metaData);
//
//        assertNull(rtn);
//    }
}