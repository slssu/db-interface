package fi.sls.ingest.fits.model.fieldresolver.image;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.*;
import java.util.Iterator;
import java.util.Optional;

/**
 * Resolver that fetches the image width from the NISOImageMetadata structure of the JHove tool result
 */
public class JHoveNISOImageMetadata extends AbstractFieldValueResolver {

    public final static String COLOR_SPACE_CFA = "CFA";
    public final static String FIELD_IMAGE_WIDTH = "imageWidth";
    public final static String FIELD_IMAGE_HEIGHT = "imageHeight";
    public final static String FIELD_BITS_PER_SAMPLE = "bitsPerSampleValue";
    public final static String FIELD_COLOR_SPACE = "colorSpace";

    String colorSpaceTarget;

    /**
     * Creates new instance of this resolver with colorSpaceTarget CFA
     * @param fitsResult
     */
    public JHoveNISOImageMetadata(String fieldName, FitsResult fitsResult) {
        super(fieldName, fitsResult);
        this.colorSpaceTarget = COLOR_SPACE_CFA;
    }

    public JHoveNISOImageMetadata(String fieldName, FieldValueResolver resolver){
        super(fieldName, resolver);
        this.colorSpaceTarget = COLOR_SPACE_CFA;
    }

    public JHoveNISOImageMetadata(String fieldName, FitsResult fitsResult, FieldValueResolver resolver) {
        super(fieldName, fitsResult, resolver);
        this.colorSpaceTarget = COLOR_SPACE_CFA;
    }

    public JHoveNISOImageMetadata(String fieldName, FitsResult fitsResult, String colorSpaceTarget) {
        super(fieldName, fitsResult);
        this.colorSpaceTarget = colorSpaceTarget;
    }


    @Override
    public Optional<String> getValue() {
        return getFieldValue(this.fieldName);
    }

    protected Optional<String> getFieldValue(String fieldName){
        XPath xPath = XPathFactory.newInstance().newXPath();

        try {

            xPath.setNamespaceContext(new JHoveNISONamespaceContext(fitsResult.getXmlDocument()));

            String expStr = "/fits:fits/fits:toolOutput/fits:tool[@name = 'Jhove']//mix:mix[mix:BasicImageInformation/mix:BasicImageCharacteristics/mix:PhotometricInterpretation/mix:colorSpace = '" + colorSpaceTarget + "']//mix:" + fieldName;

            XPathExpression expression = xPath.compile(expStr);

            NodeList nodes = (NodeList) expression.evaluate(fitsResult.getXmlDocument(), XPathConstants.NODESET);
            if(nodes.getLength() > 0){
                Node n = nodes.item(0);
                String val = n.getTextContent();

                if(val != null && !val.isEmpty()){
                    return Optional.of(val);
                }
            }

            return executeNextResolver();
        } catch (XPathExpressionException e) {
            return executeNextResolver();
        }
    }


    class JHoveNISONamespaceContext implements NamespaceContext{

        private Document sourceDocument;

        public JHoveNISONamespaceContext(Document document){
            sourceDocument = document;
        }

        @Override
        public String getNamespaceURI(String prefix) {
            if (prefix.equals(XMLConstants.DEFAULT_NS_PREFIX)) {
                return sourceDocument.lookupNamespaceURI(null);
            } else if(prefix.equals("fits")) {
                return "http://hul.harvard.edu/ois/xml/ns/fits/fits_output";
            } else if(prefix.equals("mix")){
                return "http://www.loc.gov/mix/v20";
            } else {
                return sourceDocument.lookupNamespaceURI(prefix);
            }
        }

        @Override
        public String getPrefix(String namespaceURI) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Iterator<String> getPrefixes(String namespaceURI) {
            throw new UnsupportedOperationException();
        }
    }
}


