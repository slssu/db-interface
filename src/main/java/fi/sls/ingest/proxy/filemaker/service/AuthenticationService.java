package fi.sls.ingest.proxy.filemaker.service;

import fi.sls.ingest.config.FileMakerConfig;
import fi.sls.ingest.proxy.filemaker.AuthToken;
import fi.sls.ingest.proxy.filemaker.AuthTokenStore;
import fi.sls.ingest.proxy.filemaker.request.AuthTokenRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.response.AuthenticationResponse;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

/**
 * Provides methods for authenticating this proxy API against the FileMaker API.
 *
 * Services that need to access the FileMaker API should extend this class.
 */
@Service
public class AuthenticationService {

    /**
     * RestTemplate used for making REST requests
     *
     * @see org.springframework.web.client.RestTemplate
     */
    protected RestTemplate restTemplate;

    /**
     * Configurations for FileMaker connection
     *
     * @see fi.sls.ingest.config.FileMakerConfig
     */
    protected FileMakerConfig fileMakerConfig;

    /**
     * The number of times we have retried authenticating.
     *
     * If this goes above a certain count, we stop trying and let the exception continue up the stack.
     */
    protected int numAuthRetries = 0;

    protected final int maxAuthRetries = 5;

    protected AuthTokenStore authTokenStore;


    @Autowired
    public AuthenticationService(
            AuthTokenStore authTokenStore,
            @Qualifier("mediumTimeoutRestTemplate") RestTemplate restTemplate,
            FileMakerConfig fileMakerConfig
    ){
        this.restTemplate = restTemplate;
        this.fileMakerConfig = fileMakerConfig;
        this.authTokenStore = authTokenStore;
    }


    /**
     * Fetches an authentication token from FileMaker REST API for use in subsequent calls to the API.
     *
     * If a token already exists, it is returned instead.
     *
     * @return The token string
     */
    public AuthToken authenticate(){
        return authenticate(false);
    }

    /**
     * Fetches an authentication token from FileMaker REST API for use in subsequent calls to the API.
     *
     * if forceReAuth is true, the authentication is performed even if a token already exists in the cache.
     *
     * @param forceReAuth
     * @return
     */
    public AuthToken authenticate(boolean forceReAuth){
        if(forceReAuth == true || authTokenStore.getAuthToken() == null) {
            HttpHeaders headers = createLoginHeaders(this.fileMakerConfig.getUsername(), this.fileMakerConfig.getPassword());
            headers.setContentType(MediaType.APPLICATION_JSON);

            ResponseEntity<AuthenticationResponse> response = restTemplate.exchange(
                    this.getEndpointUrl("sessions"),
                    HttpMethod.POST,
                    new HttpEntity<AuthTokenRequest>(headers),
                    AuthenticationResponse.class);

            authTokenStore.setAuthToken(response.getBody().getResponse());
            numAuthRetries = 0;
        }
        return authTokenStore.getAuthToken();
    }

    /**
     * Makes a logout request against the FileMaker REST API to invalidate the currently stored session.
     */
    public void logout(){
        if(authTokenStore.getAuthToken() != null){
            restTemplate.delete(this.getEndpointUrl("sessions/"+authTokenStore.getAuthToken().getToken()));
            resetAuthToken();
        }
    }

    /**
     * Clear the current FileMaker API auth token.
     */
    public void resetAuthToken(){
        authTokenStore.setAuthToken(null);
    }


    /**
     * Generates HttpHeaders for authentication against FileMaker (HTTP Basic auth)
     *
     * @param username A FileMaker account username to use for authenticating against the API
     * @param password A FileMaker account password to use for authenticating against the API
     *
     * @return
     */
    HttpHeaders createLoginHeaders(String username, String password){
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
        }};
    }

    /**
     * Generates the complete endpoint path for a FileMaker API resource
     *
     * @return
     */
    protected String getEndpointUrl(String endpointName){
        return this.fileMakerConfig.getUrl()+"/"+this.fileMakerConfig.getDatabase()+"/"+endpointName;
    }

    protected String getEndpointUrl(String endpointName, String layoutName){
        return this.fileMakerConfig.getUrl()+"/"+this.fileMakerConfig.getDatabase()+"/layouts/"+layoutName+"/"+endpointName;
    }

    protected String getEndpointUrl(String endpointName, String layoutName, Long recordId){
        return this.fileMakerConfig.getUrl()+"/"+this.fileMakerConfig.getDatabase()+"/layouts/"+layoutName+"/"+endpointName+"/"+recordId;
    }

}
