package fi.sls.ingest.rest;

import fi.sls.ingest.manager.IngestDigitizationProfile;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

/**
 * The request structure for starting a new ingest processing task.
 */
@Data
@NoArgsConstructor
public class IngestRequestBody {

    @NotEmpty
    @Valid
    ArrayList<IngestRequestItem> items = new ArrayList<>();

    @NotNull
    @Valid
    IngestDigitizationProfile digitizationProfile;

    public IngestRequestBody(ArrayList<IngestRequestItem> items ) {
        this.setItems(items);
    }
}
