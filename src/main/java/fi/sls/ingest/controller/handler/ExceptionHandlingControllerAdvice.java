package fi.sls.ingest.controller.handler;

import fi.sls.ingest.controller.response.RestValidationError;
import fi.sls.ingest.exception.InvalidParametersException;
import fi.sls.ingest.exception.InvalidProcessingStatusException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Iterator;

/**
 * Exception handler for REST API validation errors
 *
 * Used to inject a SLS specific error code for a error that happened during a request and returning
 * a REST client friendly error data structure.
 */
@ControllerAdvice
public class ExceptionHandlingControllerAdvice {

    /**
     * Handles errors where the client sent the wrong type of parameters in a request
     *
     * @param ex The exception that was thrown
     * @return A ResponseEntity that will be sent to the REST client
     */
    @ExceptionHandler(InvalidParametersException.class)
    public ResponseEntity<RestValidationError> restValidationError(InvalidParametersException ex){
        RestValidationError response = RestValidationError.SLS001();

        Iterator<FieldError> i = ex.getResult().getFieldErrors().iterator();

        while(i.hasNext()){
            FieldError err = i.next();
            response.addFieldError(err.getField(), err.getDefaultMessage());
        }

        return new ResponseEntity<RestValidationError>(response, HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles errors for ProcessingStatus changes
     * @param ex
     * @return
     */
    @ExceptionHandler(InvalidProcessingStatusException.class)
    public ResponseEntity<RestValidationError> processingStatusError(InvalidProcessingStatusException ex){
        RestValidationError response = new RestValidationError("SLS-002", ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}

