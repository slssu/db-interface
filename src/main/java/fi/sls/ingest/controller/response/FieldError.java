package fi.sls.ingest.controller.response;

import lombok.Getter;

public class FieldError {

    @Getter
    private String field;

    @Getter
    private String message;

    public FieldError(String field, String message){
        this.field = field;
        this.message = message;
    }
}
