package fi.sls.ingest.manager;

import fi.sls.ingest.representation.file.FilenameMetaData;
import lombok.Data;

/**
 * A slimmed down version of a IngestProcessingItem containing only fields
 * used for logging purposes to track the items progress through the system
 */
@Data
public class LoggableIngestProcessingItem {

    /**
     * @see IngestProcessingItem#getToken()
     */
    private String token;

    /**
     * @see IngestProcessingItem#getProcessingStatus()
     */
    private ProcessingStatus processingStatus;

    /**
     * @see IngestProcessingItem#getMd5ChecksumBefore()
     */
    private String md5ChecksumBefore;

    /**
     * @see IngestProcessingItem#getMd5ChecksumAfter()
     */
    private String md5ChecksumAfter;

    /**
     * @see IngestProcessingItem#getFilenameMetaData()
     */
    private FilenameMetaData filenameMetaData;

    /**
     * @see IngestProcessingItem#getItem()
     */
    private IngestItem item;
}
