package fi.sls.ingest.actuator.metrics.supplier;

import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.repository.IngestItemRepository;

import java.util.function.Supplier;

/**
 * Provides a supplier implementation that fetches the nr of IngestProcessingItems per status
 * currently in the database
 */
public class ProcessingItemCountSupplier implements Supplier<Number> {

    private IngestItemRepository source;
    private ProcessingStatus filterOnStatus;

    public ProcessingItemCountSupplier(IngestItemRepository source){
        this.source = source;
        this.filterOnStatus = null;
    }

    public ProcessingItemCountSupplier(IngestItemRepository source, ProcessingStatus status) {
        this(source);

        this.filterOnStatus = status;
    }

    @Override
    public Number get() {

        if(filterOnStatus == null){
            return source.count();
        } else {
            return source.countByProcessingStatus(filterOnStatus);
        }
    }
}
