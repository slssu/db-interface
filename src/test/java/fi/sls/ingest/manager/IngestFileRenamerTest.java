package fi.sls.ingest.manager;

import fi.sls.ingest.exception.IngestFilenameException;
import fi.sls.ingest.parser.FilenameMetaParser;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.rest.IngestFile;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class IngestFileRenamerTest {

    static Validator validator;
    IngestFileRenamer testable;
    FilenameMetaParser parser;

    @TempDir
    File temporaryFolder;

    @BeforeAll
    public static void setupCls(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }


    @BeforeEach
    void setUp() {
        parser = mock(FilenameMetaParser.class);

        testable = new IngestFileRenamer(validator, parser);

        if(temporaryFolder.listFiles().length > 0){
            for(File f : temporaryFolder.listFiles()){
                f.delete();
            }
        }

    }

    @Test
    void rename_throwsExceptionForInvalidFilename() throws IOException {
        String name = "ota_555j.1.4_brev_nooo_1.tif";

        when(parser.parseWithRelaxedValidation(name)).thenThrow(IngestFilenameException.class);

        File file = new File(temporaryFolder, name);
        IngestFile ingestFile = IngestFile.fromFile(file, temporaryFolder.getAbsolutePath());

        IngestFilenameException e = assertThrows(IngestFilenameException.class, () -> {
            testable.rename(ingestFile);
        });
    }

    @Test
    void rename_doesNotModifyCorrectFilename() throws IOException {

        File file = new File(temporaryFolder, "ota_555j.1.4_brev_00001_0001.tif");
        IngestFile ingestFile = IngestFile.fromFile(file, temporaryFolder.getAbsolutePath());

        File rtn = testable.rename(ingestFile);

        assertEquals(file.getName(), rtn.getName());

    }

    @Test
    void rename_modifiesFilenameWithMissingZeroPaddingSigNr() throws IOException{
        testRename("ota_555j.1.4_brev_1_2_3_orig.tif", "ota_555j.1.4_brev_00001_0002_03_orig.tif", false);
        testRename("ota_555j.1.4_brev_1_0001_1_orig.tif", "ota_555j.1.4_brev_00001_0001_01_orig.tif", false);
        testRename("ota_555j.1.4_brev_1_0001_1.tif", "ota_555j.1.4_brev_00001_0001_01.tif", false);
        testRename("ota_555j.1.4_brev_1_0001.tif", "ota_555j.1.4_brev_00001_0001_01.tif", true);
        testRename("ota_555j.1.4_brev_1_1.tif", "ota_555j.1.4_brev_00001_0001_01.tif", true);
        testRename("ota_555j.1.4_brev_1_1_orig.tif", "ota_555j.1.4_brev_00001_0001_01_orig.tif", true);

        testRename("ota_555j.1_foto_1_1.tif", "ota_555j.1_foto_00001_0001_01.tif", true);
        testRename("ota_555j.1_foto_1_1_orig.tif", "ota_555j.1_foto_00001_0001_01_orig.tif", true);
        testRename("ota_555j_foto_1_1.tif", "ota_555j_foto_00001_0001_01.tif", true);

        testRename("slsa_555_foto_1_1_orig.tif", "slsa_555_foto_00001_0001_01_orig.tif", true);
        testRename("slsa_555_foto_1_1.tif", "slsa_555_foto_00001_0001_01.tif", true);
        testRename("slsa_555.1_foto_001_001_2.tif", "slsa_555.1_foto_00001_0001_02.tif", false);

        testRename("slsa_1103.g.a.23_doku_0_1_1.tif", "slsa_1103.g.a.23_doku_00000_0001_01.tif", false);

    }

    private void testRename(String input, String output, boolean useDefaultVersion) throws IOException {

        FilenameMetaData metaDataMock = mock(FilenameMetaData.class);
        when(metaDataMock.isDefaultVersionNr()).thenReturn(useDefaultVersion);
        when(parser.parseWithRelaxedValidation(any())).thenReturn(metaDataMock);

        File file = new File(temporaryFolder, input);
        // write something to the file so it exists on file system
        new FileOutputStream(file).close();

        IngestFile ingestFile = IngestFile.fromFile(file, temporaryFolder.getAbsolutePath());

        File rtn = testable.rename(ingestFile);
        assertEquals(output, rtn.getName());

        // do we find the file in the new location as well
        File newFile = new File(temporaryFolder, rtn.getName());
        assertTrue(newFile.exists());

        // remove the files so a new test can run
        newFile.delete();
        file.delete();

    }
}