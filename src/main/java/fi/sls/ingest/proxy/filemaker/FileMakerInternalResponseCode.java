package fi.sls.ingest.proxy.filemaker;

/**
 * Defines codes that FileMaker uses internally and might return from API calls.
 *
 * @see <a href="https://fmhelp.filemaker.com/help/17/fmp/en/index.html#page/FMP_Help/error-codes.html">FileMaker error codes</a>
 */
public enum FileMakerInternalResponseCode {
    OK(0),
    RECORD_IS_MISSING(101),
    RECORD_IN_USE(301),
    TABLE_IN_USE(302),
    DATABASE_SCHEMA_IN_USE(303),
    NO_RECORDS_MATCH_REQUEST(401),
    VALIDATION_ERROR(506);


    private int value;

    FileMakerInternalResponseCode(int value){
        this.value = value;
    }

    public int getValue(){
        return value;
    }

    /**
     * Converts an integer value to a matching FileMakerInternalResponseCode enum.
     *
     * @param value The integer code for one of the enums in this class
     * @return
     * @throws IllegalArgumentException If the passed value does not match one of the enum values in this class
     */
    public static FileMakerInternalResponseCode fromValue(int value) throws IllegalArgumentException{
        switch(value){
            case 0:
                return FileMakerInternalResponseCode.OK;
            case 101:
                return FileMakerInternalResponseCode.RECORD_IS_MISSING;
            case 301:
                return FileMakerInternalResponseCode.RECORD_IN_USE;
            case 302:
                return FileMakerInternalResponseCode.TABLE_IN_USE;
            case 303:
                return FileMakerInternalResponseCode.DATABASE_SCHEMA_IN_USE;
            case 401:
                return FileMakerInternalResponseCode.NO_RECORDS_MATCH_REQUEST;
            case 506:
                return FileMakerInternalResponseCode.VALIDATION_ERROR;
            default:
                throw new IllegalArgumentException(String.format("Value %d cannot be converted to an FileMakerInternalResponseCode enum.", value));
        }
    }
}
