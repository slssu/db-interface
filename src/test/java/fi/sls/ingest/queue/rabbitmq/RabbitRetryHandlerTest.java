package fi.sls.ingest.queue.rabbitmq;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.repository.IngestItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.support.ListenerExecutionFailedException;

import java.io.IOException;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class RabbitRetryHandlerTest {

    RabbitRetryHandler testable;

    ObjectMapper objectMapper;
    IngestItemRepository ingestItemRepository;

    org.springframework.messaging.Message<?> message;
    ListenerExecutionFailedException exception;
    Message amqpMessage;
    MessageProperties props;


    @BeforeEach
    void setUp(){
        objectMapper = mock(ObjectMapper.class);
        ingestItemRepository = mock(IngestItemRepository.class);

        message = mock(org.springframework.messaging.Message.class);
        amqpMessage = mock(Message.class);
        exception = new ListenerExecutionFailedException("fake exception", null, amqpMessage);
        props = mock(MessageProperties.class);
        when(amqpMessage.getMessageProperties()).thenReturn(props);

        testable = new RabbitRetryHandler(objectMapper, ingestItemRepository);
    }


    @Test
    void handleError_storesErrorToItemIfExists() throws Exception {
        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");

        byte[] body = "message body".getBytes();
        when(props.isRedelivered()).thenReturn(true);
        when(amqpMessage.getBody()).thenReturn(body);

        when(objectMapper.readValue(body, IngestProcessingItem.class)).thenReturn(item);
        when(ingestItemRepository.findByToken(item.getToken())).thenReturn(Optional.of(item));

        assertThrows(AmqpRejectAndDontRequeueException.class, () -> {
            testable.handleError(amqpMessage, message, exception);
        });

        verify(item).setProcessingStatus(ProcessingStatus.REJECTED);
        verify(item).setMessage(exception.getMessage());
        verify(ingestItemRepository).save(item);
    }

    @Test
    void handleError_throwsExceptionIfItemDoesNotExist() throws Exception {

        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");

        byte[] body = "message body".getBytes();
        when(props.isRedelivered()).thenReturn(true);
        when(amqpMessage.getBody()).thenReturn(body);

        when(objectMapper.readValue(body, IngestProcessingItem.class)).thenReturn(item);
        when(ingestItemRepository.findByToken(item.getToken())).thenReturn(Optional.empty());

        Exception e = assertThrows(AmqpRejectAndDontRequeueException.class, () -> {
            testable.handleError(amqpMessage, message, exception);
        });

        assertThat(e.getCause().getMessage(), containsString("Could not store queue error for item because it does not exist"));
    }

    @Test
    void handleError_rethrowsIfMessageNotIsRedelivered() throws Exception {

        when(props.isRedelivered()).thenReturn(false);

        Exception e = assertThrows(AmqpRejectAndDontRequeueException.class, () -> {
            testable.handleError(amqpMessage, message, exception);
        });

        assertThat(e.getCause().getMessage(), containsString("fake exception"));
    }

    @Test
    void handleError_throwsAmqpRejectAndDontRequeueExceptionForIOException() throws IOException {
        byte[] body = "message body".getBytes();
        when(props.isRedelivered()).thenReturn(true);
        when(amqpMessage.getBody()).thenReturn(body);

        when(objectMapper.readValue(body, IngestProcessingItem.class)).thenThrow(IOException.class);

        Exception e = assertThrows(AmqpRejectAndDontRequeueException.class, () -> {
            testable.handleError(amqpMessage, message, exception);
        });
    }

    @Test
    void handleError_throwsAmqpRejectAndDontRequeueExceptionForIllegalArgumentException() throws IOException {
        byte[] body = "message body".getBytes();
        when(props.isRedelivered()).thenReturn(true);
        when(amqpMessage.getBody()).thenReturn(body);

        when(objectMapper.readValue(body, IngestProcessingItem.class)).thenThrow(IllegalArgumentException.class);

        Exception e = assertThrows(AmqpRejectAndDontRequeueException.class, () -> {
            testable.handleError(amqpMessage, message, exception);
        });
    }
}