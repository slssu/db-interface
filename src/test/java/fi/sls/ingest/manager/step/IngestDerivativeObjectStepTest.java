package fi.sls.ingest.manager.step;

import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.*;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import fi.sls.ingest.representation.file.FilenameMetaData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class IngestDerivativeObjectStepTest {

    IngestDerivativeObjectStep testable;
    RecordService service;

    AccessFile accessFile;
    FilenameMetaData metaData;
    IntellectualEntity intellectualEntity;
    IntellectualEntityFieldData ieFieldData;

    DerivateObject fmRtn;
    DerivateObjectFieldData fmcRtn;

    @BeforeEach
    void setUp() {
        service = mock(RecordService.class);
        testable = new IngestDerivativeObjectStep(service);

        accessFile = mock(AccessFile.class);

        metaData = mock(FilenameMetaData.class);

        ieFieldData = mock(IntellectualEntityFieldData.class);

        intellectualEntity = mock(IntellectualEntity.class);
        when(intellectualEntity.getFieldData()).thenReturn(ieFieldData);

        fmcRtn = mock(DerivateObjectFieldData.class);


        fmRtn = mock(DerivateObject.class);
        when(fmRtn.getFieldData()).thenReturn(fmcRtn);

        // default field returns
        when(fmcRtn.getFilePath()).thenReturn("mystrippedfilepath");
        when(accessFile.getStrippedFilePath()).thenReturn("mystrippedfilepath");
        when(accessFile.getFilePathFolder()).thenReturn("notarealone");
        when(metaData.getDigitalObjectIdentifier()).thenReturn("somedigitalobjectidentifier");

    }

//    @Test
//    void executeStep_returnsNullIfServiceException() throws IOException {
//
//        when(accessFile.getStrippedFilePath()).thenReturn("abc");
//        when(accessFile.getFilePathFolder()).thenReturn("def");
//        when(metaData.getDigitalObjectIdentifier()).thenReturn("abc");
//
//        when(service.findRecordByIdentifier(any())).thenThrow(IOException.class);
//        DerivateObject rtn = testable.executeStep(accessFile, metaData, intellectualEntity);
//
//        assertNull(rtn);
//    }

    @Test
    void executeStep_createsNewIfRecordNotFound() throws IOException {

        ArgumentCaptor<DerivateObject> serviceCaptor = ArgumentCaptor.forClass(DerivateObject.class);

        Optional<FileMakerResponseEntity> serviceRtn = Optional.empty();
        doReturn(serviceRtn).when(service).findRecordByIdentifier(serviceCaptor.capture());
        doReturn(fmRtn).when(service).createObjectRecord(serviceCaptor.capture());


        DerivateObject rtn = testable.executeStep(accessFile, metaData, intellectualEntity);

        // verify parameters for service call
        verify(service).findRecordByIdentifier(serviceCaptor.capture());
        assertEquals("mystrippedfilepath", serviceCaptor.getValue().getFieldData().getFilePath());
        assertEquals("somedigitalobjectidentifier", serviceCaptor.getValue().getFieldData().getIsGeneratedFrom());
        assertEquals("notarealone", serviceCaptor.getValue().getFieldData().getFilePathFolder());

        assertEquals("mystrippedfilepath", rtn.getFieldData().getFilePath());

    }

    @Test
    void executeStep_returnsExistingIfRecordFound() throws IOException {

        ArgumentCaptor<DerivateObject> serviceCaptor = ArgumentCaptor.forClass(DerivateObject.class);

        Optional<FileMakerResponseEntity> serviceRtn = Optional.of(fmRtn);
        doReturn(serviceRtn).when(service).findRecordByIdentifier(serviceCaptor.capture());
        doReturn(fmRtn).when(service).updateObjectRecord(serviceCaptor.capture());

        DerivateObject rtn = testable.executeStep(accessFile, metaData, intellectualEntity);

        // verify parameters for service call
        verify(service).findRecordByIdentifier(serviceCaptor.capture());
        assertEquals("mystrippedfilepath", serviceCaptor.getValue().getFieldData().getFilePath());
        assertEquals("somedigitalobjectidentifier", serviceCaptor.getValue().getFieldData().getIsGeneratedFrom());
        assertEquals("notarealone", serviceCaptor.getValue().getFieldData().getFilePathFolder());

        assertEquals("mystrippedfilepath", rtn.getFieldData().getFilePath());

    }

    @Test
    void executeStep_copiesFieldsIfDatabaseImage() throws IOException {
        ArgumentCaptor<FileMakerResponseEntity> serviceCaptor = ArgumentCaptor.forClass(FileMakerResponseEntity.class);

        when(accessFile.getFilePathFolder()).thenReturn("databasbilder");
        when(fmcRtn.getCFilePathTotal()).thenReturn("mycfilepathtotal");
        when(fmcRtn.getDatabasbild()).thenReturn("mydbpicturepath.jpg");

        Optional<FileMakerResponseEntity> serviceRtn = Optional.of(fmRtn);
        doReturn(serviceRtn).when(service).findRecordByIdentifier(serviceCaptor.capture());

        // chain up the return values from the recordService
        doReturn(fmRtn)
                .doReturn(fmRtn)
                .doReturn(intellectualEntity)
                .when(service).updateObjectRecord(serviceCaptor.capture());

        DerivateObject rtn = testable.executeStep(accessFile, metaData, intellectualEntity);

        List<FileMakerResponseEntity> captorValues = serviceCaptor.getAllValues();

        DerivateObject doRtn = (DerivateObject) captorValues.get(0);
        assertEquals(accessFile.getFilePathFolder(), doRtn.getFieldData().getFilePathFolder());
        verify(fmcRtn).setDatabasbild(fmcRtn.getCFilePathTotal());
        verify(fmcRtn, never()).setThumbnail(any());

        IntellectualEntity ieRtn = (IntellectualEntity) captorValues.get(3);
        assertEquals(fmcRtn.getDatabasbild(), ieRtn.getFieldData().getDatabasbild());
        assertNull(ieRtn.getFieldData().getThumbnail());

    }


    @Test
    void executeStep_copiesFieldsIfThumbnailImage() throws IOException {
        ArgumentCaptor<FileMakerResponseEntity> serviceCaptor = ArgumentCaptor.forClass(FileMakerResponseEntity.class);

        when(accessFile.getFilePathFolder()).thenReturn("thumbnails");
        when(fmcRtn.getCFilePathTotal()).thenReturn("mycfilepathtotal");
        when(fmcRtn.getThumbnail()).thenReturn("mythumbpicturepath.jpg");

        Optional<FileMakerResponseEntity> serviceRtn = Optional.of(fmRtn);
        doReturn(serviceRtn).when(service).findRecordByIdentifier(serviceCaptor.capture());

        // chain up the return values from the recordService
        doReturn(fmRtn)
                .doReturn(fmRtn)
                .doReturn(intellectualEntity)
                .when(service).updateObjectRecord(serviceCaptor.capture());

        DerivateObject rtn = testable.executeStep(accessFile, metaData, intellectualEntity);

        List<FileMakerResponseEntity> captorValues = serviceCaptor.getAllValues();

        DerivateObject doRtn = (DerivateObject) captorValues.get(0);
        assertEquals(accessFile.getFilePathFolder(), doRtn.getFieldData().getFilePathFolder());
        verify(fmcRtn).setThumbnail(fmcRtn.getCFilePathTotal());
        verify(fmcRtn, never()).setDatabasbild(any());

        IntellectualEntity ieRtn = (IntellectualEntity) captorValues.get(3);
        assertEquals(fmcRtn.getThumbnail(), ieRtn.getFieldData().getThumbnail());
        assertNull(ieRtn.getFieldData().getDatabasbild());

    }

}