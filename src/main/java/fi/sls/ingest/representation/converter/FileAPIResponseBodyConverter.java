package fi.sls.ingest.representation.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.proxy.slsfileapi.FileAPIResponseBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.AttributeConverter;
import java.io.IOException;

@Slf4j
public class FileAPIResponseBodyConverter extends JsonStringConverter implements AttributeConverter<FileAPIResponseBody, String> {
    @Autowired
    public FileAPIResponseBodyConverter(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public String convertToDatabaseColumn(FileAPIResponseBody fileAPIResponseBody) {

        if(fileAPIResponseBody == null){
            return null;
        } else {
            String rtnJson = null;

            try{
                rtnJson = objectMapper.writeValueAsString(fileAPIResponseBody);
            } catch (JsonProcessingException e) {
                log.error("Error converting from List of FileAPIResponseBody to JSON: {}", e.getMessage());
            }
            return rtnJson;
        }

    }

    @Override
    public FileAPIResponseBody convertToEntityAttribute(String jsonData) {
        if(jsonData == null){
            return null;
        } else {
            FileAPIResponseBody rtnItem = null;
            try{
                rtnItem = objectMapper.readValue(jsonData, FileAPIResponseBody.class);
            } catch (IOException e) {
                log.error("Error converting to List of FileAPIResponseBody from JSON: {}", e.getMessage());
            }

            return rtnItem;
        }
    }
}
