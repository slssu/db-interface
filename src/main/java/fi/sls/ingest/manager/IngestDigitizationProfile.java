package fi.sls.ingest.manager;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Represents a SLS digitization profile
 *
 * A digitization profile is metadata about the hardware and settings used to scan or otherwise prepare
 * a document for archiving.
 *
 * This class is part of the abstraction layer to decouple FileMaker from the ingestion process in preparation
 * for deprecating FileMaker in the future.
 *
 * In FileMaker, digitization profile data is stored in the "digitizationProfiles" table.
 */
@Data
public class IngestDigitizationProfile {

    /**
     * The primary key for this profile
     */
    @NotNull
    @Min(value = 1, message = "id must be a positive integer")
    private Long id;

    /**
     * The display name of the profile
     *
     * Also works as foreign key in some FileMaker tables [sic]
     */
    @NotBlank
    private String profileName;
}
