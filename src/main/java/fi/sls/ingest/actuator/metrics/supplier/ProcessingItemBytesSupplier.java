package fi.sls.ingest.actuator.metrics.supplier;

import fi.sls.ingest.repository.IngestItemRepository;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * Provides supplier for fetching the total nr of bytes processed by the system, as defined by the
 * items in the database.
 */
public class ProcessingItemBytesSupplier implements Supplier<Number> {
    private IngestItemRepository source;

    public ProcessingItemBytesSupplier(IngestItemRepository source) {
        this.source = source;
    }

    @Override
    public Number get() {
        Optional<Long> duration = source.getTotalBytesProcessed();

        return duration.orElse(Long.valueOf(0));
    }
}
