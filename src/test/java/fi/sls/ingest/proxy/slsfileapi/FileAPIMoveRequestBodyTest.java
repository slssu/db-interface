package fi.sls.ingest.proxy.slsfileapi;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.manager.IngestItem;
import fi.sls.ingest.manager.IngestProcessingItem;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

class FileAPIMoveRequestBodyTest {

    @Test
    void createFrom() {
        IngestItem mockIngest = Mockito.mock(IngestItem.class);
        when(mockIngest.getFitsPath()).thenReturn("/processing/myFitsPath.jpg");

        IngestProcessingItem mockItem = Mockito.mock(IngestProcessingItem.class);
        when(mockItem.getItem()).thenReturn(mockIngest);
        when(mockItem.getToken()).thenReturn("abcdef123456");

        FitsResult mockResult = Mockito.mock(FitsResult.class);
        when(mockResult.getIdentityMimetype()).thenReturn("image/jpeg");

        FileAPIMoveRequestBody rtn = FileAPIMoveRequestBody.createFrom(mockItem, mockResult, "http://my.callback.url");

        assertEquals("http://my.callback.url", rtn.getCallbackUrl());
        assertEquals("processing/myFitsPath.jpg", rtn.getPath());
        assertEquals("processing/myFitsPath.jpg.fits.xml", rtn.getMetadataPath());
        assertEquals("image/jpeg", rtn.getMimeType());
        assertEquals("abcdef123456", rtn.getToken());

    }

    @Test
    void isInstanceOfFileAPIRequestBody() {
        assertTrue(FileAPIRequestBody.class.isAssignableFrom(FileAPIMoveRequestBody.class));
    }
}