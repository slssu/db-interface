package fi.sls.ingest.exception;

public class UserNotFoundException extends EntityNotFoundException {
    public UserNotFoundException(String exception) {
        super(exception);
    }
}
