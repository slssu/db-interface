package fi.sls.ingest.proxy.filemaker.response.record;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.FitsVideoResult;
import fi.sls.ingest.manager.IngestDigitizationProfile;
import fi.sls.ingest.manager.MD5ComparisonItem;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.representation.file.ObjectCategory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DigitalObjectFieldDataTest {

    @Test
    void generatePremiseEventIdFor_returnsCorrectlyForValidInput() {
        DigitalObjectFieldData testable = new DigitalObjectFieldData();

        assertEquals("cr:", testable.generatePremiseEventIdFor("cr"));

        testable.setDigitizationProfileId("myProfileId");
        assertEquals("cr:_myProfileId", testable.generatePremiseEventIdFor("cr"));

        testable.setDateCreated("2018-12-12 12:00");
        assertEquals("cr:2018-12-12_myProfileId", testable.generatePremiseEventIdFor("cr"));
    }

    @Test
    void getCalculatedDateCreated_returnsCorrectlyForValidCreatedValues(){
        DigitalObjectFieldData testable = new DigitalObjectFieldData();

        assertEquals("", testable.getCalculatedDateCreated());

        testable.setDateCreated("2018-12-15T14:59:46");
        assertEquals("12/15/2018", testable.getCalculatedDateCreated());

        testable.setDateCreated("2018-12-15");
        assertEquals("12/15/2018", testable.getCalculatedDateCreated());

        testable.setDateCreated("");
        assertEquals("", testable.getCalculatedDateCreated());

        testable.setDateCreated("2002:12:13 19:41:42");
        assertEquals("12/13/2002", testable.getCalculatedDateCreated());

        testable.setDateCreated("2002-12-13 19:41:42");
        assertEquals("12/13/2002", testable.getCalculatedDateCreated());
    }

    @Test
    void populateFromMetadataResults_setsValuesForFitsVideoResult(){

        ObjectCategory oc = mock(ObjectCategory.class);

        FilenameMetaData filenameMetaData = mock(FilenameMetaData.class);
        when(filenameMetaData.getCategory()).thenReturn(oc);


        FitsVideoResult fitsResult = mock(FitsVideoResult.class);
        when(fitsResult.getAvBitDepth()).thenReturn(Integer.valueOf(8));

        IngestDigitizationProfile digitizationProfile = mock(IngestDigitizationProfile.class);
        MD5ComparisonItem md5Comparison = mock(MD5ComparisonItem.class);

        DigitalObjectFieldData testable = new DigitalObjectFieldData();
        testable.populateFromMetadataResults(filenameMetaData, fitsResult, digitizationProfile, md5Comparison);

        assertEquals("8", testable.getBitDepth());
    }

    @Test
    void populateFromMetadataResults_handlesAvBitDepthNullInteger(){

        ObjectCategory oc = mock(ObjectCategory.class);

        FilenameMetaData filenameMetaData = mock(FilenameMetaData.class);
        when(filenameMetaData.getCategory()).thenReturn(oc);


        FitsVideoResult fitsResult = mock(FitsVideoResult.class);
        when(fitsResult.getAvBitDepth()).thenReturn(null);

        IngestDigitizationProfile digitizationProfile = mock(IngestDigitizationProfile.class);
        MD5ComparisonItem md5Comparison = mock(MD5ComparisonItem.class);

        DigitalObjectFieldData testable = new DigitalObjectFieldData();
        testable.populateFromMetadataResults(filenameMetaData, fitsResult, digitizationProfile, md5Comparison);

        assertNull(testable.getBitDepth());
    }

    @Test
    void getEntityTypeOrderInt_worksAsExpected(){
        DigitalObjectFieldData testable = new DigitalObjectFieldData();

        testable.setEntityTypeOrder(null);
        assertNull(testable.getEntityTypeOrderInt());

        testable.setEntityTypeOrder("");
        assertNull(testable.getEntityTypeOrderInt());

        testable.setEntityTypeOrder("2");
        assertEquals(2, testable.getEntityTypeOrderInt());

    }


}