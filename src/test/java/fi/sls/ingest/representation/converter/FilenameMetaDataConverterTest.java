package fi.sls.ingest.representation.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import fi.sls.ingest.representation.file.FilenameMetaData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FilenameMetaDataConverterTest extends JsonStringConverterTestBase<FilenameMetaData> {

    @BeforeEach
    void setUp() {
        super.setUp();
        testable = new FilenameMetaDataConverter(objectMapper);
    }

    @Test
    void convertToDatabaseColumn_returnsJsonStringWhenCalledWithValues() throws JsonProcessingException {
        FilenameMetaData data = mock(FilenameMetaData.class);

        when(objectMapper.writeValueAsString(data)).thenReturn("datajson");

        String rtn = testable.convertToDatabaseColumn(data);

        assertEquals("datajson", rtn);
    }

    @Test
    void convertToDatabaseColumn_catchesExceptionFromObjectMapperAndReturnsNull() throws JsonProcessingException {
        FilenameMetaData data = mock(FilenameMetaData.class);
        when(objectMapper.writeValueAsString(data)).thenThrow(JsonProcessingException.class);

        String rtn = testable.convertToDatabaseColumn(data);

        assertNull(rtn);
    }

    @Test
    void convertToEntityAttribute_returnsListOfFlagsWhenCalledWithJson() throws IOException {
        String dataJson = "datajson";
        FilenameMetaData mock = mock(FilenameMetaData.class);

        when(objectMapper.readValue(dataJson, FilenameMetaData.class)).thenReturn(mock);

        FilenameMetaData data = testable.convertToEntityAttribute(dataJson);

        assertEquals(mock, data);
    }

    @Test
    void convertToEntityAttribute_catchesExceptionAndReturnsNull() throws IOException {
        String dataJson = "datajson";

        when(objectMapper.readValue(dataJson, FilenameMetaData.class)).thenThrow(JsonProcessingException.class);

        FilenameMetaData data = testable.convertToEntityAttribute(dataJson);

        assertNull(data);
    }
}