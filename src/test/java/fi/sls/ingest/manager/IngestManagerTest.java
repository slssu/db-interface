package fi.sls.ingest.manager;

import fi.sls.ingest.config.IngestConfig;
import fi.sls.ingest.manager.runnable.AccessFilesRunnable;
import fi.sls.ingest.manager.runnable.IngestStagingRunnable;
import fi.sls.ingest.queue.rabbitmq.sender.IngestPostProcessingItemSender;
import fi.sls.ingest.queue.rabbitmq.sender.IngestPreProcessingItemSender;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.security.AuthenticationFacade;
import fi.sls.ingest.ws.SocketMessenger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.actuate.metrics.MetricsEndpoint;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

class IngestManagerTest {

    IngestManager testable;

    IngestItemRepository ingestItemRepository;
    ProcessingStatusTransitionHandler processingStatusTransitionHandler;
    SocketMessenger socketMessenger;

    @BeforeEach
    void setUp() {

        AuthenticationFacade authenticationFacade = mock(AuthenticationFacade.class);
        IngestConfig ingestConfig = mock(IngestConfig.class);
        IngestPreProcessingItemSender ingestPreProcessingItemSender = mock(IngestPreProcessingItemSender.class);
        IngestPostProcessingItemSender ingestPostProcessingItemSender = mock(IngestPostProcessingItemSender.class);
        ingestItemRepository = mock(IngestItemRepository.class);
        socketMessenger = mock(SocketMessenger.class);
        AccessFilesRunnable accessFilesRunnable = mock(AccessFilesRunnable.class);
        IngestStagingRunnable ingestStagingRunnable = mock(IngestStagingRunnable.class);
        processingStatusTransitionHandler = mock(ProcessingStatusTransitionHandler.class);
        MetricsEndpoint metricsEndpoint = mock(MetricsEndpoint.class);
        IngestFileRenamer ingestFileRenamer = mock(IngestFileRenamer.class);

        testable = new IngestManager(
                authenticationFacade,
                ingestConfig,
                ingestPreProcessingItemSender,
                ingestPostProcessingItemSender,
                ingestItemRepository,
                socketMessenger,
                accessFilesRunnable,
                ingestStagingRunnable,
                processingStatusTransitionHandler,
                metricsEndpoint,
                ingestFileRenamer
            );
    }

    @Test
    void getProcessingItems_1() {
        Pageable p = mock(Pageable.class);
        Page<IngestProcessingItem> items = mock(Page.class);
        when(ingestItemRepository.findByProcessingStatusNot(ProcessingStatus.ARCHIVED, p)).thenReturn(items);

        assertEquals(items, testable.getProcessingItems(p));
        verify(ingestItemRepository).findByProcessingStatusNot(eq(ProcessingStatus.ARCHIVED), eq(p));
    }

    @Test
    void getProcessingItems_2_ReturnsAllIfFilterEmpty() {
        Pageable p = mock(Pageable.class);
        Page<IngestProcessingItem> items = mock(Page.class);
        when(ingestItemRepository.findByProcessingStatusNot(ProcessingStatus.ARCHIVED, p)).thenReturn(items);

        assertEquals(items, testable.getProcessingItems("", p));
        verify(ingestItemRepository).findByProcessingStatusNot(eq(ProcessingStatus.ARCHIVED), eq(p));
    }

    @Test
    void getProcessingItems_2_ReturnsFilteredIfSet() {
        Pageable p = mock(Pageable.class);
        Page<IngestProcessingItem> items = mock(Page.class);
        when(ingestItemRepository.findByProcessingStatusIn(Arrays.asList(ProcessingStatus.STAGING), p)).thenReturn(items);

        assertEquals(items, testable.getProcessingItems(ProcessingStatus.STAGING.toString(), p));
        verify(ingestItemRepository, times(0)).findByProcessingStatusNot(eq(ProcessingStatus.ARCHIVED), eq(p));
        verify(ingestItemRepository, times(1)).findByProcessingStatusIn(eq(Arrays.asList(ProcessingStatus.STAGING)), eq(p));
    }

    @Test
    void getProcessingItems_3_ReturnsAllIfFilterAndUserListEmpty() {
        Pageable p = mock(Pageable.class);
        Page<IngestProcessingItem> items = mock(Page.class);
        when(ingestItemRepository.findByProcessingStatusNot(ProcessingStatus.ARCHIVED, p)).thenReturn(items);

        assertEquals(items, testable.getProcessingItems("", Arrays.asList(), p));
        verify(ingestItemRepository).findByProcessingStatusNot(eq(ProcessingStatus.ARCHIVED), eq(p));
    }

    @Test
    void getProcessingItems_3_ReturnsFilteredIfFilterExistsAndUserListEmpty() {
        Pageable p = mock(Pageable.class);
        Page<IngestProcessingItem> items = mock(Page.class);
        when(ingestItemRepository.findByProcessingStatusIn(Arrays.asList(ProcessingStatus.STAGING), p)).thenReturn(items);

        assertEquals(items, testable.getProcessingItems(ProcessingStatus.STAGING.toString(), Arrays.asList(), p));
        verify(ingestItemRepository, times(0)).findByProcessingStatusNot(eq(ProcessingStatus.ARCHIVED), eq(p));
        verify(ingestItemRepository, times(1)).findByProcessingStatusIn(eq(Arrays.asList(ProcessingStatus.STAGING)), eq(p));
    }

    @Test
    void getProcessingItems_3_ReturnsUnarchivedForUserIfFilterNotExistsAndUserListExists() {
        Pageable p = mock(Pageable.class);
        Page<IngestProcessingItem> items = mock(Page.class);

        when(ingestItemRepository.findByProcessingStatusNotAndCreatedByIdIn(
                ProcessingStatus.ARCHIVED,
                Arrays.asList(2L),
                p
        )).thenReturn(items);

        assertEquals(items, testable.getProcessingItems("", Arrays.asList(2L), p));
        verify(ingestItemRepository, times(0)).findByProcessingStatusNot(eq(ProcessingStatus.ARCHIVED), eq(p));
        verify(ingestItemRepository, times(0)).findByProcessingStatusIn(eq(Arrays.asList(ProcessingStatus.STAGING)), eq(p));
        verify(ingestItemRepository, times(0)).findByProcessingStatusInAndCreatedByIdIn(
                eq(Arrays.asList(ProcessingStatus.STAGING)),
                eq(Arrays.asList(2L)),
                eq(p)
        );
        verify(ingestItemRepository, times(1)).findByProcessingStatusNotAndCreatedByIdIn(
                eq(ProcessingStatus.ARCHIVED),
                eq(Arrays.asList(2L)),
                eq(p)
        );

    }

    @Test
    void getProcessingItems_3_ReturnsFilteredForUserIfFilterExistsAndUserListExists() {
        Pageable p = mock(Pageable.class);
        Page<IngestProcessingItem> items = mock(Page.class);

        when(ingestItemRepository.findByProcessingStatusInAndCreatedByIdIn(
                Arrays.asList(ProcessingStatus.STAGING),
                Arrays.asList(2L),
                p
        )).thenReturn(items);

        assertEquals(items, testable.getProcessingItems(ProcessingStatus.STAGING.toString(), Arrays.asList(2L), p));
        verify(ingestItemRepository, times(0)).findByProcessingStatusNot(eq(ProcessingStatus.ARCHIVED), eq(p));
        verify(ingestItemRepository, times(0)).findByProcessingStatusIn(eq(Arrays.asList(ProcessingStatus.STAGING)), eq(p));
        verify(ingestItemRepository, times(1)).findByProcessingStatusInAndCreatedByIdIn(
                eq(Arrays.asList(ProcessingStatus.STAGING)),
                eq(Arrays.asList(2L)),
                eq(p)
        );

    }

    @Test
    void getProcessingItem() {
        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(ingestItemRepository.findByToken("mytoken")).thenReturn(Optional.of(item));

        Optional<IngestProcessingItem> rtn = testable.getProcessingItem("mytoken");

        assertTrue(rtn.isPresent());
        assertEquals(item, rtn.get());
    }


    @Test
    void changeProcessingItemStatus_returnsFalseIfItemNotFound() {
        when(ingestItemRepository.findByToken("ABC123")).thenReturn(Optional.empty());
        assertFalse(testable.changeProcessingItemStatus("ABC123", ProcessingStatus.STAGING));
    }

    @Test
    void changeProcessingItemStatus_returnsTrueIfSuccess() {
        IngestProcessingItem item = mock(IngestProcessingItem.class);
        IngestItem iItem = mock(IngestItem.class);
        when(item.getItem()).thenReturn(iItem);
        when(ingestItemRepository.findByToken("ABC123")).thenReturn(Optional.of(item));

        assertTrue(testable.changeProcessingItemStatus("ABC123", ProcessingStatus.STAGING));
        verify(processingStatusTransitionHandler, times(1)).transitionToState(item, ProcessingStatus.STAGING);
        verify(ingestItemRepository, times(1)).save(item);
        verify(socketMessenger, times(1)).broadcast(item);
    }

    @Test
    void changeProcessingItemStatus_2_callsForEachToken() {
        List<String> t = Arrays.asList("ABC123", "DEF456");
        ArrayList<String> tokens = new ArrayList<>(t);

        testable.changeProcessingItemStatus(tokens, ProcessingStatus.STAGING);

        verify(ingestItemRepository, times(1)).findByToken("ABC123");
        verify(ingestItemRepository, times(1)).findByToken("DEF456");
    }

    @Test
    void changeProcessingItemStatus_3_callsForEachTokenIfEmptyUserList() {
        IngestProcessingItem t1 = getMockProcessingItem("ABC123");
        IngestProcessingItem t2 = getMockProcessingItem("DEF456");

        Page<IngestProcessingItem> tokens = new PageImpl<>(Arrays.asList(t1, t2));

        when(ingestItemRepository.findByProcessingStatusIn(any(List.class), any(Pageable.class))).thenReturn(tokens);

        testable.changeProcessingItemStatus(ProcessingStatus.COMPLETE, ProcessingStatus.ARCHIVED, Arrays.asList());
        verify(ingestItemRepository, times(1)).findByProcessingStatusIn(eq(Arrays.asList(ProcessingStatus.COMPLETE)), any(Pageable.class));

        verify(ingestItemRepository, times(1)).findByToken("ABC123");
        verify(ingestItemRepository, times(1)).findByToken("DEF456");
    }

    @Test
    void changeProcessingItemStatus_3_callsForEachTokenWithUserList() {
        IngestProcessingItem t1 = getMockProcessingItem("ABC123");
        IngestProcessingItem t2 = getMockProcessingItem("DEF456");

        Page<IngestProcessingItem> tokens = new PageImpl<>(Arrays.asList(t1, t2));

        when(ingestItemRepository.findByProcessingStatusInAndCreatedByIdIn(any(List.class), any(List.class), any(Pageable.class))).thenReturn(tokens);

        testable.changeProcessingItemStatus(ProcessingStatus.COMPLETE, ProcessingStatus.ARCHIVED, Arrays.asList(123L));
        verify(ingestItemRepository, times(1)).findByProcessingStatusInAndCreatedByIdIn(eq(Arrays.asList(ProcessingStatus.COMPLETE)), eq(Arrays.asList(123L)), any(Pageable.class));

        verify(ingestItemRepository, times(1)).findByToken("ABC123");
        verify(ingestItemRepository, times(1)).findByToken("DEF456");
    }

    @Test
    void getNumStatusChangeableProcessingItems_noUserIds() {
        when(ingestItemRepository.countByProcessingStatus(ProcessingStatus.COMPLETE)).thenReturn(2L);
        long rtn = testable.getNumStatusChangeableProcessingItems(ProcessingStatus.COMPLETE, ProcessingStatus.ARCHIVED, Arrays.asList());
        verify(processingStatusTransitionHandler, times(1)).isStateTransitionAllowed(ProcessingStatus.COMPLETE, ProcessingStatus.ARCHIVED);

        assertEquals(2, rtn);
    }
    @Test
    void getNumStatusChangeableProcessingItems_withUserIds() {
        when(ingestItemRepository.countByProcessingStatusAndUser(ProcessingStatus.COMPLETE, Arrays.asList(123L))).thenReturn(2L);
        long rtn = testable.getNumStatusChangeableProcessingItems(ProcessingStatus.COMPLETE, ProcessingStatus.ARCHIVED, Arrays.asList(123L));
        verify(processingStatusTransitionHandler, times(1)).isStateTransitionAllowed(ProcessingStatus.COMPLETE, ProcessingStatus.ARCHIVED);

        assertEquals(2, rtn);
    }

    @Test
    void deleteProcessingItem_returnsFalseIfItemNotFound() {
        when(ingestItemRepository.findByToken("ABC123")).thenReturn(Optional.empty());
        assertFalse(testable.deleteProcessingItem("ABC123"));
    }

    @Test
    void deleteProcessingItem_returnsTrueIfSuccess() {
        IngestProcessingItem item = mock(IngestProcessingItem.class);
        IngestItem iItem = mock(IngestItem.class);
        when(item.getItem()).thenReturn(iItem);
        when(ingestItemRepository.findByToken("ABC123")).thenReturn(Optional.of(item));

        assertTrue(testable.deleteProcessingItem("ABC123"));
        verify(processingStatusTransitionHandler, times(1)).transitionToState(item, ProcessingStatus.DELETED);
        verify(ingestItemRepository, times(1)).delete(item);
        verify(socketMessenger, times(1)).broadcast(item);
    }

    @Test
    void deleteProcessingItems_callsForEachToken() {
        List<String> t = Arrays.asList("ABC123", "DEF456");
        ArrayList<String> tokens = new ArrayList<>(t);

        testable.deleteProcessingItems(tokens);

        verify(ingestItemRepository, times(1)).findByToken("ABC123");
        verify(ingestItemRepository, times(1)).findByToken("DEF456");
    }


    protected IngestProcessingItem getMockProcessingItem(String token) {
        IngestProcessingItem t = mock(IngestProcessingItem.class);
        IngestItem i = mock(IngestItem.class);
        when(t.getItem()).thenReturn(i);
        when(t.getToken()).thenReturn(token);

        return t;

    }

}