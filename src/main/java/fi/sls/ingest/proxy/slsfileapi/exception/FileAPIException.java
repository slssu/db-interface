package fi.sls.ingest.proxy.slsfileapi.exception;

/**
 * Exception thrown if the SLS File API returns an error for a processable item.
 */
public class FileAPIException extends RuntimeException {
    public FileAPIException(String exception){
        super(exception);
    }
}
