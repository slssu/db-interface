package fi.sls.ingest.migration.report;

import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.migration.DigitalObjectMigrationResult;
import fi.sls.ingest.migration.MigrationResult;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.nio.channels.AcceptPendingException;

/**
 * Wraps a MigrationResult to output Xlsx formatted version of the result
 */
public class XLSXReport extends XLSXReportBase {


    private static String[] columns = {
            "intellectualEntities::nummer",
            "intellectualEntities::entity_identifier",
            "intellectualEntities::collection_order",
            "semlaEvent::ie_nummer",
            "semlaEvent::digitalObjects_nr",
            "digitalObjects::identifier",
            "digitalObjects::dc_identifier (original)",
            "digitalObjects::dc_identifier (migrated)",
            "accessFiles::filePaths (original)",
            "accessFiles::filePaths (migrated)",
            "migrering lyckas",
            "felmeddelande"
    };

    public XLSXReport(MigrationResult result){
        super(result);
    }

    @Override
    public Workbook generate(){
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Processerade filer");

        // row position that will be written to next
        int rowPos = 0;

        rowPos = createParametersInfoBlock(sheet, rowPos);

        // add some padding between info block and rows
        rowPos += 2;

        // Create header row
        Row headerRow = sheet.createRow(rowPos++);
        for(int i = 0; i < columns.length; i++) {
            Cell headerCell = headerRow.createCell(i);
            headerCell.setCellStyle(getHeaderStyle(workbook));
            headerCell.setCellValue(columns[i]);
        }

        // populate with item results
        for(DigitalObjectMigrationResult itemResult : result.getItemResults()) {

            Row row = sheet.createRow(rowPos++);

            createCellWithValue(row, 0, itemResult.getIntellectualEntityFieldData().getEntityNumber().toString(), IndexedColors.WHITE.getIndex());
            createCellWithValue(row, 1, itemResult.getIntellectualEntityFieldData().getEntityIdentifier(), IndexedColors.WHITE.getIndex());

            if (itemResult.getIntellectualEntityFieldData().getCollectionOrder() != null && itemResult.getIntellectualEntityFieldData().getCollectionOrder() != 0) {
                createCellWithValue(row, 2, itemResult.getIntellectualEntityFieldData().getCollectionOrder().toString(), IndexedColors.LIGHT_GREEN.getIndex());
            } else {
                createCellWithValue(row, 2, "-", IndexedColors.LIGHT_ORANGE.getIndex());
            }

            createCellWithValue(row, 3, itemResult.getSemlaEventFieldData().getIeNumber(), IndexedColors.WHITE.getIndex());
            if (itemResult.getSemlaEventFieldData().getDigitalObjectsNumber() == null) {
                createCellWithValue(row, 4, itemResult.getSemlaEventFieldData().getDigitalObjectsNr(), IndexedColors.LIGHT_ORANGE.getIndex());
            } else {
                createCellWithValue(row, 4, itemResult.getSemlaEventFieldData().getDigitalObjectsNr(), IndexedColors.WHITE.getIndex());
            }

            if (itemResult.getOriginal() != null && itemResult.getMigrated() != null) {
                createCellWithValue(row, 5, itemResult.getOriginal().getFieldData().getIdentifier(), IndexedColors.WHITE.getIndex());
                createCellWithValue(row, 6, itemResult.getOriginal().getFieldData().getDcIdentifier(), IndexedColors.WHITE.getIndex());
                createCellWithValue(row, 7, itemResult.getMigrated().getFieldData().getDcIdentifier(), IndexedColors.LIGHT_GREEN.getIndex());

                // list changed paths of access files in single cell
                String originalAccessFiles = "";
                for(AccessFile f : itemResult.getOriginalAccessFiles()){
                    originalAccessFiles = originalAccessFiles.concat(f.getPath()).concat(",\n");
                }
                createCellWithValue(row, 8, originalAccessFiles, IndexedColors.WHITE.getIndex(), true);

                String migratedAccessFiles = "";
                for(AccessFile f : itemResult.getMigratedAccessFiles()){
                    migratedAccessFiles = migratedAccessFiles.concat(f.getPath()).concat(",\n");
                }
                createCellWithValue(row, 9, migratedAccessFiles, IndexedColors.LIGHT_GREEN.getIndex(), true);
            }

            if (itemResult.getProcessingStatus().equals(ProcessingStatus.COMPLETE)) {
                createCellWithValue(row, 10, "Ja", IndexedColors.LIGHT_GREEN.getIndex());
            } else if (itemResult.getProcessingStatus().equals(ProcessingStatus.DRY_RUN_MIGRATION_FILE_API)) {
                createCellWithValue(row, 10, "OK fram till File-API anrop", IndexedColors.LIGHT_YELLOW.getIndex());
            } else if (itemResult.getProcessingStatus().equals(ProcessingStatus.MIGRATION_REJECTED)) {
                createCellWithValue(row, 10, "Avvisad", IndexedColors.PALE_BLUE.getIndex());
                createCellWithValue(row, 11, itemResult.getError(), IndexedColors.PALE_BLUE.getIndex());
            } else {
                createCellWithValue(row, 10, "Nej", IndexedColors.LIGHT_ORANGE.getIndex());
                createCellWithValue(row, 11, itemResult.getError(), IndexedColors.LIGHT_ORANGE.getIndex());
            }
        }


        resizeColumns(columns.length, sheet);


        return workbook;
    }
}
