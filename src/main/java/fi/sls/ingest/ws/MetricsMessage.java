package fi.sls.ingest.ws;

import fi.sls.ingest.actuator.metrics.IngestProcessingStatusCount;
import lombok.Data;

import java.util.List;

@Data
public class MetricsMessage {
    List<IngestProcessingStatusCount> statusCounts;
}
