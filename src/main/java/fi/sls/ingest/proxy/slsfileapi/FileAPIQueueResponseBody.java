package fi.sls.ingest.proxy.slsfileapi;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Represents the response structure for when a file is sent to the SLS File API processing queue.
 */
@Data
public class FileAPIQueueResponseBody {

    /**
     * A response message indicating what the remote endpoint did with the request
     *
     * e.g. "received"
     */
    @NotBlank
    String msg;
}
