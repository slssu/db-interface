package fi.sls.ingest.fits.model.tool;

import fi.sls.ingest.fits.model.generated.Fits;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Wrapper class that provides easier access to the result structure that the Apache Tika
 * tool produces for the fields that we are interested in.
 *
 */
public class FitsTikaResult extends FitsToolResultBase {

    public FitsTikaResult(Fits generatedResult) {
        super(generatedResult, "Tika");
    }

    /**
     * Return the dcterms:created metadata field from the Tika xml result
     *
     * @see <a href="http://dublincore.org/documents/dces/">http://dublincore.org/documents/dces/</a>
     *
     * @return
     */
    public String getDateCreated(){
        return getFieldNodeValueByNameAttr("dcterms:created");
    }

    /**
     * Return the dcterms:modified metadata field from the Tika xml result
     *
     * @see <a href="http://dublincore.org/documents/dces/">http://dublincore.org/documents/dces/</a>
     *
     * @return
     */
    public String getDateModified(){
        return getFieldNodeValueByNameAttr("dcterms:modified");
    }


    /**
     * Returns the tiff:XResolution metadata field from the Tika xml result
     *
     * @return
     */
    public Integer getXResolution(){
        try {
            Float f = Float.parseFloat(getFieldNodeValueByNameAttr("tiff:XResolution"));
            return f.intValue();
        } catch (NumberFormatException | NullPointerException e){
            return null;
        }
    }

    /**
     * This is a combination of two fields from the Tika result: Make and Model
     * @return
     */
    public String getOriginalCaptureDevice(){
        String make = getFieldNodeValueByNameAttr("Make");
        String model = getFieldNodeValueByNameAttr("Model");

        // not beautiful, but should work
        if(make != null && model != null){
            return make.concat(" ").concat(model);
        } else if(make != null){
            return make;
        } else {
            return model;
        }
    }

    protected String getFieldNodeValueByNameAttr(String name){
        if(hasToolResult() && toolResult.getElementsByTagName("field").getLength() > 0){
            NodeList fields = toolResult.getElementsByTagName("field");
            for(int i = 0; i < fields.getLength(); i++){
                Node field = fields.item(i);
                NamedNodeMap attributes = field.getAttributes();
                String textVal = attributes.getNamedItem("name").getTextContent();
                if(textVal.equalsIgnoreCase(name)) {
                    return field.getFirstChild().getTextContent();
                }
            }
        }
        return null;
    }
}
