package fi.sls.ingest.representation.converter;

import javax.persistence.AttributeConverter;
import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Compresses the input string using gzip before storing to database.
 *
 * This was introduced because the string data amount returned from the FITS stack analysis often
 * closes in on being a 1MB XML structure, and since it is only stored in order to be transferred to the
 * next processing phase after the File API has processed files, it has not need to be searchable or indexable
 * within the database. Thus, compressing it to a binary format saves a lot of space on the database server
 * and network traffic.
 */
public class StringCompressionConverter implements AttributeConverter<String, byte[]> {

    @Override
    public byte[] convertToDatabaseColumn(String value) {

        if (value == null || value.length() == 0) {
            return null;
        }

        ByteArrayOutputStream str = new ByteArrayOutputStream();
        try {
            GZIPOutputStream gzip = new GZIPOutputStream(str);
            gzip.write(value.getBytes("UTF-8"));
            gzip.flush();
            gzip.close();
            return str.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String convertToEntityAttribute(byte[] value) {
        if (value == null) {
            return null;
        }

        final StringBuilder outStr = new StringBuilder();

        ByteArrayInputStream str = new ByteArrayInputStream(value);

        try {
            BufferedReader bf = null;
            if(isCompressed(value)) {
                GZIPInputStream gis = new GZIPInputStream(str);
                bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
            } else {
                bf = new BufferedReader(new InputStreamReader(str, "UTF-8"));
            }

            String line;
            while ((line = bf.readLine()) != null) {
                outStr.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return outStr.toString();

    }

    public static boolean isCompressed(final byte[] compressed) {
        return (compressed[0] == (byte) (GZIPInputStream.GZIP_MAGIC)) && (compressed[1] == (byte) (GZIPInputStream.GZIP_MAGIC >> 8));
    }
}
