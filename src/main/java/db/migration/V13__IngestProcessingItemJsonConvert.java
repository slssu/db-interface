package db.migration;

import org.flywaydb.core.api.migration.Context;

public class V13__IngestProcessingItemJsonConvert extends V12__IngestProcessingItemJsonConvert {
    @Override
    public void migrate(Context context) throws Exception {

        int offset = 1 * 39900;
        int pageSize = 20;
        int maxRows = 2 * 40000;

        runMigrationWithParams(context, pageSize, offset, maxRows);
    }
}
