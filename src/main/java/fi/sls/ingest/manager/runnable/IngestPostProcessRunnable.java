package fi.sls.ingest.manager.runnable;

import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.fits.exception.FitsResultException;
import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.generated.Fits;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.manager.IngestItem;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.IngestProcessingItemFlag;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.manager.step.*;
import fi.sls.ingest.proxy.filemaker.exception.FileMakerAPIException;
import fi.sls.ingest.proxy.filemaker.response.record.*;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import fi.sls.ingest.proxy.slsfileapi.exception.FileAPIException;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.ws.SocketMessenger;
import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.StringReader;
import java.time.Clock;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicReference;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * Accepts an IngestProcessingItem and extracts metadata from the Fits Result and filename, then stores
 * the data to FileMaker.
 *
 * If the FileMaker API is ever replaced, this file will probably need to be changed
 *
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class IngestPostProcessRunnable extends IngestRunnableBase {

    /**
     * XML processor to process the FITS xml to an object
     */
    private Jaxb2Marshaller jaxb2Marshaller;

    /**
     * XML XPath processor for accessing field values deep in the XML result structure
     */
    private DocumentBuilder documentBuilder;

    private IngestCollectionStep ingestCollectionStep;
    private IngestIntellectualEntityStep ingestIntellectualEntityStep;
    private LinkCollectionIntellectualEntityStep linkCollectionIntellectualEntityStep;
    private IngestDigitalObjectStep ingestDigitalObjectStep;
    private LinkSemlaEventStep linkSemlaEventStep;
    private IngestIntellectualEntityPartStep ingestIntellectualEntityPartStep;
    private IngestDerivativeObjectStep ingestDerivativeObjectStep;
    private LinkDerivativeObjectDigitalObjectStep linkDerivativeObjectDigitalObjectStep;
    private LinkAgentStep linkAgentStep;
    private LinkPremisEventDigitalObjectStep linkPremisEventDigitalObjectStep;
    private IngestPremiseEventStep ingestPremiseEventStep;
    private LinkThumbnailStep linkThumbnailStep;

    /**
     * Clock instance to use for timestamps of events (injected for testability)
     */
    private Clock clock;

    @Autowired
    public IngestPostProcessRunnable(IngestItemRepository ingestItemRepository,
                                     SocketMessenger socketMessenger,
                                     Jaxb2Marshaller jaxb2Marshaller,
                                     DocumentBuilder documentBuilder,
                                     IngestCollectionStep ingestCollectionStep,
                                     IngestIntellectualEntityStep ingestIntellectualEntityStep,
                                     LinkCollectionIntellectualEntityStep linkCollectionIntellectualEntityStep,
                                     IngestDigitalObjectStep ingestDigitalObjectStep,
                                     LinkSemlaEventStep linkSemlaEventStep,
                                     IngestIntellectualEntityPartStep ingestIntellectualEntityPartStep,
                                     IngestDerivativeObjectStep ingestDerivativeObjectStep,
                                     LinkDerivativeObjectDigitalObjectStep linkDerivativeObjectDigitalObjectStep,
                                     LinkAgentStep linkAgentStep,
                                     LinkPremisEventDigitalObjectStep linkPremisEventDigitalObjectStep,
                                     IngestPremiseEventStep ingestPremiseEventStep,
                                     LinkThumbnailStep linkThumbnailStep,
                                     Clock clock
    )
    {
        super(ingestItemRepository, socketMessenger);
        this.clock = clock;
        this.jaxb2Marshaller = jaxb2Marshaller;
        this.documentBuilder = documentBuilder;

        this.ingestIntellectualEntityStep = ingestIntellectualEntityStep;
        this.ingestCollectionStep = ingestCollectionStep;
        this.linkCollectionIntellectualEntityStep = linkCollectionIntellectualEntityStep;
        this.ingestDigitalObjectStep = ingestDigitalObjectStep;
        this.linkSemlaEventStep = linkSemlaEventStep;
        this.ingestIntellectualEntityPartStep = ingestIntellectualEntityPartStep;
        this.ingestDerivativeObjectStep = ingestDerivativeObjectStep;
        this.linkDerivativeObjectDigitalObjectStep = linkDerivativeObjectDigitalObjectStep;
        this.linkAgentStep = linkAgentStep;
        this.linkPremisEventDigitalObjectStep = linkPremisEventDigitalObjectStep;
        this.ingestPremiseEventStep = ingestPremiseEventStep;
        this.linkThumbnailStep = linkThumbnailStep;
    }

    @Timed("ingestapi.runnable.postProcessing")
    public void run(IngestProcessingItem item) {

        MDC.put(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken());

        log.info("start ingest processing");

        try {
            Fits fits = (Fits) jaxb2Marshaller.unmarshal(new StreamSource(new StringReader(item.getRawFitsResult())));
            String rawFitsResult = item.getRawFitsResult();

            InputSource inputXml = new InputSource(new StringReader(rawFitsResult));
            Document xmlDocument = documentBuilder.parse(inputXml);

            FitsResult fitsResult = FitsResult.createFromFits(fits, rawFitsResult, xmlDocument);

            saveArchiveData(item, item.getFilenameMetaData(), fitsResult);
        } catch (IOException | SAXException e) {
            log.error("Failed to post process item: {} {}",
                    kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                    kv("error_message", e.getMessage()),
                    kv("exception", e)
            );
            item.setMessage(e.getMessage());
            setProcessingItemStatusAndSend(ProcessingStatus.ERROR, item);
        } catch (FileMakerAPIException e){
            log.error("Failed to post process item: {} {} {}",
                    kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                    kv("error_code", e.getCode()),
                    kv("error_message", e.getMessage()),
                    kv("exception", e)
            );
            item.setMessage(e.getMessage() + ":" +e.getCode());
            setProcessingItemStatusAndSend(ProcessingStatus.ERROR, item );
        }finally {
            MDC.clear();
        }
    }

    /**
     * Processes the metadata extracted from the filename and the FITS service and saves to FileMaker
     */
    protected void saveArchiveData(IngestProcessingItem item, FilenameMetaData metaData, FitsResult fitsResultData) throws IOException {

        log.info("Post-processing item: {}",
                kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
        );

        try {
            // clear any previous errors before running this item
            item.resetErrorFlags();

            if (!item.md5ChecksumMatch()) {
                // checksums don't match, don't attempt ingest instead reject and cancel
                log.warn("MD5 Checksums calculated in Ingest and File API do not match: {} {}",
                        kv("md5_checksum_ingest", item.getMd5ChecksumBefore()),
                        kv("md5_checksum_file_api", item.getMd5ChecksumAfter()),
                        kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
                );
                item.setMessage(String.format("MD5 Checksums calculated in Ingest (%s) and File API (%s) do not match", item.getMd5ChecksumBefore(), item.getMd5ChecksumAfter()));
                setProcessingItemStatusAndSend(ProcessingStatus.REJECTED, item);
                return;
            } else {
                setProcessingItemStatusAndSend(ProcessingStatus.CREATE_DATABASE_OBJECTS, item);
            }

            // create entry in samlingar (can exist from before)
            Collection collection = ingestCollectionStep.executeStep(metaData);
            // create entry in intellectualEntities (can exist from before)
            IntellectualEntity intellectualEntity = ingestIntellectualEntityStep.executeStep(metaData, fitsResultData);
            // link collection with intellectualEntity through mt_samlingar_IE
            linkCollectionIntellectualEntityStep.executeStep(collection.getFieldData(), intellectualEntity.getFieldData());

            // create entry in teknisk_data
            DigitalObject digitalObject = ingestDigitalObjectStep.executeStep(metaData, fitsResultData, item);

            // connect digitalObject and intellectualEntity through SemlaEvents
            SemlaEvent semlaEvent = linkSemlaEventStep.executeStep(digitalObject, intellectualEntity);

            // link accessfiles
            linkAccessFiles(item, digitalObject, intellectualEntity, metaData);

            // link intellectualEntityParts
            IntellectualEntityPart intellectualEntityPart = ingestIntellectualEntityPartStep.executeStep(
                    semlaEvent,
                    digitalObject,
                    item.getCustomerCopyFilePath()
            );

            // copy thumbnail display values back to intellectualEntities
            // This is a bit odd, because this will mean the default view will always be the last
            // item ingested, which is not necessarily the first. For photos this will probably work, but letters...?
            linkThumbnailStep.executeStep(intellectualEntity, intellectualEntityPart.getFieldData());

            // Create event for ingestion
            this.linkPremisEvent(digitalObject, MTPremiseEventDOType.CREATED, PremisEventType.CREATION, digitalObject.getFieldData().getCalculatedDateCreated(), "Det digitala objektet skapades.");
            this.linkPremisEvent(digitalObject, MTPremiseEventDOType.INGESTION, PremisEventType.INGESTION, LocalDate.now(clock).format(DateTimeFormatter.ofPattern("MM/dd/yyyy")), "Filerna är införda i databasen.");
            this.linkPremisEvent(digitalObject, MTPremiseEventDOType.DIGEST_CALCULATION, PremisEventType.DIGEST_CALCULATION, LocalDate.now(clock).format(DateTimeFormatter.ofPattern("MM/dd/yyyy")), "MD5 summan kalkylerad.");

            // mark ingest process as success
            item.resetErrorFlags();
            setProcessingItemStatusAndSend(ProcessingStatus.COMPLETE, item);
        } catch (Exception e) {
            item.setMessage(e.getMessage());
            setProcessingItemStatusAndSend(ProcessingStatus.RETRY, item);
            log.warn("Error while processing item: {} {}",
                    kv("reject_reason", e.getMessage()),
                    kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
            );

            // re-throw so queue gets notified as well
            throw new IngestProcessingException(e);
        }
    }

    /**
     * Helper for linking the access files to the digital object and intellectual entity
     *
     * @param digitalObject
     * @param intellectualEntity
     * @param metaData
     */
    protected void linkAccessFiles(IngestProcessingItem item, DigitalObject digitalObject, IntellectualEntity intellectualEntity, FilenameMetaData metaData) throws IOException {

        AtomicReference<IntellectualEntity> wrappedIntellectualEntity = new AtomicReference<>();
        wrappedIntellectualEntity.set(intellectualEntity);

        //item.getFileAPIResponse().getAccessFiles().forEach((AccessFile file) -> {
        for(AccessFile file : item.getFileAPIResponse().getAccessFiles()) {
        //item.getFileAPIResponse().getAccessfilePaths().forEach((String filePath) -> {

            //DerivateObject derivateObject = ingestDerivativeObjectStep.executeStep(filePath, metaData, intellectualEntity);
            DerivateObject derivateObject = ingestDerivativeObjectStep.executeStep(file, metaData, intellectualEntity);

            // link DerivativeObject (anvandarkopior) to DigitalObject (teknisk_data) through mt_derivativeObjects_DO
            linkDerivativeObjectDigitalObjectStep.executeStep(derivateObject, digitalObject);

        }
    }

    protected void linkPremisEvent(DigitalObject digitalObject,
                                   MTPremiseEventDOType mtPremisEventDOType,
                                   PremisEventType premisEventType,
                                   String eventDate,
                                   String eventOutcomeNote) throws IOException
    {

        // link PremiseEvent to DigitalObject
        MtPremiseEventsDO mtPremiseEventsDO = linkPremisEventDigitalObjectStep.executeStep(digitalObject, mtPremisEventDOType, eventDate);
        // Create event for the link
        PremiseEvent premiseEvent = ingestPremiseEventStep.executeStep(digitalObject, mtPremiseEventsDO, mtPremisEventDOType, premisEventType, eventDate, eventOutcomeNote);
        // Link the API Agent to the event
        Agent existingAgent = linkAgentStep.executeStep(premiseEvent);
    }

}
