package fi.sls.ingest.proxy.filemaker.response.record;

import fi.sls.ingest.manager.IngestDigitizationProfile;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import lombok.Data;

/**
 * Entity representing API responses for values in the "digitizationProfiles" table/view in FileMaker.
 */
@Data
public class DigitizationProfile extends FileMakerResponseEntity {
    /**
     * The name of the FileMaker layout or table to target
     */
    public static final String LAYOUT_NAME = "ingestapi_digitizationProfiles";


    DigitizationProfileFieldData fieldData;

    public DigitizationProfile(){
        fieldData = new DigitizationProfileFieldData();
    }


    /**
     * Returns a copy of this profile as a IngestDigitizationProfile instance, for use in the rest of the API.
     *
     * This is done for abstraction, so we minimize the dependency on FileMaker specific structures as we know
     * the goal is to get rid of it eventually.
     *
     * @return A new IngestDigitizationProfile instance based on field data in this instance.
     */
    public IngestDigitizationProfile createIngestDigitizationProfile(){
        IngestDigitizationProfile profile = new IngestDigitizationProfile();

        profile.setId(this.getRecordId());
        profile.setProfileName(getFieldData().getProfileId());

        return profile;
    }

    @Override
    public String getLayoutName() {
        return LAYOUT_NAME;
    }

    @Override
    public FindQueryRequest createFindQueryRequest() {
        return null;
    }

    @Override
    public UpdateRequest createUpdateRequest() {
        return null;
    }

    @Override
    public CreateRequest createRecordRequest() {
        return null;
    }
}
