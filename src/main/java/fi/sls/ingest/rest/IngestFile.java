package fi.sls.ingest.rest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.manager.IngestItem;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Wrapper class for transporting information about files over REST endpoints
 */
@Data
public class IngestFile implements IngestItem {

    private static final long serialVersionUID = -1795242447839175467L;

    @NotEmpty
    @NotNull
    protected String path;

    @NotEmpty
    protected String pathHash;

    @NotEmpty
    @NotNull
    protected String fitsPath;

    @NotEmpty
    @NotNull
    protected String name;

    @NotNull
    protected Boolean isDirectory;

    // File size in bytes
    protected Long size;


    public static IngestFile fromFile(File f, String inboxPath){
        IngestFile rtn = new IngestFile();
        rtn.setPath(f.getAbsolutePath());
        rtn.setPathHash(calculateHash(f));

        rtn.setFitsPath(f.getAbsolutePath().replace(inboxPath, ""));
        rtn.setName(f.getName());
        rtn.setIsDirectory(f.isDirectory());
        rtn.setSize(f.length());
        return rtn;
    }

    public static String calculateHash(File f){
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(f.getAbsolutePath().getBytes("UTF-8"));
            return DatatypeConverter.printHexBinary(md.digest()).toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public File getFileCopy() {
        return new File(getPath());
    }

    public void setFileCopy(String path){
        // noop since getFileCopy returns a File object,
        // here to allow conversion from serialized json during migration V13-V14
    }
}
