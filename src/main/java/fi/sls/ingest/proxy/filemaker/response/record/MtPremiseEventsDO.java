package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Entity representing API responses for values in the "mt_premisEvent_DO" table/view in FileMaker.
 *
 * This table acts as a link table between premisEvents and digitalObjects tables
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MtPremiseEventsDO extends FileMakerResponseEntity {

    @JsonIgnore
    @Getter
    private String layoutName = "ingestapi_mt_premisEvents_DO";

    MTPremiseEventsDOFieldData fieldData;

    public MtPremiseEventsDO(){
        this.fieldData = new MTPremiseEventsDOFieldData();
    }

    @Override
    public FindQueryRequest createFindQueryRequest() {
        FindQueryRequest queryRequest = new FindQueryRequest("object_nummer", "="+getFieldData().getObjectNumber());
        queryRequest.addToExistingQueryItem(0, "event_ID", "="+getFieldData().getEventId());
        return queryRequest;
    }

    @Override
    public UpdateRequest createUpdateRequest() {
        return new UpdateRequest(getFieldData());
    }

    @Override
    public CreateRequest createRecordRequest() {
        return new CreateRequest(getFieldData());
    }
}
