package fi.sls.ingest.controller;

import fi.sls.ingest.ws.IngestStatusMessage;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.UUID;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Slf4j
@Component
public class WebSocketEventListener {

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        MDC.put("request_uuid", String.valueOf(UUID.randomUUID()));
        try {
            log.info("Received a new web socket connection");
        }finally {
            MDC.clear();
        }
    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        MDC.put("request_uuid", String.valueOf(UUID.randomUUID()));
        try {
            StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());

            String username = (String) headerAccessor.getSessionAttributes().get("username");
            if (username != null) {
                log.info("User Disconnected : {}", kv("user", username));

                IngestStatusMessage chatMessage = new IngestStatusMessage();
                chatMessage.setType(IngestStatusMessage.MessageType.LEAVE);
                chatMessage.setSender(username);

                messagingTemplate.convertAndSend("/topic/public", chatMessage);
            }
        } finally {
            MDC.clear();
        }
    }
}
