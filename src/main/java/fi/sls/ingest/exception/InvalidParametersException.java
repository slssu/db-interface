package fi.sls.ingest.exception;

import lombok.Getter;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.Iterator;

public class InvalidParametersException extends RuntimeException {

    @Getter
    BindingResult result;

    @Getter
    String parameter;


    public InvalidParametersException(String parameter, BindingResult result){
        super();

        this.parameter = parameter;
        this.result = result;
    }

    /**
     * @return A string containing all errors found on this validation
     */
    public String getFieldErrorMessage(){
        ArrayList<String> rtn = new ArrayList<>();

        Iterator<FieldError> i = this.result.getFieldErrors().iterator();

        while(i.hasNext()){
            FieldError err = i.next();
            rtn.add(err.getField() +" "+ err.getDefaultMessage());
        }
        return String.join(" and ", rtn);
    }


}
