package fi.sls.ingest.manager.step;

import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.CollectionFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntityFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.MTCollectionIntellectualEntity;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

/**
 * Provides implementation of the ingest processing steps necessary to link
 * a Collection with an IntellectualEntity.
 */
@Service
@Slf4j
public class LinkCollectionIntellectualEntityStep extends RecordServiceStep {

    @Autowired
    public LinkCollectionIntellectualEntityStep(RecordService service){
        super(service);
    }

    /**
     * Performs the tasks of this step
     *
     * @return The created or updated MTCollectionIntellectualEntity
     */
    public MTCollectionIntellectualEntity executeStep(CollectionFieldData collectionFieldData, IntellectualEntityFieldData intellectualEntityFieldData) throws IOException {
        MTCollectionIntellectualEntity link = new MTCollectionIntellectualEntity();
        link.getFieldData().setIntellectualEntityNumber(intellectualEntityFieldData.getEntityNumber());
        link.getFieldData().setCollectionNumber(collectionFieldData.getEntityNumber());

        Optional<? extends FileMakerResponseEntity> existingItem;
        // jw 2022-02-14: remove try-catch as runnable will handle all exceptions
//        try {
            existingItem = service.findRecordByIdentifier(link);
            if(existingItem.isPresent()){
                link = (MTCollectionIntellectualEntity) existingItem.get();
                log.info("Collection with number {} already linked to IntellectualEntity with number {}",
                        link.getFieldData().getCollectionNumber(),
                        link.getFieldData().getIntellectualEntityNumber()
                );

            } else {

                link = (MTCollectionIntellectualEntity) service.createObjectRecord(link);
                log.info("Created new link between Collection with number {} and IntellectualEntity with number {}",
                        link.getFieldData().getCollectionNumber(),
                        link.getFieldData().getIntellectualEntityNumber()
                );
            }

            return link;
//        } catch (IOException e) {
//            log.error(e.toString());
//            // FIXME should we throw instead?
//            return null;
//        }
    }
}
