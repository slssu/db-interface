package fi.sls.ingest.fits;

import fi.sls.ingest.fits.exception.FitsResultException;
import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.generated.Fits;
import fi.sls.ingest.validation.FitsResultValidator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.stream.StreamSource;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * Provides methods for accessing the FITS web service endpoint.
 *
 *
 */
@Service
@Slf4j
public class FitsService {

    public static final String FITS_OUTPUT_SUFFIX = ".fits.xml";

    private RestTemplate restTemplate;
    private Jaxb2Marshaller jaxb2Marshaller;
    private FitsResultValidator fitsResultValidator;

    private String apiURL;
    private String apiVideoURL;
    private String fitsProcessingPath;

    @Autowired
    public FitsService(
            RestTemplate restTemplate,
            Jaxb2Marshaller jaxb2Marshaller,
            DocumentBuilder documentBuilder,
            FitsResultValidator fitsResultValidator,
            @Value("${sls.fits.service.host}") String apiURL,
            @Value("${sls.fits.service.videoHost}") String apiVideoURL,
            @Value("${sls.fits.service.processingPath}") String fitsProcessingPath
    ){
        this.restTemplate = restTemplate;
        this.jaxb2Marshaller = jaxb2Marshaller;
        this.fitsResultValidator = fitsResultValidator;
        this.apiURL = apiURL;
        this.apiVideoURL = apiVideoURL;
        this.fitsProcessingPath = fitsProcessingPath;

    }

    /**
     * Generates the path that the fits result of a file is stored to
     *
     * @param fitsPath
     * @return
     */
    public static String getMetadataPathForFile(String fitsPath) {
        return StringUtils.trimLeadingCharacter(fitsPath.concat(FitsService.FITS_OUTPUT_SUFFIX), File.separatorChar);
    }

    public CompletableFuture<String> getVersion() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);

        HttpEntity<String> request = new HttpEntity<>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                generateApiURL("/version", ""), HttpMethod.GET, request, String.class
        );

        return CompletableFuture.completedFuture(response.getBody());
    }

    /**
     * Sends a request to the FITS service to process the file found at filename. Returns a FitsResult object.
     *
     * @param filename
     * @param examinedFilePath
     * @return
     * @throws IOException
     * @throws FitsResultException
     */
    public CompletableFuture<FitsResult> examine(String filename, String examinedFilePath) throws IOException, FitsResultException, SAXException {

        // some trickery to get both the raw xml string and the marshalled object for further processing
        BufferedReader reader = doExamine(filename, examinedFilePath);
        String fitsRawResult = IOUtils.toString(reader);
        Fits fitsResult = (Fits) jaxb2Marshaller.unmarshal(new StreamSource(new StringReader(fitsRawResult)));

        FitsResult fitsResultData = FitsResult.createFromFits(
                fitsResult,
                fitsRawResult
        );

        // check if the fits XML contains something that looks like a sane result
        fitsResultValidator.validate(fitsResultData);

        return CompletableFuture.completedFuture(fitsResultData);
    }

    /**
     * Makes the actual request to examine a file path in the FITS service.
     *
     * @param filename
     * @param examinedFilePath
     * @return
     * @throws IOException
     * @throws FitsResultException
     */
    protected BufferedReader doExamine(String filename, String examinedFilePath) throws IOException, FitsResultException {

        // make quick guess on type of file so we can direct video files to separate fits service
        File file = new File(examinedFilePath);
        Tika tika = new Tika();
        String mimeType = tika.detect(file);
        log.debug("detected mimetype on file {} {} {}",
                kv("mime_type", mimeType),
                kv("filename", filename),
                kv("examined_file_path", examinedFilePath)
        );

        Map<String, String> params = Collections.singletonMap("filename", fitsProcessingPath+"/"+filename);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);

        HttpEntity<String> request = new HttpEntity<>(null, headers);

        try{
            // response as string so we can save it to disk directly
            ResponseEntity<String> response = restTemplate.exchange(
                    generateApiURL("/examine?file={filename}", mimeType),
                    HttpMethod.GET, request, String.class, params
            );

            if(response.getStatusCode() == HttpStatus.OK){
                // store raw response to file
                Path xmlPath = this.saveRawResponse(response.getBody(), examinedFilePath);
                // read back the stored file and return as fits result, thus ensuring the file was stored correctly
                return Files.newBufferedReader(xmlPath);
            } else {
                throw new FitsResultException(response.getBody());
            }
        } catch (RestClientException e){
            throw new FitsResultException(e.getMessage());
        }
    }

    /**
     * Saves the xml response from the FITS service to a file that has the same name as examinedFilePath
     * concatenated with FITS_OUTPUT_SUFFIX
     *
     * e.g. mypic.jpg.fits.xml
     *
     * @param rawData
     * @param examinedFilePath
     * @return
     * @throws IOException If the file could not be written to disk.
     */
    protected Path saveRawResponse(String rawData, String examinedFilePath) throws IOException {

        Path path = Paths.get(examinedFilePath.concat(FITS_OUTPUT_SUFFIX));
        byte[] strToBytes = rawData.getBytes();

        Files.write(path, strToBytes);

        log.debug("Wrote FITS result for {} to {}",
                kv("examined_path", examinedFilePath),
                kv("fits_xml_path", path)
        );

        return path;
    }

    /**
     * Helper for generating complete url path to a FITS service API endpoint.
     *
     * @param endpoint
     * @return
     */
    protected String generateApiURL(String endpoint, String mimeType){
        if(mimeType.contains("video")){
            return apiVideoURL + endpoint;
        } else {
            return apiURL + endpoint;
        }
    }
}
