package fi.sls.ingest.representation.converter;

import fi.sls.ingest.manager.ProcessingStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProcessingLogStatusConverterTest {

    ProcessingLogStatusConverter testable;

    @BeforeEach
    void setUp() {
        testable = new ProcessingLogStatusConverter();
    }

    @Test
    void convertToDatabaseColumn() {
        assertEquals(Integer.valueOf(3), testable.convertToDatabaseColumn(ProcessingStatus.STAGING));
        assertEquals(Integer.valueOf(200), testable.convertToDatabaseColumn(ProcessingStatus.COMPLETE));
    }

    @Test
    void convertToEntityAttribute() {
        assertEquals(testable.convertToEntityAttribute(Integer.valueOf(3)), ProcessingStatus.STAGING);
        assertEquals(testable.convertToEntityAttribute(Integer.valueOf(200)), ProcessingStatus.COMPLETE);
    }
}