package fi.sls.ingest.parser;

import fi.sls.ingest.config.MetaDataConfig;
import fi.sls.ingest.exception.IngestFilenameException;
import fi.sls.ingest.representation.file.FilenameMetaData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import static org.junit.jupiter.api.Assertions.*;

@EnableConfigurationProperties(value = {MetaDataConfig.class})
@ContextConfiguration(classes={MetaDataConfig.class}, initializers = {ConfigFileApplicationContextInitializer.class})
@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class FilenameMetaParserTest {

    @Autowired
    MetaDataConfig metaDataConfig;

    static Validator validator;

    FilenameMetaParser testable;

    @BeforeAll
    public static void setupCls(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @BeforeEach
    public void setup(){
        this.testable = new FilenameMetaParser(metaDataConfig, validator);
    }

    @Test
    public void parseThrowsExceptionIfEmptyParameter(){
        String filename = null;

        assertThrows(IngestFilenameException.class, () -> {
            testable.parse(filename);
        });
    }

    @Test
    public void parseThrowsExceptionIfFilenameNotValid(){
        // only the actual filename should be accepted (not path)
        String filename = "/processing/test2/fmi_483.1_foto_002_001.jpg";

        assertThrows(IngestFilenameException.class, () -> {
            testable.parse(filename);
        });

        // invalid format on filename should not be accepted (missing underscore after fmi
        String filename2 = "fmi483.1_foto_002_001.jpg";

        assertThrows(IngestFilenameException.class, () -> {
            testable.parse(filename2);
        });

        // invalid format on filename should not be accepted (missing page nr at end e.g. 001_001)
        String filename3 = "fmi_483.1_foto_001.jpg";

        assertThrows(IngestFilenameException.class, () -> {
            testable.parse(filename3);
        });
    }

    @Test
    public void parserThrowsException_ifCollectionNotConfigured(){
        IngestFilenameException exception = assertThrows(IngestFilenameException.class, () -> {
            testable.parse("notreal_555j.1.4_brev_00003_0001_01_orig.tif");
        });

        assertEquals("There is not a collection configured using key: notreal", exception.getMessage());
    }

    @Test
    public void parserThrowsException_ifCategoryNotConfigured(){
        IngestFilenameException exception = assertThrows(IngestFilenameException.class, () -> {
            testable.parse("sls_555j.1.4_notreal_00003_0001_01_orig.tif");
        });

        assertEquals("There is not a category configured using key: notreal", exception.getMessage());
    }

    @Test
    public void origFileIsMarkedAsDigitalOriginal(){
        FilenameMetaData rtn = testable.parse("ota_555j.1.4_brev_00003_0001_01_orig.tif");
        assertTrue(rtn.isDigitalOriginal());

        rtn = testable.parse("ota_555j.1.4_brev_00003_0001_01.tif");
        assertFalse(rtn.isDigitalOriginal());
    }

    @Test
    public void parse_isAbleToSetCollectionLongName(){

        FilenameMetaData rtn = testable.parse("ta_555j.1.4_brev_00003_0001_01_orig.tif");
        assertEquals("Tjänstearkivet", rtn.getCollectionDescriptionOrName());
    }

    @Test
    public void parseFilename_ZeroSignumNrOK(){

        this.assertMetaFields(testable.parse(
                "slsa_1103.g.a.23_doku_00000_0001_01.tif"),
                "slsa_1103.g.a.23_doku_00000_0001_01.tif",
                "slsa",
                "SLSA",
                "doku",
                "dokument och handlingar",
                "Text",
                "1103",
                "g.a.23",
                "SLSA 1103",
                0,
                1,
                "SLSA 1103:g:a:23 dokument 0",
                "SLS:slsa1103-g-a-23_ent.doku.0",
                "tif",
                "SLS:slsa1103-g-a-23_doku.0-1-1.tif",
                "masterfil",
                "slsa/slsa1103/slsa_1103.g.a.23_doku_00000_0001_01.tif"
        );
    }

    @Test
    public void parseWithRelaxedValidation_ZeroSignumNrOK(){
        this.assertMetaFields(testable.parseWithRelaxedValidation(
                "slsa_1103.g.a.23_doku_0_1_1.tif"),
                "slsa_1103.g.a.23_doku_0_1_1.tif",
                "slsa",
                "SLSA",
                "doku",
                "dokument och handlingar",
                "Text",
                "1103",
                "g.a.23",
                "SLSA 1103",
                0,
                1,
                "SLSA 1103:g:a:23 dokument 0",
                "SLS:slsa1103-g-a-23_ent.doku.0",
                "tif",
                "SLS:slsa1103-g-a-23_doku.0-1-1.tif",
                "masterfil",
                "slsa/slsa1103/slsa_1103.g.a.23_doku_00000_0001_01.tif"
        );
    }

    @Test
    public void parseFilenameSLS2210_OK() {
        this.assertMetaFields(testable.parse(
                "sls_2110_foto_00001_0001_01.tif"),
                "sls_2110_foto_00001_0001_01.tif",
                "sls",
                "SLS",
                "foto",
                "fotografi",
                "Bild",
                "2110",
                null,
                "SLS 2110",
                1,
                1,
                "SLS 2110 foto 1",
                "SLS:sls2110_ent.foto.1",
                "tif",
                "SLS:sls2110_foto.1-1-1.tif",
                "masterfil",
                "sls/sls2110/sls_2110_foto_00001_0001_01.tif"
                );
    }
    @Test
    public void parseFilenameOTA135_OK() {
        this.assertMetaFields(testable.parse(
                "ota_135_foto_00001_0001_01.tif"),
                "ota_135_foto_00001_0001_01.tif",
                "ota",
                "ÖTA",
                "foto",
                "fotografi",
                "Bild",
                "135",
                null,
                "ÖTA 135",
                1,
                1,
                "ÖTA 135 foto 1",
                "SLS:ota135_ent.foto.1",
                "tif",
                "SLS:ota135_foto.1-1-1.tif",
                "masterfil",
                "ota/ota135/ota_135_foto_00001_0001_01.tif"
        );
    }

    @Test
    public void parseFileWithSingleDigit_OK(){
        this.assertMetaFields(testable.parse(
                "ota_7_brev_00046_0003_01.tif"),
                "ota_7_brev_00046_0003_01.tif",
                "ota",
                "ÖTA",
                "brev",
                "brev",
                "Text",
                "7",
                null,
                "ÖTA 7",
                46,
                3,
                "ÖTA 7 brev 46",
                "SLS:ota7_ent.brev.46",
                "tif",
                "SLS:ota7_brev.46-3-1.tif",
                "masterfil",
                "ota/ota7/ota_7_brev_00046_0003_01.tif"
        );
    }


    @Test
    public void parseFilenameToMetadataOK() {

        this.assertMetaFields(testable.parse(
                "ota_555j.1.4_brev_00003_0001_01.tif"),
                "ota_555j.1.4_brev_00003_0001_01.tif",
                "ota",
                "ÖTA",
                "brev",
                "brev",
                "Text",
                "555j",
                "1.4",
                "ÖTA 555 j",
                3,
                1,
                "ÖTA 555 j:1:4 brev 3",
                "SLS:ota555j-1-4_ent.brev.3",
                "tif",
                "SLS:ota555j-1-4_brev.3-1-1.tif",
                "masterfil",
                "ota/ota555j/ota_555j.1.4_brev_00003_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_555j.1.4_brev_00003_0001_01.tif"),
                "ota_555j.1.4_brev_00003_0001_01.tif",
                "ota",
                "ÖTA",
                "brev",
                "brev",
                "Text",
                "555j",
                "1.4",
                "ÖTA 555 j",
                3,
                1,
                "ÖTA 555 j:1:4 brev 3",
                "SLS:ota555j-1-4_ent.brev.3",
                "tif",
                "SLS:ota555j-1-4_brev.3-1-1.tif",
                "masterfil",
                "ota/ota555j/ota_555j.1.4_brev_00003_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_556.1.5_dbok_00004_0001_01.tif"),
                "ota_556.1.5_dbok_00004_0001_01.tif",
                "ota",
                "ÖTA",
                "dbok",
                "dagbok",
                "Text",
                "556",
                "1.5",
                "ÖTA 556",
                4,
                1,
                "ÖTA 556:1:5 dagbok 4",
                "SLS:ota556-1-5_ent.dbok.4",
                "tif",
                "SLS:ota556-1-5_dbok.4-1-1.tif",
                "masterfil",
                "ota/ota556/ota_556.1.5_dbok_00004_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_557.1.6_doku_00005_0001_01.tif"),
                "ota_557.1.6_doku_00005_0001_01.tif",
                "ota",
                "ÖTA",
                "doku",
                "dokument och handlingar",
                "Text",
                "557",
                "1.6",
                "ÖTA 557",
                5,
                1,
                "ÖTA 557:1:6 dokument 5",
                "SLS:ota557-1-6_ent.doku.5",
                "tif",
                "SLS:ota557-1-6_doku.5-1-1.tif",
                "masterfil",
                "ota/ota557/ota_557.1.6_doku_00005_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_558.1.7_svar_00006_0001_01.tif"),
                "ota_558.1.7_svar_00006_0001_01.tif",
                "ota",
                "ÖTA",
                "svar",
                "frågelistsvar",
                "Text",
                "558",
                "1.7",
                "ÖTA 558",
                6,
                1,
                "ÖTA 558:1:7 svar 6",
                "SLS:ota558-1-7_ent.svar.6",
                "tif",
                "SLS:ota558-1-7_svar.6-1-1.tif",
                "masterfil",
                "ota/ota558/ota_558.1.7_svar_00006_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_559.1.8_manu_00007_0001_01.tif"),
                "ota_559.1.8_manu_00007_0001_01.tif",
                "ota",
                "ÖTA",
                "manu",
                "manuskript",
                "Text",
                "559",
                "1.8",
                "ÖTA 559",
                7,
                1,
                "ÖTA 559:1:8 manuskript 7",
                "SLS:ota559-1-8_ent.manu.7",
                "tif",
                "SLS:ota559-1-8_manu.7-1-1.tif",
                "masterfil",
                "ota/ota559/ota_559.1.8_manu_00007_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "fmi_560.1_not_00008_0001_01.tif"),
                "fmi_560.1_not_00008_0001_01.tif",
                "fmi",
                "FMI",
                "not",
                "noter",
                "Text",
                "560",
                "1",
                "FMI 560",
                8,
                1,
                "FMI 560:1 not 8",
                "SLS:fmi560-1_ent.not.8",
                "tif",
                "SLS:fmi560-1_not.8-1-1.tif",
                "masterfil",
                "fmi/fmi560/fmi_560.1_not_00008_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_562.1.11_trks_00010_0001_01.tif"),
                "ota_562.1.11_trks_00010_0001_01.tif",
                "ota",
                "ÖTA",
                "trks",
                "trycksak",
                "Text",
                "562",
                "1.11",
                "ÖTA 562",
                10,
                1,
                "ÖTA 562:1:11 trycksak 10",
                "SLS:ota562-1-11_ent.trks.10",
                "tif",
                "SLS:ota562-1-11_trks.10-1-1.tif",
                "masterfil",
                "ota/ota562/ota_562.1.11_trks_00010_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "fmi_563.9_uppt_00011_0001_01.tif"),
                "fmi_563.9_uppt_00011_0001_01.tif",
                "fmi",
                "FMI",
                "uppt",
                "uppteckning",
                "Text",
                "563",
                "9",
                "FMI 563",
                11,
                1,
                "FMI 563:9 uppteckning 11",
                "SLS:fmi563-9_ent.uppt.11",
                "tif",
                "SLS:fmi563-9_uppt.11-1-1.tif",
                "masterfil",
                "fmi/fmi563/fmi_563.9_uppt_00011_0001_01.tif"
        );

        this.assertMetaFields(testable.parse(
                "ota_564_affi_00012_0001_01.tif"),
                "ota_564_affi_00012_0001_01.tif",
                "ota",
                "ÖTA",
                "affi",
                "affisch",
                "Bild",
                "564",
                null,
                "ÖTA 564",
                12,
                1,
                "ÖTA 564 affisch 12",
                "SLS:ota564_ent.affi.12",
                "tif",
                "SLS:ota564_affi.12-1-1.tif",
                "masterfil",
                "ota/ota564/ota_564_affi_00012_0001_01.tif"
        );

        this.assertMetaFields(testable.parse(
                "ota_565_exli_00013_0001_01.tif"),
                "ota_565_exli_00013_0001_01.tif",
                "ota",
                "ÖTA",
                "exli",
                "exlibris",
                "Bild",
                "565",
                null,
                "ÖTA 565",
                13,
                1,
                "ÖTA 565 exlibris 13",
                "SLS:ota565_ent.exli.13",
                "tif",
                "SLS:ota565_exli.13-1-1.tif",
                "masterfil",
                "ota/ota565/ota_565_exli_00013_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_566_foto_00014_0001_01.tif"),
                "ota_566_foto_00014_0001_01.tif",
                "ota",
                "ÖTA",
                "foto",
                "fotografi",
                "Bild",
                "566",
                null,
                "ÖTA 566",
                14,
                1,
                "ÖTA 566 foto 14",
                "SLS:ota566_ent.foto.14",
                "tif",
                "SLS:ota566_foto.14-1-1.tif",
                "masterfil",
                "ota/ota566/ota_566_foto_00014_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "sls_567_kart_00015_0001_01.tif"),
                "sls_567_kart_00015_0001_01.tif",
                "sls",
                "SLS",
                "kart",
                "karta",
                "Bild",
                "567",
                null,
                "SLS 567",
                15,
                1,
                "SLS 567 karta 15",
                "SLS:sls567_ent.kart.15",
                "tif",
                "SLS:sls567_kart.15-1-1.tif",
                "masterfil",
                "sls/sls567/sls_567_kart_00015_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_568_konst_00016_0001_01.tif"),
                "ota_568_konst_00016_0001_01.tif",
                "ota",
                "ÖTA",
                "konst",
                "konstverk",
                "Bild",
                "568",
                null,
                "ÖTA 568",
                16,
                1,
                "ÖTA 568 konstverk 16",
                "SLS:ota568_ent.konst.16",
                "tif",
                "SLS:ota568_konst.16-1-1.tif",
                "masterfil",
                "ota/ota568/ota_568_konst_00016_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_569_kort_00017_0001_01.tif"),
                "ota_569_kort_00017_0001_01.tif",
                "ota",
                "ÖTA",
                "kort",
                "postkort",
                "Bild",
                "569",
                null,
                "ÖTA 569",
                17,
                1,
                "ÖTA 569 postkort 17",
                "SLS:ota569_ent.kort.17",
                "tif",
                "SLS:ota569_kort.17-1-1.tif",
                "masterfil",
                "ota/ota569/ota_569_kort_00017_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "slsa_570_ritn_00018_0001_01.tif"),
                "slsa_570_ritn_00018_0001_01.tif",
                "slsa",
                "SLSA",
                "ritn",
                "ritning",
                "Bild",
                "570",
                null,
                "SLSA 570",
                18,
                1,
                "SLSA 570 ritning 18",
                "SLS:slsa570_ent.ritn.18",
                "tif",
                "SLS:slsa570_ritn.18-1-1.tif",
                "masterfil",
                "slsa/slsa570/slsa_570_ritn_00018_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_571_teckn_00019_0001_01.tif"),
                "ota_571_teckn_00019_0001_01.tif",
                "ota",
                "ÖTA",
                "teckn",
                "teckning eller skiss",
                "Bild",
                "571",
                null,
                "ÖTA 571",
                19,
                1,
                "ÖTA 571 teckning 19",
                "SLS:ota571_ent.teckn.19",
                "tif",
                "SLS:ota571_teckn.19-1-1.tif",
                "masterfil",
                "ota/ota571/ota_571_teckn_00019_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ta_572_trkb_00020_0001_01.tif"),
                "ta_572_trkb_00020_0001_01.tif",
                "ta",
                "TA",
                "trkb",
                "tryckbild",
                "Bild",
                "572",
                null,
                "TA 572",
                20,
                1,
                "TA 572 tryckbild 20",
                "SLS:ta572_ent.trkb.20",
                "tif",
                "SLS:ta572_trkb.20-1-1.tif",
                "masterfil",
                "ta/ta572/ta_572_trkb_00020_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_573_datab_00021_0001_01.xml"),
                "ota_573_datab_00021_0001_01.xml",
                "ota",
                "ÖTA",
                "datab",
                "databas",
                "Databaser, programvara & multimedia",
                "573",
                null,
                "ÖTA 573",
                21,
                1,
                "ÖTA 573 databas 21",
                "SLS:ota573_ent.datab.21",
                "xml",
                "SLS:ota573_datab.21-1-1.xml",
                "masterfil",
                "ota/ota573/ota_573_datab_00021_0001_01.xml"
        );
        this.assertMetaFields(testable.parse(
                "ota_574_multi_00022_0001_01.xml"),
                "ota_574_multi_00022_0001_01.xml",
                "ota",
                "ÖTA",
                "multi",
                "multimediaproduktion",
                "Databaser, programvara & multimedia",
                "574",
                null,
                "ÖTA 574",
                22,
                1,
                "ÖTA 574 multimedia 22",
                "SLS:ota574_ent.multi.22",
                "xml",
                "SLS:ota574_multi.22-1-1.xml",
                "masterfil",
                "ota/ota574/ota_574_multi_00022_0001_01.xml"
        );
        this.assertMetaFields(testable.parse(
                "ota_575_fore_00023_0001_01.tif"),
                "ota_575_fore_00023_0001_01.tif",
                "ota",
                "ÖTA",
                "fore",
                "annat föremål",
                "Föremål",
                "575",
                null,
                "ÖTA 575",
                23,
                1,
                "ÖTA 575 föremål 23",
                "SLS:ota575_ent.fore.23",
                "tif",
                "SLS:ota575_fore.23-1-1.tif",
                "masterfil",
                "ota/ota575/ota_575_fore_00023_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_576_konsth_00024_0001_01.tif"),
                "ota_576_konsth_00024_0001_01.tif",
                "ota",
                "ÖTA",
                "konsth",
                "konsthantverk",
                "Föremål",
                "576",
                null,
                "ÖTA 576",
                24,
                1,
                "ÖTA 576 konsthantverk 24",
                "SLS:ota576_ent.konsth.24",
                "tif",
                "SLS:ota576_konsth.24-1-1.tif",
                "masterfil",
                "ota/ota576/ota_576_konsth_00024_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_577_3d_00025_0001_01.tif"),
                "ota_577_3d_00025_0001_01.tif",
                "ota",
                "ÖTA",
                "3d",
                "konstverk (3D)",
                "Föremål",
                "577",
                null,
                "ÖTA 577",
                25,
                1,
                "ÖTA 577 konst3d 25",
                "SLS:ota577_ent.3d.25",
                "tif",
                "SLS:ota577_3d.25-1-1.tif",
                "masterfil",
                "ota/ota577/ota_577_3d_00025_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_578_instr_00026_0001_01.tif"),
                "ota_578_instr_00026_0001_01.tif",
                "ota",
                "ÖTA",
                "instr",
                "musikinstrument",
                "Föremål",
                "578",
                null,
                "ÖTA 578",
                26,
                1,
                "ÖTA 578 instrument 26",
                "SLS:ota578_ent.instr.26",
                "tif",
                "SLS:ota578_instr.26-1-1.tif",
                "masterfil",
                "ota/ota578/ota_578_instr_00026_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_579_textil_00027_0001_01.tif"),
                "ota_579_textil_00027_0001_01.tif",
                "ota",
                "ÖTA",
                "textil",
                "textilie",
                "Föremål",
                "579",
                null,
                "ÖTA 579",
                27,
                1,
                "ÖTA 579 textilie 27",
                "SLS:ota579_ent.textil.27",
                "tif",
                "SLS:ota579_textil.27-1-1.tif",
                "masterfil",
                "ota/ota579/ota_579_textil_00027_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_580_verktyg_00028_0001_01.tif"),
                "ota_580_verktyg_00028_0001_01.tif",
                "ota",
                "ÖTA",
                "verktyg",
                "verktyg eller redskap",
                "Föremål",
                "580",
                null,
                "ÖTA 580",
                28,
                1,
                "ÖTA 580 verktyg 28",
                "SLS:ota580_ent.verktyg.28",
                "tif",
                "SLS:ota580_verktyg.28-1-1.tif",
                "masterfil",
                "ota/ota580/ota_580_verktyg_00028_0001_01.tif"
        );
        this.assertMetaFields(testable.parse(
                "ota_581_ljud_00029_0001_01.wav"),
                "ota_581_ljud_00029_0001_01.wav",
                "ota",
                "ÖTA",
                "ljud",
                "",
                "Ljud",
                "581",
                null,
                "ÖTA 581",
                29,
                1,
                "ÖTA 581 ljud 29",
                "SLS:ota581_ent.ljud.29",
                "wav",
                "SLS:ota581_ljud.29-1-1.wav",
                "masterfil",
                "ota/ota581/ota_581_ljud_00029_0001_01.wav"
        );
        this.assertMetaFields(testable.parse(
                "ota_582_av_00030_0001_01.mp4"),
                "ota_582_av_00030_0001_01.mp4",
                "ota",
                "ÖTA",
                "av",
                "",
                "Rörlig bild",
                "582",
                null,
                "ÖTA 582",
                30,
                1,
                "ÖTA 582 av 30",
                "SLS:ota582_ent.av.30",
                "mp4",
                "SLS:ota582_av.30-1-1.mp4",
                "masterfil",
                "ota/ota582/ota_582_av_00030_0001_01.mp4"
        );

        //--- special cases start

        this.assertMetaFields(testable.parse(
                "ota_555j.1.4_brev_00003_0001_01_orig.tif"),
                "ota_555j.1.4_brev_00003_0001_01_orig.tif",
                "ota",
                "ÖTA",
                "brev",
                "brev",
                "Text",
                "555j",
                "1.4",
                "ÖTA 555 j",
                3,
                1,
                "ÖTA 555 j:1:4 brev 3",
                "SLS:ota555j-1-4_ent.brev.3",
                "tif",
                "SLS:ota555j-1-4_brev.3-1-1_orig.tif",
                "digitalt original",
                "original/ota/ota555j/ota_555j.1.4_brev_00003_0001_01_orig.tif"
        );

        // capital letters ok in suffix
        this.assertMetaFields(testable.parse(
                "sls_2002_foto_00001_0001_01.JPG"),
                "sls_2002_foto_00001_0001_01.JPG",
                "sls",
                "SLS",
                "foto",
                "fotografi",
                "Bild",
                "2002",
                null,
                "SLS 2002",
                1,
                1,
                "SLS 2002 foto 1",
                "SLS:sls2002_ent.foto.1",
                "JPG",
                "SLS:sls2002_foto.1-1-1.JPG",
                "masterfil",
                "sls/sls2002/sls_2002_foto_00001_0001_01.JPG"
        );

        // version nr is accepted in filename
        this.assertMetaFields(testable.parse(
                "sls_2002_foto_00001_0001_01.JPG"),
                "sls_2002_foto_00001_0001_01.JPG",
                "sls",
                "SLS",
                "foto",
                "fotografi",
                "Bild",
                "2002",
                null,
                "SLS 2002",
                1,
                1,
                "SLS 2002 foto 1",
                "SLS:sls2002_ent.foto.1",
                "JPG",
                "SLS:sls2002_foto.1-1-1.JPG",
                "masterfil",
                "sls/sls2002/sls_2002_foto_00001_0001_01.JPG"
        );

        // version nr is accepted in filename
        this.assertMetaFields(testable.parse(
                "sls_2002_foto_00001_0001_01_orig.JPG"),
                "sls_2002_foto_00001_0001_01_orig.JPG",
                "sls",
                "SLS",
                "foto",
                "fotografi",
                "Bild",
                "2002",
                null,
                "SLS 2002",
                1,
                1,
                "SLS 2002 foto 1",
                "SLS:sls2002_ent.foto.1",
                "JPG",
                "SLS:sls2002_foto.1-1-1_orig.JPG",
                "digitalt original",
                "original/sls/sls2002/sls_2002_foto_00001_0001_01_orig.JPG"
        );

        // migration case test sls_865.1920.t_foto_00120_0001_01.tif
        this.assertMetaFields(testable.parse(
                "sls_865.1920.t_foto_00120_0001_01.tif"),
                "sls_865.1920.t_foto_00120_0001_01.tif",
                "sls",
                "SLS",
                "foto",
                "fotografi",
                "Bild",
                "865",
                "1920.t",
                "SLS 865",
                120,
                1,
                "SLS 865:1920:t foto 120",
                "SLS:sls865-1920-t_ent.foto.120",
                "tif",
                "SLS:sls865-1920-t_foto.120-1-1.tif",
                "masterfil",
                "sls/sls865/sls_865.1920.t_foto_00120_0001_01.tif"
        );
    }

    void assertMetaFields(FilenameMetaData testable, String expectedFileName, String expectedCollectionKey, String expectedCollectionName, String expectedSubCategoryKey, String expectedSubCategoryName, String expectedCategory, String expectedArchiveNr, String expectedArchiveCatalogNr, String expectedArchiveCollectionName, int expectedSignumNr, int expectedPageNr, String expectedArchiveSignum, String expectedEntityIdentifier, String expectedFileSuffix, String expectedDigitalObjectIdentifier, String expectedEntityType, String expectedDcIdentifier){
        assertNotNull(testable);
        assertNotNull(testable.getCategory());
        assertEquals(expectedFileName, testable.getFilename());
        assertEquals(expectedCollectionKey, testable.getCollectionKey());
        assertEquals(expectedCollectionName, testable.getCollectionName());
        assertEquals(expectedSubCategoryKey, testable.getCategory().getFileKey());
        assertEquals(expectedSubCategoryName, testable.getCategory().getSubCategory());
        assertEquals(expectedCategory, testable.getCategory().getCategoryLabel());
        assertEquals(expectedArchiveNr, testable.getArchiveNr());
        assertEquals(expectedArchiveCatalogNr, testable.getArchiveCatalogNr());
        assertEquals(expectedArchiveCollectionName, testable.getArchiveCollectionName());
        assertEquals(expectedSignumNr, testable.getSigumNr());
        assertEquals(expectedPageNr, testable.getPageNr());
        assertEquals(expectedArchiveSignum, testable.getArchiveSignum());
        assertEquals(expectedEntityIdentifier, testable.getEntityIdentifier());
        assertEquals(expectedFileSuffix, testable.getFileSuffix());
        assertEquals(expectedDigitalObjectIdentifier, testable.getDigitalObjectIdentifier());
        assertEquals(expectedEntityType, testable.getEntityType());
        assertEquals(expectedDcIdentifier, testable.getDcIdentifier());

    }
}