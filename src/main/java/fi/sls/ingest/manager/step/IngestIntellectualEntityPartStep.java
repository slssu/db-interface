package fi.sls.ingest.manager.step;

import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.DigitalObject;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntityPart;
import fi.sls.ingest.proxy.filemaker.response.record.SemlaEvent;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@Service
public class IngestIntellectualEntityPartStep extends RecordServiceStep {
    public IngestIntellectualEntityPartStep(RecordService service) {
        super(service);
    }


    public IntellectualEntityPart executeStep(SemlaEvent semlaEvent, DigitalObject digitalObject, String customerCopyFilePath) throws IOException {

        IntellectualEntityPart record = new IntellectualEntityPart();
        record.getFieldData().setEntityNumber(semlaEvent.getFieldData().getIePartNr());

        Optional<? extends FileMakerResponseEntity> existingItem;
//        jw 2022-02-14: remove try-catch as runnable will handle all exceptions
//        try {
            existingItem = service.findRecordByIdentifier(record);
            if(existingItem.isPresent()){
                record = (IntellectualEntityPart) existingItem.get();
                log.info("IntellectualEntityPart with number {} already exists, updating...",
                        record.getFieldData().getEntityNumber()
                );

                record.getFieldData().setTitle(digitalObject.getFieldData().getEntityLabel());
                record.getFieldData().setOrderNr(digitalObject.getFieldData().getEntityOrder());
                record.getFieldData().setFilePath(customerCopyFilePath);

                // value is calculated field in FileMaker, sending it back causes error so nullify
                // FIXME: would be much cooler to define an annotation that would nullify fields
                record.getFieldData().setDatabaseImage(null);
                record.getFieldData().setDatabaseThumbnail(null);

                record = (IntellectualEntityPart) service.updateObjectRecord(record);
                log.info("IntellectualEntityPart with number {} updated",
                        record.getFieldData().getEntityNumber()
                );

                log.info("Link between SemlaEvent with number {} and IntellectualEntityPart with number {} already exists, will not re-create",
                        semlaEvent.getFieldData().getEntityNumber(),
                        record.getFieldData().getEntityNumber()
                );

            } else {
                record.getFieldData().setTitle(digitalObject.getFieldData().getEntityLabel());
                record.getFieldData().setOrderNr(digitalObject.getFieldData().getEntityOrder());
                record.getFieldData().setFilePath(customerCopyFilePath);

                record = (IntellectualEntityPart) service.createObjectRecord(record);

                log.info("Created new IntellectualEntityPart with number {}",
                        record.getFieldData().getEntityNumber()
                );


                semlaEvent.getFieldData().setIePartNr(record.getFieldData().getEntityNumber());
                semlaEvent = (SemlaEvent) service.updateObjectRecord(semlaEvent);

                log.info("Created link between SemlaEvent with number {} and IntellectualEntityPart with number {}",
                            semlaEvent.getFieldData().getEntityNumber(),
                            record.getFieldData().getEntityNumber()
                        );

            }
            return record;
//        } catch (IOException e) {
//            log.error(e.toString());
//            // FIXME should we throw instead?
//            return null;
//        }
    }
}
