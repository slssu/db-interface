package fi.sls.ingest.queue.rabbitmq.queue;

import org.springframework.amqp.core.Queue;

public class IngestItemPostProcessingQueue extends Queue {
    public IngestItemPostProcessingQueue(String queueName){
        super(queueName);
    }
}
