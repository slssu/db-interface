package fi.sls.ingest.queue.rabbitmq.receiver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.proxy.filemaker.exception.FileMakerAPIException;
import fi.sls.ingest.queue.rabbitmq.exception.IngestAmqpReceiverException;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.SocketMessenger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * This class provides basic features for receiving a IngestProcessingItem and starting processing of it with
 * some runnable.
 */
@Slf4j
public abstract class IngestItemReceiverBase {

    protected static int DEFAULT_MAX_RETRIES = 3;

    protected ObjectMapper objectMapper;
    protected IngestItemRepository ingestItemRepository;
    protected SocketMessenger socketMessenger;

    @Autowired
    public IngestItemReceiverBase(ObjectMapper objectMapper, IngestItemRepository ingestItemRepository, SocketMessenger socketMessenger){
        this.objectMapper = objectMapper;
        this.ingestItemRepository = ingestItemRepository;
        this.socketMessenger = socketMessenger;
    }


    /**
     * Ensures the json can be parsed and represents an existing IngestProcessingItem,
     * then initiates processing of the item.
     *
     * @param json
     * @param acceptStatus
     * @throws AmqpRejectAndDontRequeueException
     */
    public void receiveItem(String json, ProcessingStatus acceptStatus) throws IngestAmqpReceiverException {
        log.debug("Receiving work item with acceptStatus: {}", kv("acceptStatus", acceptStatus));
        try {
            IngestProcessingItem item = objectMapper.readValue(json, IngestProcessingItem.class);

            Optional<IngestProcessingItem> checkItemExists = ingestItemRepository.findByToken(item.getToken());
            if(checkItemExists.isPresent()){

                if(item.getProcessingStatus() == acceptStatus){
                    log.info("Start processing item with token and status: {} {} ",
                            kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                            kv("item_status", item.getProcessingStatus()),
                            kv("accept_status", acceptStatus),
                            kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
                    );
                    startItemProcessing(checkItemExists.get());
                    log.info("Processing complete for item with token and status: {} {}",
                            kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                            kv("item_status", item.getProcessingStatus()),
                            kv("accept_status", acceptStatus),
                            kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
                    );
                } else {
                    log.warn("Will not process item, processing status does not match: {} {} {}",
                            kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                            kv("item_status", item.getProcessingStatus()),
                            kv("accept_status", acceptStatus),
                            kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
                    );
                    throw new IngestProcessingException(
                            String.format("Item with token=%s failed. Processing status does not match expected status for the receiver: accept_status=%s item_status=%s",
                                    item.getToken(),
                                    acceptStatus,
                                    item.getProcessingStatus()
                            )
                    );
                }

            } else {
                log.warn("Could not start processing item because an item with the given token does not exist in the ingestItemRepository: {}",
                        kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                        kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
                );
                throw new IngestProcessingException(
                        String.format("Could not start processing item because an item with the given token does not exist: token=%s",
                                item.getToken()
                        )
                );
            }
        } catch(IOException | IllegalArgumentException | HttpClientErrorException | FileMakerAPIException e){
            log.error(e.getMessage());
            throw new IngestAmqpReceiverException(e);
        }
    }

    /**
     * Marks the item as being rejected due to max retries and stops processing
     * @param json
     */
    protected void storeMaxRetryError(String json, Long numRetries) {
        try {
            IngestProcessingItem item = objectMapper.readValue(json, IngestProcessingItem.class);

            Optional<IngestProcessingItem> checkItemExists = ingestItemRepository.findByToken(item.getToken());
            if(checkItemExists.isPresent()){
                log.debug(String.format("Giving up on trying to process item with token %s, have tried %d times", item.getToken(), numRetries),
                        kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                        kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
                );
                checkItemExists.get().setProcessingStatus(ProcessingStatus.REJECTED);
                checkItemExists.get().setMessage(String.format("Giving up on trying to process item with token %s, have tried %d times", item.getToken(), numRetries));
                ingestItemRepository.save(checkItemExists.get());
                socketMessenger.broadcast(checkItemExists.get());

            } else {
                log.warn("Could not store processing error because an item with the given token does not exist in the ingestItemRepository: {}",
                        kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                        kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
                );
            }


        } catch (JsonProcessingException e) {
            // would indicate that the received message has a problem so not much can be done other than logging and
            // ensuring the message is not sent again
            log.error(e.getMessage());

            return;
        }
    }

    /**
     * Determines if the incoming request has been retried too many times to warrant another retry
     *
     * @param message
     * @return
     */
    protected boolean hasExceededRetryCount(Message message) {
        List<Map<String, ?>> xDeathHeader = message.getMessageProperties().getXDeathHeader();
        if (xDeathHeader != null && xDeathHeader.size() >= 1) {
            Long count = (Long) xDeathHeader.get(0).get("count");
            return count >= DEFAULT_MAX_RETRIES;
        }

        return false;
    }

    protected Long getRetryCount(Message message) {
        List<Map<String, ?>> xDeathHeader = message.getMessageProperties().getXDeathHeader();
        if (xDeathHeader != null && xDeathHeader.size() >= 1) {
            Long count = (Long) xDeathHeader.get(0).get("count");
            return count;
        }

        return 0L;
    }



    /**
     * Starts the actual processing of an IngestProcessingItem.
     *
     * @param item
     */
    public abstract void startItemProcessing(IngestProcessingItem item);

}
