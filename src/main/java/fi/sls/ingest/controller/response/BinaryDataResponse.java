package fi.sls.ingest.controller.response;

import lombok.Getter;
import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

/**
 * Class that wraps some binary data in a json parseable structure
 *
 * The data property is expected to contain the binary value as Base64 encoded string
 */
public class BinaryDataResponse implements Serializable {

    @Getter
    protected String name;

    @Getter
    protected String data;

    public BinaryDataResponse(ByteArrayOutputStream bin, String name) {
        this.name = name;
        this.data = new String(Base64.encodeBase64(bin.toByteArray()));

    }

}
