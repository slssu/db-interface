package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import fi.sls.ingest.proxy.filemaker.request.RecordPayload;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SemlaEventFieldData implements RecordPayload {

    // ID
    @JsonProperty("nummer")
    Long entityNumber;

    // HACK: FileMaker API requires Number fields in db to be strings in JSON (wtf!)
    @JsonProperty("ie_nummer")
    String ieNumber;

    @JsonProperty("nr_ieParts")
    Long iePartNr;

    /**
     * Setter/Getter defined so we update the ieNumber field with what the FileMaker API wants, i.e. strings, but
     * can still  use correctly typed values in our own code
     */
    @JsonIgnore
    Long intellectualEntityNumber;
    public void setIntellectualEntityNumber(Long intellectualEntityNumber){
        this.intellectualEntityNumber = intellectualEntityNumber;
        if(intellectualEntityNumber != null){
            this.setIeNumber(intellectualEntityNumber.toString());
        }

    }
    public Long getIntellectualEntityNumber(){
        if(this.intellectualEntityNumber == null && this.ieNumber != null && !this.ieNumber.isEmpty()){
            this.intellectualEntityNumber = Long.parseLong(this.ieNumber);
        }
        return this.intellectualEntityNumber;
    }

    // HACK: FileMaker API requires Number fields in db to be strings in JSON (wtf!)
    @JsonProperty("digitalObjects_nr")
    String digitalObjectsNr;

    /**
     * Setter/Getter defined so we update the ieNumber field with what the FileMaker API wants, i.e. strings, but
     * can still  use correctly typed values in our own code
     */
    @JsonIgnore
    Long digitalObjectsNumber;
    public void setDigitalObjectsNumber(Long digitalObjectsNumber){
        this.digitalObjectsNumber = digitalObjectsNumber;
        if(digitalObjectsNumber != null){
            this.setDigitalObjectsNr(digitalObjectsNumber.toString());
        }

    }
    public Long getDigitalObjectsNumber(){
        if(this.digitalObjectsNumber == null && this.digitalObjectsNr != null && !this.digitalObjectsNr.isEmpty()){
            this.digitalObjectsNumber = Long.parseLong(this.digitalObjectsNr);
        }
        return this.digitalObjectsNumber;
    }
}
