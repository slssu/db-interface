package fi.sls.ingest.manager.runnable;

import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.SocketMessenger;
import lombok.Getter;
import lombok.Setter;

/**
 * Base class used by the runnables that process ingest items
 */
public abstract class IngestRunnableBase {

    /**
     * The wrapper item containing information about the file being processed
     */
//    @Getter
//    @Setter
//    protected IngestProcessingItem item;

    /**
     * Queue storage that stores the status of the IngestProcessingItems
     */
    protected IngestItemRepository ingestItemRepository;

    /**
     * WebSocket used to push out updates on the processing state of the current item
     */
    protected SocketMessenger socketMessenger;


    protected IngestRunnableBase(
            IngestItemRepository ingestItemRepository,
            SocketMessenger socketMessenger
    ){
        this.ingestItemRepository = ingestItemRepository;
        this.socketMessenger = socketMessenger;
    }

    /**
     * Helper for setting a new status for a IngestProcessingItem and broadcasting the update
     *
     * @param status
     * @param item
     */
    protected void setProcessingItemStatusAndSend(ProcessingStatus status, IngestProcessingItem item){
        item.setProcessingStatus(status);
        ingestItemRepository.save(item);
        socketMessenger.broadcast(item);
    }

}
