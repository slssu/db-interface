package fi.sls.ingest.manager.runnable;

import fi.sls.ingest.config.IngestConfig;
import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.fits.FitsService;
import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.manager.IngestItem;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.parser.FilenameMetaParser;
import fi.sls.ingest.proxy.slsfileapi.FileAPIMoveRequestBody;
import fi.sls.ingest.proxy.slsfileapi.SLSFileAPI;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.ws.SocketMessenger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.web.client.HttpServerErrorException;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class IngestPreProcessRunnableTest {

    IngestPreProcessRunnable testable;

    IngestItemRepository ingestItemRepository;
    SocketMessenger socketMessenger;
    FilenameMetaParser filenameMetaParser;
    FitsService fitsService;
    SLSFileAPI slsFileAPI;
    IngestConfig ingestConfig;



    @BeforeEach
    void setUp() {

        ingestItemRepository = Mockito.mock(IngestItemRepository.class);
        socketMessenger = Mockito.mock(SocketMessenger.class);

        filenameMetaParser = Mockito.mock(FilenameMetaParser.class);
        fitsService = Mockito.mock(FitsService.class);
        slsFileAPI = Mockito.mock(SLSFileAPI.class);
        ingestConfig = Mockito.mock(IngestConfig.class);

        testable = new IngestPreProcessRunnable(
                ingestItemRepository,
                socketMessenger,
                filenameMetaParser,
                fitsService,
                slsFileAPI,
                ingestConfig
        );
    }

    @Test
    void run_worksAsExpectedWithValidData() throws IOException, SAXException, ExecutionException, InterruptedException {

        IngestProcessingItem mockProcessingItem = createMockIngestProcessingItem();

//        FilenameMetaData mockMeta = Mockito.mock(FilenameMetaData.class);
//        when(filenameMetaParser.parse("myfile.jpg")).thenReturn(mockMeta);


        FitsResult mockFitsResult = Mockito.mock(FitsResult.class);
        //when(mockFitsResult.getFileMD5()).thenReturn("5d41402abc4b2a76b9719d911017c592");
        when(mockFitsResult.getFileSize()).thenReturn(12345L);
        when(mockFitsResult.getRawResult()).thenReturn("rawresultforfits");
        CompletableFuture<FitsResult> mockResult = Mockito.mock(CompletableFuture.class);
        when(mockResult.get()).thenReturn(mockFitsResult);
        when(fitsService.examine("/processing/myfile.jpg", "/processing/myfile.jpg")).thenReturn(mockResult);

        when(ingestConfig.getHostRelativeAPIUrl(anyString())).thenReturn("/my/host/relative/url");

        IngestPreProcessRunnable spyTestable = spy(testable);
        //spyTestable.setItem(mockProcessingItem);
        spyTestable.run(mockProcessingItem);

        // verify arguments to various methods/services
//        ArgumentCaptor<String> filenameCaptor = ArgumentCaptor.forClass(String.class);
//        verify(filenameMetaParser).parse(filenameCaptor.capture());
//        assertEquals("myfile.jpg", filenameCaptor.getValue());

        ArgumentCaptor<String> fitsPathCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> filePathCaptor = ArgumentCaptor.forClass(String.class);
        verify(fitsService).examine(fitsPathCaptor.capture(), filePathCaptor.capture());
        assertEquals("/processing/myfile.jpg", fitsPathCaptor.getValue());

        ArgumentCaptor<FileAPIMoveRequestBody> bodyCaptor = ArgumentCaptor.forClass(FileAPIMoveRequestBody.class);
        verify(slsFileAPI).move(bodyCaptor.capture());
        assertEquals("/my/host/relative/url", bodyCaptor.getValue().getCallbackUrl());
        assertEquals("sometoken", bodyCaptor.getValue().getToken());

        // verify the items change status as expected
        //verify(spyTestable).setProcessingItemStatusAndSend(ProcessingStatus.PROCESSING_FILENAME_METADATA, mockProcessingItem);
        verify(spyTestable).setProcessingItemStatusAndSend(ProcessingStatus.PROCESSING_METADATA, mockProcessingItem);
        verify(spyTestable).setProcessingItemStatusAndSend(ProcessingStatus.INIT_PROCESSING_USER_COPIES, mockProcessingItem);

        ArgumentCaptor<IngestProcessingItem> itemCaptor = ArgumentCaptor.forClass(IngestProcessingItem.class);
        ArgumentCaptor<ProcessingStatus> statusCaptor = ArgumentCaptor.forClass(ProcessingStatus.class);

        verify(spyTestable, times(3)).setProcessingItemStatusAndSend(statusCaptor.capture(), itemCaptor.capture());
        assertEquals(ProcessingStatus.PROCESSING_USER_COPIES, statusCaptor.getValue());
        assertEquals("sometoken", itemCaptor.getValue().getToken());
        //assertEquals(mockMeta, itemCaptor.getValue().getFilenameMetaData());
        //assertEquals("5d41402abc4b2a76b9719d911017c592", itemCaptor.getValue().getMd5ChecksumBefore());
        assertEquals(Long.valueOf(12345L), itemCaptor.getValue().getFileSize());
        assertEquals("rawresultforfits", itemCaptor.getValue().getRawFitsResult());
        assertEquals(ProcessingStatus.PROCESSING_USER_COPIES, itemCaptor.getValue().getProcessingStatus());

    }

    // fitsService examine does validation call, so test is not needed but leaving for a while until an equivalent tests exists for the examine method

//    @Test
//    void run_causesRejectMessageIfMd5NotReturnedFromFits() throws ExecutionException, InterruptedException, IOException, SAXException {
//
//        IngestProcessingItem mockProcessingItem = createMockIngestProcessingItem();
//
//        FilenameMetaData mockMeta = Mockito.mock(FilenameMetaData.class);
//        when(filenameMetaParser.parse("myfile.jpg")).thenReturn(mockMeta);
//
//
//        FitsResult mockFitsResult = Mockito.mock(FitsResult.class);
//        // return null should cause error
//        when(mockFitsResult.getFileMD5()).thenReturn(null);
//
//        when(mockFitsResult.getRawResult()).thenReturn("rawresultforfits");
//        CompletableFuture<FitsResult> mockResult = Mockito.mock(CompletableFuture.class);
//        when(mockResult.get()).thenReturn(mockFitsResult);
//        when(fitsService.examine("/processing/myfile.jpg", "/processing/myfile.jpg")).thenReturn(mockResult);
//
//        IngestPreProcessRunnable spyTestable = spy(testable);
//        spyTestable.setItem(mockProcessingItem);
//        IngestProcessingException e =  assertThrows(IngestProcessingException.class, () -> {
//            spyTestable.run();
//        });
//
//        verify(spyTestable).setProcessingItemStatusAndSend(ProcessingStatus.RETRY, mockProcessingItem);
//        assertEquals("FITS did not return valid MD5 checksum", e.getMessage());
//    }


//    @Test
//    void run_causesRejectMessageIfMd5DoesNotLookValid() throws ExecutionException, InterruptedException, IOException, SAXException {
//
//        IngestProcessingItem mockProcessingItem = createMockIngestProcessingItem();
//
//        FilenameMetaData mockMeta = Mockito.mock(FilenameMetaData.class);
//        when(filenameMetaParser.parse("myfile.jpg")).thenReturn(mockMeta);
//
//
//        FitsResult mockFitsResult = Mockito.mock(FitsResult.class);
//        // return string that does not look like MD5 should cause error
//        when(mockFitsResult.getFileMD5()).thenReturn("thisisnotreallyMD5");
//
//        when(mockFitsResult.getRawResult()).thenReturn("rawresultforfits");
//        CompletableFuture<FitsResult> mockResult = Mockito.mock(CompletableFuture.class);
//        when(mockResult.get()).thenReturn(mockFitsResult);
//        when(fitsService.examine("/processing/myfile.jpg", "/processing/myfile.jpg")).thenReturn(mockResult);
//
//        IngestPreProcessRunnable spyTestable = spy(testable);
//        spyTestable.setItem(mockProcessingItem);
//        IngestProcessingException e =  assertThrows(IngestProcessingException.class, () -> {
//            spyTestable.run();
//        });
//
//        verify(spyTestable).setProcessingItemStatusAndSend(ProcessingStatus.RETRY, mockProcessingItem);
//        assertEquals("FITS did not return valid MD5 checksum", e.getMessage());
//    }

//    @Test
//    void run_causesRejectMessageIfRawResultNotReturnedFromFits() throws ExecutionException, InterruptedException, IOException, SAXException {
//
//        IngestProcessingItem mockProcessingItem = createMockIngestProcessingItem();
//
//        FilenameMetaData mockMeta = Mockito.mock(FilenameMetaData.class);
//        when(filenameMetaParser.parse("myfile.jpg")).thenReturn(mockMeta);
//
//
//        FitsResult mockFitsResult = Mockito.mock(FitsResult.class);
//        // return null should cause error
//        when(mockFitsResult.getFileMD5()).thenReturn("5d41402abc4b2a76b9719d911017c592");
//        when(mockFitsResult.getFileSize()).thenReturn(12345L);
//        when(mockFitsResult.getRawResult()).thenReturn(null);
//        CompletableFuture<FitsResult> mockResult = Mockito.mock(CompletableFuture.class);
//        when(mockResult.get()).thenReturn(mockFitsResult);
//        when(fitsService.examine("/processing/myfile.jpg", "/processing/myfile.jpg")).thenReturn(mockResult);
//
//        IngestPreProcessRunnable spyTestable = spy(testable);
//        spyTestable.setItem(mockProcessingItem);
//        IngestProcessingException e = assertThrows(IngestProcessingException.class, () -> {
//            spyTestable.run();
//        });
//
//        verify(spyTestable).setProcessingItemStatusAndSend(ProcessingStatus.RETRY, mockProcessingItem);
//        assertEquals("FITS did not return the raw FITS result XML", e.getMessage());
//    }

//    @Test
//    void run_causesRejectMessageIfAToolResultIsMarkedAsFailedFromFits() throws ExecutionException, InterruptedException, IOException, SAXException {
//
//        IngestProcessingItem mockProcessingItem = createMockIngestProcessingItem();
//
//        FilenameMetaData mockMeta = Mockito.mock(FilenameMetaData.class);
//        when(filenameMetaParser.parse("myfile.jpg")).thenReturn(mockMeta);
//
//
//        FitsResult mockFitsResult = Mockito.mock(FitsResult.class);
//        // return null should cause error
//        when(mockFitsResult.getFileMD5()).thenReturn("5d41402abc4b2a76b9719d911017c592");
//        when(mockFitsResult.getFileSize()).thenReturn(12345L);
//        when(mockFitsResult.getRawResult()).thenReturn(null);
//        CompletableFuture<FitsResult> mockResult = Mockito.mock(CompletableFuture.class);
//        when(mockResult.get()).thenReturn(mockFitsResult);
//        when(fitsService.examine("/processing/myfile.jpg", "/processing/myfile.jpg")).thenReturn(mockResult);
//
//        IngestPreProcessRunnable spyTestable = spy(testable);
//        spyTestable.setItem(mockProcessingItem);
//        IngestProcessingException e = assertThrows(IngestProcessingException.class, () -> {
//            spyTestable.run();
//        });
//
//        verify(spyTestable).setProcessingItemStatusAndSend(ProcessingStatus.RETRY, mockProcessingItem);
//        assertEquals("FITS did not return the raw FITS result XML", e.getMessage());
//    }

    @Test
    void run_causesRejectOnHttpServerErrorException() throws ExecutionException, InterruptedException, IOException, SAXException {

        IngestProcessingItem mockProcessingItem = createMockIngestProcessingItem();

        FilenameMetaData mockMeta = Mockito.mock(FilenameMetaData.class);
        when(filenameMetaParser.parse("myfile.jpg")).thenReturn(mockMeta);

        when(fitsService.examine("/processing/myfile.jpg", "/processing/myfile.jpg")).thenThrow(HttpServerErrorException.class);

        IngestPreProcessRunnable spyTestable = spy(testable);
        //spyTestable.setItem(mockProcessingItem);
        HttpServerErrorException e = assertThrows(HttpServerErrorException.class, () -> {
            spyTestable.run(mockProcessingItem);
        });

        verify(spyTestable).setProcessingItemStatusAndSend(ProcessingStatus.RETRY, mockProcessingItem);
    }

    protected IngestProcessingItem createMockIngestProcessingItem(){
        IngestItem mockItem = Mockito.mock(IngestItem.class);
        when(mockItem.getName()).thenReturn("myfile.jpg");
        when(mockItem.getPath()).thenReturn("/processing/myfile.jpg");
        when(mockItem.getFitsPath()).thenReturn("/processing/myfile.jpg");

        IngestProcessingItem mockProcessingItem = Mockito.mock(IngestProcessingItem.class);
        when(mockProcessingItem.getItem()).thenReturn(mockItem);
        when(mockProcessingItem.getToken()).thenReturn("sometoken");
        doCallRealMethod().when(mockProcessingItem).setFilenameMetaData(any(FilenameMetaData.class));
        doCallRealMethod().when(mockProcessingItem).getFilenameMetaData();
        doCallRealMethod().when(mockProcessingItem).setMd5ChecksumBefore(anyString());
        doCallRealMethod().when(mockProcessingItem).getMd5ChecksumBefore();
        doCallRealMethod().when(mockProcessingItem).setFileSize(anyLong());
        doCallRealMethod().when(mockProcessingItem).getFileSize();
        doCallRealMethod().when(mockProcessingItem).setRawFitsResult(anyString());
        doCallRealMethod().when(mockProcessingItem).getRawFitsResult();
        doCallRealMethod().when(mockProcessingItem).setProcessingStatus(any(ProcessingStatus.class));
        doCallRealMethod().when(mockProcessingItem).getProcessingStatus();

        return mockProcessingItem;
    }
}