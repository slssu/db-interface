package fi.sls.ingest.fits.model;

import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.image.FitsImageMetadata;
import fi.sls.ingest.fits.model.fieldresolver.image.JHoveNISOImageMetadata;
import fi.sls.ingest.fits.model.fieldresolver.tool.TikaMetadata;
import fi.sls.ingest.fits.model.generated.Fits;

/**
 * Represents the FITS analysis result for a file that has been identified as image data
 */
public class FitsImageResult extends FitsResult {
    public FitsImageResult(Fits generatedResult) {
        super(generatedResult);
    }

    public FitsImageResult(Fits generatedResult, String rawResult) {
        super(generatedResult, rawResult);
    }


    /**
     * Target field in FileMaker: teknisk_data -> width
     *
     * @return
     */
    public Integer getImageWidth(){
        // Chain up filters. The outermost class gets first dibs or resolving the value
        FieldValueResolver parser = new FitsImageMetadata(
                FitsImageMetadata.FIELD_IMAGE_WIDTH,
                new JHoveNISOImageMetadata(JHoveNISOImageMetadata.FIELD_IMAGE_WIDTH, this)
        );

        String val = parser.getValue().orElse(null);

        if(val != null){
            return Integer.parseInt(val);
        } else {
            return null;
        }
    }

    /**
     * Target field in FileMaker: teknisk_data -> height
     *
     * @return
     */
    public Integer getImageHeight(){
        // Chain up filters. The outermost class gets first dibs or resolving the value
        FieldValueResolver parser = new FitsImageMetadata(
                FitsImageMetadata.FIELD_IMAGE_HEIGHT,
                new JHoveNISOImageMetadata(JHoveNISOImageMetadata.FIELD_IMAGE_HEIGHT, this)
        );

        String val = parser.getValue().orElse(null);

        if(val != null){
            return Integer.parseInt(val);
        } else {
            return null;
        }
    }

    /**
     * Target field in FileMaker: teknisk_data -> resolution
     *
     * @return
     */
    public Integer getXSamplingFrequency(){

        FieldValueResolver parser = new FitsImageMetadata(
                FitsImageMetadata.FIELD_X_SAMPLING_FREQUENCY,
                new TikaMetadata(TikaMetadata.FIELD_X_RESOLUTION, this)
        );

        String val = parser.getValue().orElse(null);

        if(val != null){
            // we are not interested in float values beyond this point,
            // but some of the tools might return the value as a float
            // so we round our way out of it.
            // FIXME: is this correct? Check FileMaker field definition once we have access again
            return Math.round(Float.parseFloat(val));
        } else {
            return null;
        }
    }

    /**
     * Currently only used as helper here
     *
     * @return
     */
    public String getBitsPerSample(){
        // Chain up filters. The outermost class gets first dibs or resolving the value
        FieldValueResolver parser = new FitsImageMetadata(
                FitsImageMetadata.FIELD_BITS_PER_SAMPLE,
                new JHoveNISOImageMetadata(JHoveNISOImageMetadata.FIELD_BITS_PER_SAMPLE, this)
        );

        return parser.getValue().orElse(null);

    }

    /**
     * Target field in FileMaker: teknisk_data -> bit_depth
     *
     * @return
     */
    public Integer getBithDepth(){

        String val = getBitsPerSample();

        if(val != null){
            // split by space and return first index, because some place the value is found in
            // have the format 8 8 8
            String[] split = val.split(" ");
            return Integer.parseInt(split[0]);
        }

        return null;
    }

    /**
     * Target field in FileMaker: teknisk_data -> color_mode
     *
     * @return
     */
    public String getColorSpace(){
        // Chain up filters. The outermost class gets first dibs or resolving the value
        FieldValueResolver parser = new FitsImageMetadata(
                FitsImageMetadata.FIELD_COLOR_SPACE,
                new JHoveNISOImageMetadata(JHoveNISOImageMetadata.FIELD_COLOR_SPACE, this)
        );

        return parser.getValue().orElse(null);
    }

    /**
     * Target field in FileMaker: teknisk_data -> color_profile
     *
     * @return
     */
    public String getIccProfileName(){
        FieldValueResolver parser = new FitsImageMetadata(FitsImageMetadata.FIELD_ICC_PROFILE_NAME, this);

        return parser.getValue().orElse(null);
    }

    /**
     * Target field in FileMaker: teknisk_data -> originalProcessingSoftware
     *
     * @return
     */
    public String getScanningSoftwareName(){
        FieldValueResolver parser = new FitsImageMetadata(FitsImageMetadata.FIELD_SCANNING_SOFTWARE_NAME, this);

        return parser.getValue().orElse(null);
    }

//    /**
//     * Fetches an element from the metadata->image structure of the Fits xml
//     *
//     * @override
//     */
//    protected Element findMetadataElementByName(String name){
//        Element rtn = null;
//
//        // lets try with the image tag first
//        if(generatedResult.getMetadata().getImage() != null){
//            Optional<Element> item = generatedResult.
//                    getMetadata().getImage().
//                    getAny().
//                    stream().
//                    filter(
//                            element ->
//                                    element.getTagName() == name
//
//                    ).findFirst();
//
//            if(item.isPresent()){
//                rtn = item.get();
//            }
//        }
//
//        return rtn;
//
//    }

}
