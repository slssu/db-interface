package fi.sls.ingest.proxy.filemaker.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class that wraps data for updating a record in the FileMaker database
 */
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateRequest extends CreateRequest {

    public UpdateRequest(RecordPayload fieldData) {
        this.fieldData = fieldData;
    }
}
