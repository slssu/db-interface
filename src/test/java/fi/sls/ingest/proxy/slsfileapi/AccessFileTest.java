package fi.sls.ingest.proxy.slsfileapi;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class AccessFileTest {

    @Test
    void getRoleTitle() {
        AccessFile testable = new AccessFile("accessfiles/kundkopior/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("Kundkopia", testable.getRoleTitle());

        testable = new AccessFile("accessfiles/databasbilder/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("Databasbild", testable.getRoleTitle());

        testable = new AccessFile("accessfiles/thumbnails/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("Thumbnail", testable.getRoleTitle());

        testable = new AccessFile("accessfiles/mellanmasters/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("Mellanmaster", testable.getRoleTitle());

        testable = new AccessFile("accessfiles/standardversioner/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("Standardversion", testable.getRoleTitle());
    }

    @Test
    void getRoleDescription() {
        AccessFile testable = new AccessFile("accessfiles/kundkopior/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertThat(testable.getRoleDescription(), Matchers.containsString("Kundkopian"));

        testable = new AccessFile("accessfiles/databasbilder/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertThat(testable.getRoleDescription(), Matchers.containsString("Databasbilden"));

        testable = new AccessFile("accessfiles/thumbnails/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertThat(testable.getRoleDescription(), Matchers.containsString("Thumbnailen"));

        testable = new AccessFile("accessfiles/mellanmasters/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertThat(testable.getRoleDescription(), Matchers.containsString("Mellanmastern"));

        testable = new AccessFile("accessfiles/standardversioner/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertThat(testable.getRoleDescription(), Matchers.containsString("Standardversion"));
    }

    @Test
    void getFilePathFolder() {
        AccessFile testable = new AccessFile("accessfiles/kundkopior/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("kundkopior", testable.getFilePathFolder());

        testable = new AccessFile("accessfiles/databasbilder/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("databasbilder", testable.getFilePathFolder());

        testable = new AccessFile("accessfiles/thumbnails/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("thumbnails", testable.getFilePathFolder());

        testable = new AccessFile("accessfiles/mellanmasters/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("mellanmasters", testable.getFilePathFolder());

        testable = new AccessFile("accessfiles/standardversioner/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("standardversioner", testable.getFilePathFolder());
        // TODO: test for invalid path
    }

    @Test
    void getFileType() {
        AccessFile testable = new AccessFile("accessfiles/databasbilder/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("image/jpeg", testable.getFileType());

    }

    @Test
    void getStrippedFilePath(){
        AccessFile testable = new AccessFile("accessfiles/databasbilder/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif", testable.getStrippedFilePath());

        testable = new AccessFile("accessfiles/databasbilder/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif", testable.getStrippedFilePath());

        testable = new AccessFile("accessfiles/thumbnails/Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif");
        assertEquals("Ingestverktyget/testsamling_4/sls_74_uppt_001_002.tif", testable.getStrippedFilePath());


    }
}