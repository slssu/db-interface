package fi.sls.ingest.manager;

import fi.sls.ingest.config.IngestConfig;
import fi.sls.ingest.exception.IngestFilenameException;
import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.exception.ProcessingItemNotFoundException;
import fi.sls.ingest.filesystem.filter.NoHiddenFilesFilter;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.manager.runnable.AccessFilesRunnable;
import fi.sls.ingest.manager.runnable.IngestStagingRunnable;
import fi.sls.ingest.proxy.slsfileapi.FileAPIResponseBody;
import fi.sls.ingest.queue.rabbitmq.sender.IngestItemSenderBase;
import fi.sls.ingest.queue.rabbitmq.sender.IngestPostProcessingItemSender;
import fi.sls.ingest.queue.rabbitmq.sender.IngestPreProcessingItemSender;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.representation.User;
import fi.sls.ingest.rest.IngestFile;
import fi.sls.ingest.security.AuthenticationFacade;
import fi.sls.ingest.security.jwt.UserPrincipal;
import fi.sls.ingest.ws.SocketMessenger;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.MetricsEndpoint;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * This service provides an abstraction layer between the various parts of the ingest process
 *
 *
 */
@Service
@Slf4j
public class IngestManager {

    AuthenticationFacade authenticationFacade;
    IngestConfig metaDataConfig;
    IngestPreProcessingItemSender preProcessingQueueSender;
    IngestPostProcessingItemSender postProcessingItemSender;
    IngestItemRepository ingestItemRepository;
    AccessFilesRunnable accessFilesRunnable;
    IngestStagingRunnable ingestStagingRunnable;
    ProcessingStatusTransitionHandler processingStatusTransitionHandler;
    MetricsEndpoint metricsEndpoint;

    /**
     * WebSocket used to push out updates on the processing state of the current item
     */
    SocketMessenger socketMessenger;

    /**
     * Service for correcting a ingestable filename before it is passed on in the process
     */
    private IngestFileRenamer fileRenamer;


    @Autowired
    public IngestManager(
            AuthenticationFacade authenticationFacade,
            IngestConfig metaDataConfig,
            IngestPreProcessingItemSender preProcessingQueueSender,
            IngestPostProcessingItemSender postProcessingItemSender,
            IngestItemRepository ingestItemRepository,
            SocketMessenger socketMessenger,
            AccessFilesRunnable accessFilesRunnable,
            IngestStagingRunnable ingestStagingRunnable,
            ProcessingStatusTransitionHandler processingStatusTransitionHandler,
            MetricsEndpoint metricsEndpoint,
            IngestFileRenamer fileRenamer){

        this.authenticationFacade = authenticationFacade;
        this.metaDataConfig = metaDataConfig;
        this.preProcessingQueueSender = preProcessingQueueSender;
        this.postProcessingItemSender = postProcessingItemSender;
        this.ingestItemRepository = ingestItemRepository;
        this.socketMessenger = socketMessenger;
        this.accessFilesRunnable= accessFilesRunnable;
        this.ingestStagingRunnable = ingestStagingRunnable;
        this.processingStatusTransitionHandler = processingStatusTransitionHandler;
        this.metricsEndpoint = metricsEndpoint;
        this.fileRenamer = fileRenamer;
    }

    /**
     * Fetch all processing items that have not been marked as archived
     * @return
     */
    public Page<IngestProcessingItem> getProcessingItems(Pageable pageable){

        return ingestItemRepository.findByProcessingStatusNot(ProcessingStatus.ARCHIVED, pageable);
    }

    /**
     * Fetch all processing items that match a given filter. If the filter is empty, fetch all items except archived
     *
     * @param filterBy
     * @return
     */
    public Page<IngestProcessingItem> getProcessingItems(String filterBy, Pageable pageable){

        if(filterBy.isEmpty()){
            return getProcessingItems(pageable);
        } else {
            List<ProcessingStatus> filterByStatus = ProcessingStatus.fromStatusListString(filterBy);
            return ingestItemRepository.findByProcessingStatusIn(filterByStatus, pageable);
        }
    }

    /**
     * Fetch all processing items that match a given filter and belong to a user in the userIdList. If the filter is emtpy, fetch all items except archived
     *
     * @param filterBy
     * @return
     */
    public Page<IngestProcessingItem> getProcessingItems(String filterBy, List<Long> userIdList, Pageable pageable){
        if(filterBy.isEmpty() && userIdList.isEmpty()){
            return getProcessingItems(pageable);
        } else if(!filterBy.isEmpty() && userIdList.isEmpty()) {
            return getProcessingItems(filterBy, pageable);
        } else if(filterBy.isEmpty() && !userIdList.isEmpty()) {
            return ingestItemRepository.findByProcessingStatusNotAndCreatedByIdIn(
                            ProcessingStatus.ARCHIVED,
                            userIdList,
                            pageable);
        } else {
            return ingestItemRepository.findByProcessingStatusInAndCreatedByIdIn(
                    ProcessingStatus.fromStatusListString(filterBy),
                    userIdList,
                    pageable);
        }
    }


    public Optional<IngestProcessingItem> getProcessingItem(String token){
        return ingestItemRepository.findByToken(token);
    }

    /**
     * Returns true if item was deleted, false otherwise
     * @param token
     * @return
     */
    public Boolean deleteProcessingItem(String token){
        Optional<IngestProcessingItem> item = this.getProcessingItem(token);

        if(item.isPresent()){
            IngestProcessingItem processingItem = item.get();
            processingStatusTransitionHandler.transitionToState(processingItem, ProcessingStatus.DELETED);

            ingestItemRepository.delete(processingItem);
            socketMessenger.broadcast(processingItem);
            log.debug("Deleted processing item: {} {}",
                    kv(LogMessageKey.INGEST_ITEM_TOKEN, processingItem.getToken()),
                    kv(LogMessageKey.FILE_PATH, processingItem.getItem().getFitsPath()),
                    kv(LogMessageKey.INGEST_PROCESSING_ITEM, processingItem.getLoggableItem())
            );
            return true;
        } else {
            log.warn("Item not found, unable to delete processing item with token: {}", kv(LogMessageKey.INGEST_ITEM_TOKEN, token));
            return false;
        }
    }

    /**
     * Removes all items that match the tokens
     *
     * @param tokens
     * @return
     */
    public void deleteProcessingItems(ArrayList<String> tokens){
        tokens.forEach((token) -> {
            deleteProcessingItem(token);
        });
    }

    public long getNumStatusChangeableProcessingItems(ProcessingStatus fromStatus, ProcessingStatus toStatus, List<Long> userIds) {

        // do a dry run for a state change to see if it is even allowed
        boolean isAllowed = processingStatusTransitionHandler.isStateTransitionAllowed(fromStatus, toStatus);

        if(userIds.isEmpty()) {
            return ingestItemRepository.countByProcessingStatus(fromStatus);
        } else {
            return ingestItemRepository.countByProcessingStatusAndUser(fromStatus, userIds);
        }
    }


    /**
     * Change the status of a single item based on token
     * @param token
     * @param newStatus
     * @return
     */
    public Boolean changeProcessingItemStatus(String token, ProcessingStatus newStatus){
        Optional<IngestProcessingItem> item = this.getProcessingItem(token);

        if(item.isPresent()){
            IngestProcessingItem processingItem = item.get();

            processingStatusTransitionHandler.transitionToState(processingItem, newStatus);

            //processingItem.setProcessingStatus(newStatus);
            ingestItemRepository.save(processingItem);
            socketMessenger.broadcast(processingItem);
            log.debug("Changed processing item status: {} {} {}",
                    kv(LogMessageKey.INGEST_ITEM_TOKEN, processingItem.getToken()),
                    kv("ingest_item_processing_status", processingItem.getProcessingStatus()),
                    kv(LogMessageKey.FILE_PATH, processingItem.getItem().getFitsPath()),
                    kv(LogMessageKey.INGEST_PROCESSING_ITEM, processingItem.getLoggableItem())
            );
            return true;
        } else {
            log.warn("Item not found, unable to change status of processing item with token: {}", kv(LogMessageKey.INGEST_ITEM_TOKEN, token));
            return false;
        }
    }

    /**
     * Change the status of a batch of items based on their tokens
     *
     * @param tokens
     * @return
     */
    public void changeProcessingItemStatus(ArrayList<String> tokens, ProcessingStatus newStatus){
        tokens.forEach((token) -> {
            changeProcessingItemStatus(token, newStatus);
        });
    }


    /**
     * Change all items having the given fromStatus into the given toStatus, limited to the list of userIds
     *
     * @param fromStatus
     * @param toStatus
     * @param userIds
     */
    public void changeProcessingItemStatus(ProcessingStatus fromStatus, ProcessingStatus toStatus, List<Long> userIds) {
        Page<IngestProcessingItem> page;
        do {
            Pageable p = PageRequest.of(0, 50);

            if(userIds.isEmpty()) {
                page = ingestItemRepository.findByProcessingStatusIn(Arrays.asList(fromStatus), p);
            } else {
                page = ingestItemRepository.findByProcessingStatusInAndCreatedByIdIn(Arrays.asList(fromStatus), userIds, p);
            }

            page.stream().forEach(token -> {
                changeProcessingItemStatus(token.getToken(), toStatus);
            });

        } while(page.hasNext());
    }

    /**
     * Deletes all processing items having the given status and belonging to given userIds
     *
     * If userIds is empty, all items with given status are deleted regardless of owner
     *
     * @param fromStatus
     * @param userIds
     */
    public void deleteItemsWithProcessingStatus(ProcessingStatus fromStatus, List<Long> userIds){

        Page<IngestProcessingItem> page;
        do {
            Pageable p = PageRequest.of(0, 50);

            if(userIds.isEmpty()) {
                page = ingestItemRepository.findByProcessingStatusIn(Arrays.asList(fromStatus), p);
            } else {
                page = ingestItemRepository.findByProcessingStatusInAndCreatedByIdIn(Arrays.asList(fromStatus), userIds, p);
            }

            page.stream().forEach(token -> {
                deleteProcessingItem(token.getToken());
            });

        } while(page.hasNext());
    }

    /**
     * Removes all items from the Ingest item repository
     */
    public void clearQueue(){
        ingestItemRepository.deleteAll();
    }


    /**
     * Iterates each item and checks if it's a valid file path on the server.
     *
     * If yes, the item is sent to the processing queue for further processing eventually.
     * If no, the item is marked as REJECTED and not processed further.
     *
     * @param items
     * @param digitizationProfile
     * @return A list of processing tokens with the status of each item (SEND_TO_PRE_PROCESSING_QUEUE or REJECTED)
     */
    public List<IngestProcessingItem> processItems(List<IngestItem> items, IngestDigitizationProfile digitizationProfile){

        ArrayList<IngestProcessingItem> processingTokens = new ArrayList<>();

        items.forEach((item -> {
            // check if the path exists and is a folder or file on the Ingest interface system
            File f = new File(item.getPath());
            processItemPath(f, processingTokens, digitizationProfile);
        }));

        return processingTokens;
    }

    protected void processItemPath(File f, ArrayList<IngestProcessingItem> processingTokens, IngestDigitizationProfile digitizationProfile){
        if(f.isDirectory()){
            log.debug("Processing directory: {} ", kv("directory_path", f.getAbsolutePath()));
            // iterate the contents of the directory and add each file in it to the processing list,
            // do not recurse into sub folders
            NoHiddenFilesFilter ff = new NoHiddenFilesFilter();
            for (File file : f.listFiles(ff)) {
                if (file.isFile()) {
                    processingTokens.add(createAndQueueProcessingItem(file, digitizationProfile));
                    log.debug("File added to processing queue: {} ", kv(LogMessageKey.FILE_PATH, file.getAbsolutePath()), kv("directory_path", f.getAbsolutePath()));
                } else if (file.isDirectory()) {
                    // recurse into subdirs
                    processItemPath(file, processingTokens, digitizationProfile);
                } else {
                    processingTokens.add(new IngestProcessingItem(
                            ProcessingStatus.REJECTED,
                            String.format("Path '%s' does not point to an existing file", file.getAbsolutePath())
                    ));
                    log.debug("Rejected file, path does not point to an existing file: {} ", kv(LogMessageKey.FILE_PATH, file.getAbsolutePath()), kv("directory_path", f.getAbsolutePath()));
                }
            }

        } else if(f.isFile()){
            // add file to processing directly, just in case we use the validated file to create
            // the processing item
            processingTokens.add(createAndQueueProcessingItem(f, digitizationProfile));
            log.debug("File added to processing queue: {} ", kv(LogMessageKey.FILE_PATH, f.getAbsolutePath()));
        } else {
            processingTokens.add(new IngestProcessingItem(
                    ProcessingStatus.REJECTED,
                    String.format("Path '%s' does not point to an existing file or directory", f.getAbsolutePath())
            ));
            log.debug("Rejected file, path does not point to an existing file or directory: {} ", kv(LogMessageKey.FILE_PATH, f.getAbsolutePath()));
        }
    }

    /**
     * Start preprocessing items, limit items to the ones created
     * by the users in the userIdList.
     *
     * @param userIdList
     */
    public void preProcessItems(List<Long> userIdList){

        List<IngestProcessingItem> items = ingestItemRepository.findByProcessingStatusInAndCreatedByIdIn(
                Arrays.asList(ProcessingStatus.STAGING),
                userIdList
        );

        items.forEach((item) -> {
            item.setProcessingStatus(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);
            preProcessingQueueSender.send(item);
        });
    }

    /**
     * Send the item identified by itemHash to the post processing queue.
     *
     * @param token The hash to be found in the token store.
     */
    public void postProcessItem(String token, FileAPIResponseBody processingResult){
        Optional<IngestProcessingItem> item = getProcessingItem(token);

        if(item.isPresent()){
            IngestProcessingItem storedItem = item.get();
            storedItem.setProcessingStatus(ProcessingStatus.PROCESSING_USER_COPIES_COMPLETE);
            // File API only returns OK if it was able to calculate checksums both before and after
            // so the values will always match
            storedItem.setMd5ChecksumBefore(processingResult.getMd5());
            storedItem.setMd5ChecksumAfter(processingResult.getMd5());
            storedItem.setFileAPIResponse(processingResult);
            ingestItemRepository.save(storedItem);

            queueItem(storedItem, postProcessingItemSender);
        } else {
            throwItemNotFoundException(token);
        }
    }

    /**
     * Send the item identified by itemHash to the post processing queue.
     *
     * @param token The hash to be found in the token store.
     */

    public void postProcessItem(String token){
        Optional<IngestProcessingItem> item = getProcessingItem(token);

        if(item.isPresent()){
            IngestProcessingItem storedItem = item.get();
            postProcessItem(storedItem.getToken(), storedItem.getFileAPIResponse());
        } else {
            throwItemNotFoundException(token);
        }
    }

    /**
     * Send a list of items to the post processing queue
     * @param tokens
     */
    public void postProcessItems(ArrayList<String> tokens){
        tokens.forEach((token) -> {
            this.postProcessItem(token);
        });
    }

    public void retryAccessFiles(String token){
        Optional<IngestProcessingItem> item = getProcessingItem(token);

        if(item.isPresent()){
            IngestProcessingItem storedItem = item.get();

            //accessFilesRunnable.setItem(storedItem);
            accessFilesRunnable.run(storedItem);

        } else {
            throwItemNotFoundException(token);
        }
    }

    /**
     * Retry processing an item in case we had an error for some reason
     *
     * @param token
     */
    public void retryProcessItem(String token){
        Optional<IngestProcessingItem> item = getProcessingItem(token);

        if(item.isPresent()){
            IngestProcessingItem storedItem = item.get();
            // determine state of item to know what the next step should be
            if(storedItem.getFilenameMetaData() == null || storedItem.getRawFitsResult() == null || storedItem.getFileAPIResponse() == null){
                storedItem.setProcessingStatus(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);
                queueItem(storedItem, preProcessingQueueSender);
            } else {
                postProcessItem(storedItem.getToken(), storedItem.getFileAPIResponse());
            }
        } else {
            throwItemNotFoundException(token);
        }
    }

    /**
     * Send a list of items to the post processing queue
     * @param tokens
     */
    public void retryProcessItems(ArrayList<String> tokens){
        tokens.forEach((token) -> {
            this.retryProcessItem(token);
        });
    }

    /**
     * Marks the item as being rejected
     *
     * @param token
     * @param reason
     */
    public void rejectProcessingItem(String token, String reason){
        Optional<IngestProcessingItem> item = getProcessingItem(token);

        if(item.isPresent()){
            IngestProcessingItem storedItem = item.get();
            storedItem.setProcessingStatus(ProcessingStatus.REJECTED);
            storedItem.setMessage(reason);
            ingestItemRepository.save(storedItem);

            socketMessenger.broadcast(storedItem);

        } else {
            throwItemNotFoundException(token);
        }

    }

    protected IngestProcessingItem createAndQueueProcessingItem(File file, IngestDigitizationProfile digitizationProfile){
        return createAndQueueProcessingItem(file, digitizationProfile, null);
    }

    /**
     * Create a new IngestProcessingItem instance and send to queue for further processing eventually.
     *
     * @param file
     * @param digitizationProfile
     * @return
     */
    protected IngestProcessingItem createAndQueueProcessingItem(File file, IngestDigitizationProfile digitizationProfile, IngestProcessingItemFlag flag){
        IngestItem processable = IngestFile.fromFile(file, metaDataConfig.getInboxPath());
        IngestProcessingItem queueItem = new IngestProcessingItem(
                processable,
                processable.getPathHash(),
                ProcessingStatus.PRE_STAGING,
                digitizationProfile
        );

        // link the current logged in user to the new item as the creator
        Authentication currentAuth = authenticationFacade.getAuthentication();
        User u = ((UserPrincipal) currentAuth.getPrincipal()).getUser();
        queueItem.setCreatedBy(u);

        // if a flag was passed, store it with the item
        if(flag != null){
            queueItem.addErrorFlag(flag);
        }

        // catch duplicate key exceptions here, because it means the user is trying to add a file that has already been saved
        // to the DB and thus might have been processed already once.
        try{
            ingestItemRepository.save(queueItem);
        } catch(DataIntegrityViolationException e){
            log.warn("Attempted to add a file that already has a token in ingest database: {} {}",
                        queueItem.getToken(),
                        processable.getFitsPath(),
                        kv(LogMessageKey.INGEST_ITEM_TOKEN, queueItem.getToken()),
                        kv(LogMessageKey.FILE_PATH, processable.getFitsPath())
                    );
            throw new IngestProcessingException(
                    String.format("File token already exists in ingest database and the file has probably already been processed. Please manually check status of file: %s (%s)",
                        processable.getFitsPath(),
                        queueItem.getToken()
                    )
            );
        }

        // attempt to process and if it fails, try renaming file then process again
        String filename = processable.getName();
        try {
            //ingestStagingRunnable.setItem(queueItem);
            ingestStagingRunnable.run(queueItem);
        } catch (IngestFilenameException e){
            // try to rename file (will fail if filename does not work at all)
            File newFile = null;
            try {
                newFile = fileRenamer.rename(processable);

                // a file with the same name inside an accessfiles subfolder should also be renamed
                renameAccessFileIfExist(processable, newFile);

                // move worked, so lets re-queue and delete old item from queue, add flag to give user notice of move
                createAndQueueProcessingItem(newFile, digitizationProfile,
                        new IngestProcessingItemFlag(IngestProcessingItemFlag.Severity.WARNING,
                                String.format("File renamed from %s to %s", filename, newFile.getName())
                        )
                );
                deleteProcessingItem(queueItem.getToken());
            }
            catch (IngestFilenameException ex){
                // filename still wrong, so reject the item
                rejectProcessingItem(queueItem.getToken(), ex.getMessage());
            } catch (IOException ex) {
                throw new IngestProcessingException(
                        String.format("Could not process file with path (token) = %s (%s), reason: %s",
                                processable.getFitsPath(),
                                queueItem.getToken(),
                                ex.getMessage()
                        )
                );
            }
        }

        return queueItem; //queueItem(queueItem, preProcessingQueueSender);
    }

    /**
     * Checks if a file with the same original name exists in a subfolder
     * to the original file called accessfiles
     *
     * @param processable
     * @param newFile
     */
    protected void renameAccessFileIfExist(IngestItem processable, File newFile) throws IOException {
        File origFile = processable.getFileCopy();
        String parent = origFile.getParent().concat("/accessfiles");

        File accessFileSource = new File(parent, origFile.getName());

        if(accessFileSource.exists()){
            File newAccessFile = new File(accessFileSource.getParent(), newFile.getName());
            FileUtils.moveFile(accessFileSource, newAccessFile);
            log.debug("Moved access file source to new name: {}, {}",
                    kv("original_access_file_source", accessFileSource.getAbsolutePath()),
                    kv("new_access_file_source", newAccessFile.getAbsolutePath())
            );
        }
    }

    /**
     * Send the given processing item to the queue for further processing
     * @param item
     * @return
     */
    protected IngestProcessingItem queueItem(IngestProcessingItem item, IngestItemSenderBase queueSender){

        try{
            item = queueSender.send(item);
        } catch (IngestProcessingException e){
            item.setProcessingStatus(ProcessingStatus.REJECTED);
            item.setMessage(e.getMessage());
            ingestItemRepository.save(item);
        }

        socketMessenger.broadcast(item);

        return item;
    }

    /**
     * Helper to throw an exception for when an item with a given token cannot be found in the processing queue
     *
     * @param token
     */
    protected void throwItemNotFoundException(String token){
        log.warn("Item not found in processing queue store: {}", kv(LogMessageKey.INGEST_ITEM_TOKEN, token));
        String err = String.format("Item with processing token %s could not be found in queue store.", token);
        throw new ProcessingItemNotFoundException(err);
    }

}
