package fi.sls.ingest.migration;

import com.fasterxml.jackson.annotation.JsonInclude;
import fi.sls.ingest.controller.request.IngestMigrationRequest;
import fi.sls.ingest.manager.ProcessingStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Wrapper for the overall result of a filename migration run
 *
 * Stores the parameters used for starting the migration.
 */
@Data
@NoArgsConstructor
public class MigrationResult implements Serializable {

    /**
     * Name of the top level collection that was processed
     */
    protected String collectionName;

    /**
     * The collection number (archiveNr) that was processed
     */
    protected Long archiveNr;

    /**
     * True if this was a dry run migration, i.e. no actual data was changed
     */
    protected Boolean isDryRun;

    public Long getNumSuccess(){
        return itemResults.stream().filter((item) -> item.getProcessingStatus().equals(ProcessingStatus.COMPLETE)).count();
    }

    public Long getNumErrors(){
        return itemResults.stream().filter((item) -> !item.getProcessingStatus().equals(ProcessingStatus.ERROR)).count();
    }

    /**
     * List of migration results for the individually found digital objects
     */
    protected List<DigitalObjectMigrationResult> itemResults;

    public MigrationResult(String collectionName, Long archiveNr, Boolean isDryRun){
        this.collectionName = collectionName;
        this.archiveNr = archiveNr;
        this.isDryRun = isDryRun;

        itemResults = new ArrayList<>();
    }
}
