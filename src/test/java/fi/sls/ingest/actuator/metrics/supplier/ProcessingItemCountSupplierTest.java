package fi.sls.ingest.actuator.metrics.supplier;

import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.repository.IngestItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ProcessingItemCountSupplierTest {

    ProcessingItemCountSupplier testable;
    IngestItemRepository repository;

    @BeforeEach
    void setUp(){
        repository = mock(IngestItemRepository.class);
        when(repository.count()).thenReturn(12L);
        when(repository.countByProcessingStatus(ProcessingStatus.COMPLETE)).thenReturn(2L);
        when(repository.countByProcessingStatus(ProcessingStatus.ARCHIVED)).thenReturn(10L);
    }

    @Test
    void get_returnsTotalCountIfFilterStatusNull() {
        testable = new ProcessingItemCountSupplier(repository);

        assertEquals(12L, testable.get());
    }

    @Test
    void get_returnsCorrectCountForComplete() {
        testable = new ProcessingItemCountSupplier(repository, ProcessingStatus.COMPLETE);
        assertEquals(2L, testable.get());
    }

    @Test
    void get_returnsCorrectCountForArchived() {
        testable = new ProcessingItemCountSupplier(repository, ProcessingStatus.ARCHIVED);
        assertEquals(10L, testable.get());
    }
}