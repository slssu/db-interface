package fi.sls.ingest.filesystem.filter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class NoHiddenFilesFilterTest {

    NoHiddenFilesFilter testable;

    @BeforeEach
    void setUp(){
        testable = new NoHiddenFilesFilter();
    }

    @Test
    void accept_returnsFalseForHiddenFile() {
        File mock = mock(File.class);

        when(mock.isHidden()).thenReturn(true);

        assertFalse(testable.accept(mock));

    }

    @Test
    void accept_returnsTrueForNonHiddenFile() {
        File mock = mock(File.class);

        when(mock.isHidden()).thenReturn(false);

        assertTrue(testable.accept(mock));

    }
}