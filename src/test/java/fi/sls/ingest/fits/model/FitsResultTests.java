package fi.sls.ingest.fits.model;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class FitsResultTests extends FitsResultTestsBase{

    String jpgWithMeta = "metadata-extractor-output/image/fmi_485.1_foto_002_001.jpg.fits.xml";
    String noMeta = "metadata-extractor-output/ota_582_av_030_001.mp4.fits_no_meta.xml";
    String tiffWithMeta = "metadata-extractor-output/image/fmi_484.1_foto_001_001.tif.fits.xml";
    String audioWithMeta = "metadata-extractor-output/audio/slsa_1110_ljud_001_001.wav.fits.xml";
    String videoWithMeta = "metadata-extractor-output/video/slsa_1390_av_018_001.mkv.fits.xml";
    String videoWithMeta2 = "metadata-extractor-output/video/slsa_1181_av_001_001.mkv.fits.xml";
    String audioWithFailedToolResult = "metadata-extractor-output/audio/sls_2339_ljud_012_001_failed.fits.xml";

    @Test
    public void createFromFits_canIdentifyFormatClass() throws IOException {

        FitsResult testable = FitsResult.createFromFits(loadExampleXML(jpgWithMeta));
        assertNotNull(testable);
        assertTrue(testable instanceof FitsImageResult);
    }

    @Test
    public void createFromFits_canCreateWithRawResult() throws IOException {

        FitsResult testable = FitsResult.createFromFits(loadExampleXML(jpgWithMeta), "myrawxml");
        assertNotNull(testable);
        assertTrue(testable instanceof FitsImageResult);
        assertEquals("myrawxml", testable.getRawResult());
    }


    @Test
    public void createFromFits_canCreateAudioResult() throws IOException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioWithMeta));

        assertNotNull(testable);
        assertThat(testable, instanceOf(FitsResult.class));
    }

    @Test
    public void createFromFits_canCreateVideoResult() throws IOException {
        FitsVideoResult testable = (FitsVideoResult) FitsResult.createFromFits(loadExampleXML(videoWithMeta));

        assertNotNull(testable);
        assertThat(testable, instanceOf(FitsResult.class));
    }

    @Test
    public void getIdentityFormat() throws IOException {
        FitsResult testable = FitsResult.createFromFits(loadExampleXML(jpgWithMeta));

        assertEquals("JPEG EXIF", testable.getIdentityFormat());
    }

    @Test
    public void getExternalIdentifier_returnsValid() throws IOException {
        FitsResult testable = FitsResult.createFromFits(loadExampleXML(tiffWithMeta));

        assertEquals("Tagged Image File Format", testable.getIdentityFormat());
    }

    @Test
    public void getExternalIdentifier_missingReturnsNull() throws IOException {
        FitsResult testable = FitsResult.createFromFits(loadExampleXML(jpgWithMeta));

        assertNull(testable.getExternalIdentifier());
    }

    @Test
    public void getModified_returnsValid() throws IOException {
        FitsResult testable = FitsResult.createFromFits(loadExampleXML(tiffWithMeta));
        assertEquals("2018-10-22T20:28:54", testable.getModified());

        testable = FitsResult.createFromFits(loadExampleXML(videoWithMeta2));
        assertEquals("2019-09-17T11:26:50", testable.getModified());
    }

    @Test
    public void getModified_missingReturnsNull() throws IOException {
        FitsResult testable = FitsResult.createFromFits(loadExampleXML(noMeta));

        assertNull(testable.getModified());
    }

    @Test
    public void hasFailedToolResults_returnsFalseForValidXml() throws IOException {
        FitsResult testable = FitsResult.createFromFits(loadExampleXML(jpgWithMeta));
        assertFalse(testable.hasFailedToolResults());
    }

    @Test
    public void hasFailedToolResults_returnsTrueForInvalidXml() throws IOException {
        FitsResult testable = FitsResult.createFromFits(loadExampleXML(audioWithFailedToolResult));
        assertTrue(testable.hasFailedToolResults());
    }

}
