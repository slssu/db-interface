package fi.sls.ingest.manager.step;

import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.Collection;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import fi.sls.ingest.representation.file.FilenameMetaData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

/**
 * Provides implementation of the ingest processing steps needed to create or update
 * a SLS Collection (samling) in FileMaker. E.g. SLS, SLSA, ZTS, FMA
 */
@Slf4j
@Service
public class IngestCollectionStep extends RecordServiceStep{

    @Autowired
    public IngestCollectionStep(RecordService service){
        super(service);
    }

    /**
     * Performs the tasks of this step
     *
     * @param metaData
     * @return
     */
    public Collection executeStep(FilenameMetaData metaData) throws IOException {

        Collection collection = new Collection();

        // We use collection description if defined because some collections have been defined
        // with a different pattern compared to others in the collection table in FileMaker
        collection.getFieldData().setCollectionName(metaData.getCollectionDescriptionOrName());
        collection.getFieldData().setArchiveNumber(metaData.getArchiveNrDigit());
        collection.getFieldData().setArchiveNumberSuffix(metaData.getArchiveNrSuffix());

        Optional<? extends FileMakerResponseEntity> existingItem;
// jw 2022-02-14: removing exception handling from step, as runnable will catch and handle all exceptions
        //        try {
            existingItem = service.findRecordByIdentifier(collection);
            if(existingItem.isPresent()){
                collection = (Collection) existingItem.get();
                log.info("Collection with identifier {} {} {} already exists with number {}",
                        metaData.getCollectionDescriptionOrName(),
                        metaData.getArchiveNrDigit(),
                        metaData.getArchiveNrSuffix(),
                        collection.getFieldData().getEntityNumber()
                );
            } else {

                collection = (Collection) service.createObjectRecord(collection);

                log.info("Created new collection with identifier {} {} {} having number {}",
                        metaData.getCollectionDescriptionOrName(),
                        metaData.getArchiveNrDigit(),
                        metaData.getArchiveNrSuffix(),
                        collection.getFieldData().getEntityNumber()
                );
            }
            return collection;
//        } catch (IOException e) {
//            log.error(e.toString());
//            return null;
//        }
    }
}
