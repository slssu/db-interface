package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.request.parameters.SortOrder;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.representation.file.FilenameMetaData;
import lombok.Data;
import lombok.Getter;

@Data
public class IntellectualEntity extends FileMakerResponseEntity {

    @Getter
    @JsonIgnore
    private String layoutName = "ingestapi_intellectualEntities";

    IntellectualEntityFieldData fieldData;

    public IntellectualEntity(){
        this.fieldData = new IntellectualEntityFieldData();
    }

    /**
     * Utility method that calls the same method on the fieldData object
     *
     * @param filenameMetaData
     * @param fitsResult
     */
    public void populateFromMetadataResults(FilenameMetaData filenameMetaData, FitsResult fitsResult){
        this.fieldData.populateFromMetadataResults(filenameMetaData, fitsResult);
    }

    @Override
    public FindQueryRequest createFindQueryRequest() {
        return new FindQueryRequest("entity_identifier", "="+getFieldData().getEntityIdentifier());
    }

    @Override
    public FindQueryRequest createFindQueryRequest(boolean ignoreNullFields, Long pageSize, Long pageNr){
        if(ignoreNullFields == false){
            return createFindQueryRequest();
        } else {
            FindQueryRequest queryRequest = new FindQueryRequest();

            if(pageSize != null && pageSize > 0){
                queryRequest.setLimit(pageSize.toString());
            }

            if(pageNr != null && pageNr > 0){
                // calculate real offset
                Long offset = Integer.parseInt(queryRequest.getLimit()) * pageNr + 1;
                queryRequest.setOffset(offset.toString());
            }

            if(getFieldData().getEntityIdentifier() != null){
                queryRequest.addQueryItem("entity_identifier", "="+getFieldData().getEntityIdentifier());
                queryRequest.addSortItem("entity_identifier", SortOrder.ASCENDING);
            }

            return queryRequest;

        }
    }

    @Override
    public UpdateRequest createUpdateRequest() {
        return new UpdateRequest(getFieldData());
    }

    @Override
    public CreateRequest createRecordRequest() {
        return new CreateRequest(getFieldData());
    }
}
