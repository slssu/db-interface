package fi.sls.ingest.rest;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;

/**
 * Defines request body for processing a list of tokens.
 */
@Data
public class ListTokensRequestBody {

    @NotEmpty
    @Valid
    ArrayList<String> tokens = new ArrayList<>();

}
