package fi.sls.ingest.proxy.filemaker.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileMakerErrorResponse extends FileMakerResponse {

    private FileMakerError response;

}
