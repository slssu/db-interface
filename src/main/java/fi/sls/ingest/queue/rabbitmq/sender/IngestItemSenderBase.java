package fi.sls.ingest.queue.rabbitmq.sender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.SocketMessenger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Slf4j
public abstract class IngestItemSenderBase {

    /**
     * Template to use for sending the item to the queue
     */
    protected RabbitTemplate template;

    /**
     * The queue to which we send the item
     */
    protected Queue queue;

    /**
     * JSON mapper for converting the processing item to a json string
     */
    ObjectMapper objectMapper;

    /**
     * Storage for storing the item so we can poll the status of it in client apps.
     */
    IngestItemRepository ingestItemRepository;

    /**
     * WebSocket used to push out updates on the processing state of the current item
     */
    SocketMessenger socketMessenger;

    public IngestItemSenderBase(RabbitTemplate template, Queue queue, ObjectMapper objectMapper, SocketMessenger socketMessenger, IngestItemRepository ingestItemRepository){
        this.template = template;
        this.queue = queue;
        this.objectMapper = objectMapper;
        this.socketMessenger = socketMessenger;
        this.ingestItemRepository = ingestItemRepository;
    }

    public abstract IngestProcessingItem send(IngestProcessingItem item) throws IngestProcessingException;

    /**
     * Sends our ingest item to the queue for processing.
     *
     * @param item The IngestProcessingItem to send to the queue.
     * @return The item with updated processing status field.
     */
    public IngestProcessingItem send(IngestProcessingItem item, ProcessingStatus newStatus) throws IngestProcessingException {

        // check that an item with pathHash is not already in queue
        Optional<IngestProcessingItem> storedItem = ingestItemRepository.findByToken(item.getToken());

        if(storedItem.isPresent() && storedItem.get().getProcessingStatus() == newStatus){
            String err = String.format("Item with token %s and status %s already queued for processing, will not add again until old item is removed from queue", item.getToken(), item.getProcessingStatus());
            log.info("Item with token and status already queued, will not add again: {} {}",
                    kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                    kv("item_status", item.getProcessingStatus()),
                    kv("item_status_new", newStatus),
                    kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
            );
            throw new IngestProcessingException(err);
        } else if(storedItem.isEmpty()){
            log.error("Could not find stored item with token: {}",
                    kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                    kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
            );
            throw new IngestProcessingException(String.format("Could not find stored item with token: %s", item.getToken()));

        }

        log.info("Sending item with token to queue: {} {}",
                kv("item_token", item.getToken()),
                kv("queue_name", queue.getName()),
                kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
        );
        String msg = null;
        try {
            // convert the item to a json string for storage in queue
            msg = objectMapper.writeValueAsString(item);
        } catch (JsonProcessingException e) {
            log.error("Failed to write json string: {}", e.getMessage(), kv("exception", e));
            throw new IngestProcessingException(e.getMessage());
        }

        // use the stored item to be sure we have latest info
        item = storedItem.get();
        item.setProcessingStatus(newStatus);
        ingestItemRepository.save(item);

        // Rabbit MQ can't be polled for an item without removing the item from the queue, so this is fire and forget :(
        // This is why we store the item in a store as well
        doSendToQueue(msg, item);
//        template.convertAndSend(queue.getName(), msg);

        // send update to clients
        socketMessenger.broadcast(item);

        return item;
    }

    /**
     * Separate method to provide override point for queue selection
     * @param msg
     */
    protected void doSendToQueue(String msg, IngestProcessingItem item){
        // Rabbit MQ can't be polled for an item without removing the item from the queue, so this is fire and forget :(
        // This is why we store the item in a store as well
        template.convertAndSend(queue.getName(), msg);
    }
}
