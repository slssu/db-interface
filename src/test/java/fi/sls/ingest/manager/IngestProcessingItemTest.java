package fi.sls.ingest.manager;

import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import fi.sls.ingest.proxy.slsfileapi.FileAPIResponseBody;
import fi.sls.ingest.representation.file.FilenameMetaData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class IngestProcessingItemTest {

    IngestProcessingItem testable;
    FileAPIResponseBody fileAPIResponseBody;

    @BeforeEach
    void setUp(){

        fileAPIResponseBody = mock(FileAPIResponseBody.class);

        testable = new IngestProcessingItem();
        testable.setFileAPIResponse(fileAPIResponseBody);
    }

    @Test
    void constructors_workAsExpected(){
        IngestProcessingItem t = new IngestProcessingItem();
        assertNull(t.getToken());

        t = new IngestProcessingItem(ProcessingStatus.REJECTED, "no good");
        assertEquals(ProcessingStatus.REJECTED, t.getProcessingStatus());
        assertEquals("no good", t.getErrors().get(0).getMessage());

        IngestItem item = mock(IngestItem.class);

        t = new IngestProcessingItem(item, "mytoken", ProcessingStatus.STAGING);
        assertEquals(ProcessingStatus.STAGING, t.getProcessingStatus());
        assertEquals(item, t.getItem());
        assertEquals("mytoken", t.getToken());

        IngestDigitizationProfile profile = mock(IngestDigitizationProfile.class);
        t = new IngestProcessingItem(item, "mytoken", ProcessingStatus.STAGING, profile);
        assertEquals(ProcessingStatus.STAGING, t.getProcessingStatus());
        assertEquals(item, t.getItem());
        assertEquals("mytoken", t.getToken());
        assertEquals(profile, t.getDigitizationProfile());

    }

    @Test
    void getCustomerCopyFilePath_isEmptyIfNullAccessFiles(){
        testable.setFileAPIResponse(null);

        assertEquals("", testable.getCustomerCopyFilePath());
    }

    @Test
    void getCustomerCopyFilePath_isEmptyIfNoAccessFiles() {
        when(fileAPIResponseBody.getAccessfilePaths()).thenReturn(new ArrayList<>());

        assertEquals("", testable.getCustomerCopyFilePath());
    }

    @Test
    void getCustomerCopyFilePath_returnsPathIfFound(){

        List<String> files = new ArrayList<>();
        files.add("accessfiles/thumbnails/somefile.jpg");
        files.add("accessfiles/kundkopior/anotherfile.jpg");


        when(fileAPIResponseBody.getAccessfilePaths()).thenReturn(files);
        when(fileAPIResponseBody.getAccessFiles()).thenReturn(null);

        assertEquals("kundkopior/anotherfile.jpg", testable.getCustomerCopyFilePath());
    }

    @Test
    void getCustomerCopyFilePath_returnsPathIfFoundInAccessFiles(){

        List<AccessFile> files = new ArrayList<>();
        files.add(new AccessFile("accessfiles/thumbnails/somefile.jpg", "image/jpeg"));
        files.add(new AccessFile("accessfiles/kundkopior/anotherfile.jpg", "image/jpeg"));


        when(fileAPIResponseBody.getAccessfilePaths()).thenReturn(null);
        when(fileAPIResponseBody.getAccessFiles()).thenReturn(files);

        assertEquals("kundkopior/anotherfile.jpg", testable.getCustomerCopyFilePath());
    }

    @Test
    void getCustomerCopyFilePath_returnsEmtpyIfNoAccessFiles(){

        List<AccessFile> files = new ArrayList<>();

        when(fileAPIResponseBody.getAccessfilePaths()).thenReturn(null);
        when(fileAPIResponseBody.getAccessFiles()).thenReturn(null);

        assertEquals("", testable.getCustomerCopyFilePath());

        when(fileAPIResponseBody.getAccessFiles()).thenReturn(null);
        assertEquals("", testable.getCustomerCopyFilePath());
    }

    @Test
    void getCustomerCopyFilePath_usesDeprecatedAccessfilePathsIfAccessFilesEmtpy(){
        List<String> deprecatedFiles = new ArrayList<>();
        deprecatedFiles.add("accessfiles/thumbnails/somefile.jpg");
        deprecatedFiles.add("accessfiles/kundkopior/deprecatedfile.jpg");

        List<AccessFile> files = new ArrayList<>();


        when(fileAPIResponseBody.getAccessfilePaths()).thenReturn(deprecatedFiles);
        when(fileAPIResponseBody.getAccessFiles()).thenReturn(files);

        assertEquals("kundkopior/deprecatedfile.jpg", testable.getCustomerCopyFilePath());
    }

    @Test
    void getCustomerCopyFilePath_returnsEmptyIfDeprecatedAndAccessFilesEmpty(){
        List<String> deprecatedFiles = new ArrayList<>();
        List<AccessFile> files = new ArrayList<>();


        when(fileAPIResponseBody.getAccessfilePaths()).thenReturn(deprecatedFiles);
        when(fileAPIResponseBody.getAccessFiles()).thenReturn(files);

        assertEquals("", testable.getCustomerCopyFilePath());
    }

    @Test
    void getCustomerCopyFilePath_prefersAccessFilesPropertyOverAccessfilePaths(){

        List<AccessFile> files = new ArrayList<>();
        files.add(new AccessFile("accessfiles/thumbnails/somefile.jpg", "image/jpeg"));
        files.add(new AccessFile("accessfiles/kundkopior/anotherfile.jpg", "image/jpeg"));

        List<String> filesDeprecated = new ArrayList<>();
        filesDeprecated.add("accessfiles/thumbnails/somefile.jpg");
        filesDeprecated.add("accessfiles/kundkopior/shouldnotbefound.jpg");


        when(fileAPIResponseBody.getAccessfilePaths()).thenReturn(filesDeprecated);
        when(fileAPIResponseBody.getAccessFiles()).thenReturn(files);

        assertEquals("kundkopior/anotherfile.jpg", testable.getCustomerCopyFilePath());
    }
    @Test
    void getLoggableItem_createsInstanceWithFieldsPopulated() {

        testable.setFilenameMetaData(mock(FilenameMetaData.class));
        testable.setMd5ChecksumAfter("checksumafter");
        testable.setMd5ChecksumBefore("checksumbefore");
        testable.setProcessingStatus(ProcessingStatus.STAGING);
        testable.setToken("mytoken");
        testable.setItem(mock(IngestItem.class));


        LoggableIngestProcessingItem item = testable.getLoggableItem();

        assertEquals(testable.getFilenameMetaData(), item.getFilenameMetaData());
        assertEquals(testable.getMd5ChecksumAfter(), item.getMd5ChecksumAfter());
        assertEquals(testable.getMd5ChecksumBefore(), item.getMd5ChecksumBefore());
        assertEquals(testable.getProcessingStatus(), item.getProcessingStatus());
        assertEquals(testable.getToken(), item.getToken());
        assertEquals(testable.getItem(), item.getItem());
    }

    @Test
    void addErrorFlag_addsFlagToErrors(){

        IngestProcessingItemFlag flag = mock(IngestProcessingItemFlag.class);

        testable.addErrorFlag(flag);

        assertEquals(1, testable.getErrors().size());
        assertEquals(flag, testable.getErrors().get(0));

    }

    @Test
    void addErrorFlag_ignoresDuplicateErrors(){

        IngestProcessingItemFlag flag = mock(IngestProcessingItemFlag.class);
        when(flag.getMessage()).thenReturn("my duplicate error");

        IngestProcessingItemFlag flag2 = mock(IngestProcessingItemFlag.class);
        when(flag2.getMessage()).thenReturn("my duplicate error");


        testable.addErrorFlag(flag);
        testable.addErrorFlag(flag2);

        assertEquals(1, testable.getErrors().size());
        assertEquals(flag, testable.getErrors().get(0));

    }

    @Test
    void prePersistHandler_updatesProcessingTimestampsForSelectStatuses(){

        IngestProcessingItem spyItem = spy(IngestProcessingItem.class);

        // does not update
        spyItem.setProcessingStatus(ProcessingStatus.STAGING);
        spyItem.setProcessingStartedAt(null);

        spyItem.prePersistHandler();
        verify(spyItem, times(1)).updateProcessingTimestamps();
        assertNull(spyItem.getProcessingStartedAt());
    }

    @Test
    void prePersistHandler_updatesCreatedTimestampWhenStatusSendToPreprocessing(){

        IngestProcessingItem spyItem = spy(IngestProcessingItem.class);

        // does not update
        spyItem.setProcessingStatus(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);
        spyItem.setProcessingStartedAt(null);

        spyItem.prePersistHandler();
        verify(spyItem, times(1)).updateProcessingTimestamps();
        assertNotNull(spyItem.getProcessingStartedAt());
    }

    @Test
    void prePersistHandler_updatesCompletedTimestampWhenStatusComplete(){

        IngestProcessingItem spyItem = spy(IngestProcessingItem.class);

        // does not update
        spyItem.setProcessingStatus(ProcessingStatus.COMPLETE);
        spyItem.setProcessingCompletedAt(null);

        spyItem.prePersistHandler();
        verify(spyItem, times(1)).updateProcessingTimestamps();
        assertNotNull(spyItem.getProcessingCompletedAt());
    }

    @Test
    void preUpdateHandler_updatesProcessingTimestampsForSelectStatuses(){

        IngestProcessingItem spyItem = spy(IngestProcessingItem.class);

        // does not update
        spyItem.setProcessingStatus(ProcessingStatus.STAGING);
        spyItem.setProcessingStartedAt(null);

        spyItem.preUpdateHandler();
        verify(spyItem, times(1)).updateProcessingTimestamps();
        assertNull(spyItem.getProcessingStartedAt());
    }

    @Test
    void preUpdateHandler_updatesCreatedTimestampWhenStatusSendToPreprocessing(){

        IngestProcessingItem spyItem = spy(IngestProcessingItem.class);

        // does not update
        spyItem.setProcessingStatus(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);
        spyItem.setProcessingStartedAt(null);

        spyItem.preUpdateHandler();
        verify(spyItem, times(1)).updateProcessingTimestamps();
        assertNotNull(spyItem.getProcessingStartedAt());
    }

    @Test
    void preUpdateHandler_updatesCompletedTimestampWhenStatusComplete(){

        IngestProcessingItem spyItem = spy(IngestProcessingItem.class);

        // does not update
        spyItem.setProcessingStatus(ProcessingStatus.COMPLETE);
        spyItem.setProcessingCompletedAt(null);

        spyItem.preUpdateHandler();
        verify(spyItem, times(1)).updateProcessingTimestamps();
        assertNotNull(spyItem.getProcessingCompletedAt());
    }

    @Test
    void preUpdateHandler_doesNotClearRawFitsResultForStatuses(){

        IngestProcessingItem spyItem = spy(IngestProcessingItem.class);

        // does not update for non ARCHIVED status
        spyItem.setProcessingStatus(ProcessingStatus.STAGING);
        spyItem.setRawFitsResult("this is my fake result");

        spyItem.preUpdateHandler();
        verify(spyItem, times(1)).clearRawFitsResultsOnArchived();
        assertEquals("this is my fake result", spyItem.getRawFitsResult());
    }

    @Test
    void preUpdateHandler_clearsRawFitsResultForStatusArchived(){

        IngestProcessingItem spyItem = spy(IngestProcessingItem.class);

        // does not update for non ARCHIVED status
        spyItem.setProcessingStatus(ProcessingStatus.ARCHIVED);
        spyItem.setRawFitsResult("this is my fake result");

        spyItem.preUpdateHandler();
        verify(spyItem, times(1)).clearRawFitsResultsOnArchived();
        assertNull(spyItem.getRawFitsResult());
    }
}