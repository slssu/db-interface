package fi.sls.ingest.security.ldap;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.ldap.userdetails.LdapUserDetails;

import java.util.Collection;

@Data
public class CustomLdapUserDetails implements LdapUserDetails {

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    protected LdapUserDetails details;

    /**
     * LDAP displayName property value
     */
    protected String displayName;

    /**
     * LDAP objectGUID property value
     */
    protected String objectGUID;

    public CustomLdapUserDetails(LdapUserDetails details) {
        this.details = details;
    }

    @Override
    public String getDn() {
        return details.getDn();
    }

    @Override
    public void eraseCredentials() {
        details.eraseCredentials();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return details.getAuthorities();
    }

    @Override
    public String getPassword() {
        return details.getPassword();
    }

    @Override
    public String getUsername() {
        return details.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return details.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return details.isAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return details.isCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return details.isEnabled();
    }
}
