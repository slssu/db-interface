package fi.sls.ingest.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Component
@ConfigurationProperties(prefix="sls.ingest")
public class IngestConfig {

    /**
     * Path on the host from which this API reads the list of files available for processing.
     */
    private String inboxPath;

    /**
     * Full url to the server at which this API is running.
     */
    private String hostUrl;

    /**
     * Base path to the API (added to hostUrl)
     */
    @Value("${spring.data.rest.basePath}")
    private String apiBasePath;

    /**
     * Returns a string concatenated with the configured host url, api basepath and endpoint strings
     *
     * Naive approach to getting the complete URL to use as a callback for other services in order to continue
     * processing ingest items.
     *
     * @param endpoint
     * @return
     */
    public String getHostRelativeAPIUrl(String endpoint){

        List<String> parts = new ArrayList<>();
        parts.add(trimSlashes(hostUrl));
        parts.add(trimSlashes(apiBasePath));
        parts.add(trimSlashes(endpoint));

        return parts.stream().collect(Collectors.joining("/"));
    }

    /**
     * Helper for removing leading and trailing slashes from string
     * @param str
     * @return
     */
    private String trimSlashes(String str){
        return StringUtils.trimTrailingCharacter(StringUtils.trimLeadingCharacter(str, '/'), '/');
    }

}
