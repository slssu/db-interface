package fi.sls.ingest.representation.projection;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import fi.sls.ingest.manager.*;
import fi.sls.ingest.proxy.slsfileapi.FileAPIResponseBody;
import fi.sls.ingest.representation.file.FilenameMetaData;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

/**
 * Provides a minimal representation of the processing item
 *
 */
@Projection(name="minimal", types={IngestProcessingItem.class})
@JsonPropertyOrder(alphabetic=true)
public interface IngestProcessingItemMinimal {

    String getToken();
    IngestDigitizationProfile getDigitizationProfile();
    UserMinimal getCreatedBy();

    //String getMessage();

    ProcessingStatus getProcessingStatus();
    IngestItem getItem();

    FileAPIResponseBody getFileAPIResponse();

    FilenameMetaData getFilenameMetaData();

    String getMd5ChecksumBefore();
    String getMd5ChecksumAfter();
    String getFileSize();

    List<IngestProcessingItemFlag> getErrors();
}
