package fi.sls.ingest.fits.model.fieldresolver.video;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Optional;

/**
 * Resolver that fetches the video data from the fits->metadata->video structure of the FITS result
 */
public class FitsVideoMetadata extends AbstractFieldValueResolver {

    public final static String SUBSECTION_TRACK = "track";
    public final static String SUBSECTION_TYPE_TRACK_AUDIO = "audio";
    public final static String SUBSECTION_TYPE_TRACK_VIDEO = "video";

    public final static String FIELD_DURATION = "duration";
    public final static String FIELD_BIT_RATE = "bitRate";
    public final static String FIELD_BIT_DEPTH = "bitDepth";
    public final static String FIELD_ASPECT_RATIO = "aspectRatio";
    public final static String FIELD_CODEC_NAME = "codecName";
    public final static String FIELD_CODEC_INFO = "codecInfo";
    public final static String FIELD_CODEC_ID = "codecId";
    public final static String FIELD_FRAME_COUNT = "frameCount";
    public final static String FIELD_SCANNING_FORMAT = "scanningFormat";
    public static final String FIELD_SAMPLE_RATE = "samplingRate";
    public static final String FIELD_AUDIO_NUM_CHANNELS = "channels";
    public static final String FIELD_AUDIO_CODEC_NAME = "codecFamily";


    protected String subSectionName;
    protected String subSectionType;

    public FitsVideoMetadata(String fieldName, FieldValueResolver resolver) {
        super(fieldName, resolver);
    }

    public  FitsVideoMetadata(String fieldName, FitsResult fitsResult) {
        super(fieldName, fitsResult);
    }

    public FitsVideoMetadata(String fieldName, FitsResult fitsResult, FieldValueResolver resolver) {
        super(fieldName, fitsResult, resolver);
    }

    public FitsVideoMetadata(String fieldName, String subSectionName, String subSectionType, FitsResult fitsResult){
        this(fieldName, fitsResult);

        this.subSectionName = subSectionName;
        this.subSectionType = subSectionType;

    }

    @Override
    public Optional<String> getValue() {

        if(fitsResult.getFitsResult().getMetadata() != null && fitsResult.getFitsResult().getMetadata().getVideo() != null){
            // for subsections we do two filters
            if(this.subSectionName != null && this.subSectionType != null){
                Optional<Element> item = fitsResult.getFitsResult().
                        getMetadata().getVideo().
                        getAny().
                        stream().
                        filter(
                                element ->
                                        element.getTagName() == subSectionName

                        ).filter(element -> element.getAttribute("type").equals(subSectionType)
                ).findFirst();

                if(item.isPresent()){
                    NodeList nodes = item.get().getElementsByTagName(fieldName);

                    if(nodes.getLength() > 0){
                        return Optional.of(nodes.item(0).getTextContent());
                    }
                }
            } else {
                // regular filtering for top level tags
                Optional<Element> item = fitsResult.getFitsResult().
                        getMetadata().getVideo().
                        getAny().
                        stream().
                        filter(
                                element ->
                                        element.getTagName() == fieldName

                        ).findFirst();

                if(item.isPresent() && !item.get().getTextContent().isEmpty()){
                    return Optional.of(item.get().getTextContent());
                }
            }
        }



        return executeNextResolver();
    }
}
