package fi.sls.ingest.manager.step;

import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.*;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Service
@Slf4j
public class IngestPremiseEventStep extends RecordServiceStep {
    public IngestPremiseEventStep(RecordService service) {
        super(service);
    }

    public PremiseEvent executeStep(
            DigitalObject digitalObject,
            MtPremiseEventsDO mtPremiseEventsDO,
            MTPremiseEventDOType mtPremiseEventDOType,
            PremisEventType premisEventType,
            String eventDate,
            String eventOutcomeNote) throws IOException {
        return executeStep(digitalObject, mtPremiseEventsDO, mtPremiseEventDOType, premisEventType, eventDate, null, eventOutcomeNote);
    }
    /**
     * Performs the tasks of this step
     *
     * @return The PremiseEvent that was linked to the given digitalObject and premiseEventDOType
     */
    public PremiseEvent executeStep(
            DigitalObject digitalObject,
            MtPremiseEventsDO mtPremiseEventsDO,
            MTPremiseEventDOType mtPremiseEventDOType,
            PremisEventType premisEventType,
            String eventDate,
            String createDate,
            String eventOutcomeNote) throws IOException {

        PremiseEvent premiseEvent = new PremiseEvent();

        // FIXME: not cool that we need to inject the createDate just to search on correct identifier, but migration code needs it atm
        // FIXME: do we even need to generate the premisEventId again, maybe instead use mtPremiseEventsDO value?
        premiseEvent.getFieldData().setEventIdentifierValue(digitalObject.getPremiseEventIdForType(mtPremiseEventDOType.getCode(), createDate));

        Optional<? extends FileMakerResponseEntity> existingItem;

//        jw 2022-02-14: remove try-catch as it is handled by runnable
//        try {
            existingItem = service.findRecordByIdentifier(premiseEvent);

            if(existingItem.isPresent()){
                premiseEvent = (PremiseEvent) existingItem.get();
            } else {
                premiseEvent = new PremiseEvent();
            }

            premiseEvent.getFieldData().setEventIdentifierValue(mtPremiseEventsDO.getFieldData().getEventId());
            premiseEvent.getFieldData().setEventType(premisEventType.getCode());
            premiseEvent.getFieldData().setEventDateTime(eventDate);
            premiseEvent.getFieldData().setDigitizationProfileId(digitalObject.getFieldData().getDigitizationProfileId());
            premiseEvent.getFieldData().setEventOutcomeDetailNote(eventOutcomeNote);


            if(premiseEvent.isNewRecord()){
                premiseEvent = (PremiseEvent) service.createObjectRecord(premiseEvent);
                log.debug("Created new premiseEvent with id: {}", premiseEvent.getRecordId());
            } else {
                premiseEvent = (PremiseEvent) service.updateObjectRecord(premiseEvent);
                log.debug("Updated premiseEvent with id: {}", premiseEvent.getRecordId());
            }

            return premiseEvent;

//        } catch (IOException e) {
//            // FIXME: probably critical, should re-throw instead
//            log.error("Error creating PremiseEvent: {}", e.getMessage());
//            return null;
//        }
    }
}
