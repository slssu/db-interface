package fi.sls.ingest.parser;

import fi.sls.ingest.validation.StrictValidProcessableFile;
import lombok.Data;

/**
 * Wrapper class for strictly validating the filename that the FilenameMetaParser should be able to handle
 */
@Data
public class StrictIngestFilename {

    @StrictValidProcessableFile
    private String name;

    public StrictIngestFilename(String name){
        this.name = name;
    }
}
