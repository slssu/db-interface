package fi.sls.ingest.actuator.metrics.supplier;

import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.repository.DigitalObjectMigrationResultRepository;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.representation.projection.MigrationResultCount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MigrationResultCountSupplierTest {

    MigrationResultCountSupplier testable;
    DigitalObjectMigrationResultRepository repository;

    @BeforeEach
    void setUp(){
        repository = mock(DigitalObjectMigrationResultRepository.class);
        when(repository.count()).thenReturn(12L);
        when(repository.countByProcessingStatusAndCollection(ProcessingStatus.COMPLETE, "SLSA", 1150L)).thenReturn(2L);
        when(repository.countByProcessingStatusAndCollection(ProcessingStatus.MIGRATION_REJECTED, "ÖTA", 112L)).thenReturn(10L);
    }

    @Test
    void get_returnsTotalCountIfFilterStatusNull() {
        testable = new MigrationResultCountSupplier(repository);

        assertEquals(12L, testable.get());
    }

    @Test
    void get_returnsCorrectCountForComplete() {

        MigrationResultCount filter = mock(MigrationResultCount.class);
        when(filter.getArchiveNr()).thenReturn(1150L);
        when(filter.getCollectionName()).thenReturn("SLSA");
        when(filter.getProcessingStatus()).thenReturn(ProcessingStatus.COMPLETE);

        testable = new MigrationResultCountSupplier(repository, filter);
        assertEquals(2L, testable.get());
    }

    @Test
    void get_returnsCorrectCountForArchived() {
        MigrationResultCount filter = mock(MigrationResultCount.class);
        when(filter.getArchiveNr()).thenReturn(112L);
        when(filter.getCollectionName()).thenReturn("ÖTA");
        when(filter.getProcessingStatus()).thenReturn(ProcessingStatus.MIGRATION_REJECTED);

        testable = new MigrationResultCountSupplier(repository, filter);
        assertEquals(10L, testable.get());
    }
}