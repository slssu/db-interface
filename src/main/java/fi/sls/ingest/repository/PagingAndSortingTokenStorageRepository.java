package fi.sls.ingest.repository;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

/**
 * Interface to be implemented by repositories that provide access to items with a token field
 */
@NoRepositoryBean
public interface PagingAndSortingTokenStorageRepository<T, Long> extends PagingAndSortingRepository<T, Long> {

    /**
     * Find an item in the repository based on the given token string
     *
     * @param token
     * @return
     */
    Optional<T> findByToken(String token);
}
