package fi.sls.ingest.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.manager.runnable.IngestPostProcessRunnable;
import fi.sls.ingest.manager.runnable.IngestPreProcessRunnable;
import fi.sls.ingest.migration.runnable.MigrationPostProcessRunnable;
import fi.sls.ingest.queue.rabbitmq.queue.IngestItemPostProcessingQueue;
import fi.sls.ingest.queue.rabbitmq.queue.MigrationItemPostProcessingQueue;
import fi.sls.ingest.queue.rabbitmq.receiver.IngestPostProcessingItemReceiver;
import fi.sls.ingest.queue.rabbitmq.receiver.IngestPreProcessingItemReceiver;
import fi.sls.ingest.queue.rabbitmq.receiver.IngestPreProcessingItemVideoReceiver;
import fi.sls.ingest.queue.rabbitmq.receiver.MigrationItemPostProcessingReceiver;
import fi.sls.ingest.repository.DigitalObjectMigrationResultRepository;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.SocketMessenger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class RabbitMQConfig {

    ObjectMapper objectMapper;
    SocketMessenger socketMessenger;
    IngestItemRepository ingestItemRepository;
    DigitalObjectMigrationResultRepository migrationItemRepository;

    @Value("${sls.queue.preProcessing}")
    protected String preProcessingQueueName;

    @Value("${sls.queue.preProcessingVideo}")
    protected String preProcessingVideoQueueName;


    @Value("${sls.queue.postProcessing}")
    protected String postProcessingQueueName;




    @Value("${sls.queue.migrationPostProcessing}")
    protected String migrationPostProcessingQueueName;

    @Value("${sls.exchange.name}")
    protected String exchangeName;

    @Value("${spring.rabbitmq.host}")
    protected String rabbitHost;

    @Value("${spring.rabbitmq.username}")
    protected String rabbitUsername;

    @Value("${spring.rabbitmq.password}")
    private String rabbitPassword;

    private ApplicationContext context;

    @Autowired
    public RabbitMQConfig(
            ObjectMapper objectMapper,
            IngestItemRepository ingestItemRepository,
            SocketMessenger socketMessenger,
            DigitalObjectMigrationResultRepository migrationItemRepository,
            ApplicationContext applicationContext
            ){

        this.objectMapper = objectMapper;
        this.socketMessenger = socketMessenger;
        this.ingestItemRepository = ingestItemRepository;
        this.migrationItemRepository = migrationItemRepository;
        this.context = applicationContext;
    }

    private String getWaitQueueName(String queuePrefix){
        return queuePrefix.concat(".wait");
    }

    @Bean
    public ConnectionFactory connectionFactory(){
        CachingConnectionFactory factory = new CachingConnectionFactory(rabbitHost);
        factory.setUsername(rabbitUsername);
        factory.setPassword(rabbitPassword);

        return factory;
    }

    @Bean
    public RabbitListenerContainerFactory<SimpleMessageListenerContainer> preProcessingRabbitListenerContainerFactory() {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        // scaling should happen through more container instances, so only accept one message at a time
        factory.setPrefetchCount(1);
        // do not requeue automatically on failure, this should send the message to the wait queue
        factory.setDefaultRequeueRejected(false);
        return factory;
    }

    @Bean
    public RabbitListenerContainerFactory<SimpleMessageListenerContainer> noRequeRejectedRabbitListenerContainerFactory() {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        // scaling should happen through more container instances, so only accept one message at a time
        factory.setPrefetchCount(1);
        // do not requeue automatically on failure, this should send the message to the wait queue
        factory.setDefaultRequeueRejected(false);
        return factory;
    }

    @Bean
    public RabbitListenerContainerFactory<SimpleMessageListenerContainer> singlePrefetchDefaultRabbitListenerContainerFactory() {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        // scaling should happen through more container instances, so only accept one message at a time
        factory.setPrefetchCount(1);
        return factory;
    }


    @Bean
    public RabbitTemplate rabbitTemplate(){
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        template.setUsePublisherConnection(true);

        return template;
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(exchangeName);
    }

    @Bean
    public Queue ingestPreProcessingQueue(){
        return QueueBuilder.durable(preProcessingQueueName)
                .deadLetterExchange(exchangeName)
                .deadLetterRoutingKey(getWaitQueueName(preProcessingQueueName))
                .build();
    }

    @Bean
    public Queue ingestPreProcessingWaitQueue(){
        return QueueBuilder.durable(getWaitQueueName(preProcessingQueueName))
                .deadLetterExchange(exchangeName)
                .deadLetterRoutingKey(preProcessingQueueName)
                .ttl(10000) // 10 sec
                .build();
    }

    @Bean
    public Queue ingestPreProcessingVideoQueue(){
        return QueueBuilder.durable(preProcessingVideoQueueName)
                .deadLetterExchange(exchangeName)
                .deadLetterRoutingKey(getWaitQueueName(preProcessingVideoQueueName))
                .build();
    }

    @Bean
    public Queue ingestPreProcessingVideoWaitQueue(){
        return QueueBuilder.durable(getWaitQueueName(preProcessingVideoQueueName))
                .deadLetterExchange(exchangeName)
                .deadLetterRoutingKey(preProcessingVideoQueueName)
                .ttl(10000) // 10 sec
                .build();
    }

    @Bean
    public Queue ingestPostProcessingQueue(){
        return QueueBuilder.durable(postProcessingQueueName)
                .deadLetterExchange(exchangeName)
                .deadLetterRoutingKey(getWaitQueueName(postProcessingQueueName))
                .build();
    }

    @Bean
    public Queue ingestPostProcessingWaitQueue(){
        return QueueBuilder.durable(getWaitQueueName(postProcessingQueueName))
                .deadLetterExchange(exchangeName)
                .deadLetterRoutingKey(postProcessingQueueName)
                .ttl(10000) // 10 sec
                .build();
    }

    @Bean
    Binding primaryPreProcessingBinding(@Qualifier("ingestPreProcessingQueue") Queue ingestPreProcessingQueue, DirectExchange exchange) {
        return BindingBuilder.bind(ingestPreProcessingQueue).to(exchange).with(ingestPreProcessingQueue.getName());
    }

    @Bean
    Binding waitPreProcessingBinding(@Qualifier("ingestPreProcessingWaitQueue") Queue waitQueue, DirectExchange exchange){
        return BindingBuilder.bind(waitQueue).to(exchange).with(waitQueue.getName());
    }

    @Bean
    Binding primaryPreProcessingVideoBinding(@Qualifier("ingestPreProcessingVideoQueue") Queue ingestPreProcessingQueue, DirectExchange exchange) {
        return BindingBuilder.bind(ingestPreProcessingQueue).to(exchange).with(ingestPreProcessingQueue.getName());
    }

    @Bean
    Binding waitPreProcessingVideoBinding(@Qualifier("ingestPreProcessingVideoWaitQueue") Queue waitQueue, DirectExchange exchange){
        return BindingBuilder.bind(waitQueue).to(exchange).with(waitQueue.getName());
    }

    @Bean
    Binding primaryPostProcessingBinding(@Qualifier("ingestPostProcessingQueue") Queue ingestPostProcessingQueue, DirectExchange exchange) {
        return BindingBuilder.bind(ingestPostProcessingQueue).to(exchange).with(ingestPostProcessingQueue.getName());
    }

    @Bean
    Binding waitPostProcessingBinding(@Qualifier("ingestPostProcessingWaitQueue") Queue waitQueue, DirectExchange exchange){
        return BindingBuilder.bind(waitQueue).to(exchange).with(waitQueue.getName());
    }

//    @Bean
//    public IngestItemPostProcessingQueue createPostProcessingQueue(){
//        return new IngestItemPostProcessingQueue(postProcessingQueueName);
//    }

    @Bean
    public MigrationItemPostProcessingQueue createMigrationPostProcessingQueue(){
        return new MigrationItemPostProcessingQueue(migrationPostProcessingQueueName);
    }

    @Bean
    public IngestPreProcessingItemReceiver createPreProcessingReceiver(){
        return new IngestPreProcessingItemReceiver(objectMapper, ingestItemRepository, socketMessenger,
                context.getBean(IngestPreProcessRunnable.class)
        );
    }

    @Bean
    public IngestPreProcessingItemVideoReceiver createPreProcessingVideoReceiver(){
        return new IngestPreProcessingItemVideoReceiver(objectMapper, ingestItemRepository, socketMessenger,
                context.getBean(IngestPreProcessRunnable.class)
        );
    }

    @Bean
    public IngestPostProcessingItemReceiver createPostProcessingReceiver(){
        return new IngestPostProcessingItemReceiver(objectMapper, ingestItemRepository, socketMessenger,
                context.getBean(IngestPostProcessRunnable.class)
        );
    }

    @Bean
    public MigrationItemPostProcessingReceiver createMigrationPostProcessingReceiver(){
        return new MigrationItemPostProcessingReceiver(objectMapper,
                context.getBean(MigrationPostProcessRunnable.class),
                migrationItemRepository
        );
    }
}
