package fi.sls.ingest.representation.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.manager.IngestProcessingItemFlag;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class AccessFileArrayConverter extends JsonStringConverter implements AttributeConverter<List<AccessFile>, String> {

    public AccessFileArrayConverter(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public String convertToDatabaseColumn(List<AccessFile> accessFiles) {
        String rtnJson = null;

        if(accessFiles == null || accessFiles.isEmpty()){
            return null;
        } else {
            try{
                rtnJson = objectMapper.writeValueAsString(accessFiles);
            } catch (JsonProcessingException e) {
                log.error("Error converting from List of AccessFiles to JSON: {}", e.getMessage());
            }
            return rtnJson;
        }
    }

    @Override
    public List<AccessFile> convertToEntityAttribute(String jsonData) {
        List<AccessFile> rtnItem = new ArrayList<>();

        if(jsonData != null){
            try{
                rtnItem = objectMapper.readValue(jsonData, objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, AccessFile.class));
            } catch (IOException e) {
                log.error("Error converting to List of AccessFile from JSON: {}", e.getMessage());
            }
        }
        return rtnItem;
    }
}
