package fi.sls.ingest.actuator.health;

import fi.sls.ingest.proxy.filemaker.AuthToken;
import fi.sls.ingest.proxy.filemaker.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
/**
 * Provides a health endpoint for checking status of the SLS FileMaker instance service endpoint
 *
 * The check adds the following json structure to the /actuator/health endpoint
 *
 * {
 *    ...
 *
 *    "fileMaker": {
 *         "status": "UP",
 *         "details": {
 *             "response_time_ms": 123
 *         }
 *     }
 *
 *    ...
 * }
 */
@Component("fileMaker")
@ConditionalOnProperty(prefix = "management.health.fileMaker", name = "enabled", matchIfMissing = true)
public class FilemakerHealthCheck implements HealthIndicator {

    AuthenticationService service;

    @Autowired
    public FilemakerHealthCheck(@Qualifier("healthCheckAuthenticationService") AuthenticationService service){
        this.service = service;
    }

    @Override
    public Health health() {

        String errorMsg;

        try{
            long start = System.currentTimeMillis();
            AuthToken result = service.authenticate(true);
            long stop = System.currentTimeMillis();

            if(result != null && result.getToken() != null){
                // logout so the session does not stay alive since we force an auth
                service.logout();
                return Health.up().withDetail("response_time_ms", stop - start).build();
            } else {
                errorMsg = "Could not determine state of service";
            }


        } catch (Exception e) {
            errorMsg = e.getMessage();
        }

        return Health.down().withDetail("error", errorMsg).build();
    }
}
