package fi.sls.ingest.manager;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the different states a IngestProcessingItem can have in the system
 */
public enum ProcessingStatus {

    PRE_STAGING(1),
    STAGING(3),
    POST_STAGING(4),
    SEND_TO_PRE_PROCESSING_QUEUE(5),
    SEND_TO_POST_PROCESSING_QUEUE(7),
    ACCEPTED(10),
    REJECTED(20),
    STAGING_REJECTED(25),
    PROCESSING_FILENAME_METADATA(30),
    PROCESSING_FILENAME_METADATA_STAGING(35),
    PROCESSING_METADATA(40),
    CREATE_DATABASE_OBJECTS(50),
    INIT_PROCESSING_USER_COPIES(60),
    PROCESSING_USER_COPIES(65),
    PROCESSING_USER_COPIES_COMPLETE(70),
    PROCESSING_USER_COPIES_ERROR(80),
    COMPLETE(200),
    ARCHIVED(999),
    DELETED(400),
    ERROR(500),
    RETRY(503),

    MIGRATION_PRE_PROCESSING(1000),
    DRY_RUN_MIGRATION_FILE_API(1010),
    MIGRATION_FILE_API(1050),
    MIGRATION_POST_PROCESSING(1100),
    MIGRATION_REJECTED(1550);

    private Integer statusCode;

    ProcessingStatus(Integer statusCode){
        this.statusCode = statusCode;
    }

    /**
     * @return The numeric value assigned to the enums in the class
     */
    public Integer getStatusCode(){
        return this.statusCode;
    }

    /**
     * Converts a given status code integer to a matching ProcessingStatus enum value
     *
     * @param statusCode
     * @return
     * @throws IllegalArgumentException If the given statusCode is not a valid value in this enum
     */
    public static ProcessingStatus fromStatusCode(Integer statusCode){

        for (ProcessingStatus status : ProcessingStatus.values()){
            if(status.getStatusCode().equals(statusCode)){
                return status;
            }
        }

        throw new IllegalArgumentException("statusCode [" + statusCode + "] not supported.");
    }

    /**
     * Converts a given string to a matching ProcessingStatus enum value
     *
     * Comparison is done case insensitive.
     *
     * @param statusCode
     * @return
     * @throws IllegalArgumentException If the given string is not a valid value in this enum
     */
    public static ProcessingStatus fromStatusCodeString(String statusCode){

        for (ProcessingStatus status : ProcessingStatus.values()){
            if(status.name().equalsIgnoreCase(statusCode)){
                return status;
            }
        }

        throw new IllegalArgumentException("statusCode string [" + statusCode + "] not supported.");
    }

    /**
     * Converts a given string containing values corresponding to enums in this class to a list of
     * enums
     *
     * @param statusList
     * @return
     */
    public static List<ProcessingStatus> fromStatusListString(String statusList){
        String[] splits = statusList.split(",");

        List<ProcessingStatus> rtn = new ArrayList<>();

        for(String label : splits){
            rtn.add(ProcessingStatus.fromStatusCodeString(label.trim().toUpperCase()));
        }

        return rtn;
    }
}
