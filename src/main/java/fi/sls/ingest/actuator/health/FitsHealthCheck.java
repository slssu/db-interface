package fi.sls.ingest.actuator.health;

import fi.sls.ingest.fits.FitsService;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Provides a health endpoint for checking status of the FITS metadata service endpoint
 *
 * The check adds the following json structure to the /actuator/health endpoint
 *
 * {
 *    ...
 *
 *    "fits": {
 *         "status": "UP",
 *         "details": {
 *             "version": "1.4.1",
 *             "response_time_ms": 123
 *         }
 *     }
 *
 *    ...
 * }
 */
@Component("fits")
@ConditionalOnProperty(prefix = "management.health.fits", name = "enabled", matchIfMissing = true)
public class FitsHealthCheck implements HealthIndicator {

    FitsService service;


    public FitsHealthCheck(FitsService service){
        this.service = service;
    }

    @Override
    public Health health() {

        String errorMsg;


        try{
            long start = System.currentTimeMillis();
            CompletableFuture<String> fitsResult = service.getVersion();
            long stop = System.currentTimeMillis();

            if(fitsResult.get() != null){
                return Health.up()
                        .withDetail("version", fitsResult.get().trim())
                        .withDetail("response_time_ms", stop - start)
                        .build();
            } else {
                errorMsg = "Could not determine state of service";
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
        }


        return Health.down().withDetail("error", errorMsg).build();
    }
}
