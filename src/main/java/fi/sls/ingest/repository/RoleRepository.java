package fi.sls.ingest.repository;

import fi.sls.ingest.representation.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface RoleRepository extends PagingAndSortingRepository<Role, Long> {

    Optional<Role> findByLabel(String label);
}
