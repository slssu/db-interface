package fi.sls.ingest.manager.step;

import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.CollectionFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntityFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.MTCollectionIntellectualEntity;
import fi.sls.ingest.proxy.filemaker.response.record.MTCollectionIntellectualEntityFieldData;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class LinkCollectionIntellectualEntityStepTest {

    LinkCollectionIntellectualEntityStep testable;
    RecordService service;

    @BeforeEach
    void setUp() {
        service = mock(RecordService.class);
        testable = new LinkCollectionIntellectualEntityStep(service);
    }

    @Test
    void executeStep_returnsExistingRecordIfPresent() throws IOException {
        CollectionFieldData cfd = mock(CollectionFieldData.class);
        when(cfd.getEntityNumber()).thenReturn(123L);
        IntellectualEntityFieldData iefd = mock(IntellectualEntityFieldData.class);
        when(iefd.getEntityNumber()).thenReturn(321L);

        MTCollectionIntellectualEntity data = mock(MTCollectionIntellectualEntity.class);
        when(data.getFieldData()).thenReturn(mock(MTCollectionIntellectualEntityFieldData.class));
        // use doReturn because
        doReturn(Optional.of(data)).when(service).findRecordByIdentifier(any(MTCollectionIntellectualEntity.class));

        MTCollectionIntellectualEntity rtn = testable.executeStep(cfd, iefd);

        assertEquals(data, rtn);
    }

    @Test
    void executeStep_createsRecordIfNotPresent() throws IOException {
        CollectionFieldData cfd = mock(CollectionFieldData.class);
        when(cfd.getEntityNumber()).thenReturn(123L);
        IntellectualEntityFieldData iefd = mock(IntellectualEntityFieldData.class);
        when(iefd.getEntityNumber()).thenReturn(321L);

        MTCollectionIntellectualEntity data = mock(MTCollectionIntellectualEntity.class);
        when(data.getFieldData()).thenReturn(mock(MTCollectionIntellectualEntityFieldData.class));
        when(service.findRecordByIdentifier(any(MTCollectionIntellectualEntity.class))).thenReturn(Optional.empty());

        MTCollectionIntellectualEntity createData = mock(MTCollectionIntellectualEntity.class);
        MTCollectionIntellectualEntityFieldData fieldData = mock(MTCollectionIntellectualEntityFieldData.class);
        when(createData.getFieldData()).thenReturn(fieldData);
        when(service.createObjectRecord(any(MTCollectionIntellectualEntity.class))).thenReturn(createData);

        MTCollectionIntellectualEntity rtn = testable.executeStep(cfd, iefd);

        assertSame(createData, rtn);
        verify(service).createObjectRecord(any(MTCollectionIntellectualEntity.class));
    }
}