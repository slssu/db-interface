package fi.sls.ingest.representation.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.manager.IngestProcessingItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.AttributeConverter;
import java.io.IOException;

@Slf4j
public class IngestProcessingItemConverter implements AttributeConverter<IngestProcessingItem, String> {

    ObjectMapper objectMapper;

    @Autowired
    public IngestProcessingItemConverter(ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }

    @Override
    public String convertToDatabaseColumn(IngestProcessingItem item) {
        String rtnJson = null;

        if(item == null){
            return null;
        } else {
            try{
                rtnJson = objectMapper.writeValueAsString(item);
            } catch (JsonProcessingException e) {
                log.error("Error converting from IngestProcessingItem to JSON: {}", e.getMessage());
            }
            return rtnJson;
        }


    }

    @Override
    public IngestProcessingItem convertToEntityAttribute(String jsonData) {


        if(jsonData == null){
            return null;
        } else {
            IngestProcessingItem rtnItem = null;
            try{
                rtnItem = objectMapper.readValue(jsonData, IngestProcessingItem.class);
            } catch (IOException e) {
                log.error("Error converting to IngestProcessingItem from JSON: {}", e.getMessage());
            }

            return rtnItem;

        }
    }

}
