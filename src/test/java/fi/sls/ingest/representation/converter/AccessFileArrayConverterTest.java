package fi.sls.ingest.representation.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AccessFileArrayConverterTest extends JsonStringConverterTestBase<List<AccessFile>>{

    @BeforeEach
    void setUp() {
        super.setUp();
        testable = new AccessFileArrayConverter(objectMapper);
    }

    @Test
    @Override
    void convertToEntityAttribute_returnsNullForNullInput() throws IOException {
        List<AccessFile> data = testable.convertToEntityAttribute(null);
        assertNotNull(data);
        assertEquals(0, data.size());
    }


    @Test
    void convertToDatabaseColumn_returnsJsonStringWhenCalledWithValues() throws JsonProcessingException {
        List<AccessFile> flags = mock(ArrayList.class);

        when(objectMapper.writeValueAsString(flags)).thenReturn("accessfilejson");

        String rtn = testable.convertToDatabaseColumn(flags);

        assertEquals("accessfilejson", rtn);
    }

    @Test
    void convertToDatabaseColumn_catchesExceptionFromObjectMapperAndReturnsNull() throws JsonProcessingException {
        List<AccessFile> flags = mock(ArrayList.class);

        when(objectMapper.writeValueAsString(flags)).thenThrow(JsonProcessingException.class);

        String rtn = testable.convertToDatabaseColumn(flags);

        assertNull(rtn);
    }

    @Test
    void convertToEntityAttribute_returnsListOfFlagsWhenCalledWithJson() throws IOException {
        // re-init here, because final class reference in objectMapper can't be mocked?
        objectMapper = new ObjectMapper();
        testable = new AccessFileArrayConverter(objectMapper);

        List<AccessFile> flags = mock(ArrayList.class);
        String flagJson = "flagjson";

        List<AccessFile> newFlags = testable.convertToEntityAttribute(flagJson);

        assertNotNull(newFlags);
    }
}