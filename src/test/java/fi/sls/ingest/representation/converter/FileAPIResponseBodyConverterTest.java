package fi.sls.ingest.representation.converter;

import fi.sls.ingest.proxy.slsfileapi.FileAPIResponseBody;
import org.junit.jupiter.api.BeforeEach;

class FileAPIResponseBodyConverterTest extends JsonStringConverterTestBase<FileAPIResponseBody> {

    @BeforeEach
    void setUp() {
        super.setUp();

        testable = new FileAPIResponseBodyConverter(objectMapper);
    }
}