package fi.sls.ingest.manager;

public interface MD5ComparisonItem {

    /**
     * The MD5 hash of the file content before it is transferred to final storage
     */
    String getMd5ChecksumBefore();

    /**
     * The MD5 hash of the file content after it has been moved to final storage
     *
     * Final storage transfer is handled by a separate service.
     */
    String getMd5ChecksumAfter();


    /**
     * Returns true if the value returned by getMd5ChecksumBefore is equal to the value returned by getMd5ChecksumAfter
     *
     * The values must be 32 characters long
     *
     * @return
     */
    default boolean md5ChecksumMatch() {
        return getMd5ChecksumAfter() != null &&
                getMd5ChecksumBefore() != null &&
                getMd5ChecksumAfter().length() == 32 &&
                getMd5ChecksumBefore().length() == 32 &&
                getMd5ChecksumBefore().toUpperCase().equals(getMd5ChecksumAfter().toUpperCase());
    }
}
