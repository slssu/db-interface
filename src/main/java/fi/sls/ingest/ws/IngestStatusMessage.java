package fi.sls.ingest.ws;

import fi.sls.ingest.representation.projection.DigitalObjectMigrationResultMinimal;
import fi.sls.ingest.representation.projection.IngestProcessingItemMinimal;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Wrapper for web socket message sent to notify clients about
 * IngestProcessingItem or DigitalObjectMigrationItem updates
 */
@Data
@NoArgsConstructor
public class IngestStatusMessage {

    protected MessageType type;
    protected Object content;
    protected String sender;

    public IngestStatusMessage(String sender, DigitalObjectMigrationResultMinimal item) {
        this.type = MessageType.MIGRATION_UPDATE;
        this.sender = sender;
        this.content = item;
    }

    public enum MessageType {
        CHAT,
        METRIC,
        MIGRATION_UPDATE,
        JOIN,
        LEAVE
    }

    public IngestStatusMessage(String sender, IngestProcessingItemMinimal item){
        this.type = MessageType.CHAT;
        this.content = item;
        this.sender = sender;
    }

    public IngestStatusMessage(MessageType messageType, String sender, MetricsMessage stats){
        this.type = messageType;
        this.sender = sender;
        this.content = stats;
    }
}
