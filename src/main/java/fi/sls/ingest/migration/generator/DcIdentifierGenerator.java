package fi.sls.ingest.migration.generator;

import fi.sls.ingest.config.MetaDataConfig;
import fi.sls.ingest.exception.MigrationProcessingException;
import fi.sls.ingest.migration.generator.fieldstrategy.FieldGeneratorStrategy;
import fi.sls.ingest.migration.generator.fieldstrategy.StringRegexStrategy;
import fi.sls.ingest.proxy.filemaker.response.record.CollectionFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.DigitalObjectFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntityFieldData;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.representation.file.ObjectCategory;
import fi.sls.ingest.representation.file.ObjectCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Attempts to generate a valid DigitalObject->dcIdentifier value based on the given inputs
 *
 */
@Service
public class DcIdentifierGenerator {

    /**
     * Config for collection names
     */
    MetaDataConfig metaDataConfig;

    /**
     * Map of strategies to use for generating the value of various fields, in case the default cannot be used for the collection
     */
    Map<String, FieldGeneratorStrategy> fieldGeneratorStrategyMap;

    @Autowired
    public DcIdentifierGenerator(MetaDataConfig metaDataConfig){
        this.metaDataConfig = metaDataConfig;

        fieldGeneratorStrategyMap = new HashMap<>();

        fieldGeneratorStrategyMap.put("archiveCatalogNr_ota_112", new StringRegexStrategy("[a-z0-9]+-([0-9]+)_[a-z]+\\.[0-9]+", "getIdentifier"));
        fieldGeneratorStrategyMap.put("archiveCatalogNr_sls_2269_ljud", new StringRegexStrategy("[a-z0-9]+_[a-z]+\\.([0-9]+)-[0-9]+", "getIdentifier"));
        fieldGeneratorStrategyMap.put("archiveCatalogNr_sls_1859_ljud", new StringRegexStrategy("[a-z0-9]+_[a-z]+\\.([0-9]+)-[0-9]+", "getIdentifier"));
        fieldGeneratorStrategyMap.put("archiveCatalogNr_sls_983_ljud", new StringRegexStrategy("[a-z0-9]+_[a-z]+\\.([0-9]+)-[0-9]+", "getIdentifier"));
        fieldGeneratorStrategyMap.put("signumNr", new StringRegexStrategy("([0-9]+)$"));
    }

    public FilenameMetaData generate(CollectionFieldData collection, IntellectualEntityFieldData intellectualEntity, DigitalObjectFieldData digitalObject){
        // structure that we want (follow new strict format):
        // fmi/fmi257/fmi_257_foto_00004_0001_01.tif
        // fmi/fmi476/fmi_476_ljud_00011_0003_01.wav

        FilenameMetaData metaData = new FilenameMetaData();

        ObjectCollection oc = metaDataConfig.getCollectionByLabel(collection.getCollectionName());
        metaData.setCollectionDescription(oc.getDescription());
        metaData.setCollectionName(oc.getLabel());
        metaData.setCollectionKey(oc.getFileKey());

        ObjectCategory oCat = metaDataConfig.getCategoryBySubCategoryFuzzy(intellectualEntity.getDcType(), intellectualEntity.getDcType2());
        metaData.setCategory(oCat);

        metaData.setArchiveNr(collection.getArchiveNumber().toString());

        // set the catalog nr if the collection strategy calls for one
        FieldGeneratorStrategy<String, String> fgsArchiveCatalogNr = getStrategyForFieldByCollection(
                "archiveCatalogNr",
                oc.getFileKey(),
                collection.getArchiveNumber(),
                oCat.getFileKey()
        );
        if(fgsArchiveCatalogNr != null && fgsArchiveCatalogNr.getSourceObjectMethodName() != null){
            java.lang.reflect.Method method;
            try {
                method = digitalObject.getClass().getMethod(fgsArchiveCatalogNr.getSourceObjectMethodName());
                metaData.setArchiveCatalogNr(fgsArchiveCatalogNr.generate( (String) method.invoke(digitalObject)));
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                throw new MigrationProcessingException(e.getMessage());
            }
        } else {
            metaData.setArchiveCatalogNr(collection.getArchiveNumberSuffix());
        }

        // extract last nr from entity_identifier and set as signum nr, seems to work?
        metaData.setSigumNr(extractSigumFromIdentifier(intellectualEntity.getEntityIdentifier()));

        // default pagenr to 1 if no value exists
        if(digitalObject.getEntityOrderInt() != null){
            metaData.setPageNr(digitalObject.getEntityOrderInt());
        } else {
            metaData.setPageNr(1);
        }

        metaData.setFileSuffix(digitalObject.getFileTypeAcronym());

        // attempt to fetch entity type order from digitalObject, but this assumes a value in FileMaker.
        // For objects that don't have this, we're atm out of luck and assume just v1
        if(digitalObject.getEntityTypeOrderInt() != null && digitalObject.getEntityTypeOrderInt() > 0){
            metaData.setVersionNr(digitalObject.getEntityTypeOrderInt());
        }

        // the new filename should now be possible to generate by getting the dcidentifier and removing the part after slash
        String filename = metaData.getDcIdentifier().substring(metaData.getDcIdentifier().lastIndexOf("/") + 1);
        metaData.setFilename(filename);

        return metaData;
    }

    protected FieldGeneratorStrategy getStrategyForFieldByCollection(String fieldName, String collectionName, Long archiveNumber, String type) {

        // first attempt fetching including file type
        FieldGeneratorStrategy<String, String> strategy = fieldGeneratorStrategyMap.get(fieldName+"_"+collectionName+"_"+archiveNumber+"_"+type);

        if(strategy == null){
            // no match, try relaxed
            return fieldGeneratorStrategyMap.get(fieldName+"_"+collectionName+"_"+archiveNumber);
        } else {
            return strategy;
        }
    }

    public int extractSigumFromIdentifier(String identifier){

        FieldGeneratorStrategy<String, String> fgs = fieldGeneratorStrategyMap.get("signumNr");

        String rtn = fgs.generate(identifier);

        if(rtn != null){
            return Integer.parseInt(rtn);
        } else {
            return 0;
        }
    }
}
