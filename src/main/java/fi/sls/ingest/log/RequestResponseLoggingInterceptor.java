package fi.sls.ingest.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * Intercepts the requests and responses going through the API to log their content
 */
public class RequestResponseLoggingInterceptor implements ClientHttpRequestInterceptor {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        logRequest(request, body);
        ClientHttpResponse response = execution.execute(request, body);
        logResponse(response);
        return response;
    }

    private void logRequest(HttpRequest request, byte[] body) throws IOException {
        log.debug("request begin: {}",
                kv("request_uri", request.getURI()),
                kv("request_method", request.getMethod()),
                kv("request_headers", request.getHeaders()),
                kv(LogMessageKey.REQUEST_BODY, new String(body, "UTF-8"))
        );
    }

    private void logResponse(ClientHttpResponse response) throws IOException {
        log.debug("response begin: {}",
                kv("response_status", response.getRawStatusCode()),
                kv("response_text", response.getStatusText()),
                kv("response_headers", response.getHeaders()),
                kv("response_body", StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()))
        );
    }
}
