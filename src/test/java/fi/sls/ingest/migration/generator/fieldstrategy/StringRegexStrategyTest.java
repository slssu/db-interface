package fi.sls.ingest.migration.generator.fieldstrategy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringRegexStrategyTest {

    @Test
    void generate_matchesOTA112For_ArchiveCatalogNr() {

        StringRegexStrategy testable = new StringRegexStrategy("[a-z]+[0-9]+_([0-9])+_[a-z]+_");

        String rtn = testable.generate("OTAfoto/ota112/mapp_1/ota112_1_foto_00005.tif");

        assertEquals("1", rtn);

    }

    @Test
    void generate_matchesSignumNr() {

        StringRegexStrategy testable = new StringRegexStrategy("([0-9]+)$");

        String rtn = testable.generate("OTA:ota112-1_ent.foto.05754");

        assertEquals("05754", rtn);

    }
}