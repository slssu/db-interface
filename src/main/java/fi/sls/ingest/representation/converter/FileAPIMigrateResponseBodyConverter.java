package fi.sls.ingest.representation.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.proxy.slsfileapi.FileAPIMigrateResponseBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.AttributeConverter;
import java.io.IOException;

@Slf4j
public class FileAPIMigrateResponseBodyConverter extends JsonStringConverter implements AttributeConverter<FileAPIMigrateResponseBody, String> {
    @Autowired
    public FileAPIMigrateResponseBodyConverter(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public String convertToDatabaseColumn(FileAPIMigrateResponseBody fileAPIResponseBody) {

        if(fileAPIResponseBody == null){
            return null;
        } else {
            String rtnJson = null;

            try{
                rtnJson = objectMapper.writeValueAsString(fileAPIResponseBody);
            } catch (JsonProcessingException e) {
                log.error("Error converting from FileAPIMigrateResponseBody to JSON: {}", e.getMessage());
            }
            return rtnJson;
        }

    }

    @Override
    public FileAPIMigrateResponseBody convertToEntityAttribute(String jsonData) {
        if(jsonData == null){
            return null;
        } else {
            FileAPIMigrateResponseBody rtnItem = null;
            try{
                rtnItem = objectMapper.readValue(jsonData, FileAPIMigrateResponseBody.class);
            } catch (IOException e) {
                log.error("Error converting to FileAPIMigrateResponseBody from JSON: {}", e.getMessage());
            }

            return rtnItem;
        }
    }
}
