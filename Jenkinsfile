#!/usr/bin/env groovy

// stores reference to the locally built docker image
def buildImage
// tag that will be used for the docker image when pushed to registry
def buildTag

pipeline {
    environment {
        deployUser = "deploy"
        deployPath = "/var/www/ingest_api"
        deployServer = "ingest01.sls.fi"
        registry = "slsfinland/dbinterface"
        // Id of credentials stored in SLS Jenkins to auth against slsfinland docker hub registry
        registryCredential = 'dockerhub_credentials'
        // Use Pipeline Utility Steps plugin to read information from pom.xml into env variables (https://plugins.jenkins.io/pipeline-utility-steps/)
        artifactId = readMavenPom().getArtifactId()
        artifactVersion = readMavenPom().getVersion()
        mavenVersion = '3.6.2-jdk-11'
    }
    agent any

    // For develop branch, we run unit tests + build image + push to docker cloud as :dev
    // For release/* branches we run unit tests + build image + push to docker cloud as :rc + publish to staging (once we have separate staging/prod)
    // For master branch we run unit tests + build docker image + push to docker cloud + publish to production (with manual confirm)

    stages {

        stage('Unit tests') {
            agent {
                docker {
                    image "maven:${mavenVersion}"
                    reuseNode true
                    args "-v /var/lib/jenkins/.m2:/root/.m2 -v ${WORKSPACE}/docker:/output/docker"
                }
            }
            steps {
                sh 'mvn clean test'
                junit 'target/surefire-reports/**/*.xml'
            }
        }

        stage('Build release package') {
            agent {
                docker {
                    image "maven:${mavenVersion}"
                    reuseNode true
                    args "-v /var/lib/jenkins/.m2:/root/.m2 -v ${WORKSPACE}/docker:/output/docker"
                }
            }
            steps {
                sh 'mvn clean package -Dmaven.test.skip=true'
                sh "cp ./target/${artifactId}*.jar /output/docker/ingest.jar"
            }

        }

        stage('Build Docker image') {
            steps {
                script {
                    if(env.BRANCH_NAME == 'master'){
                        buildTag = artifactVersion
                    } else if(env.BRANCH_NAME.startsWith("release/")){
                        buildTag = 'rc'
                    } else {
                        buildTag = 'dev'
                    }

                    buildImage = docker.build("${registry}:${buildTag}", "./docker")
                }
            }
        }

        stage('Publish docker image') {
            steps {
                script {
                    docker.withRegistry( '', registryCredential ) {
                        buildImage.push()
                    }
                }
            }
        }

        stage('Release to staging') {
            when{
                branch pattern: "release/*", comparator: "REGEXP"
                beforeInput true
            }
            steps {
                echo "Release to staging not yet implemented"
      //          sshagent (credentials: ["${deployUser}"]) {
     //               sh '''
    //                    scp ${WORKSPACE}/ingest-stack/docker-compose.yml \
   //                      ${WORKSPACE}/ingest-stack/docker-compose.prod.yml \
  //                       ${deployUser}@${deployServer}:${deployPath}/
 //                   '''
//                    sh "ssh -l ${deployUser} ${deployServer} 'cd ${deployPath} && docker-compose -f docker-compose.yml -f docker-compose.prod.yml config > ingest-swarm2.yml'"
//                    sh "ssh -l ${deployUser} ${deployServer} 'cd ${deployPath} && docker stack deploy --compose-file ingest-swarm.yml sls_ingest'"
//                }

            }
        }

        stage('Release to production') {
            when {
                branch "master"
                beforeInput true
            }
            input { message 'Will release to PRODUCTION, do you want to proceed?'}

            steps {
                sshagent (credentials: ["${deployUser}"]) {
                    sh '''
                        scp ${WORKSPACE}/ingest-stack/docker-compose.yml \
                         ${WORKSPACE}/ingest-stack/docker-compose.prod.yml \
                         ${deployUser}@${deployServer}:${deployPath}/
                    '''
                    sh "ssh -l ${deployUser} ${deployServer} 'cd ${deployPath} && docker-compose -f docker-compose.yml -f docker-compose.prod.yml config > ingest-swarm.yml'"
                    sh "ssh -l ${deployUser} ${deployServer} 'cd ${deployPath} && docker stack deploy --compose-file ingest-swarm.yml sls_ingest'"
                }
            }
        }
    }
}