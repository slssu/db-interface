package fi.sls.ingest.proxy.filemaker.response;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Optional;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class FileMakerResponse implements Serializable, FileMakerResponseInterface {

    ArrayList<FileMakerResponseCode> messages;

    public FileMakerResponse(){
        this.messages = new ArrayList<>();
    }

    /**
     * Return the first response code from the messages array, or empty if none exists
     *
     * @return
     */
    public Optional<FileMakerResponseCode> getFirstResponseCode(){
        if(this.getMessages().size() > 0){
            return Optional.of(this.getMessages().get(0));
        } else {
            return Optional.empty();
        }
    }
}
