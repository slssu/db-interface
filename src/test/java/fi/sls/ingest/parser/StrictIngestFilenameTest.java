package fi.sls.ingest.parser;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class StrictIngestFilenameTest {

    static Validator validator;

    @BeforeAll
    public static void setupCls(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void validator_requiresFixedLengthSignumNr(){
        StrictIngestFilename testable = new StrictIngestFilename("ota_555j.1.4_brev_1234_1234.tif");
        Set<ConstraintViolation<StrictIngestFilename>> constraintViolations = validator.validate(testable);
        assertEquals(1, constraintViolations.size());
        assertEquals("File name must follow the SLS naming convention strictly.", constraintViolations.iterator().next().getMessage());

        testable = new StrictIngestFilename("ota_555j.1.4_brev_12345_1234.tif");
        constraintViolations = validator.validate(testable);
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void validator_requiresFixedLengthPageNr(){
        StrictIngestFilename testable = new StrictIngestFilename("ota_555j.1.4_brev_12345_123.tif");
        Set<ConstraintViolation<StrictIngestFilename>> constraintViolations = validator.validate(testable);
        assertEquals(1, constraintViolations.size());
        assertEquals("File name must follow the SLS naming convention strictly.", constraintViolations.iterator().next().getMessage());

        testable = new StrictIngestFilename("ota_555j.1.4_brev_12345_1234.tif");
        constraintViolations = validator.validate(testable);
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void validator_requiresFixedLengthVersionNrIfExists(){
        StrictIngestFilename testable = new StrictIngestFilename("ota_555j.1.4_brev_12345_1234_1.tif");
        Set<ConstraintViolation<StrictIngestFilename>> constraintViolations = validator.validate(testable);
        assertEquals(1, constraintViolations.size());
        assertEquals("File name must follow the SLS naming convention strictly.", constraintViolations.iterator().next().getMessage());

        testable = new StrictIngestFilename("ota_555j.1.4_brev_12345_1234_01.tif");
        constraintViolations = validator.validate(testable);
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void validator_allowsAlphaInCollection(){
        StrictIngestFilename testable = new StrictIngestFilename("ota_555j.a_brev_12345_1234_1.tif");
        Set<ConstraintViolation<StrictIngestFilename>> constraintViolations = validator.validate(testable);
        assertEquals(1, constraintViolations.size());
        assertEquals("File name must follow the SLS naming convention strictly.", constraintViolations.iterator().next().getMessage());

        testable = new StrictIngestFilename("ota_555j.a_brev_12345_1234_01.tif");
        constraintViolations = validator.validate(testable);
        assertEquals(0, constraintViolations.size());

        testable = new StrictIngestFilename("ota_555j.a.12_brev_12345_1234_01.tif");
        constraintViolations = validator.validate(testable);
        assertEquals(0, constraintViolations.size());

        testable = new StrictIngestFilename("ota_555j.c66_brev_12345_1234_01.tif");
        constraintViolations = validator.validate(testable);
        assertEquals(0, constraintViolations.size());
    }
}