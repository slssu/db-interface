package fi.sls.ingest.actuator.metrics.supplier;

import fi.sls.ingest.repository.IngestItemRepository;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * Calculates and returns the average time in seconds it takes for a IngestProcessingItem to go through the system,
 * based on the values of processingStartedAt and processingCompletedAt and items having the state COMPLETE or ARCHIVED
 */
public class ProcessingItemAverageDurationSupplier implements Supplier<Number> {

    private IngestItemRepository source;

    public ProcessingItemAverageDurationSupplier(IngestItemRepository source) {
        this.source = source;
    }

    @Override
    public Number get() {

        Optional<Double> avg = source.getAverageProcessingDuration();

        return avg.orElse(Double.valueOf(0));
    }
}
