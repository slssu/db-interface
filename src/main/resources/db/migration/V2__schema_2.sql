
# Add index on processing_status
CREATE INDEX `idx_processing_status` ON `ingest_processing_item` (`processing_status`);

# Add displayName column on user
ALTER TABLE `user` ADD COLUMN `display_name` VARCHAR(255) AFTER `uuid`;