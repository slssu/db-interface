package fi.sls.ingest.proxy.filemaker.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fi.sls.ingest.proxy.filemaker.AuthToken;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticationResponse extends FileMakerResponse {

    AuthToken response;
}
