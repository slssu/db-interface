package fi.sls.ingest.controller.request;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AuthenticationRequestTest {

    @Test
    void getNoSuffixUsername() {

        AuthenticationRequest testable = new AuthenticationRequest();

        testable.setUsername("mister");
        assertEquals("mister", testable.getNoSuffixUsername());

        testable.setUsername("mister@");
        assertEquals("mister", testable.getNoSuffixUsername());

        testable.setUsername("mister@sls.fi");
        assertEquals("mister", testable.getNoSuffixUsername());

        testable.setUsername("mister@sls.fi@some.com");
        assertEquals("mister", testable.getNoSuffixUsername());

        testable.setUsername("@");
        assertEquals("", testable.getNoSuffixUsername());

        testable.setUsername(null);
        assertEquals(null, testable.getNoSuffixUsername());
    }
}