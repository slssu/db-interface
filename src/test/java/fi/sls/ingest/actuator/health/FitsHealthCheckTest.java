package fi.sls.ingest.actuator.health;

import fi.sls.ingest.fits.FitsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.web.client.RestClientException;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FitsHealthCheckTest {

    FitsHealthCheck testable;
    FitsService service;

    @BeforeEach
    void setUp(){
        service = mock(FitsService.class);

        testable = new FitsHealthCheck(service);
    }

    @Test
    void health_returnsUpForServiceOkResponse() {

        when(service.getVersion()).thenReturn(CompletableFuture.completedFuture("1.4.1"));

        Health response = testable.health();

        assertEquals(Status.UP, response.getStatus());
        assertNotNull(response.getDetails().get("version"));
        assertEquals("1.4.1", response.getDetails().get("version"));
        assertNotNull(response.getDetails().get("response_time_ms"));

        Long t = (Long)response.getDetails().get("response_time_ms");
    }

    @Test
    void health_returnsDownForServiceNullResponse() {

        when(service.getVersion()).thenReturn(CompletableFuture.completedFuture(null));

        Health response = testable.health();

        assertEquals(Status.DOWN, response.getStatus());
        assertNotNull(response.getDetails().get("error"));
        assertEquals("Could not determine state of service", response.getDetails().get("error"));

    }

    @Test
    void health_returnsDownForExecutionException() throws ExecutionException, InterruptedException {

        CompletableFuture<String> mockFuture = mock(CompletableFuture.class);

        ExecutionException e = new ExecutionException(new Throwable("mock exception"));

        when(mockFuture.get()).thenThrow(e);
        when(service.getVersion()).thenReturn(mockFuture);

        Health response = testable.health();

        assertEquals(Status.DOWN, response.getStatus());
        assertNotNull(response.getDetails().get("error"));
        assertEquals("java.lang.Throwable: mock exception", response.getDetails().get("error"));
    }

    @Test
    void health_returnsDownForInterruptedException() throws ExecutionException, InterruptedException {

        CompletableFuture<String> mockFuture = mock(CompletableFuture.class);

        InterruptedException e = new InterruptedException("mock exception");

        when(mockFuture.get()).thenThrow(e);
        when(service.getVersion()).thenReturn(mockFuture);

        Health response = testable.health();

        assertEquals(Status.DOWN, response.getStatus());
        assertNotNull(response.getDetails().get("error"));
        assertEquals("mock exception", response.getDetails().get("error"));
    }

    @Test
    void health_returnsDownForException() throws ExecutionException, InterruptedException {

        CompletableFuture<String> mockFuture = mock(CompletableFuture.class);

        RestClientException e = new RestClientException("mock exception");

        when(service.getVersion()).thenThrow(e);

        Health response = testable.health();

        assertEquals(Status.DOWN, response.getStatus());
        assertNotNull(response.getDetails().get("error"));
        assertEquals("mock exception", response.getDetails().get("error"));
    }
}