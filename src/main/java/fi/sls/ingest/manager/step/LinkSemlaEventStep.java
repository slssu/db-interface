package fi.sls.ingest.manager.step;

import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.DigitalObject;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntity;
import fi.sls.ingest.proxy.filemaker.response.record.SemlaEvent;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@Service
public class LinkSemlaEventStep extends RecordServiceStep {
    public LinkSemlaEventStep(RecordService service) {
        super(service);
    }

    /**
     * Performs the tasks of this step
     *
     * @return The created or updated SemlaEvent
     * @param digitalObject
     * @param intellectualEntity
     */
    public SemlaEvent executeStep(DigitalObject digitalObject, IntellectualEntity intellectualEntity) throws IOException {

        SemlaEvent link = new SemlaEvent();
        link.getFieldData().setDigitalObjectsNumber(digitalObject.getFieldData().getEntityNumber());
        link.getFieldData().setIntellectualEntityNumber(intellectualEntity.getFieldData().getEntityNumber());

        Optional<? extends FileMakerResponseEntity> existingItem;
//        jw 2022-02-14: remove try-catch as runnable will handle all exceptions
//        try {
            existingItem = service.findRecordByIdentifier(link);
            if(existingItem.isPresent()){
                link = (SemlaEvent) existingItem.get();
                log.info("DigitalObject with number {} already linked to IntellectualEntity with number {}",
                        link.getFieldData().getDigitalObjectsNumber(),
                        link.getFieldData().getIntellectualEntityNumber()
                );

            } else {

                link = (SemlaEvent) service.createObjectRecord(link);
                log.info("Created new link between DigitalObject with number {} and IntellectualEntity with number {}",
                        link.getFieldData().getDigitalObjectsNumber(),
                        link.getFieldData().getIntellectualEntityNumber()
                );
            }

            return link;
//        } catch (IOException e) {
//            log.error(e.toString());
//            // FIXME should we throw instead?
//            return null;
//        }
    }
}
