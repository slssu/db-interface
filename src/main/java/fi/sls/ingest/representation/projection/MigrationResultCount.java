package fi.sls.ingest.representation.projection;

import fi.sls.ingest.manager.ProcessingStatus;

/**
 * Projection interface for representing result from a aggregate query against a migration result repository
 */
public interface MigrationResultCount {

    Long getNumItems();
    String getCollectionName();
    Long getArchiveNr();
    ProcessingStatus getProcessingStatus();
}
