package fi.sls.ingest.manager;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ProcessingStatusTest {

    @Test
    void getStatusCode() {

        assertEquals(Integer.valueOf(3), ProcessingStatus.STAGING.getStatusCode());
        assertEquals(Integer.valueOf(20), ProcessingStatus.REJECTED.getStatusCode());
    }

    @Test
    void fromStatusCode() {

        assertEquals(ProcessingStatus.STAGING, ProcessingStatus.fromStatusCode(3));
        assertEquals(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE, ProcessingStatus.fromStatusCode(5));
        assertEquals(ProcessingStatus.PROCESSING_USER_COPIES, ProcessingStatus.fromStatusCode(65));
    }

    @Test
    void fromStatusCode_throwExceptionForUnsupportedCode() {

        assertThrows(IllegalArgumentException.class, () -> {
            ProcessingStatus.fromStatusCode(666);
        });
    }

    @Test
    void fromStatusCodeString(){
        assertEquals(ProcessingStatus.STAGING, ProcessingStatus.fromStatusCodeString("STAGING"));
        assertEquals(ProcessingStatus.COMPLETE, ProcessingStatus.fromStatusCodeString("COMPLETE"));
        assertEquals(ProcessingStatus.ARCHIVED, ProcessingStatus.fromStatusCodeString("ARCHIVED"));
    }

    @Test
    void fromStatusCodeString_ignoresCase(){
        assertEquals(ProcessingStatus.STAGING, ProcessingStatus.fromStatusCodeString("staGING"));
        assertEquals(ProcessingStatus.COMPLETE, ProcessingStatus.fromStatusCodeString("Complete"));
        assertEquals(ProcessingStatus.ARCHIVED, ProcessingStatus.fromStatusCodeString("archived"));
    }

    @Test
    void fromStatusCodeString_throwExceptionForUnsupportedCode() {

        assertThrows(IllegalArgumentException.class, () -> {
            ProcessingStatus.fromStatusCodeString("WHAT_IS_DIS");
        });
    }

    @Test
    void fromStatusListString_convertsToArrayOfEnums(){
        List<ProcessingStatus> expected = Arrays.asList(
                ProcessingStatus.STAGING,
                ProcessingStatus.COMPLETE,
                ProcessingStatus.ARCHIVED
        );

        assertEquals(expected, ProcessingStatus.fromStatusListString("STAGING,COMPLETE,ARCHIVED"));
        assertEquals(expected, ProcessingStatus.fromStatusListString("STAGING,complete, ARCHIVED "));
    }
}