package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.proxy.filemaker.request.RecordPayload;
import fi.sls.ingest.representation.file.FilenameMetaData;
import lombok.Data;
import org.springframework.util.StringUtils;

/**
 * Represents data from the intellectualEntities layout view in FileMaker
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IntellectualEntityFieldData implements RecordPayload {

    // ID c_nummer = nummer in FileMaker
    @JsonProperty("nummer")
    Long entityNumber;

    @JsonProperty("dc_type")
    String dcType;

    @JsonProperty("dc_type2")
    String dcType2;

    @JsonProperty("dc_publisher2")
    String dcPublisher2;

    @JsonProperty("dcterms_isPartOf")
    String dcTermsIsPartOf;

    @JsonProperty("dc_source")
    String dcSource;

    @JsonProperty("entity_identifier")
    String entityIdentifier;

    @JsonProperty("thumbnail")
    String thumbnail;

    @JsonProperty("databasbild")
    String databasbild;

    @JsonProperty("shown_page_nr")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String showPageNr;

    @JsonProperty("collection_order")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Long collectionOrder;

    /**
     * Populates all applicable metadata fields on this object from the provided result objects
     *
     * @param filenameMetaData
     * @param fitsResult
     */
    public void populateFromMetadataResults(FilenameMetaData filenameMetaData, FitsResult fitsResult){
        this.setDcType(filenameMetaData.getCategory().getCategoryLabel());
        this.setDcType2(filenameMetaData.getCategory().getSubCategory());
        this.setDcPublisher2(filenameMetaData.getCollectionDescriptionOrName());
        this.setDcTermsIsPartOf(filenameMetaData.getArchiveCollectionName());
        this.setDcSource(filenameMetaData.getArchiveSignum());
        this.setEntityIdentifier(filenameMetaData.getEntityIdentifier());
        this.setCollectionOrder(Long.valueOf(filenameMetaData.getSigumNr()));
    }
}
