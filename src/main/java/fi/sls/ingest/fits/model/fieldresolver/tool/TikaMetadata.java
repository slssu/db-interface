package fi.sls.ingest.fits.model.fieldresolver.tool;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.*;
import java.util.Iterator;
import java.util.Optional;

/**
 * Provides algorithm for extracting a field value from the TIKA tool
 * result XML.
 *
 * If a new field needs to be extracted, it should be enough to define a new
 * constant with a value for the field name attribute.
 */
public class TikaMetadata extends AbstractFieldValueResolver {

    public final static String FIELD_X_RESOLUTION = "tiff:XResolution";
    public final static String FIELD_DATE_CREATED = "dcterms:created";
    public final static String FIELD_DATE_MODIFIED = "dcterms:modified";
    public final static String FIELD_MAKE = "Make";
    public final static String FIELD_MODEL = "Model";

    public TikaMetadata(String fieldName, FieldValueResolver resolver){
        super(fieldName, resolver);
    }

    public TikaMetadata(String fieldName, FitsResult fitsResult){
        super(fieldName, fitsResult);
    }

    public TikaMetadata(String fieldName, FitsResult fitsResult, FieldValueResolver resolver){
        super(fieldName, fitsResult, resolver);
    }

    @Override
    public Optional<String> getValue() {

        XPath xPath = XPathFactory.newInstance().newXPath();

        try {

            xPath.setNamespaceContext(new TikaNamespaceContext(fitsResult.getXmlDocument()));

            String expStr = "/fits:fits/fits:toolOutput/fits:tool[@name = 'Tika']/metadata/field[@name = '" + fieldName + "']/value";

            XPathExpression expression = xPath.compile(expStr);

            NodeList nodes = (NodeList) expression.evaluate(fitsResult.getXmlDocument(), XPathConstants.NODESET);
            if(nodes.getLength() > 0){
                Node n = nodes.item(0);
                String val = n.getTextContent();

                if(val != null && !val.isEmpty()){
                    return Optional.of(val);
                }
            }

            return executeNextResolver();

        } catch (XPathExpressionException e) {
            return executeNextResolver();
        }
    }

    class TikaNamespaceContext implements NamespaceContext {

        private Document sourceDocument;

        public TikaNamespaceContext(Document document){
            sourceDocument = document;
        }

        @Override
        public String getNamespaceURI(String prefix) {
            if (prefix.equals(XMLConstants.DEFAULT_NS_PREFIX)) {
                return sourceDocument.lookupNamespaceURI(null);
            } else if(prefix.equals("fits")) {
                return "http://hul.harvard.edu/ois/xml/ns/fits/fits_output";
            } else {
                return sourceDocument.lookupNamespaceURI(prefix);
            }
        }

        @Override
        public String getPrefix(String namespaceURI) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Iterator<String> getPrefixes(String namespaceURI) {
            throw new UnsupportedOperationException();
        }
    }
}

