package fi.sls.ingest.validation;

import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.IngestProcessingItemFlag;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.repository.IngestItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * Compares IngestProcessingItems with each other and flags items that belong
 * to the same collection, or have the same name except the suffix.
 *
 * Some of the flags are considered errors that should block the processing, other flags
 * are merely warnings that should be brought to the users attention.
 *
 * Only items with the status ProcessingStatus.STAGING are considered, except for validating
 * if the exact same path is already being processed.
 *
 * The error flags are added to the IngestProcessableItem as a list with statuses.
 */
@Service
@Slf4j
public class StagingItemsValidator {

    /**
     * Reference to the repository holding the current ingestItemQueue
     */
    IngestItemRepository ingestItemRepository;


    public StagingItemsValidator(IngestItemRepository ingestItemRepository){
        this.ingestItemRepository = ingestItemRepository;
    }


    /**
     * Loads all items from the current IngestProcessingItems queue
     * and validates the new item against the existing items.
     *
     * This assumes the processingItem has already passed the ValidProcessableFile validation
     */
    public void validate(IngestProcessingItem processingItem){

        List<ProcessingStatus> checkStatuses = Arrays.asList(
                ProcessingStatus.STAGING,
                ProcessingStatus.STAGING_REJECTED,
                ProcessingStatus.PROCESSING_FILENAME_METADATA_STAGING,
                ProcessingStatus.STAGING_REJECTED,
                ProcessingStatus.PRE_STAGING,
                ProcessingStatus.POST_STAGING);

        Iterable<IngestProcessingItem> items = ingestItemRepository.findByProcessingStatusIn(checkStatuses, Pageable.unpaged());

        items.forEach((IngestProcessingItem item)-> {

            // validate the entity identifier does not already exist in queue
            if(item.getFilenameMetaData() != null
                    && processingItem.getFilenameMetaData() != null
                    && item.getFilenameMetaData().getDigitalObjectIdentifierNoSuffix().equalsIgnoreCase(processingItem.getFilenameMetaData().getDigitalObjectIdentifierNoSuffix())
            ){
                processingItem.setProcessingStatus(ProcessingStatus.REJECTED);
                processingItem.setMessage(
                        String.format("An item with the same identifier is already queued: %s = %s",
                                processingItem.getFilenameMetaData().getDigitalObjectIdentifierNoSuffix(),
                                item.getFilenameMetaData().getDigitalObjectIdentifierNoSuffix()
                        )
                );
                //processingItem.addErrorFlag(createProcessingFlag(IngestProcessingItemFlag.Severity.ERROR, processingItem.getMessage()));
                log.debug("An item with the same identifier is already queued: {} {}",
                        kv("item_entity_identifier", processingItem.getFilenameMetaData().getDigitalObjectIdentifierNoSuffix()),
                        kv("existing_item_entity_identifier", item.getFilenameMetaData().getDigitalObjectIdentifierNoSuffix()),
                        kv(LogMessageKey.INGEST_PROCESSING_ITEM, processingItem.getLoggableItem())
                );
            }

            // check for warnings, but only against other staging items owned by the same user
            if( (item.getProcessingStatus() == ProcessingStatus.STAGING || item.getProcessingStatus() == ProcessingStatus.STAGING_REJECTED)
                    && processingItem.getCreatedBy().getId() == item.getCreatedBy().getId()
                    && item.getFilenameMetaData() != null
                    && processingItem.getFilenameMetaData() != null
                    && !item.getFilenameMetaData().getCollectionKey().equalsIgnoreCase(processingItem.getFilenameMetaData().getCollectionKey())
            ){

                processingItem.addErrorFlag(createProcessingFlag(IngestProcessingItemFlag.Severity.WARNING,
                        String.format("Item does not belong to same collection as existing items: %s != %s",
                                processingItem.getFilenameMetaData().getCollectionKey(),
                                item.getFilenameMetaData().getCollectionKey()
                        ))
                );
                log.debug("Item does not belong to same collection as existing staging items: {} {}",
                        kv("item_collection", processingItem.getFilenameMetaData().getCollectionKey()),
                        kv("existing_item_collection", item.getFilenameMetaData().getCollectionKey()),
                        kv(LogMessageKey.INGEST_PROCESSING_ITEM, processingItem.getLoggableItem())
                );
            }

        });

    }

    private IngestProcessingItemFlag createProcessingFlag(IngestProcessingItemFlag.Severity severity, String message) {

        return new IngestProcessingItemFlag(severity, message);
    }
}
