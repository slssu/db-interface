package fi.sls.ingest.queue.rabbitmq.receiver;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.manager.runnable.IngestPreProcessRunnable;
import fi.sls.ingest.queue.rabbitmq.exception.IngestAmqpReceiverException;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.SocketMessenger;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Handles IngestProcessingItems that are sent from the pre-processing queue
 *
 * This means items that have not yet been sent to the masterfiles service.
 */
@RabbitListener(queues = "${sls.queue.preProcessing}", containerFactory = "preProcessingRabbitListenerContainerFactory", concurrency = "${sls.concurrency.preProcessing:1}")
@Slf4j
public class IngestPreProcessingItemReceiver extends IngestItemReceiverBase {

    /**
     * Reference to runnable used to actually process the given IngestProcessingItem
     */
    IngestPreProcessRunnable ingestPreProcessRunnable;

    @Autowired
    public IngestPreProcessingItemReceiver(
            ObjectMapper objectMapper,
            IngestItemRepository ingestItemRepository,
            SocketMessenger socketMessenger,
            IngestPreProcessRunnable ingestPreProcessRunnable
    ){
        super(objectMapper, ingestItemRepository, socketMessenger);

        this.ingestPreProcessRunnable = ingestPreProcessRunnable;
    }

    @RabbitHandler
    public void receive(String json, Message message) throws IngestAmqpReceiverException {
        if(hasExceededRetryCount(message)){
            storeMaxRetryError(json, getRetryCount(message));
        } else {
            receiveItem(json, ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);
        }
    }

    @Override
    public void startItemProcessing(IngestProcessingItem item) {
        MDC.getCopyOfContextMap();
        //ingestPreProcessRunnable.setItem(item);
        ingestPreProcessRunnable.run(item);
    }
}
