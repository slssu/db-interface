package fi.sls.ingest.fits.model.fieldresolver.audio;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import org.w3c.dom.Element;

import java.util.Optional;

/**
 * Resolver that fetches the audio data from the fits->metadata->audiostructure of the FITS result
 */
public class FitsAudioMetadata extends AbstractFieldValueResolver {
    public final static String FIELD_SAMPLE_RATE = "sampleRate";
    public final static String FIELD_CHANNELS = "channels";
    public final static String FIELD_BIT_DEPTH = "bitDepth";
    public final static String FIELD_AUDIO_DATA_ENCODING = "audioDataEncoding";
    public final static String FIELD_NUM_SAMPLES = "numSamples";

    public FitsAudioMetadata(String fieldName, FieldValueResolver resolver) {
        super(fieldName, resolver);
    }

    public FitsAudioMetadata(String fieldName, FitsResult fitsResult) {
        super(fieldName, fitsResult);
    }

    public FitsAudioMetadata(String fieldName, FitsResult fitsResult, FieldValueResolver resolver) {
        super(fieldName, fitsResult, resolver);
    }

    @Override
    public Optional<String> getValue() {
        if(fitsResult.getFitsResult().getMetadata() != null && fitsResult.getFitsResult().getMetadata().getAudio() != null){

            Optional<Element> item = fitsResult.getFitsResult().
                    getMetadata().getAudio().
                    getAny().
                    stream().
                    filter(
                            element ->
                                    element.getTagName() == fieldName

                    ).findFirst();

            if(item.isPresent() && !item.get().getTextContent().isEmpty()){
                return Optional.of(item.get().getTextContent());
            }
        }

        return executeNextResolver();
    }
}
