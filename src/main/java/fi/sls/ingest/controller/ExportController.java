package fi.sls.ingest.controller;

import fi.sls.ingest.controller.response.BinaryDataResponse;
import fi.sls.ingest.migration.report.XLSXMigrationStatusReport;
import fi.sls.ingest.reporting.IngestReportingService;
import fi.sls.ingest.reporting.IngestSummaryReport;
import fi.sls.ingest.reporting.StagingItemsReport;
import fi.sls.ingest.reporting.xlsx.XLSXStagingItemsReport;
import fi.sls.ingest.security.AuthenticationFacade;
import fi.sls.ingest.security.jwt.UserPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * Provides API endpoints for exporting data from the ingest database in various formats
 */
@BasePathAwareController
@RestController
@Slf4j
public class ExportController {

    /**
     * Reference to source of currently authenticated user
     */
    AuthenticationFacade authenticationFacade;

    IngestReportingService reportingService;

    @Autowired
    public ExportController(IngestReportingService reportingService,
                            AuthenticationFacade authenticationFacade){

        this.reportingService = reportingService;
        this.authenticationFacade = authenticationFacade;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/reporting/export")
    public IngestSummaryReport export(
            @RequestParam(name = "from", value = "", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> fromDate,
            @RequestParam(name = "to", value = "", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> toDate
    ) {
        log.debug("Received export request: {} {}",
                kv("from_date", fromDate),
                kv("to_date", toDate)
        );

        return reportingService.getSummaryReport(fromDate.orElse(null), toDate.orElse(null));
    }


    /**
     * Export an report on the current staging items for the authenticated user
     */
    @RequestMapping(method = RequestMethod.GET, value = "/reporting/staging/items", produces = {MediaType.APPLICATION_JSON_VALUE})
    public StagingItemsReport exportStagingItemsReport(){
        log.debug("Received exportStagingItemsReport request");

        return reportingService.getStagingItemReport(((UserPrincipal) authenticationFacade.getAuthentication().getPrincipal()).getUser());
    }

    /**
     * Export an report on the current staging items for the authenticated user in XLSX format
     */
    @RequestMapping(method = RequestMethod.GET, value = "/reporting/staging/items", produces = {"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    public void exportXLSXStagingItemsReport(HttpServletResponse response) throws IOException {
        log.debug("Received exportXLSXStagingItemsReport request");

        StagingItemsReport result = reportingService.getStagingItemReport(((UserPrincipal) authenticationFacade.getAuthentication().getPrincipal()).getUser());

        XLSXStagingItemsReport report = new XLSXStagingItemsReport(result);
        Workbook book = report.generate();

        LocalDate now = LocalDate.now();
        String filename = "staging_items-"+now.format(DateTimeFormatter.ISO_LOCAL_DATE)+".xlsx";

        populateReportResponse(book, filename, response);
    }

    /**
     * Export an report on the current staging items for the authenticated user in XLSX format
     *
     * Response is returned in a JSON structure as base64 encoded
     */
    @RequestMapping(method = RequestMethod.GET, value = "/reporting/staging/items/xlsx")
    public BinaryDataResponse exportXLSXStagingItemsReportWrapped(HttpServletResponse response) throws IOException {
        log.debug("Received exportXLSXStagingItemsReportDirect request");

        StagingItemsReport result = reportingService.getStagingItemReport(((UserPrincipal) authenticationFacade.getAuthentication().getPrincipal()).getUser());

        XLSXStagingItemsReport report = new XLSXStagingItemsReport(result);
        Workbook book = report.generate();

        LocalDate now = LocalDate.now();
        String filename = "staging_items-"+now.format(DateTimeFormatter.ISO_LOCAL_DATE)+".xlsx";

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        book.write(out);
        book.close();


        return new BinaryDataResponse(out, filename);
    }

    /**
     * Helper for generating a downloadable XLSX response for the response body
     *
     * TODO: move to a helper class that can be used by several services
     *
     * @param book
     * @param response
     * @throws IOException
     */
    protected void populateReportResponse(Workbook book, String filename, HttpServletResponse response) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        book.write(out);
        book.close();

        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());

        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+ URLEncoder.encode(filename, "UTF-8")+"\"");
        response.setContentLength(out.size());

        IOUtils.copy(in, response.getOutputStream());
    }
}
