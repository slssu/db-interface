package fi.sls.ingest.rest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.manager.IngestItem;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.File;

/**
 * Data transfer object for sending necessary data from client to server to
 * set up a new IngestProcessingItem.
 */
@Data
@NoArgsConstructor
public class IngestRequestItem implements IngestItem {

    /**
     * The name of the file to process
     */
    @NotBlank
    protected String name;

    /**
     * The full filesystem path to the file or directory to process.
     *
     * In case of a directory all files within that directory will be processed.
     */
//    @Pattern(regexp = "^[a-z]{2,4}_[0-9]+[a-z]*(.[0-9]+)*_[a-z]+_[0-9]{1,4}_[0-9]{1,4}(_orig)?.[a-z]+$", flags = Pattern.Flag.UNICODE_CASE )
    @NotBlank
    protected String path;

    /**
     * The full file path translated to a path relative to the root of the FITS /processing folder
     */
    protected String fitsPath;

    /**
     * Indicates if the path points to a directory or a file
     */
    protected Boolean isDirectory;

    /**
     * MD5 checksum of the path
     */
    protected String pathHash;

    @JsonIgnore
    public File getFileCopy(){
        // noop
        return null;
    }

    public IngestRequestItem(String path) {
        this.setPath(path);
    }
}
