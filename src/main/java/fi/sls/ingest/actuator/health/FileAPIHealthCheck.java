package fi.sls.ingest.actuator.health;

import fi.sls.ingest.proxy.slsfileapi.SLSFileAPI;
import fi.sls.ingest.proxy.slsfileapi.exception.FileAPIException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * Provides a health endpoint for checking status of the SLS File API service endpoint
 *
 * The check adds the following json structure to the /actuator/health endpoint
 *
 * {
 *    ...
 *
 *    "slsFileAPI": {
 *         "status": "UP",
 *         "details": {
 *             "response_time_ms": 123
 *         }
 *     }
 *
 *    ...
 * }
 */
@Component("slsFileAPI")
@ConditionalOnProperty(prefix = "management.health.slsFileAPI", name = "enabled", matchIfMissing = true)
public class FileAPIHealthCheck implements HealthIndicator {

    SLSFileAPI service;

    public FileAPIHealthCheck(@Qualifier("slsFileApiHealthCheckService") SLSFileAPI service){
        this.service = service;
    }

    @Override
    public Health health() {
        String errorMsg;

        try{
            long start = System.currentTimeMillis();
            String response = service.healthcheck();
            long stop = System.currentTimeMillis();
            if(response != null && response.equalsIgnoreCase("OK")){
                return Health.up().withDetail("response_time_ms", stop - start).build();
            } else {
                errorMsg = "Could not determine state of service";
            }

        } catch (FileAPIException e){
                errorMsg = e.getMessage();
        }

        return Health.down().withDetail("error", errorMsg).build();
    }
}
