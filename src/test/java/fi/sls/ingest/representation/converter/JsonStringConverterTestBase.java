package fi.sls.ingest.representation.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import javax.persistence.AttributeConverter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;

public abstract class JsonStringConverterTestBase<T> {

    AttributeConverter<T, String> testable;

    ObjectMapper objectMapper;

    void setUp() {
        objectMapper = mock(ObjectMapper.class);
    }

    @Test
    void convertToDatabaseColumn_returnsNullForNullInput() throws JsonProcessingException {
        String rtn = testable.convertToDatabaseColumn(null);
        assertNull(rtn);
    }

    @Test
    void convertToEntityAttribute_returnsNullForNullInput() throws IOException {
        T data = testable.convertToEntityAttribute(null);
        assertNull(data);
    }
}
