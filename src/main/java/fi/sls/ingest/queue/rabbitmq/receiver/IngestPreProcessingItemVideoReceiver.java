package fi.sls.ingest.queue.rabbitmq.receiver;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.manager.runnable.IngestPreProcessRunnable;
import fi.sls.ingest.queue.rabbitmq.exception.IngestAmqpReceiverException;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.SocketMessenger;
import org.slf4j.MDC;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

@RabbitListener(queues = "${sls.queue.preProcessingVideo}", containerFactory = "preProcessingRabbitListenerContainerFactory", concurrency = "${sls.concurrency.preProcessingVideo:1}")
public class IngestPreProcessingItemVideoReceiver extends IngestItemReceiverBase {
    /**
     * Reference to runnable used to actually process the given IngestProcessingItem
     */
    IngestPreProcessRunnable ingestPreProcessRunnable;

    @Autowired
    public IngestPreProcessingItemVideoReceiver(
            ObjectMapper objectMapper,
            IngestItemRepository ingestItemRepository,
            SocketMessenger socketMessenger,
            IngestPreProcessRunnable ingestPreProcessRunnable
    ){
        super(objectMapper, ingestItemRepository, socketMessenger);

        this.ingestPreProcessRunnable = ingestPreProcessRunnable;
    }

    @RabbitHandler
    public void receive(String json, Message message) throws IngestAmqpReceiverException {
        if(hasExceededRetryCount(message)){
            storeMaxRetryError(json, getRetryCount(message));
        } else {
            receiveItem(json, ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);
        }
    }

    @Override
    public void startItemProcessing(IngestProcessingItem item) {
        MDC.getCopyOfContextMap();
        //ingestPreProcessRunnable.setItem(item);
        ingestPreProcessRunnable.run(item);
    }
}
