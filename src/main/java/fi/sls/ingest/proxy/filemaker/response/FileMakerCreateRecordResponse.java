package fi.sls.ingest.proxy.filemaker.response;

import lombok.Data;

@Data
public class FileMakerCreateRecordResponse extends FileMakerResponse {
    FileMakerRecordId response;
}
