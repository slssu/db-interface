package fi.sls.ingest.manager;

import fi.sls.ingest.exception.InvalidProcessingStatusException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validates that a given item is allowed to transition to the new state
 *
 * E.g. only an item with the status COMPLETE is allowed to transition to ARCHIVED state
 */
@Service
public class ProcessingStatusTransitionHandler {

    protected Map<ProcessingStatus, List<ProcessingStatus>> allowedStatusMap;

    public ProcessingStatusTransitionHandler(){
        allowedStatusMap = new HashMap<>();

        // allowed states from COMPLETE
        allowedStatusMap.put(ProcessingStatus.COMPLETE, createAllowedStatusList(ProcessingStatus.COMPLETE));

        // allowed states from PROCESSING_USER_COPIES
        allowedStatusMap.put(ProcessingStatus.PROCESSING_USER_COPIES, createAllowedStatusList(ProcessingStatus.PROCESSING_USER_COPIES));

        // allowed states from PROCESSING_METADATA
        allowedStatusMap.put(ProcessingStatus.PROCESSING_METADATA, createAllowedStatusList(ProcessingStatus.PROCESSING_METADATA));

        // allowed states from STAGING
        allowedStatusMap.put(ProcessingStatus.STAGING, createAllowedStatusList(ProcessingStatus.STAGING));

        // allowed states from REJECTED
        allowedStatusMap.put(ProcessingStatus.REJECTED, createAllowedStatusList(ProcessingStatus.REJECTED));

        // allowed states from STAGING_REJECTED
        allowedStatusMap.put(ProcessingStatus.STAGING_REJECTED, createAllowedStatusList(ProcessingStatus.STAGING_REJECTED));

        // allowed states from CREATE_DATABASE_OBJECTS
        allowedStatusMap.put(ProcessingStatus.CREATE_DATABASE_OBJECTS, createAllowedStatusList(ProcessingStatus.CREATE_DATABASE_OBJECTS));

        // allowed states from PROCESSING_FILENAME_METADATA_STAGING
        allowedStatusMap.put(ProcessingStatus.PROCESSING_FILENAME_METADATA_STAGING, createAllowedStatusList(ProcessingStatus.PROCESSING_FILENAME_METADATA_STAGING));

        // allowed state from SEND_TO_PRE_PROCESSING_QUEUE
        allowedStatusMap.put(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE, createAllowedStatusList(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE));

    }


    /**
     * Compares the current ProcessingStatus of the processingItem with the requested newStatus
     * to determine if the item is allowed to transition to the new state.
     *
     * Sets the new state if the transition was allowed. Throws an exception if the transition was not allowed
     */
    public IngestProcessingItem transitionToState(IngestProcessingItem processingItem, ProcessingStatus newStatus){

        if(isStateTransitionAllowed(processingItem.getProcessingStatus(), newStatus)){
            processingItem.setProcessingStatus(newStatus);
        }

        return processingItem;
    }

    /**
     * Determines if a requested status change would be allowed
     *
     * @param fromStatus
     * @param toStatus
     * @return
     */
    public boolean isStateTransitionAllowed(ProcessingStatus fromStatus, ProcessingStatus toStatus){
        List<ProcessingStatus> transitionsAllowed = allowedStatusMap.get(fromStatus);

        if(transitionsAllowed == null){
            // the status does not have a mapping at all, so cannot be allowed either
            throw new InvalidProcessingStatusException(String.format("Transition not allowed from ProcessingStatus %s to ProcessingStatus %s", fromStatus, toStatus));
        }

        if(transitionsAllowed.contains(toStatus)){
            return true;
        } else {
            throw new InvalidProcessingStatusException(String.format("Transition not allowed from ProcessingStatus %s to ProcessingStatus %s", fromStatus, toStatus));
        }
    }

    protected List<ProcessingStatus> createAllowedStatusList(ProcessingStatus forStatus){
        List<ProcessingStatus> items = new ArrayList();

        // ALL statuses can transition to REJECTED status, except COMPLETE and ARCHIVED
        if(!forStatus.equals(ProcessingStatus.COMPLETE) && !forStatus.equals(ProcessingStatus.ARCHIVED)){
            items.add(ProcessingStatus.REJECTED);
        }

        switch (forStatus){
            case COMPLETE:
                items.add(ProcessingStatus.ARCHIVED);
                break;
            case CREATE_DATABASE_OBJECTS:
                items.add(ProcessingStatus.DELETED);
                items.add(ProcessingStatus.COMPLETE);
            case PROCESSING_USER_COPIES:
                items.add(ProcessingStatus.PROCESSING_USER_COPIES_COMPLETE);
                items.add(ProcessingStatus.DELETED);
                break;
            case PROCESSING_METADATA:
                items.add(ProcessingStatus.DELETED);
                break;
            case STAGING:
                items.add(ProcessingStatus.DELETED);
                items.add(ProcessingStatus.STAGING_REJECTED);
                break;
            case SEND_TO_PRE_PROCESSING_QUEUE:
                items.add(ProcessingStatus.DELETED);
                items.add(ProcessingStatus.PROCESSING_FILENAME_METADATA);
            case REJECTED:
            case STAGING_REJECTED:
            case PROCESSING_FILENAME_METADATA_STAGING:
                items.add(ProcessingStatus.DELETED);
                break;
        }

        return items;
    }
}
