package fi.sls.ingest.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.proxy.filemaker.AuthToken;
import fi.sls.ingest.proxy.filemaker.AuthTokenStore;
import fi.sls.ingest.proxy.filemaker.service.AuthenticationService;
import fi.sls.ingest.proxy.slsfileapi.SLSFileAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class HealthCheckServiceConfig {

    AuthTokenStore authTokenStore;
    RestTemplate restTemplate;
    FileMakerConfig fileMakerConfig;
    SLSFileAPIConfig slsFileAPIConfig;
    ObjectMapper objectMapper;


    @Autowired
    public HealthCheckServiceConfig(
            AuthTokenStore authTokenStore,
            @Qualifier("shortTimeoutRestTemplate") RestTemplate restTemplate,
            FileMakerConfig fileMakerConfig,
            SLSFileAPIConfig fileAPIConfig,
            ObjectMapper objectMapper
    ){
        this.authTokenStore = authTokenStore;
        this.restTemplate = restTemplate;
        this.fileMakerConfig = fileMakerConfig;
        this.slsFileAPIConfig = fileAPIConfig;
        this.objectMapper = objectMapper;
    }

    @Bean(name = "healthCheckAuthenticationService")
    public AuthenticationService healthCheckAuthenticationService(){
        return new AuthenticationService(this.authTokenStore, this.restTemplate, this.fileMakerConfig);
    }

    @Bean(name = "slsFileApiHealthCheckService")
    public SLSFileAPI slsFileApiHealthCheckService(){
        return new SLSFileAPI(restTemplate, slsFileAPIConfig, objectMapper);
    }
}
