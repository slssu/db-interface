package fi.sls.ingest.proxy.filemaker.controller;

import fi.sls.ingest.proxy.filemaker.AuthToken;
import fi.sls.ingest.proxy.filemaker.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * This class provides an authentication REST endpoint to the local filemaker proxy implementation.
 *
 * REST endpoints in the proxy.filemaker namespace should only be used to test that the connection between
 * this client (db-interface) and the FileMaker API works.
 */
@BasePathAwareController
@RestController
public class AuthenticationController {

    AuthenticationService authenticationService;

    @Autowired
    public AuthenticationController(AuthenticationService authenticationService){
        this.authenticationService = authenticationService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/proxy/filemaker/authenticate")
    public AuthToken authenticate() throws IOException {

        return authenticationService.authenticate();
    }

}
