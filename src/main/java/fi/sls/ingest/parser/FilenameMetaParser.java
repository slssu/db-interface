package fi.sls.ingest.parser;

import fi.sls.ingest.config.MetaDataConfig;
import fi.sls.ingest.exception.IngestFilenameException;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.representation.file.ObjectCategory;
import fi.sls.ingest.representation.file.ObjectCollection;
import fi.sls.ingest.validation.ValidProcessableFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Iterator;
import java.util.Set;


/**
 * This class provides a method for extracting metadata from a string that follows the conventions
 * for naming files in the SLS ingest process.
 *
 * <p>The extracted metadata object can be used to target a certain entry in the SLS Archiva database.
 *
 * @see ValidProcessableFile for the expected regular expression.
 */
@Service
@Validated
public class FilenameMetaParser {


    private MetaDataConfig metaDataConfig;
    private Validator validator;

    @Autowired
    public FilenameMetaParser(MetaDataConfig metaDataConfig, Validator validator){
        this.metaDataConfig = metaDataConfig;
        this.validator = validator;
    }

    /**
     * Parses a filename string in order to extract information about a file
     *
     * @param filename The string from which metadata should be extracted
     * @return A object populated with values from the
     * @throws IllegalArgumentException If the filename does not pass validation
     */
    public FilenameMetaData parse(String filename) throws IllegalArgumentException{

        Set<ConstraintViolation<StrictIngestFilename>> constraintViolations = validator.validate(new StrictIngestFilename(filename));

        if(!constraintViolations.isEmpty()){
            throw new IngestFilenameException(constraintViolations.iterator().next().getMessage());
        }

        return extractParts(filename);

    }

    /**
     * Extracts ingest item parts doing a relaxed filename validation
     *
     * @param filename
     * @return
     * @throws IllegalArgumentException
     */
    public FilenameMetaData parseWithRelaxedValidation(String filename) throws IllegalArgumentException {
        Set<ConstraintViolation<IngestFilename>> constraintViolations = validator.validate(new IngestFilename(filename));

        if(!constraintViolations.isEmpty()){
            throw new IngestFilenameException(constraintViolations.iterator().next().getMessage());
        }

        return extractParts(filename);
    }

    protected FilenameMetaData extractParts(String filename){
        FilenameMetaData rtn = new FilenameMetaData(filename, metaDataConfig.getIdentityPrefix());

        // remove the file suffix, because not needed for parsing
        rtn.setFileSuffix(filename.substring(filename.lastIndexOf(".")+1));
        String fn = filename.substring(0, filename.lastIndexOf("."));

        String[] splits = fn.split("_");

        // 0 = collection (Samling)
        rtn.setCollectionKey(splits[0]);
        rtn.setCollectionName(getCollectionByKey(rtn.getCollectionKey()));
        rtn.setCollectionDescription(getCollectionDescriptionByKey(rtn.getCollectionKey()));

        // 1 = archiveNr + index (arkivnummer + förteckningssiffror. förteckningssiffror valfria, split on '.' )
        if(splits[1].indexOf(".") >= 0){
            rtn.setArchiveNr(splits[1].substring(0, splits[1].indexOf(".")));
            rtn.setArchiveCatalogNr(splits[1].substring(splits[1].indexOf(".")+1));
        } else {
            rtn.setArchiveNr(splits[1]);
        }

        // 2 = subCategory (Underkategori, Arkiva)
        rtn.setCategory(this.getCategoryByKey(splits[2]));

        // 3 = sigNr (signumnummer)
        rtn.setSigumNr(Integer.parseInt(splits[3]));

        // 4 = pageNr (sidnummer)
        rtn.setPageNr(Integer.parseInt(splits[4]));

        // determine if file name contained version nr or not
        if(splits.length == 6 && !hasDigitalOriginalPart(splits) || splits.length == 7 && hasDigitalOriginalPart(splits)){
            // we have a file version, extract it
            // 5 = versionNr (entity_typeOrder)
            rtn.setVersionNr(Integer.parseInt(splits[5]));
            rtn.setDefaultVersionNr(false);
        } else {
            // no file version, default to 1
            rtn.setVersionNr(1);
            rtn.setDefaultVersionNr(true);
        }

        // last part before file suffix digitalt original or masterfil
        if(hasDigitalOriginalPart(splits)){
            rtn.setDigitalOriginal(true);
        }

        return rtn;
    }

    protected boolean hasDigitalOriginalPart(String[] splits){
        return splits[splits.length-1].equalsIgnoreCase("orig");
    }

    /**
     * Helper for fetching the name of a SLS collection from the configuration based on the given key.
     *
     * @param key The key to look for in the configuration
     * @return The defined collection label or null if none is found
     */
    protected String getCollectionByKey(String key){

        Iterator<ObjectCollection> it = this.metaDataConfig.getCollections().iterator();

        while(it.hasNext()){
            ObjectCollection oc = it.next();
            if(oc.getFileKey().equalsIgnoreCase(key)){
                return oc.getLabel();
            }
        }

        throw new IngestFilenameException(String.format("There is not a collection configured using key: %s", key));
    }

    /**
     * Helper for fetching the description / long name of a SLS collection
     * from the configuration based on the given key.
     *
     * @param key The key to look for in the configuration
     * @return The defined collection description if one is found
     */
    protected String getCollectionDescriptionByKey(String key){
        Iterator<ObjectCollection> it = this.metaDataConfig.getCollections().iterator();

        while(it.hasNext()){
            ObjectCollection oc = it.next();
            if(oc.getFileKey().equalsIgnoreCase(key)){
                return oc.getDescription();
            }
        }

        // in current parse() implementation, this line will never run because a previous
        // method call will already have thrown in case of a missing collection key
        throw new IngestFilenameException(String.format("There is not a collection configured using key: %s", key));
    }

    /**
     * Helper for fetching the SLS object category from the configuration.
     *
     * @param key The object key to look for
     * @return
     */
    protected ObjectCategory getCategoryByKey(String key){

        Iterator<ObjectCategory> it = this.metaDataConfig.getCategories().iterator();

        while(it.hasNext()){
            ObjectCategory oc = it.next();
            if(oc.getFileKey().equalsIgnoreCase(key)){
                return oc;
            }
        }

        throw new IngestFilenameException(String.format("There is not a category configured using key: %s", key));
    }
}
