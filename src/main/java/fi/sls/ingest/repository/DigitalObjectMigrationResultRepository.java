package fi.sls.ingest.repository;

import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.migration.DigitalObjectMigrationResult;
import fi.sls.ingest.representation.projection.MigrationResultCount;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DigitalObjectMigrationResultRepository extends PagingAndSortingTokenStorageRepository<DigitalObjectMigrationResult, Long> {


    List<DigitalObjectMigrationResult> findAllByCollectionNameAndArchiveNr(String collectionName, Long archiveNr);


    @Query("SELECT count(do.id) as numItems, do.collectionName as collectionName, do.archiveNr as archiveNr, do.processingStatus as processingStatus "
            +"FROM DigitalObjectMigrationResult do GROUP BY do.collectionName, do.archiveNr, do.processingStatus")
   List<MigrationResultCount> countMigrationResults();

    @Query(value = "SELECT COUNT(item) FROM DigitalObjectMigrationResult item WHERE item.collectionName = :collectionName AND item.archiveNr = :archiveNr AND item.processingStatus = :processingStatus ")
    long countByProcessingStatusAndCollection(ProcessingStatus processingStatus, String collectionName, Long archiveNr);
}
