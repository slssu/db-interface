package fi.sls.ingest.proxy.filemaker;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthToken implements Serializable{

    @Getter
    @Setter
    private String token;

    public AuthToken(){}

    public AuthToken(String token){
        this.token = token;
    }
}
