package fi.sls.ingest.manager.step;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.DigitalObject;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import fi.sls.ingest.representation.file.FilenameMetaData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Slf4j
@Service
public class IngestDigitalObjectStep extends RecordServiceStep {

    @Autowired
    public IngestDigitalObjectStep(RecordService service) {
        super(service);
    }


    /**
     * Performs the tasks of this step
     *
     * @return The created or updated DigitalObject
     */
    public DigitalObject executeStep(FilenameMetaData metaData, FitsResult fitsResult, IngestProcessingItem item) throws IOException {

        DigitalObject record = new DigitalObject();
        record.getFieldData().setIdentifier(metaData.getDigitalObjectIdentifier());

        Optional<? extends FileMakerResponseEntity> existingItem;
//        jw 2022-02-14: remove try-catch as runnable will handle all exceptions
//        try {
            existingItem = service.findRecordByIdentifier(record);
            if(existingItem.isPresent()){
                record = (DigitalObject) existingItem.get();
                record.populateFromMetadataResults(metaData, fitsResult, item.getDigitizationProfile(), item);

                // value is calculated field in FileMaker, sending it back causes error so nullify
                if(record.getFieldData().getEntityNumber() != null && record.getFieldData().getEntityNumber() > 0){
                    record.getFieldData().setEntityNumber(null);
                }

                record = (DigitalObject) service.updateObjectRecord(record);
                log.info("Updated DigitalObject with identifier {} having number {}",
                        record.getFieldData().getEntityIdentifier(),
                        record.getFieldData().getEntityNumber()
                );
            } else {
                record.populateFromMetadataResults(metaData, fitsResult, item.getDigitizationProfile(), item);
                // Jens: Decided w. Rasmus to set the created timestamp here, because this is called after FileAPI returns a OK response
                // and thus after the file has been created in masterfiles
                LocalDateTime now = LocalDateTime.now(ZoneId.of("Europe/Helsinki"));
                record.getFieldData().setDateCreated(now.truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_DATE_TIME));
                record.getFieldData().setOriginalDateTimeCreated(record.getFieldData().getDateCreated());


                record = (DigitalObject) service.createObjectRecord(record);

                log.info("Created new DigitalObject with identifier {} having number {}",
                        record.getFieldData().getEntityIdentifier(),
                        record.getFieldData().getEntityNumber()
                );
            }
            return record;
//        } catch (IOException e) {
//            log.error(e.toString());
//            // FIXME should we throw instead?
//            return null;
//        }
    }
}
