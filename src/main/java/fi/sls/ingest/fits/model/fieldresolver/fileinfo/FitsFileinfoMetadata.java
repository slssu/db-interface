package fi.sls.ingest.fits.model.fieldresolver.fileinfo;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import fi.sls.ingest.fits.model.generated.FitsMetadataType;

import javax.xml.bind.JAXBElement;
import java.util.Optional;

public class FitsFileinfoMetadata extends AbstractFieldValueResolver {

    public static final String FIELD_MD5_CHECKSUM = "md5checksum";
    public static final String FIELD_FILESIZE = "size";
    public static final String FIELD_FILENAME = "filename";

    public FitsFileinfoMetadata(String fieldName, FieldValueResolver resolver) {
        super(fieldName, resolver);
    }

    public FitsFileinfoMetadata(String fieldName, FitsResult fitsResult){
        super(fieldName, fitsResult);
    }

    /**
     *
     * @param fitsResult
     * @param resolver
     */
    public FitsFileinfoMetadata(String fieldName, FitsResult fitsResult, FieldValueResolver resolver){
        super(fieldName, fitsResult, resolver);
    }

    @Override
    public Optional<String> getValue() {

        if(fitsResult.getFitsResult().getFileinfo() != null){
            Optional<JAXBElement<FitsMetadataType>> item = fitsResult.getFitsResult().
                    getFileinfo().
                    getFileInfoElements().
                    stream().
                    filter(
                            fitsMetadataTypeJAXBElement ->
                                    fitsMetadataTypeJAXBElement.
                                            getName().getLocalPart().equals(fieldName)
                    ).findFirst();

            if(item.isPresent()){
                return Optional.of(item.get().getValue().getContent().get(0).toString());
            }
        }

        return executeNextResolver();
    }
}
