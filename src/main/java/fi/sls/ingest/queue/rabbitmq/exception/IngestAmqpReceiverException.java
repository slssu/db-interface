package fi.sls.ingest.queue.rabbitmq.exception;

/**
 * Wrapper for exceptions thrown from AMQP receiver methods
 */
public class IngestAmqpReceiverException extends RuntimeException {
    public IngestAmqpReceiverException(Exception e) {
        super(e);
    }
}
