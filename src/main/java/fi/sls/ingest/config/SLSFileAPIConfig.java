package fi.sls.ingest.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="sls.fileapi")
@Data
public class SLSFileAPIConfig {
    private String url;


    public String getEndpointURL(String path){
        return url.concat(path);
    }
}
