package fi.sls.ingest.manager.step;

import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntity;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntityPartFieldData;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
public class LinkThumbnailStep extends RecordServiceStep {
    public LinkThumbnailStep(RecordService service) {
        super(service);
    }


    public IntellectualEntity executeStep(IntellectualEntity intellectualEntity, IntellectualEntityPartFieldData iePartFieldData) throws IOException {

        intellectualEntity.getFieldData().setThumbnail(iePartFieldData.getDatabaseThumbnail());
        intellectualEntity.getFieldData().setDatabasbild(iePartFieldData.getDatabaseImage());
        intellectualEntity.getFieldData().setShowPageNr(iePartFieldData.getOrderNr());

//        jw 2022-02-14: remove try-catch as it is handled by runnable
//        try {

            // value is calculated field in FileMaker, sending it back causes error so nullify
            if(intellectualEntity.getFieldData().getEntityNumber() != null && intellectualEntity.getFieldData().getEntityNumber() > 0){
                intellectualEntity.getFieldData().setEntityNumber(null);
            }

            intellectualEntity = (IntellectualEntity) service.updateObjectRecord(intellectualEntity);
            log.info("Updated IntellectualEntity with identifier {} having number {}",
                    intellectualEntity.getFieldData().getEntityIdentifier(),
                    intellectualEntity.getFieldData().getEntityNumber()
            );
            return intellectualEntity;
//        } catch (IOException e) {
//            log.error(e.toString());
//            // FIXME should we throw instead?
//            return null;
//        }
    }
}
