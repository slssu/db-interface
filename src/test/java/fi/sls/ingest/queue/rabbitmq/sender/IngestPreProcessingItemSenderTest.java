package fi.sls.ingest.queue.rabbitmq.sender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.manager.IngestItem;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.queue.rabbitmq.queue.IngestItemPreProcessingQueue;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.ws.SocketMessenger;
import org.apache.tika.Tika;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class IngestPreProcessingItemSenderTest {

    RabbitTemplate template;
    IngestItemPreProcessingQueue queue;
    Queue videoQueue;
    ObjectMapper objectMapper;
    SocketMessenger socketMessenger;
    IngestItemRepository ingestItemRepository;

    IngestPreProcessingItemSender testable;

    @BeforeEach
    void setUp() throws IOException {

        template = mock(RabbitTemplate.class);
        queue = mock(IngestItemPreProcessingQueue.class);
        when(queue.getName()).thenReturn("defaultQueue");
        videoQueue = mock(Queue.class);
        when(videoQueue.getName()).thenReturn("videoQueue");
        objectMapper = mock(ObjectMapper.class);
        socketMessenger = mock(SocketMessenger.class);
        ingestItemRepository = mock(IngestItemRepository.class);

        testable = new IngestPreProcessingItemSender(template, queue, videoQueue, objectMapper, socketMessenger, ingestItemRepository);
        testable.tika = mock(Tika.class);
        when(testable.tika.detect(any(File.class))).thenReturn("image/jpeg");
    }
    @Test
    void send_callsOverloadWithCorrectParam() {
        IngestProcessingItem item = mock(IngestProcessingItem.class);
        IngestProcessingItem item2 = mock(IngestProcessingItem.class);

        IngestItem i1 = mock(IngestItem.class);
        when(i1.getPath()).thenReturn("mocketypath1");

        when(item.getProcessingStatus()).thenReturn(ProcessingStatus.SEND_TO_POST_PROCESSING_QUEUE);

        when(item.getToken()).thenReturn("itemToken");
        when(item.getItem()).thenReturn(i1);
        when(item.getFilenameMetaData()).thenReturn(mock(FilenameMetaData.class));

        when(item2.getToken()).thenReturn("itemToken");
        when(item2.getItem()).thenReturn(i1);
        when(item.getFilenameMetaData()).thenReturn(mock(FilenameMetaData.class));

        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.of(item));

        IngestPreProcessingItemSender spyTestable = Mockito.spy(testable);

        spyTestable.send(item2);

        Mockito.verify(spyTestable).send(item2, ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);

    }

    @Test
    void send_throwsExceptionIfItemAlreadyQueued(){
        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");
        when(item.getProcessingStatus()).thenReturn(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);

        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.of(item));



        IngestProcessingException exception = assertThrows(IngestProcessingException.class, () -> {
            testable.send(item);
        });

        assertThat(exception.getMessage(), containsString("status SEND_TO_PRE_PROCESSING_QUEUE"));
        assertThat(exception.getMessage(), containsString("will not add again"));

    }

    @Test
    void send_throwsExceptionIfTikaFails() throws IOException {
        IngestItem i1 = mock(IngestItem.class);
        when(i1.getPath()).thenReturn("mocketypath1");

        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");
        when(item.getItem()).thenReturn(i1);
        when(item.getProcessingStatus()).thenReturn(ProcessingStatus.STAGING);
        when(item.getFilenameMetaData()).thenReturn(mock(FilenameMetaData.class));

        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.of(item));

        String mockJson = "jsonSerializedItem";
        when(objectMapper.writeValueAsString(item)).thenReturn(mockJson);

        when(testable.tika.detect(any(File.class))).thenThrow(IOException.class);

        IngestProcessingException e = assertThrows(IngestProcessingException.class, () -> {
            testable.send(item);
        });
    }


    @Test
    void send_failsIfItemNotPresentInRepository() throws JsonProcessingException {
        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");
        when(item.getProcessingStatus()).thenReturn(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);

        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.empty());

        String mockJson = "jsonSerializedItem";
        when(objectMapper.writeValueAsString(item)).thenReturn(mockJson);

        IngestProcessingException e = assertThrows(IngestProcessingException.class, () -> {
            testable.send(item);
        });

        assertEquals("Could not find stored item with token: itemToken", e.getMessage());



    }

    @Test
    void send_sendsItemToQueueAndSavesToRepository() throws JsonProcessingException {
        IngestItem i1 = mock(IngestItem.class);
        when(i1.getPath()).thenReturn("mocketypath1");

        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");
        when(item.getItem()).thenReturn(i1);
        when(item.getProcessingStatus())
                .thenReturn(ProcessingStatus.STAGING)
                .thenReturn(ProcessingStatus.REJECTED);
        when(item.getFilenameMetaData()).thenReturn(mock(FilenameMetaData.class));


        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.of(item));

        String mockJson = "jsonSerializedItem";
        when(objectMapper.writeValueAsString(item)).thenReturn(mockJson);

        testable.send(item);

        verify(template).convertAndSend(queue.getName(), mockJson);
        verify(item).setProcessingStatus(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);
        verify(ingestItemRepository).save(item);
    }

    @Test
    void send_sendsItemToVideoQueue() throws IOException {
        IngestItem i1 = mock(IngestItem.class);
        when(i1.getPath()).thenReturn("mocketypath1");

        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");
        when(item.getItem()).thenReturn(i1);
        when(item.getProcessingStatus())
                .thenReturn(ProcessingStatus.STAGING)
                .thenReturn(ProcessingStatus.REJECTED);
        when(item.getFilenameMetaData()).thenReturn(mock(FilenameMetaData.class));


        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.of(item));

        String mockJson = "jsonSerializedItem";
        when(objectMapper.writeValueAsString(item)).thenReturn(mockJson);

        when(testable.tika.detect(any(File.class))).thenReturn("video/matroska");

        testable.send(item);

        verify(template).convertAndSend(videoQueue.getName(), mockJson);
        verify(item).setProcessingStatus(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);
        verify(ingestItemRepository).save(item);
    }

    @Test
    void send_trowsExceptionIfItemNotSerializable() throws JsonProcessingException {
        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");

        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.empty());

        String mockJson = "jsonSerializedItem";
        when(objectMapper.writeValueAsString(item)).thenThrow(JsonProcessingException.class);

        assertThrows(IngestProcessingException.class, () -> {
            testable.send(item);
        });


    }
}