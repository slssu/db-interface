# Alter database to use utf8mb4
ALTER DATABASE ingest_api CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

# Alter all tables to use utf8mb4
ALTER TABLE
    `user`
    CONVERT TO CHARACTER SET utf8mb4
    COLLATE utf8mb4_unicode_ci;

ALTER TABLE
    `flyway_schema_history`
    CONVERT TO CHARACTER SET utf8mb4
    COLLATE utf8mb4_unicode_ci;

ALTER TABLE
    `ingest_processing_item`
    CONVERT TO CHARACTER SET utf8mb4
    COLLATE utf8mb4_unicode_ci;

ALTER TABLE
    `ingest_processing_item`
    CONVERT TO CHARACTER SET utf8mb4
    COLLATE utf8mb4_unicode_ci;

# Change to blob for compressed storage
ALTER TABLE `ingest_processing_item` MODIFY COLUMN `raw_fits_result` LONGBLOB;

# Optimize tables to ensure integrity
REPAIR TABLE `user`;
OPTIMIZE TABLE `user`;

REPAIR TABLE `flyway_schema_history`;
OPTIMIZE TABLE `flyway_schema_history`;

REPAIR TABLE `ingest_processing_item`;
OPTIMIZE TABLE `ingest_processing_item`;