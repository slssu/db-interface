package fi.sls.ingest.actuator.info;

import fi.sls.ingest.config.FileMakerConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.actuate.info.Info;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class FilemakerInfoContributorTest {

    FileMakerConfig config;
    FilemakerInfoContributor testable;

    @BeforeEach
    void setUp(){

        config = mock(FileMakerConfig.class);
        when(config.getUrl()).thenReturn("myurl");
        when(config.getDatabase()).thenReturn("mydatabase");
        when(config.getAgentName()).thenReturn("myagent");
        when(config.getAgentVersion()).thenReturn("1.3");

        testable = new FilemakerInfoContributor(config);
    }

    @Test
    void contribute_addsExpectedDataFromConfig() {

        Info.Builder builder = mock(Info.Builder.class);

        ArgumentCaptor<String> propCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Map<String, String>> detailsCaptor = ArgumentCaptor.forClass(Map.class);

        testable.contribute(builder);

        verify(builder).withDetail(propCaptor.capture(), detailsCaptor.capture());

        assertEquals("filemaker", propCaptor.getValue());

        assertEquals("myurl", detailsCaptor.getValue().get("url"));
        assertEquals("mydatabase", detailsCaptor.getValue().get("database"));
        assertEquals("myagent", detailsCaptor.getValue().get("agentName"));
        assertEquals("1.3", detailsCaptor.getValue().get("agentVersion"));

    }
}