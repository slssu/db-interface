package fi.sls.ingest.controller;

import fi.sls.ingest.manager.IngestDigitizationProfile;
import fi.sls.ingest.proxy.filemaker.response.record.DigitizationProfile;
import fi.sls.ingest.proxy.filemaker.service.DigitizationProfileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@BasePathAwareController
@RestController
@Slf4j
public class DigitizationProfileController {

    /**
     * Reference to service used for fetching DigitizationProfiles from FileMaker
     */
    DigitizationProfileService api;

    @Autowired
    public DigitizationProfileController(DigitizationProfileService api){
        this.api = api;
    }


    /**
     * @return A list of profiles used by SLS when digitizing material
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.GET, value = "/digitizationProfile")
    public List<IngestDigitizationProfile> listDigitizationProfiles() throws IOException {
        log.debug("Received listDigitizationProfiles request");
        ArrayList<IngestDigitizationProfile> rtn = new ArrayList<>();

        for (DigitizationProfile item : api.fetchDigitizationProfiles()) {
            rtn.add(item.createIngestDigitizationProfile());
        }

        return rtn;
    }
}
