package fi.sls.ingest.fits.model;

import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.tool.MediaInfoMetadata;
import fi.sls.ingest.fits.model.fieldresolver.video.FitsVideoMetadata;
import fi.sls.ingest.fits.model.generated.Fits;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.TimeZone;

/**
 * Represents the FITS analysis result for a file that has been identified as video data
 */
public class FitsVideoResult extends FitsAudioResult {
    public FitsVideoResult(Fits generatedResult) {
        super(generatedResult);
    }

    public FitsVideoResult(Fits generatedResult, String rawResult) {
        super(generatedResult, rawResult);
    }



    public Long getDuration(){
        FieldValueResolver parser = new FitsVideoMetadata(
                FitsVideoMetadata.FIELD_DURATION,
                this
        );

        return parseLongOrNull(parser);

    }


    /**
     * Target field in FileMaker: teknisk_data -> duration
     *
     * format is: HH:mm:ss.SSS
     *
     * @return
     */
    public String getFormattedDuration(){

        if(getDuration() != null){
            BigDecimal durationInMs = BigDecimal.valueOf(getDuration());

            Date date = Date.from(Instant.ofEpochMilli(durationInMs.longValue()));
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            return dateFormat.format(date);
        } else {
            return null;
        }
    }

    /**
     * Target field in FileMaker: teknisk_data -> av_bitRate
     *
     * @return
     */
    public Long getAvBitRate() {
        FieldValueResolver parser = new FitsVideoMetadata(
                FitsVideoMetadata.FIELD_BIT_RATE,
                this
        );

        return parseLongOrNull(parser);
    }

    /**
     * Target field in FileMaker: teknisk_data -> bit_depth
     * @return
     */
    public Integer getAvBitDepth(){
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_BIT_DEPTH,
                MediaInfoMetadata.TRACK_TYPE_VIDEO,
                this
        );

        return parseIntegerOrNull(parser);
    }

    /**
     * Target field in FileMaker: teknisk_data -> av_videoFormat
     *
     * @return
     */
    public String getBroadcastStandard(){
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_STANDARD,
                MediaInfoMetadata.TRACK_TYPE_VIDEO,
                this
        );

        return parseStringOrNull(parser);
    }

    public Integer getVideoWidth() {
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_WIDTH,
                MediaInfoMetadata.TRACK_TYPE_VIDEO,
                this
        );

        return parseIntegerOrNull(parser);

    }

    public Integer getVideoHeight() {
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_HEIGHT,
                MediaInfoMetadata.TRACK_TYPE_VIDEO,
                this
        );

        return parseIntegerOrNull(parser);
    }

    public String getFrameRateMode(){
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_FRAME_RATE_MODE,
                MediaInfoMetadata.TRACK_TYPE_VIDEO,
                this
        );

        return parseStringOrNull(parser);
    }

    public String getAvBitRateMode(){
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_BIT_RATE_MODE,
                MediaInfoMetadata.TRACK_TYPE_VIDEO,
                this
        );

        return parseStringOrNull(parser);
    }


    public String getFrameRate() {
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_FRAME_RATE,
                MediaInfoMetadata.TRACK_TYPE_VIDEO,
                this
        );

        return parseStringOrNull(parser);
    }

    public String getAvSampling() {
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_CHROMA_SUBSAMPLING,
                MediaInfoMetadata.TRACK_TYPE_VIDEO,
                this
        );

        return parseStringOrNull(parser);
    }

    public String getPixelAspectRatio() {
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_PIXEL_ASPECT_RATIO,
                MediaInfoMetadata.TRACK_TYPE_VIDEO,
                this
        );

        return parseStringOrNull(parser);
    }


    public String getDisplayAspectRatio(){
        FieldValueResolver parser = new FitsVideoMetadata(
                FitsVideoMetadata.FIELD_ASPECT_RATIO,
                FitsVideoMetadata.SUBSECTION_TRACK,
                FitsVideoMetadata.SUBSECTION_TYPE_TRACK_VIDEO,
                this
        );

        return parseStringOrNull(parser);

    }

    public Integer getNumVideoStreams(){
        FieldValueResolver parser = new MediaInfoMetadata(
                MediaInfoMetadata.FIELD_COUNT_OF_STREAMS,
                MediaInfoMetadata.TRACK_TYPE_VIDEO,
                this
        );

        return parseIntegerOrNull(parser);
    }

    /**
     * Target field in FileMaker: teknisk_data -> av_codecFormat
     *
     * @return
     */
    public String getCodecName(){
        FieldValueResolver parser = new FitsVideoMetadata(
                FitsVideoMetadata.FIELD_CODEC_NAME,
                FitsVideoMetadata.SUBSECTION_TRACK,
                FitsVideoMetadata.SUBSECTION_TYPE_TRACK_VIDEO,
                this
        );

        return parseStringOrNull(parser);

    }

    /**
     * Target field in FileMaker: teknisk_data -> av_codecInfo
     *
     * @return
     */
    public String getCodecInfo(){
        FieldValueResolver parser = new FitsVideoMetadata(
                FitsVideoMetadata.FIELD_CODEC_INFO,
                FitsVideoMetadata.SUBSECTION_TRACK,
                FitsVideoMetadata.SUBSECTION_TYPE_TRACK_VIDEO,
                this
        );

        return parseStringOrNull(parser);

    }

    /**
     * Target field in FileMaker: teknisk_data -> av_codecInfo
     *
     * @return
     */
    public String getCodecId(){
        FieldValueResolver parser = new FitsVideoMetadata(
                FitsVideoMetadata.FIELD_CODEC_ID,
                FitsVideoMetadata.SUBSECTION_TRACK,
                FitsVideoMetadata.SUBSECTION_TYPE_TRACK_VIDEO,
                this
        );

        return parseStringOrNull(parser);
    }

    /**
     * Target field in FileMaker: teknisk_data -> av_frameCount
     *
     * @return
     */
    public Long getFrameCount(){
        FieldValueResolver parser = new FitsVideoMetadata(
                FitsVideoMetadata.FIELD_FRAME_COUNT,
                FitsVideoMetadata.SUBSECTION_TRACK,
                FitsVideoMetadata.SUBSECTION_TYPE_TRACK_VIDEO,
                this
        );

        return parseLongOrNull(parser);
    }


    /**
     * Target field in FileMaker: teknisk_data -> av_scan
     *
     * @return
     */
    public String getScanningFormat(){
        FieldValueResolver parser = new FitsVideoMetadata(
                FitsVideoMetadata.FIELD_SCANNING_FORMAT,
                FitsVideoMetadata.SUBSECTION_TRACK,
                FitsVideoMetadata.SUBSECTION_TYPE_TRACK_VIDEO,
                this
        );

        return parseStringOrNull(parser);
    }


//    /**
//     * Target field in FileMaker: teknisk_data -> ljud_samplingFrequency
//     *
//     * @return
//     */
//    public Integer getAudioSamplingRate(){
//        FieldValueResolver parser = new FitsVideoMetadata(
//                FitsVideoMetadata.FIELD_SAMPLE_RATE,
//                FitsVideoMetadata.SUBSECTION_TRACK,
//                FitsVideoMetadata.SUBSECTION_TYPE_TRACK_AUDIO,
//                this
//        );
//
//        return parseIntegerOrNull(parser);
//    }
//
//    /**
//     * Target field in FileMaker: teknisk_data -> ljud_numChannels
//     *
//     * @return
//     */
//    public Integer getAudioNumChannels(){
//        FieldValueResolver parser = new FitsVideoMetadata(
//                FitsVideoMetadata.FIELD_AUDIO_NUM_CHANNELS,
//                FitsVideoMetadata.SUBSECTION_TRACK,
//                FitsVideoMetadata.SUBSECTION_TYPE_TRACK_AUDIO,
//                this
//        );
//
//        return parseIntegerOrNull(parser);
//    }
//
//    /**
//     * Target field in FileMaker: teknisk_data -> ljud_codec_name
//     *
//     * @return
//     */
//    public String getAudioCodecName(){
//        FieldValueResolver parser = new FitsVideoMetadata(
//                FitsVideoMetadata.FIELD_AUDIO_CODEC_NAME,
//                FitsVideoMetadata.SUBSECTION_TRACK,
//                FitsVideoMetadata.SUBSECTION_TYPE_TRACK_AUDIO,
//                this
//        );
//
//        return parseStringOrNull(parser);
//    }
}
