package fi.sls.ingest.actuator.metrics;

import fi.sls.ingest.manager.ProcessingStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class IngestProcessingStatusCount {

    ProcessingStatus processingStatus;
    long count;

}
