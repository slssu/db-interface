#!/bin/bash

# Build script runs a maven build,
# copies the result to the docker folder,
# runs docker build and tags the image with local/latest/version nr tags
#
#
#

# Script version
SCRIPT_VERSION=1.0.0
DOCKER_REPO=slsfinland/dbinterface

usage() {
  echo
  echo "Usage: scripts/build.sh [-t <versionTag>] [-v] [-p] [-y]"
  echo
  echo "-l build and tag local image only. Overrides other publish options."
  echo "-p publish the resulting docker image to Docker Hub."
  echo "-y Publish without asking for confirmation."
  echo "-t <versionTag> Tag the image with the given version string. If not provided, the version will be extracted from pom.xml."
  echo "-v prints version of this script and exists."
  echo
  echo "The script requires 'mvn' and 'docker' executables to exist on the path."
  echo
}


if [ $# -eq 0 ]; then
    usage
    exit 1
fi

versionTag=""
doForcePublish=0
doPrintVersion=0
doPublish=0
doLocalOnly=0

while getopts lypt:v flag
do
    case "${flag}" in
        t) versionTag=${OPTARG};;
        v) echo "Build script version: $SCRIPT_VERSION"; exit;;
        p) doPublish=1;;
        y) doForcePublish=1;;
        l) doLocalOnly=1;
    esac
done

if [ -z "$versionTag" ]; then
  echo "no versionTag provided, extracting from pom.xml..."
  versionTag=$(mvn -q \
      -Dexec.executable=echo \
      -Dexec.args='${project.version}' \
      --non-recursive \
      exec:exec)

  echo "Version extracted from pom.xml : $versionTag"
else
  echo "Will use version tag: $versionTag";
fi

if [ $doLocalOnly -eq 1 ]; then
  echo "Will tag local image only.";
  doPublish=0
  doForcePublish=0
fi

if [ $doPublish -eq 1 ]; then
    echo "Will tag image for publish to docker hub.";
else
    echo "Will NOT tag image for publish to docker hub. (Use -p flag to tag with latest/version tags.)";
fi

if [ $doForcePublish -eq 1 ]; then
    echo "Will automatically publish result image to docker hub.";
else
    echo "Will NOT automatically publish result image to docker hub.";
fi


echo "Running mvn clean package with tests..."
mvn clean package -Dspring.config.location=classpath:application.yml,classpath:application-test.yml

echo "Copy build jar to docker folder..."
cp target/ingest-api-*.jar docker/ingest.jar

if [ $doPublish -eq 1 ]; then
    echo "Build docker image and tag with $DOCKER_REPO:local, $DOCKER_REPO:latest, $DOCKER_REPO:$versionTag..."
    docker build -t slsfinland/dbinterface:local -t slsfinland/dbinterface:local -t slsfinland/dbinterface:$versionTag docker/.

    if [ $doForcePublish -eq 1 ]; then
      echo "publising docker tags $DOCKER_REPO:latest, $DOCKER_REPO:$versionTag to docker hub..."
      docker push $DOCKER_REPO:$versionTag
      docker push $DOCKER_REPO:latest
    else
      echo "Answering yes will publish $DOCKER_REPO:latest, $DOCKER_REPO:$versionTag to docker hub..."
      read -p "Are you sure [Y/n]? " -n 1 -r
      REPLY="${REPLY:-y}"
      echo    # (optional) move to a new line
      if [[ $REPLY =~ ^[Yy]$ ]]; then
          echo "publising docker tags $DOCKER_REPO:latest, $DOCKER_REPO:$versionTag to docker hub..."
          docker push $DOCKER_REPO:$versionTag
          docker push $DOCKER_REPO:latest
      else
          echo "aborting..."
      fi
    fi
else
    echo "Build docker image and tag with $DOCKER_REPO:local..."
    docker build -t slsfinland/dbinterface:local docker/.
fi

echo "done shifting the bits!"