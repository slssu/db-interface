package fi.sls.ingest.fits.model.fieldresolver.tool;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import org.w3c.dom.Element;

import java.util.Optional;

public class DroidMetadata extends AbstractFieldValueResolver {

    public final static String FIELD_FILE_PUID = "filePuid";


    public DroidMetadata(String fieldName, FieldValueResolver resolver) {
        super(fieldName, resolver);
    }

    public DroidMetadata(String fieldName, FitsResult fitsResult) {
        super(fieldName, fitsResult);
    }

    public DroidMetadata(String fieldName, FitsResult fitsResult, FieldValueResolver resolver) {
        super(fieldName, fitsResult, resolver);
    }

    @Override
    public Optional<String> getValue() {

        Element toolResult = getToolResultByName(fitsResult.getFitsResult(), "droid");

        if(toolResult != null && toolResult.getElementsByTagName(fieldName).getLength() > 0){
            String val = toolResult.getElementsByTagName(fieldName).item(0).getTextContent();
            if(val != null && !val.isEmpty()){
                return Optional.of(val);
            }
        }

        return executeNextResolver();
    }
}
