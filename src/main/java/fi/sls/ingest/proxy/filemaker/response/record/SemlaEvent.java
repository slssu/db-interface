package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import lombok.Data;
import lombok.Getter;

@Data
public class SemlaEvent extends FileMakerResponseEntity {

    @JsonIgnore
    @Getter
    private String layoutName = "ingestapi_semla_deltagande";

    SemlaEventFieldData fieldData;

    public SemlaEvent(){
        this.fieldData = new SemlaEventFieldData();
    }

    public SemlaEvent(Long intellectualEntityNumber, Long digitalObjectsNumber){
        this();

        getFieldData().setIntellectualEntityNumber(intellectualEntityNumber);
        getFieldData().setDigitalObjectsNumber(digitalObjectsNumber);

    }

    @Override
    public FindQueryRequest createFindQueryRequest() {
        FindQueryRequest queryRequest = new FindQueryRequest("ie_nummer", "="+getFieldData().getIeNumber());
        queryRequest.addToExistingQueryItem(0, "digitalObjects_nr", "="+getFieldData().getDigitalObjectsNr());
        return queryRequest;
    }

    @Override
    public FindQueryRequest createFindQueryRequest(boolean ignoreNullFields, Long pageSize){
        if(ignoreNullFields == false){
            return createFindQueryRequest();
        } else {
            FindQueryRequest queryRequest = new FindQueryRequest();

            if(pageSize != null && pageSize > 0){
                queryRequest.setLimit(pageSize.toString());
            }

            if(getFieldData().getIeNumber() != null){
                queryRequest.addQueryItem("ie_nummer", "="+getFieldData().getIeNumber());
            }

            if(getFieldData().getDigitalObjectsNr() != null){
                if(queryRequest.getQuery().isEmpty()){
                    queryRequest.addQueryItem("digitalObjects_nr", "="+getFieldData().getDigitalObjectsNr());
                } else {
                    queryRequest.addToExistingQueryItem(0, "digitalObjects_nr", "="+getFieldData().getDigitalObjectsNr());
                }
            }

            return queryRequest;

        }

    }

    @Override
    public UpdateRequest createUpdateRequest() {
        return new UpdateRequest(getFieldData());
    }

    @Override
    public CreateRequest createRecordRequest() {
        return new CreateRequest(getFieldData());
    }
}
