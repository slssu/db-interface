package fi.sls.ingest.manager.step;

import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.DerivateObject;
import fi.sls.ingest.proxy.filemaker.response.record.DigitalObject;
import fi.sls.ingest.proxy.filemaker.response.record.MTDerivateObjectsDO;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@Service
public class LinkDerivativeObjectDigitalObjectStep extends RecordServiceStep{
    public LinkDerivativeObjectDigitalObjectStep(RecordService service) {
        super(service);
    }

    /**
     * Performs the tasks of this step
     *
     * @return The created or updated MTDerivateObjectsDO
     */
    public MTDerivateObjectsDO executeStep(DerivateObject derivateObject, DigitalObject digitalObject) throws IOException {
        MTDerivateObjectsDO link = new MTDerivateObjectsDO();
        link.getFieldData().setDerNumber(derivateObject.getFieldData().getId().toString());
        link.getFieldData().setDoNumber(digitalObject.getFieldData().getEntityNumber());


        Optional<? extends FileMakerResponseEntity> existingItem;
//        jw 2022-02-14: remove try-catch as it is handled by runnable
//        try {
            existingItem = service.findRecordByIdentifier(link);
            if(existingItem.isPresent()){
                link = (MTDerivateObjectsDO) existingItem.get();
                log.info("DerivativeObject with number {} already linked to DigitalObject with number {}",
                        link.getFieldData().getDerNumber(),
                        link.getFieldData().getDoNumber()
                );

            } else {

                // add additional values that should be stored in link table
                link.getFieldData().setDoIdentifier(digitalObject.getFieldData().getIdentifier());

                link = (MTDerivateObjectsDO) service.createObjectRecord(link);
                log.info("Created new link between DerivativeObject with number {} and DigitalObject with number {}",
                        link.getFieldData().getDerNumber(),
                        link.getFieldData().getDoNumber()
                );
            }

            return link;
//        } catch (IOException e) {
//            log.error(e.toString());
//            // FIXME should we throw instead?
//            return null;
//        }
    }
}
