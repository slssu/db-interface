package fi.sls.ingest.proxy.filemaker.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.config.FileMakerConfig;
import fi.sls.ingest.proxy.filemaker.AuthTokenStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Service layer for accessing FileMaker API layouts
 */
@Service
public class RecordService extends RecordServiceBase {
    @Autowired
    public RecordService(
            AuthTokenStore authTokenStore,
            @Qualifier("mediumTimeoutRestTemplate") RestTemplate restTemplate,
            FileMakerConfig fileMakerConfig,
            ObjectMapper objectMapper
    ) {
        super(authTokenStore, restTemplate, fileMakerConfig, objectMapper);
    }
}
