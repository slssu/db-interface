package fi.sls.ingest.queue.rabbitmq.receiver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.manager.runnable.IngestPreProcessRunnable;
import fi.sls.ingest.queue.rabbitmq.exception.IngestAmqpReceiverException;
import fi.sls.ingest.queue.rabbitmq.receiver.IngestItemReceiverBase;
import fi.sls.ingest.queue.rabbitmq.receiver.IngestPreProcessingItemReceiver;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.SocketMessenger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.util.*;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class IngestPreProcessingItemReceiverTest {


    IngestPreProcessingItemReceiver testable;

    ObjectMapper objectMapper;
    IngestItemRepository ingestItemRepository;
    IngestPreProcessRunnable ingestPreProcessRunnable;
    SocketMessenger socketMessenger;
    Message message;
    MessageProperties messageProperties;


    @BeforeEach
    void setUp(){

        objectMapper = mock(ObjectMapper.class);
        ingestItemRepository = mock(IngestItemRepository.class);
        ingestPreProcessRunnable = mock(IngestPreProcessRunnable.class);
        socketMessenger = mock(SocketMessenger.class);
        message = mock(Message.class);

        messageProperties = mock(MessageProperties.class);
        when(message.getMessageProperties()).thenReturn(messageProperties);


        testable = new IngestPreProcessingItemReceiver(objectMapper, ingestItemRepository, socketMessenger, ingestPreProcessRunnable);
    }

    @Test
    void isInstanceOf_IngestItemReceiverBase(){
        assertThat(testable, instanceOf(IngestItemReceiverBase.class));
    }

    @Test
    void receive() throws IOException {
        String fakeJson = "{\"some\": \"testable\"}";
        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");
        when(item.getProcessingStatus()).thenReturn(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);

        IngestPreProcessingItemReceiver spyTestable = spy(testable);
        when(objectMapper.readValue(fakeJson, IngestProcessingItem.class)).thenReturn(item);
        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.of(item));

        spyTestable.receive(fakeJson, message);

        verify(spyTestable).receiveItem(fakeJson, ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);
    }

    @Test
    void receive_callsStartItemProcessingForValidItem() throws IOException {
        String fakeJson = "{\"some\": \"testable\"}";
        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");
        when(item.getProcessingStatus()).thenReturn(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);

        IngestPreProcessingItemReceiver spyTestable = spy(testable);
        when(objectMapper.readValue(fakeJson, IngestProcessingItem.class)).thenReturn(item);
        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.of(item));

        spyTestable.receive(fakeJson, message);

        verify(spyTestable).startItemProcessing(item);
    }

    @Test
    void receive_stopsAfterNRetries() throws IOException {
        String fakeJson = "{\"some\": \"testable\"}";
        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");
        when(item.getProcessingStatus()).thenReturn(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);

        Map<String, Long> deathMap = new HashMap<>();
        deathMap.put("count", Long.valueOf(3));

        List<Map<String,?>> xDeathHeader = new ArrayList<>();
        xDeathHeader.add(deathMap);

        when(messageProperties.getXDeathHeader()).thenReturn(xDeathHeader);

        IngestPreProcessingItemReceiver spyTestable = spy(testable);
        when(objectMapper.readValue(fakeJson, IngestProcessingItem.class)).thenReturn(item);
        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.of(item));

        spyTestable.receive(fakeJson, message);

        verify(spyTestable, times(0)).startItemProcessing(item);

        verify(item).setProcessingStatus(ProcessingStatus.REJECTED);
        verify(item).setProcessingStatus(ProcessingStatus.REJECTED);
    }

    @Test
    void receive_throwsExceptionIfItemStatusDoesNotMatchExpected() throws IOException {
        String fakeJson = "{\"some\": \"testable\"}";
        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");
        when(item.getProcessingStatus()).thenReturn(ProcessingStatus.STAGING);

        IngestPreProcessingItemReceiver spyTestable = spy(testable);
        when(objectMapper.readValue(fakeJson, IngestProcessingItem.class)).thenReturn(item);
        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.of(item));

        IngestProcessingException exception = assertThrows(IngestProcessingException.class, () -> {
            spyTestable.receive(fakeJson, message);
        });

        assertThat(exception.getMessage(), containsString("token=itemToken"));
        assertThat(exception.getMessage(), containsString("accept_status=SEND_TO_PRE_PROCESSING_QUEUE"));
        assertThat(exception.getMessage(), containsString("item_status=STAGING"));
    }

    @Test
    void receive_throwsExceptionIfItemDoesNotExistInRepository() throws IOException {
        String fakeJson = "{\"some\": \"testable\"}";
        IngestProcessingItem item = mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");

        IngestPreProcessingItemReceiver spyTestable = spy(testable);
        when(objectMapper.readValue(fakeJson, IngestProcessingItem.class)).thenReturn(item);
        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.empty());

        IngestProcessingException exception = assertThrows(IngestProcessingException.class, () -> {
            spyTestable.receive(fakeJson, message);
        });

        assertThat(exception.getMessage(), containsString("token=itemToken"));
    }

    @Test
    void receive_throwsExceptionIfDependencyException() throws IOException {
        String fakeJson = "{\"some\": \"testable\"}";

        when(objectMapper.readValue(fakeJson, IngestProcessingItem.class)).thenThrow(JsonProcessingException.class);

        IngestAmqpReceiverException exception = assertThrows(IngestAmqpReceiverException.class, () -> {
            testable.receive(fakeJson, message);
        });

        when(objectMapper.readValue("", IngestProcessingItem.class)).thenThrow(IllegalArgumentException.class);
        exception = assertThrows(IngestAmqpReceiverException.class, () -> {
            testable.receive("", message);
        });

        // FIXME: not sure a HttpClientErrorException is ever throw. And either case, the runnable should probably wrap it in a custom exception?
        when(objectMapper.readValue("clientError", IngestProcessingItem.class)).thenThrow(HttpClientErrorException.class);
        exception = assertThrows(IngestAmqpReceiverException.class, () -> {
            testable.receive("clientError", message);
        });
    }

    @Test
    void startItemProcessing() {
        IngestProcessingItem item = mock(IngestProcessingItem.class);

        testable.startItemProcessing(item);

        //verify(ingestPreProcessRunnable).setItem(item);
        verify(ingestPreProcessRunnable).run(item);

    }
}