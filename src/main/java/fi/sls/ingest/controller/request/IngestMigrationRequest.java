package fi.sls.ingest.controller.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IngestMigrationRequest implements Serializable {

    /**
     * Name of collection for which to migrate files.
     *
     * Must match a name in FileMaker layout ingestapi_samlingar->slsArkiv
     */
    @NotBlank
    String collectionName;

    /**
     * The number of the collection/archive
     *
     * Must match number in FileMaker layout ingestapi_samlingar->arkivetsNr
     */
    @Min(value = 0)
    @NotNull
    Long archiveNr;

    /**
     * The page nr to start from when doing paged requests
     *
     * Note that the next page rows will start at pageNr + limit
     *
     * 0 = first page
     */
    @Min(value = 0)
    Long pageNr = 0L;

    /**
     * The maximum nr of pages to process
     *
     * 0 = no limit, continue until FileMaker does not return anything anymore
     */
    @Min(value = 0)
    Long maxPageNr = 0L;

    /**
     * The nr of results per page to process
     *
     * 0 = unlimited, as in max value that FileMaker will return in one request (500 results)
     */
    @Min(value = 0)
    Long limit = 100L;

    /**
     * A Flag indicating if this is a dry-run request or not
     */
    @NotNull
    Boolean isDryRun = true;

    @JsonIgnore
    public Boolean isDryRun(){
        return isDryRun;
    }

    /**
     * The prefix for entityIdentifier to use for further filtering the results, if set
     *
     * Must be at least 2 characters long to be valid
     */
    @Size(min = 2)
    String entityIdentifier = null;
}
