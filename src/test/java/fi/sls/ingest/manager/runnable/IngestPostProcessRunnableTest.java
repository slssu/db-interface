package fi.sls.ingest.manager.runnable;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.manager.step.*;
import fi.sls.ingest.proxy.filemaker.response.record.*;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import fi.sls.ingest.proxy.slsfileapi.FileAPIResponseBody;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.ws.SocketMessenger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import javax.xml.parsers.DocumentBuilder;
import java.io.IOException;
import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import static org.mockito.Mockito.*;

class IngestPostProcessRunnableTest {

    IngestPostProcessRunnable testable;
    // dependencies below. It's a crazy amount, but...
    IngestItemRepository ingestItemRepository;
    SocketMessenger socketMessenger;
    Jaxb2Marshaller jaxb2Marshaller;
    DocumentBuilder documentBuilder;
    IngestCollectionStep ingestCollectionStep;
    IngestIntellectualEntityStep ingestIntellectualEntityStep;
    LinkCollectionIntellectualEntityStep linkCollectionIntellectualEntityStep;
    IngestDigitalObjectStep ingestDigitalObjectStep;
    LinkSemlaEventStep linkSemlaEventStep;
    IngestIntellectualEntityPartStep ingestIntellectualEntityPartStep;
    IngestDerivativeObjectStep ingestDerivativeObjectStep;
    LinkDerivativeObjectDigitalObjectStep linkDerivativeObjectDigitalObjectStep;
    LinkAgentStep linkAgentStep;
    LinkPremisEventDigitalObjectStep linkPremisEventDigitalObjectStep;
    IngestPremiseEventStep ingestPremiseEventStep;
    LinkThumbnailStep linkThumbnailStep;

    IngestProcessingItem ingestProcessingItem;

    Clock clock;

    Clock fixedClock;

    private static final LocalDate LOCAL_DATE = LocalDate.of(2019,10,22);


    @BeforeEach
    void setUp() {

        clock = mock(Clock.class);

        fixedClock = Clock.fixed(LOCAL_DATE.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
        doReturn(fixedClock.instant()).when(clock).instant();
        doReturn(fixedClock.getZone()).when(clock).getZone();

        ingestItemRepository = mock(IngestItemRepository.class);
        socketMessenger = mock(SocketMessenger.class);
        jaxb2Marshaller = mock(Jaxb2Marshaller.class);
        documentBuilder = mock(DocumentBuilder.class);
        ingestCollectionStep = mock(IngestCollectionStep.class);
        ingestIntellectualEntityStep = mock(IngestIntellectualEntityStep.class);
        linkCollectionIntellectualEntityStep = mock(LinkCollectionIntellectualEntityStep.class);
        ingestDigitalObjectStep = mock(IngestDigitalObjectStep.class);
        linkSemlaEventStep = mock(LinkSemlaEventStep.class);
        ingestIntellectualEntityPartStep = mock(IngestIntellectualEntityPartStep.class);
        ingestDerivativeObjectStep = mock(IngestDerivativeObjectStep.class);
        linkDerivativeObjectDigitalObjectStep = mock(LinkDerivativeObjectDigitalObjectStep.class);
        linkAgentStep = mock(LinkAgentStep.class);
        linkPremisEventDigitalObjectStep = mock(LinkPremisEventDigitalObjectStep.class);
        ingestPremiseEventStep = mock(IngestPremiseEventStep.class);
        linkThumbnailStep = mock(LinkThumbnailStep.class);

        testable = new IngestPostProcessRunnable(
                ingestItemRepository,
                socketMessenger,
                jaxb2Marshaller,
                documentBuilder,
                ingestCollectionStep,
                ingestIntellectualEntityStep,
                linkCollectionIntellectualEntityStep,
                ingestDigitalObjectStep,
                linkSemlaEventStep,
                ingestIntellectualEntityPartStep,
                ingestDerivativeObjectStep,
                linkDerivativeObjectDigitalObjectStep,
                linkAgentStep,
                linkPremisEventDigitalObjectStep,
                ingestPremiseEventStep,
                linkThumbnailStep,
                fixedClock);

        ingestProcessingItem = mock(IngestProcessingItem.class);
        //testable.setItem(ingestProcessingItem);
    }

    @Test
    void saveArchiveData_stopsWithErrorIfChecksumMismatch() throws IOException {

        IngestPostProcessRunnable spyTestable = spy(testable);


        IngestProcessingItem ingestProcessingItem = mock(IngestProcessingItem.class);
        when(ingestProcessingItem.md5ChecksumMatch()).thenReturn(false);

        FilenameMetaData filenameMetaData = mock(FilenameMetaData.class);
        FitsResult fitsResult = mock(FitsResult.class);

        spyTestable.saveArchiveData(ingestProcessingItem, filenameMetaData, fitsResult);

        verify(ingestProcessingItem, times(1)).resetErrorFlags();

        verify(spyTestable).setProcessingItemStatusAndSend(ProcessingStatus.REJECTED, ingestProcessingItem);

        verify(ingestCollectionStep, times(0)).executeStep(any());
    }

    @Test
    void saveArchiveData_callsNecessarySteps() throws IOException {

        // setup mocks
        IngestPostProcessRunnable spyTestable = spy(testable);

        IngestProcessingItem ingestProcessingItem = mock(IngestProcessingItem.class);
        when(ingestProcessingItem.md5ChecksumMatch()).thenReturn(true);
        when(ingestProcessingItem.getCustomerCopyFilePath()).thenReturn("customer/copy/filepath");

        FilenameMetaData filenameMetaData = mock(FilenameMetaData.class);
        FitsResult fitsResult = mock(FitsResult.class);

        CollectionFieldData collectionFieldData = mock(CollectionFieldData.class);
        Collection collection = mock(Collection.class);
        when(collection.getFieldData()).thenReturn(collectionFieldData);
        when(ingestCollectionStep.executeStep(filenameMetaData)).thenReturn(collection);

        IntellectualEntity intellectualEntity = mock(IntellectualEntity.class);
        IntellectualEntityFieldData intellectualEntityFieldData = mock(IntellectualEntityFieldData.class);
        when(intellectualEntity.getFieldData()).thenReturn(intellectualEntityFieldData);
        when(ingestIntellectualEntityStep.executeStep(filenameMetaData, fitsResult)).thenReturn(intellectualEntity);


        DigitalObject digitalObject = mock(DigitalObject.class);
        DigitalObjectFieldData digitalObjectFieldData = mock(DigitalObjectFieldData.class);
        when(digitalObjectFieldData.getCalculatedDateCreated()).thenReturn("10/22/2019");
        when(digitalObject.getFieldData()).thenReturn(digitalObjectFieldData);
        when(ingestDigitalObjectStep.executeStep(filenameMetaData, fitsResult, ingestProcessingItem)).thenReturn(digitalObject);

        SemlaEvent semlaEvent = mock(SemlaEvent.class);
        when(linkSemlaEventStep.executeStep(digitalObject, intellectualEntity)).thenReturn(semlaEvent);

        IntellectualEntityPart intellectualEntityPart = mock(IntellectualEntityPart.class);
        IntellectualEntityPartFieldData intellectualEntityPartFieldData = mock(IntellectualEntityPartFieldData.class);
        when(intellectualEntityPart.getFieldData()).thenReturn(intellectualEntityPartFieldData);
        when(ingestIntellectualEntityPartStep.executeStep(semlaEvent, digitalObject, ingestProcessingItem.getCustomerCopyFilePath())).thenReturn(intellectualEntityPart);



        // stub testable methods
        doNothing().when(spyTestable).linkAccessFiles(ingestProcessingItem, digitalObject, intellectualEntity, filenameMetaData);
        doNothing().when(spyTestable).linkPremisEvent(any(DigitalObject.class), any(MTPremiseEventDOType.class), any(PremisEventType.class), anyString(), anyString());


        // execute method
        spyTestable.saveArchiveData(ingestProcessingItem, filenameMetaData, fitsResult);


        // verifications
        verify(ingestProcessingItem, times(2)).resetErrorFlags();
        verify(spyTestable).setProcessingItemStatusAndSend(ProcessingStatus.CREATE_DATABASE_OBJECTS, ingestProcessingItem);

        verify(ingestCollectionStep, times(1)).executeStep(filenameMetaData);
        verify(ingestIntellectualEntityStep, times(1)).executeStep(filenameMetaData, fitsResult);

        verify(linkCollectionIntellectualEntityStep, times(1)).executeStep(collectionFieldData, intellectualEntityFieldData);

        verify(ingestDigitalObjectStep, times(1)).executeStep(filenameMetaData, fitsResult, ingestProcessingItem);

        verify(linkSemlaEventStep, times(1)).executeStep(digitalObject, intellectualEntity);

        verify(spyTestable, times(1)).linkAccessFiles(ingestProcessingItem, digitalObject, intellectualEntity, filenameMetaData);

        verify(ingestIntellectualEntityPartStep).executeStep(semlaEvent, digitalObject, "customer/copy/filepath");

        verify(spyTestable).linkPremisEvent(digitalObject, MTPremiseEventDOType.CREATED, PremisEventType.CREATION, "10/22/2019", "Det digitala objektet skapades.");
        verify(spyTestable).linkPremisEvent(digitalObject, MTPremiseEventDOType.INGESTION, PremisEventType.INGESTION, LOCAL_DATE.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")), "Filerna är införda i databasen.");
        verify(spyTestable).linkPremisEvent(digitalObject, MTPremiseEventDOType.DIGEST_CALCULATION, PremisEventType.DIGEST_CALCULATION, LOCAL_DATE.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")), "MD5 summan kalkylerad.");

        verify(linkThumbnailStep).executeStep(intellectualEntity, intellectualEntityPartFieldData);

        verify(spyTestable).setProcessingItemStatusAndSend(ProcessingStatus.COMPLETE, ingestProcessingItem);
    }


    @Test
    void linkAccessFiles() throws IOException {

        FileAPIResponseBody fileAPIResponseBody = mock(FileAPIResponseBody.class);
        when(fileAPIResponseBody.getAccessfilePaths()).thenReturn(Arrays.asList("filePath1"));

        AccessFile accessFile = mock(AccessFile.class);
        when(fileAPIResponseBody.getAccessFiles()).thenReturn(Arrays.asList(accessFile));
        when(ingestProcessingItem.getFileAPIResponse()).thenReturn(fileAPIResponseBody);

        FilenameMetaData filenameMetaData = mock(FilenameMetaData.class);
        IntellectualEntity intellectualEntity = mock(IntellectualEntity.class);

        DigitalObject digitalObject = mock(DigitalObject.class);
        DerivateObject derivateObject = mock(DerivateObject.class);



        when(ingestDerivativeObjectStep.executeStep(
                accessFile,
                filenameMetaData,
                intellectualEntity)
        ).thenReturn(derivateObject);

        testable.linkAccessFiles(ingestProcessingItem, digitalObject, intellectualEntity, filenameMetaData);

        verify(ingestDerivativeObjectStep, times(1)).executeStep(
                accessFile,
                filenameMetaData,
                intellectualEntity
        );

        verify(linkDerivativeObjectDigitalObjectStep, times(1)).executeStep(derivateObject, digitalObject);

        // FIXME: verify the order?
    }

    @Test
    void linkPremisEvent() throws IOException {
        DigitalObject digitalObject = mock(DigitalObject.class);
        MTPremiseEventDOType mtPremiseEventDOType = MTPremiseEventDOType.CREATED;
        PremisEventType premisEventType = PremisEventType.CREATION;
        String eventDate = "2019-10-21";
        String eventOutcomeNote = "created";

        MtPremiseEventsDO mtPremiseEventsDO = mock(MtPremiseEventsDO.class);
        when(linkPremisEventDigitalObjectStep.executeStep(digitalObject, mtPremiseEventDOType, eventDate)).thenReturn(mtPremiseEventsDO);

        PremiseEvent premiseEvent = mock(PremiseEvent.class);
        when(ingestPremiseEventStep.executeStep(
                digitalObject,
                mtPremiseEventsDO,
                mtPremiseEventDOType,
                premisEventType,
                eventDate, eventOutcomeNote
        )).thenReturn(premiseEvent);

        testable.linkPremisEvent(digitalObject, mtPremiseEventDOType, premisEventType, eventDate, eventOutcomeNote);

        verify(linkPremisEventDigitalObjectStep, times(1)).executeStep(digitalObject, mtPremiseEventDOType, eventDate);

        verify(ingestPremiseEventStep, times(1)).executeStep(
                digitalObject,
                mtPremiseEventsDO,
                mtPremiseEventDOType,
                premisEventType,
                eventDate, eventOutcomeNote
        );

        verify(linkAgentStep, times(1)).executeStep(premiseEvent);
    }
}