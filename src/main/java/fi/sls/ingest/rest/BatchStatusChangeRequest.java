package fi.sls.ingest.rest;

import fi.sls.ingest.manager.ProcessingStatus;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
public class BatchStatusChangeRequest {

    @NotNull
    @Valid
    ProcessingStatus fromStatus;

    @NotNull
    @Valid
    ProcessingStatus toStatus;

    @Valid
    String userIds = "";

}
