package fi.sls.ingest.fits.model.tool;

import fi.sls.ingest.fits.model.generated.Fits;
import org.w3c.dom.Element;

import java.util.Optional;

/**
 * Base class for tool specific output structures
 */
public class FitsToolResultBase {

    /**
     * Reference to the tool result structure, if one was found
     */
    Element toolResult;

    /**
     * The name of the tool whose result structure is stored
     */
    String toolName;

    public FitsToolResultBase(Fits generatedResult, String toolName){
        this.toolName = toolName;
        this.setToolResultByName(generatedResult, toolName);
    }

    public Element getToolResult(){
        return this.toolResult;
    }

    /**
     * Finds and sets the Element that contains tool specific results.
     *
     * If a result for the given tool is found, it is stored in the toolResult field of this class.
     *
     * @param name
     * @return The result element, if found
     */
    protected Element setToolResultByName(Fits generatedResult, String name){
        toolResult = null;

        if(generatedResult.getToolOutput() != null){
            Optional<Element> element = generatedResult.getToolOutput().getAny().stream().filter(
                    e -> e.getAttribute("name").equalsIgnoreCase(name)
            ).findFirst();

            if(element.isPresent()){
                toolResult = element.get();
            }
        }


        return toolResult;
    }

    /**
     * Returns true if this instance contains a result structure for a tool, i.e. the toolResult field has a value.
     * @return
     */
    public boolean hasToolResult(){
        return this.toolResult != null;
    }
}
