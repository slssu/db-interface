package fi.sls.ingest.actuator.metrics.supplier;

import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.repository.DigitalObjectMigrationResultRepository;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.representation.projection.MigrationResultCount;

import java.util.function.Supplier;

/**
 * Provides a supplier implementation that fetches the nr of DigitalObjectMigrationResult per status
 * currently in the database
 */
public class MigrationResultCountSupplier implements Supplier<Number> {

    private DigitalObjectMigrationResultRepository source;
    private MigrationResultCount filter;

    public MigrationResultCountSupplier(DigitalObjectMigrationResultRepository source){
        this.source = source;
        this.filter = null;
    }

    public MigrationResultCountSupplier(DigitalObjectMigrationResultRepository source, MigrationResultCount filter){
        this(source);
        this.filter = filter;
    }

    @Override
    public Number get() {
        if(filter == null){
            return source.count();
        } else {
            return source.countByProcessingStatusAndCollection(filter.getProcessingStatus(), filter.getCollectionName(), filter.getArchiveNr());
        }
    }
}
