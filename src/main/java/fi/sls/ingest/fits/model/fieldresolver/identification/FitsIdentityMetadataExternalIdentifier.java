package fi.sls.ingest.fits.model.fieldresolver.identification;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;

import java.util.Optional;

public class FitsIdentityMetadataExternalIdentifier extends AbstractFieldValueResolver {
    public FitsIdentityMetadataExternalIdentifier(FieldValueResolver resolver) {
        super("externalIdentifier", resolver);
    }

    public FitsIdentityMetadataExternalIdentifier(FitsResult fitsResult) {
        super("externalIdentifier", fitsResult);
    }

    public FitsIdentityMetadataExternalIdentifier(FitsResult fitsResult, FieldValueResolver resolver) {
        super("externalIdentifier", fitsResult, resolver);
    }

    @Override
    public Optional<String> getValue() {
        if(!fitsResult.getFitsResult().getIdentification().getIdentity().isEmpty() &&
                !fitsResult.getFitsResult().getIdentification().getIdentity().get(0).getExternalIdentifier().isEmpty()){
            String rtn = fitsResult.getFitsResult().getIdentification().getIdentity().get(0).getExternalIdentifier().get(0).getValue();

            if(rtn != null && !rtn.isEmpty()){
                return Optional.of(rtn);
            }
        }

        return executeNextResolver();
    }
}
