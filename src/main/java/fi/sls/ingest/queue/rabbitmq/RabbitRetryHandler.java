package fi.sls.ingest.queue.rabbitmq;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.repository.IngestItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.RabbitListenerErrorHandler;
import org.springframework.amqp.rabbit.support.ListenerExecutionFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Component
@Slf4j
class RabbitRetryHandler implements RabbitListenerErrorHandler {

    /**
     * Storage for storing the item so we can poll the status of it in client apps.
     */
    IngestItemRepository ingestItemRepository;

    /**
     * JSON mapper for converting the processing item to a json string
     */
    ObjectMapper objectMapper;

    @Autowired
    public RabbitRetryHandler(ObjectMapper objectMapper, IngestItemRepository ingestItemRepository){
        this.objectMapper = objectMapper;
        this.ingestItemRepository = ingestItemRepository;
    }

    @Override
    public Object handleError(Message amqpMessage, org.springframework.messaging.Message<?> message,
                              ListenerExecutionFailedException exception) throws Exception {
        log.debug("Check redelivery status of AMQP Message: {} ",
                kv("amqp_is_redelivered", amqpMessage.getMessageProperties().isRedelivered())
        );
        if (amqpMessage.getMessageProperties().isRedelivered()) {
            // Attempt to save the failed processing item error to the token store so we get feedback in the UI
            try {
                IngestProcessingItem item = objectMapper.readValue(amqpMessage.getBody(), IngestProcessingItem.class);
                Optional<IngestProcessingItem> checkItemExists = ingestItemRepository.findByToken(item.getToken());
                if(checkItemExists.isPresent()){
                    log.debug("Storing processing error for item with token: {}",
                            kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                            kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
                            );
                    checkItemExists.get().setProcessingStatus(ProcessingStatus.REJECTED);
                    checkItemExists.get().setMessage(exception.getMessage());
                    ingestItemRepository.save(checkItemExists.get());
                } else {
                    log.error("Could not store queue error for item because it does not exists in ingestItemRepository: {}",
                            kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken())
                    );
                    throw new IngestProcessingException(
                            String.format("Could not store queue error for item because it does not exist in ingestItemRepostory: %s", item.getToken())
                    );
                }
            } catch(IOException | IllegalArgumentException | IngestProcessingException e){
                log.error(e.getMessage());
                throw new AmqpRejectAndDontRequeueException(e);
            }
        }

        // Always throw the original exception as well to prevent retries, even after saving it to item status
        throw new AmqpRejectAndDontRequeueException(exception);
    }

}