package fi.sls.ingest.representation.file;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Represents a single SLS object category as defined by the sls.metadata.categories configuration
 * after the configuration has been initialized.
 */

@Data
@NoArgsConstructor
public class ObjectCategory implements Serializable {

    protected String fileKey;

    protected String archiveSignumKey;

    protected String categoryLabel;

    protected String subCategory;

    protected String dc2Type;

    public ObjectCategory(String fileKey, String archiveSignumKey, String categoryLabel, String subCategory, String dc2Type){
        this.fileKey = fileKey;
        this.archiveSignumKey = archiveSignumKey;
        this.categoryLabel = categoryLabel;
        this.subCategory = subCategory;
        this.dc2Type = dc2Type;
    }
}
