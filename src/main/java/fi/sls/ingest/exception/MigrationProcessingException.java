package fi.sls.ingest.exception;

/**
 * Exception thrown if something goes wrong during the ingest migration process.
 *
 * This is an unchecked exception, so we expect the caller to know to handle it if they want to.
 */
public class MigrationProcessingException extends RuntimeException {
    public MigrationProcessingException(String exception){
        super(exception);
    }
}
