package fi.sls.ingest.proxy.filemaker.response.record;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import fi.sls.ingest.fits.model.FitsAudioResult;
import fi.sls.ingest.fits.model.FitsImageResult;
import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.FitsVideoResult;
import fi.sls.ingest.manager.IngestDigitizationProfile;
import fi.sls.ingest.manager.MD5ComparisonItem;
import fi.sls.ingest.proxy.filemaker.request.RecordPayload;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.util.FITSDateUtil;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Represents data from the table digitalObjects, layout view teknisk_data in FileMaker
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DigitalObjectFieldData implements RecordPayload {

    // ID (nummer in FileMaker)
    @JsonProperty("nummer")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Long entityNumber;

    @JsonProperty("identifier")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String identifier;

    @JsonProperty("entity_identifier")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String entityIdentifier;

    @JsonProperty("entity_type")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String entityType;

    @JsonProperty("entity_label")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String entityLabel;

    /**
     * A string representing the digitization profile selected for this object
     *
     * @see fi.sls.ingest.proxy.filemaker.response.record.DigitizationProfileFieldData#profileId
     *
     */
    @JsonProperty("digiprofil_ID")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String digitizationProfileId;

    /**
     * A string that is calculated in FileMaker to represent a version of the dateCreated field
     *
     * Same as digitalObjects::c_created, but since that field is not returned from the json data we re-calculate it here
     */
    @JsonIgnore
    public String getCalculatedDateCreated(String dateCreated){
        String rtn = "";

        if(dateCreated != null && dateCreated != ""){

            try {
                LocalDateTime dateTime = FITSDateUtil.parseString(dateCreated);
                rtn = dateTime.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
            } catch (IllegalArgumentException e){
                // noop, we return empty string below
            }
        }

        return rtn;
    }

    @JsonIgnore
    public String getCalculatedDateCreated(){
        return this.getCalculatedDateCreated(dateCreated);
    }


    /**
     * A datetime for when the file was created, if available from metadata of file
     *
     * The string should be formatted as YYYY-MM-DD HH:mm:ss
     *
     * Because the FileMaker field for this is configured as a string, we pass a string instead of a real
     * LocalDateTime object.
     *
     * Note: We store the timestamp for when the FileAPI returns an OK response, indicating that the file
     * has been moved to masterfiles successfully
     *
     */
    @JsonProperty("created")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String dateCreated;

    @JsonProperty("modified")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String dateModified;

    @JsonProperty("entity_order")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String entityOrder;

    public void setEntityOrderInt(Integer entityOrderInt){
        if(entityOrderInt != null){
            this.entityOrder = entityOrderInt.toString();
        }
    }

    @JsonIgnore
    public Integer getEntityOrderInt(){
        if(this.entityOrder != null && !this.entityOrder.isEmpty()){
            return Integer.parseInt(this.entityOrder);
        } else {
            return null;
        }
    }



    // (2 for digital original, 1 for rest)
    @JsonProperty("entity_typeOrder")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String entityTypeOrder;

    public void setEntityTypeOrderInt(Integer entityTypeOrderInt){
        if(entityTypeOrderInt != null){
            this.entityTypeOrder = entityTypeOrderInt.toString();
        }
    }

    @JsonIgnore
    public Integer getEntityTypeOrderInt(){
        if(this.entityTypeOrder != null && !this.entityTypeOrder.isBlank()){
            return Integer.parseInt(this.entityTypeOrder);
        } else {
            return null;
        }
    }


    @JsonProperty("md5")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String md5Checksum;

    @JsonProperty("file_size")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Long fileSize;


    @JsonProperty("mediaStorage_filePath")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String mediaStorageFilePath;

    @JsonProperty("dc_identifier")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String dcIdentifier;

    @JsonProperty("dc2_type")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String dc2Type;

    @JsonProperty("DC2terms_hasFormat")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String dc2TermsHasFormat;

    @JsonProperty("width")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Integer width;

    @JsonProperty("height")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Integer height;

    @JsonProperty("resolution")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Integer resolution;

    @JsonProperty("bit_depth")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String bitDepth;

    @JsonProperty("color_mode")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String colorMode;

    @JsonProperty("color_profile")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String colorProfile;

    @JsonProperty("filetype_name")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String fileTypeName;

    @JsonProperty("filetype_MIME")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String fileTypeMime;

    @JsonProperty("filetype_pronom")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String fileTypePronom;

    @JsonProperty("filetype_acronym")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String fileTypeAcronym;

    @JsonProperty("filetype_formatVersion")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String fileTypeFormatVersion;

    @JsonProperty("originalMD5")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String originalMD5;

    @JsonProperty("originalDateTimeProcessed")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String originalDateTimeProcessed;

    @JsonProperty("originalDateTimeCreated")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String originalDateTimeCreated;

    @JsonProperty("originalFileName")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String originalFileName;

    @JsonProperty("originalFileType")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String originalFileType;

    @JsonProperty("originalCaptureDevice")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String originalCaptureDevice;

    @JsonProperty("originalProcessingSoftware")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String originalProcessingSoftware;

    @JsonProperty("software")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String software;

    // audio specific fields below

    @JsonProperty("ljud_samplingFrequency")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Integer sampleRate;

    @JsonProperty("ljud_numChannels")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Integer channels;

    @JsonProperty("ljud_numStreams")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Integer numStreams;

    @JsonProperty("ljud_bitsPerSample")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Integer audioBitDepth;

    @JsonProperty("ljud_codecName")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String audioDataEncoding;

    @JsonProperty("ljud_codecId")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String audioCodecId;

    @JsonProperty("duration")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String formattedDuration;

    @JsonProperty("ljud_dataRate")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Long bitRate;

    @JsonProperty("ljud_dataRateMode")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String bitRateMode;


    // AV specific fields below

    @JsonProperty("av_bitRate")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Long avBitRate;

    @JsonProperty("av_videoFormat")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String avVideoFormat;

    @JsonProperty("av_frameRateMode")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String avFrameRateMode;

    @JsonProperty("av_frameRate")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String avFrameRate;

    @JsonProperty("av_sampling")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String avSampling;

    @JsonProperty("av_pixelAspectRatio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String avPixelAspectRatio;

    @JsonProperty("av_displayAspectRatio")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String avDisplayAspectRatio;

    @JsonProperty("av_scan")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String avScanningFormat;


    @JsonProperty("av_numStreams")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Integer avNumStreams;

    @JsonProperty("av_codecFormat")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String avCodecFormat;

    @JsonProperty("av_codecInfo")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String avCodecInfo;

    @JsonProperty("av_codecId")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String avCodecId;

    @JsonProperty("av_frameCount")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Long avFrameCount;


    /**
     * Populates all applicable metadata fields on this object from the provided result objects
     *
     * @param filenameMetaData
     * @param fitsResult
     */
    public void populateFromMetadataResults(FilenameMetaData filenameMetaData, FitsResult fitsResult, IngestDigitizationProfile digitizationProfile, MD5ComparisonItem md5Comparison){

        this.setDc2TermsHasFormat(filenameMetaData.getDcIdentifier());
        this.setEntityLabel(""+filenameMetaData.getPageNr());
        this.setEntityType(filenameMetaData.getEntityType());
        this.setEntityTypeOrderInt(filenameMetaData.getVersionNr());
        this.setDc2Type(filenameMetaData.getCategory().getDc2Type());
        this.setDcIdentifier(filenameMetaData.getDcIdentifier());
        this.setIdentifier(filenameMetaData.getDigitalObjectIdentifier());
        this.setEntityIdentifier(filenameMetaData.getEntityIdentifier());


        this.setEntityOrderInt(filenameMetaData.getPageNr());
        this.setFileTypeAcronym(filenameMetaData.getFileSuffix());

        this.setFileTypeFormatVersion(fitsResult.getFileTypeFormatVersion());

        // Jens: Decided with Niklas that we store the same value in both created and originalDateTimeCreated for now.
        // Once we add processing more file types this might change
        // We store the value that has been attempted to be converted to real ISO format
        // Jens: Decided with Rasmus that dateCreated should actually be the date when the FileAPI returns a OK response
        // since that is the date when the file has been moved/created to masterfiles. So we set this value only
        // in the ingest post process, and only if value does not exist already

        //this.setDateCreated(fitsResult.getCreated());
        //this.setOriginalDateTimeCreated(this.getDateCreated());

        this.setDateModified(fitsResult.getModified());
        // same as date modified
        this.setOriginalDateTimeProcessed(this.getDateModified());

        // same as above, once more file types are added, this might change.
        this.setMd5Checksum(md5Comparison.getMd5ChecksumBefore());
        this.setOriginalMD5(md5Comparison.getMd5ChecksumAfter());

        this.setOriginalFileName(fitsResult.getFileName());
        this.setOriginalFileType(fitsResult.getIdentityMimetype());

        this.setOriginalCaptureDevice(fitsResult.getOriginalCaptureDevice());

        // FIXME: dcTerms_issued is a date as well, from where is that date taken?

        // These should be returned by Madsens client, and they are stored in anvandarkopior layout
        // filePath
        // isGeneratedFrom

        // FIXME: make media storage path configurable, or extract from filenameMetaData?
        this.setMediaStorageFilePath("//sls.fi/lta/hesa/masterfiles/");

        this.setDigitizationProfileId(digitizationProfile.getProfileName());


        this.setFileSize(fitsResult.getFileSize());
        this.setFileTypeName(fitsResult.getIdentityFormat());
        this.setFileTypeMime(fitsResult.getIdentityMimetype());
        this.setFileTypePronom(fitsResult.getExternalIdentifier());

        // Populate image specific metadata
        if(fitsResult instanceof FitsImageResult){
            this.setWidth(((FitsImageResult) fitsResult).getImageWidth());
            this.setHeight(((FitsImageResult) fitsResult).getImageHeight());
            this.setResolution(((FitsImageResult) fitsResult).getXSamplingFrequency());
            this.setBitDepth(((FitsImageResult) fitsResult).getBithDepth().toString());
            this.setColorMode(((FitsImageResult) fitsResult).getColorSpace());
            this.setColorProfile(((FitsImageResult) fitsResult).getIccProfileName());

            // not sure if images are the only ones that have original processing software, might need to move this
            this.setOriginalProcessingSoftware(((FitsImageResult) fitsResult).getScanningSoftwareName());
            this.setSoftware(((FitsImageResult) fitsResult).getScanningSoftwareName());
        }
        // populate audio specific metadata
        if(fitsResult instanceof FitsAudioResult){

            this.setSampleRate(((FitsAudioResult) fitsResult).getSampleRate());
            this.setChannels(((FitsAudioResult) fitsResult).getChannels());
            this.setAudioBitDepth(((FitsAudioResult) fitsResult).getBitDepth());
            this.setAudioDataEncoding(((FitsAudioResult) fitsResult).getAudioDataEncoding());
            this.setFormattedDuration(((FitsAudioResult) fitsResult).getFormattedDuration());
            this.setBitRate(((FitsAudioResult) fitsResult).getBitRate());

            this.setBitRateMode(((FitsAudioResult) fitsResult).getBitRateMode());
            this.setAudioCodecId(((FitsAudioResult) fitsResult).getAudioCodecId());
            this.setNumStreams(((FitsAudioResult) fitsResult).getNumAudioStreams());
        }
        // populate video specific metadata
        if (fitsResult instanceof FitsVideoResult){

            this.setFormattedDuration(((FitsVideoResult) fitsResult).getFormattedDuration());
            this.setAvBitRate(((FitsVideoResult) fitsResult).getAvBitRate());

            if(((FitsVideoResult) fitsResult).getAvBitDepth() != null){
                this.setBitDepth(((FitsVideoResult) fitsResult).getAvBitDepth().toString());
            }
            this.setAvVideoFormat(((FitsVideoResult) fitsResult).getBroadcastStandard());

            this.setWidth(((FitsVideoResult) fitsResult).getVideoWidth());
            this.setHeight(((FitsVideoResult) fitsResult).getVideoHeight());
            this.setAvFrameRateMode(((FitsVideoResult) fitsResult).getFrameRateMode());
            this.setAvFrameRate(((FitsVideoResult) fitsResult).getFrameRate());
            this.setAvSampling(((FitsVideoResult) fitsResult).getAvSampling());
            this.setAvPixelAspectRatio(((FitsVideoResult) fitsResult).getPixelAspectRatio());
            this.setAvDisplayAspectRatio(((FitsVideoResult) fitsResult).getDisplayAspectRatio());

            this.setAvNumStreams(((FitsVideoResult) fitsResult).getNumVideoStreams());


            this.setAvCodecFormat(((FitsVideoResult) fitsResult).getCodecName());
            this.setAvCodecInfo(((FitsVideoResult) fitsResult).getCodecInfo());
            this.setAvCodecId(((FitsVideoResult) fitsResult).getCodecId());
            this.setAvFrameCount(((FitsVideoResult) fitsResult).getFrameCount());
            this.setAvScanningFormat(((FitsVideoResult) fitsResult).getScanningFormat());

        }
    }

    /**
     * Generates the eventId string for this object and the given event type
     *
     * The value is used to link the digitalObject to the mt_premisEvents_DO table in FileMaker
     *
     * @param eventType
     * @param eventDate The date of the event as a string in the format YYYY-MM-DD
     * @return
     */
    public String generatePremiseEventIdFor(String eventType, String eventDate){
        String created = "";

        if(eventDate == null){
            if(dateCreated != null && !dateCreated.isEmpty()){
                created = dateCreated.substring(0, 10);
            }
        } else {
            created = eventDate;
        }

        String profileId = "";
        if(digitizationProfileId != null){
            profileId = "_"+digitizationProfileId;
        }

        return eventType+":"+created+profileId;
    }


    /**
     * Generates the eventId string for this object and the given event type
     *
     * The value is used to link the digitalObject to the mt_premisEvents_DO table in FileMaker
     *
     * @param eventType
     * @return
     */
    public String generatePremiseEventIdFor(String eventType){
        return generatePremiseEventIdFor(eventType, null);
    }
}
