package fi.sls.ingest.fits.exception;

public class FitsResultException extends RuntimeException {

    public FitsResultException(String message){
        super(message);
    }
}
