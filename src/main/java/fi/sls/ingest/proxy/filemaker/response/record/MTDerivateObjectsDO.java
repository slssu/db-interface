package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import lombok.Data;
import lombok.Getter;

@Data
public class MTDerivateObjectsDO extends FileMakerResponseEntity {
    @JsonIgnore
    @Getter
    private String layoutName = "ingestapi_mt_derivateObjects_DO";

    MTDerivateObjectsDOFieldData fieldData;

    public MTDerivateObjectsDO(){
        this.fieldData = new MTDerivateObjectsDOFieldData();
    }

    @Override
    public FindQueryRequest createFindQueryRequest() {

        FindQueryRequest queryRequest = new FindQueryRequest("der_nummer", "="+getFieldData().getDerNumber());
        queryRequest.addToExistingQueryItem(0, "do_nummer", "="+getFieldData().getDoNumber());
        return queryRequest;
    }

    @Override
    public FindQueryRequest createFindQueryRequest(boolean ignoreNullFields, Long pageSize){
        if(!ignoreNullFields){
            return createFindQueryRequest();
        } else {
            FindQueryRequest queryRequest = new FindQueryRequest();

            if(pageSize != null && pageSize > 0){
                queryRequest.setLimit(pageSize.toString());
            }

            if(getFieldData().getDerNumber() != null){
                queryRequest.addQueryItem("der_nummer", "="+getFieldData().getDerNumber());
            }

            if(getFieldData().getDoNumber() != null){
                if(queryRequest.getQuery().isEmpty()){
                    queryRequest.addQueryItem("do_nummer", "="+getFieldData().getDoNumber());
                } else {
                    queryRequest.addToExistingQueryItem(0, "do_nummer", "="+getFieldData().getDoNumber());
                }
            }

            return queryRequest;
        }
    }

    @Override
    public UpdateRequest createUpdateRequest() {
        return new UpdateRequest(getFieldData());
    }

    @Override
    public CreateRequest createRecordRequest() {

        return new CreateRequest(getFieldData());
    }
}
