package fi.sls.ingest.fits.model.tool;

import fi.sls.ingest.fits.model.generated.Fits;

/**
 * Wrapper class that provides easier access to the result structure that the JHove
 * tool produces for the fields that we are interested in.
 */
public class FitsJHoveResult extends FitsToolResultBase {
    public FitsJHoveResult(Fits generatedResult) {
        super(generatedResult, "JHove");
    }

    public String getFileTypeFormatVersion(){
        if(hasToolResult() && toolResult.getElementsByTagName("version").getLength() > 0){
            return toolResult.getElementsByTagName("version").item(0).getTextContent();
        }
        return null;
    }
    public String getLastModified(){
        if(hasToolResult() && toolResult.getElementsByTagName("lastModified").getLength() > 0){
            return toolResult.getElementsByTagName("lastModified").item(0).getTextContent();
        }
        return null;
    }
}
