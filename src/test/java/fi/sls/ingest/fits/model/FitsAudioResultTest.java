package fi.sls.ingest.fits.model;

import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class FitsAudioResultTest extends FitsResultTestsBase {

    String audioWithMeta = "metadata-extractor-output/audio/slsa_1110_ljud_001_001.wav.fits.xml";
    String audioWithMetaNoBitRate = "metadata-extractor-output/audio/fmi_319_av_001_001.mkv.fits.xml";
    String audioNoMeta = "metadata-extractor-output/audio/slsa_1110_ljud_001_001.wav.fits_no_meta.xml";
    String audioWithMetaNoOISAudioInfo = "metadata-extractor-output/audio/sls_2325_ljud_029_001.wav.fits.xml";


    @Test
    void sampleRate_returnsResultIfAvailable() throws IOException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioWithMeta));

        assertEquals(Integer.valueOf(96000), testable.getSampleRate());
    }

    @Test
    void sampleRate_returnsNullIfNotAvailable() throws IOException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioNoMeta));

        assertNull(testable.getSampleRate());
    }


    @Test
    void channels_returnsResultIfAvailable() throws IOException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioWithMeta));

        assertEquals(Integer.valueOf(2), testable.getChannels());
    }

    @Test
    void channels_returnsNullIfNotAvailable() throws IOException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioNoMeta));

        assertNull(testable.getChannels());
    }

    @Test
    void bitDepth_returnsResultIfAvailable() throws IOException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioWithMeta));

        assertEquals(Integer.valueOf(24), testable.getBitDepth());
    }

    @Test
    void bitDepth_returnsNullIfNotAvailable() throws IOException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioNoMeta));

        assertNull(testable.getBitDepth());
    }

    @Test
    void audioDataEncoding_returnsResultIfAvailable() throws IOException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioWithMeta));

        assertEquals(String.valueOf("PCM"), testable.getAudioDataEncoding());
    }

    @Test
    void audioDataEncoding_returnsFromMediaInfoIfCodeFamily() throws IOException, ParserConfigurationException, SAXException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioWithMetaNoOISAudioInfo),
                null,
                loadExampleDocument(audioWithMetaNoOISAudioInfo)
        );

        assertEquals(String.valueOf("PCM"), testable.getAudioDataEncoding());
    }


    @Test
    void audioDataEncoding_returnsNullIfNotAvailable() throws IOException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioNoMeta));

        assertNull(testable.getAudioDataEncoding());
    }

    @Test
    void numSamples_returnsResultIfAvailable() throws IOException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioWithMeta));

        assertEquals(Long.valueOf(231268956), testable.getNumSamples());
    }

    @Test
    void numSamples_returnsNullIfNotAvailable() throws IOException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioNoMeta));

        assertNull(testable.getNumSamples());
    }


    @Test
    void duration_returnsResultFromOISAudio() throws IOException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioWithMeta));

        assertEquals(String.valueOf("00:40:09.052"), testable.getFormattedDuration());
    }

    @Test
    void duration_returnsNullIfNotAvailable() throws IOException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioNoMeta));

        assertNull(testable.getFormattedDuration());
    }

    @Test
    void duration_returnsResultFromMediaInfo() throws IOException, ParserConfigurationException, SAXException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioWithMetaNoOISAudioInfo),
                null,
                loadExampleDocument(audioWithMetaNoOISAudioInfo));

        assertEquals(String.valueOf("02:11:47.560"), testable.getFormattedDuration());
    }

    @Test
    void bitRate_returnsResultIfAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioWithMeta), null, loadExampleDocument(audioWithMeta));

        assertEquals(Long.valueOf(4608000), testable.getBitRate());
    }

    @Test
    void bitRate_calculatesRateIfCBRAndValuesAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioWithMetaNoBitRate), null, loadExampleDocument(audioWithMetaNoBitRate));

        assertEquals(Long.valueOf(1536000), testable.getBitRate());
    }

    @Test
    void bitRate_returnsNullIfNotAvailable() throws IOException, ParserConfigurationException, SAXException {
        FitsAudioResult testable = (FitsAudioResult) FitsResult.createFromFits(loadExampleXML(audioNoMeta), null, loadExampleDocument(audioNoMeta));

        assertNull(testable.getBitRate());
    }
}