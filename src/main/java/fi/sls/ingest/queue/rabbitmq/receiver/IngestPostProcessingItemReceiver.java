package fi.sls.ingest.queue.rabbitmq.receiver;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.manager.runnable.IngestPostProcessRunnable;
import fi.sls.ingest.queue.rabbitmq.exception.IngestAmqpReceiverException;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.SocketMessenger;
import org.slf4j.MDC;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Handles IngestProcessingItems that are sent from the post-processing queue
 *
 * This means items that have been processed by the masterfiles service (File API)
 */
@RabbitListener(queues = "${sls.queue.postProcessing}", containerFactory = "noRequeRejectedRabbitListenerContainerFactory", concurrency = "${sls.concurrency.postProcessing:1}")
public class IngestPostProcessingItemReceiver extends IngestItemReceiverBase {

    /**
     * Reference to runnable used to actually process the given IngestProcessingItem
     */
    IngestPostProcessRunnable ingestPostProcessRunnable;

    @Autowired
    public IngestPostProcessingItemReceiver(
            ObjectMapper objectMapper,
            IngestItemRepository ingestItemRepository,
            SocketMessenger socketMessenger,
            IngestPostProcessRunnable ingestPostProcessRunnable
    ){
        super(objectMapper, ingestItemRepository, socketMessenger);

        this.ingestPostProcessRunnable = ingestPostProcessRunnable;
    }

    @RabbitHandler
    public void receive(String json, Message message) throws IngestAmqpReceiverException {
        if(hasExceededRetryCount(message)){
            storeMaxRetryError(json, getRetryCount(message));
        } else {
            receiveItem(json, ProcessingStatus.PROCESSING_USER_COPIES_COMPLETE);
        }
    }

    @Override
    public void startItemProcessing(IngestProcessingItem item) {
        MDC.getCopyOfContextMap();
        //ingestPostProcessRunnable.setItem(item);
        ingestPostProcessRunnable.run(item);
    }
}