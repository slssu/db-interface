package fi.sls.ingest.representation.projection;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.migration.DigitalObjectMigrationResult;
import org.springframework.data.rest.core.config.Projection;

@Projection(name="minimal", types={DigitalObjectMigrationResult.class})
@JsonPropertyOrder(alphabetic=true)
public interface DigitalObjectMigrationResultMinimal {
    String getToken();
    ProcessingStatus getProcessingStatus();
    String getError();

    String getOriginalDcIdentifier();
    String getMigratedDcIdentifier();

    IntellectualEntityFieldDataMinimal getIntellectualEntityFieldData();
}
