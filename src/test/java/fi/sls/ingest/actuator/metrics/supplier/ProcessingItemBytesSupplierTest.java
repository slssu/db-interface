package fi.sls.ingest.actuator.metrics.supplier;

import fi.sls.ingest.repository.IngestItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ProcessingItemBytesSupplierTest {

    ProcessingItemBytesSupplier testable;
    IngestItemRepository repository;

    @BeforeEach
    void setUp(){
        repository = mock(IngestItemRepository.class);
        testable = new ProcessingItemBytesSupplier(repository);

    }

    @Test
    void get_returnsValueIfExists() {
        when(repository.getTotalBytesProcessed()).thenReturn(Optional.of(123L));
        assertEquals(123L, testable.get());
    }

    @Test
    void get_returnsZeroIfNoValues() {
        when(repository.getTotalBytesProcessed()).thenReturn(Optional.empty());
        assertEquals(0L, testable.get());
    }
}