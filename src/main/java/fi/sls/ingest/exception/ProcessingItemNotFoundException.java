package fi.sls.ingest.exception;

public class ProcessingItemNotFoundException extends EntityNotFoundException{
    public ProcessingItemNotFoundException(String exception){
            super(exception);
        }
}
