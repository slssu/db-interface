package fi.sls.ingest.proxy.filemaker.request.parameters;

public enum SortOrder {

    ASCENDING {
        public String toString(){
            return "ascend";
        }
    },

    DESCENDING {
        public String toString(){
            return "descend";
        }
    }
}
