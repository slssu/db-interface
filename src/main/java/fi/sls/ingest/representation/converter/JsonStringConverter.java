package fi.sls.ingest.representation.converter;

import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class JsonStringConverter {
    ObjectMapper objectMapper;

    public JsonStringConverter(ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }
}
