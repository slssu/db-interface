package fi.sls.ingest.queue.rabbitmq.queue;

import org.springframework.amqp.core.Queue;

public class IngestItemPreProcessingQueue extends Queue {
    public IngestItemPreProcessingQueue(String queueName){
        super(queueName);
    }
}
