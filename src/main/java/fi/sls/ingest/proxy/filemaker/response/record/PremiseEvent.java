package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Entity representing API responses for values in the "premisEvents" table/view in FileMaker.
 *
 * These record various changes that happen to a file as an event log. E.g. created, ingested, etc.
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PremiseEvent extends FileMakerResponseEntity {

    @JsonIgnore
    @Getter
    private String layoutName = "ingestapi_premisEvents";

    PremiseEventFieldData fieldData;

    public PremiseEvent(){
        this.fieldData = new PremiseEventFieldData();
    }

    @Override
    public FindQueryRequest createFindQueryRequest() {
        return new FindQueryRequest("eventIdentifierValue", "="+getFieldData().getEventIdentifierValue());
    }

    @Override
    public UpdateRequest createUpdateRequest() {
        return new UpdateRequest(getFieldData());
    }

    @Override
    public CreateRequest createRecordRequest() {
        return new CreateRequest(getFieldData());
    }
}
