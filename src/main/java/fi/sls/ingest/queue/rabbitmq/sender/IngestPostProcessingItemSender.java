package fi.sls.ingest.queue.rabbitmq.sender;


import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.queue.rabbitmq.queue.IngestItemPostProcessingQueue;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.SocketMessenger;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Handles sending a processing item to the post processing queue,
 * from where it is eventually picked up by an available receiver.
 */
@Service
public class IngestPostProcessingItemSender extends IngestItemSenderBase {

    @Autowired
    public IngestPostProcessingItemSender(
            RabbitTemplate template,
            //IngestItemPostProcessingQueue queue,
            @Qualifier(value = "ingestPostProcessingQueue") Queue queue,
            ObjectMapper objectMapper,
            SocketMessenger socketMessenger,
            IngestItemRepository ingestItemRepository
    ){
        super(template, queue, objectMapper, socketMessenger, ingestItemRepository);
    }

    /**
     * Sends our ingest item to the queue for processing.
     *
     * @param item The IngestProcessingItem to send to the queue.
     * @return The item with updated processing status field.
     */
    public IngestProcessingItem send(IngestProcessingItem item) throws IngestProcessingException {
        return send(item, ProcessingStatus.SEND_TO_POST_PROCESSING_QUEUE);
    }
}
