package fi.sls.ingest.manager.step;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.DerivateObject;
import fi.sls.ingest.proxy.filemaker.response.record.DerivateObjectFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntity;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntityFieldData;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.representation.file.ObjectCategory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class IngestIntellectualEntityStepTest {

    IngestIntellectualEntityStep testable;
    RecordService service;
    FilenameMetaData metaData;
    FitsResult fitsResult;
    IntellectualEntityFieldData fmcRtn;
    IntellectualEntity fmRtn;

    @BeforeEach
    void setUp() {
        service = mock(RecordService.class);
        testable = new IngestIntellectualEntityStep(service);

        fitsResult = mock(FitsResult.class);
        metaData = mock(FilenameMetaData.class);

        ObjectCategory oc = mock(ObjectCategory.class);
        when(oc.getCategoryLabel()).thenReturn("somecategorylabel");
        when(oc.getSubCategory()).thenReturn("somesubcategory");

        when(metaData.getCategory()).thenReturn(oc);
        when(metaData.getCollectionDescriptionOrName()).thenReturn("somecollectionorname");
        when(metaData.getArchiveCollectionName()).thenReturn("somearchivecollectionname");
        when(metaData.getArchiveSignum()).thenReturn("somearchivesignum");
        when(metaData.getEntityIdentifier()).thenReturn("someentityidentifier");
        when(metaData.getSigumNr()).thenReturn(2);

        fmcRtn = mock(IntellectualEntityFieldData.class);
        fmRtn = mock(IntellectualEntity.class);
        when(fmRtn.getFieldData()).thenReturn(fmcRtn);

        // default field returns
        when(fmcRtn.getDcType()).thenReturn("Bild");
        when(fmcRtn.getDcType2()).thenReturn("fotografi");
        when(fmcRtn.getDcPublisher2()).thenReturn("SLS");
        when(fmcRtn.getDcTermsIsPartOf()).thenReturn("SLS 666");
        when(fmcRtn.getDcSource()).thenReturn("SLS 666");
        when(fmcRtn.getEntityIdentifier()).thenReturn("SLS:sls666_ent.foto.1");
        when(fmcRtn.getCollectionOrder()).thenReturn(2L);


    }

//    @Test
//    void executeStep_returnsNullIfServiceException() throws IOException {
//
//        when(service.findRecordByIdentifier(any())).thenThrow(IOException.class);
//        IntellectualEntity rtn = testable.executeStep(metaData, fitsResult);
//
//        assertNull(rtn);
//    }

    @Test
    void executeStep_returnsExistingRecordIfFoundWithoutUpdating() throws IOException {
        ArgumentCaptor<IntellectualEntity> serviceCaptor = ArgumentCaptor.forClass(IntellectualEntity.class);

        Optional<FileMakerResponseEntity> serviceRtn = Optional.of(fmRtn);
        doReturn(serviceRtn).when(service).findRecordByIdentifier(serviceCaptor.capture());

        IntellectualEntity rtn = testable.executeStep(metaData, fitsResult);

        // verify service calls
        verify(service).findRecordByIdentifier(serviceCaptor.capture());
        verify(service, never()).updateObjectRecord(serviceCaptor.capture());

        // verify return is same that was passed in params
        assertNotNull(rtn);
        assertEquals("SLS 666", rtn.getFieldData().getDcSource());
        assertEquals("SLS:sls666_ent.foto.1", rtn.getFieldData().getEntityIdentifier());

    }

    @Test
    void executeStep_createsNewIfRecordNotFound() throws IOException {

        ArgumentCaptor<IntellectualEntity> serviceCaptor = ArgumentCaptor.forClass(IntellectualEntity.class);

        Optional<FileMakerResponseEntity> serviceRtn = Optional.empty();
        doReturn(serviceRtn).when(service).findRecordByIdentifier(serviceCaptor.capture());
        doReturn(fmRtn).when(service).createObjectRecord(serviceCaptor.capture());

        IntellectualEntity rtn = testable.executeStep(metaData, fitsResult);

        // verify parameters for service call
        verify(service).findRecordByIdentifier(serviceCaptor.capture());
        verify(service).createObjectRecord(serviceCaptor.capture());

        // verify return is the new object and not same as was passed in params
        assertNotNull(rtn);
        assertEquals("SLS 666", rtn.getFieldData().getDcSource());
        assertEquals("SLS:sls666_ent.foto.1", rtn.getFieldData().getEntityIdentifier());

        // verify params to create object
        assertEquals("somecategorylabel", serviceCaptor.getValue().getFieldData().getDcType());
        assertEquals("somesubcategory", serviceCaptor.getValue().getFieldData().getDcType2());
        assertEquals("somecollectionorname", serviceCaptor.getValue().getFieldData().getDcPublisher2());
        assertEquals("somearchivecollectionname", serviceCaptor.getValue().getFieldData().getDcTermsIsPartOf());
        assertEquals("somearchivesignum", serviceCaptor.getValue().getFieldData().getDcSource());
        assertEquals("someentityidentifier", serviceCaptor.getValue().getFieldData().getEntityIdentifier());
        assertEquals(Long.valueOf(2), serviceCaptor.getValue().getFieldData().getCollectionOrder());
    }
}