package fi.sls.ingest.config;

import fi.sls.ingest.log.RequestResponseLoggingInterceptor;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

class RestTemplateConfigTest {

    RestTemplateConfig testable;

    @BeforeEach
    void setUp() {
        testable = new RestTemplateConfig();
    }

    @Test
    void restTemplate() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        RestTemplateBuilder builder = mock(RestTemplateBuilder.class);

        RestTemplate rtn = testable.restTemplate(builder);
        assertNotNull(rtn);
        assertInterceptor(rtn);
    }

    @Test
    void shortTimeoutRestTemplate() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        RestTemplateBuilder builder = mock(RestTemplateBuilder.class);
        HttpClientBuilder clientBuilder = mock(HttpClientBuilder.class);
        when(clientBuilder.build()).thenReturn(mock(CloseableHttpClient.class));

        RestTemplateConfig spyTestable = spy(testable);
        when(spyTestable.createClientBuilder()).thenReturn(clientBuilder);

        RestTemplate rtn = spyTestable.shortTimeoutRestTemplate(builder);
        assertNotNull(rtn);
        assertInterceptor(rtn);

    }


    @Test
    void mediumTimeoutRestTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        RestTemplateBuilder builder = mock(RestTemplateBuilder.class);
        HttpClientBuilder clientBuilder = mock(HttpClientBuilder.class);
        when(clientBuilder.build()).thenReturn(mock(CloseableHttpClient.class));

        RestTemplateConfig spyTestable = spy(testable);
        when(spyTestable.createClientBuilder()).thenReturn(clientBuilder);

        RestTemplate rtn = spyTestable.mediumTimeoutRestTemplate(builder);
        assertNotNull(rtn);
        assertInterceptor(rtn);
    }

    @Test
    void createClientBuilder() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        HttpClientBuilder rtn = testable.createClientBuilder();
        assertNotNull(rtn);
    }

    @Test
    void createRestTemplateWithClient() {
        CloseableHttpClient client = mock(CloseableHttpClient.class);

        RestTemplate rtn = testable.createRestTemplateWithClient(client);
        assertNotNull(rtn);
        assertInterceptor(rtn);
    }

    protected void assertInterceptor(RestTemplate template) {
        assertEquals(1, template.getInterceptors().size());
        assertEquals(RequestResponseLoggingInterceptor.class, template.getInterceptors().get(0).getClass());

    }
}