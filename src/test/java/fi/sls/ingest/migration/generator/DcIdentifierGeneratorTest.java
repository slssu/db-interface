package fi.sls.ingest.migration.generator;

import fi.sls.ingest.config.MetaDataConfig;
import fi.sls.ingest.proxy.filemaker.response.record.CollectionFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.DigitalObjectFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntityFieldData;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.representation.file.ObjectCategory;
import fi.sls.ingest.representation.file.ObjectCollection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DcIdentifierGeneratorTest {

    DcIdentifierGenerator testable;

    MetaDataConfig mockConfig;

    @BeforeEach
    void setUp() {
        mockConfig = mock(MetaDataConfig.class);
        testable = new DcIdentifierGenerator(mockConfig);
    }

    @Test
    void generate_returnsCorrectIdentifierForValidInputs() {
        CollectionFieldData collection = mock(CollectionFieldData.class);
        when(collection.getCollectionName()).thenReturn("SLSA");
        when(collection.getArchiveNumber()).thenReturn(1150L);

        ObjectCollection oc = mock(ObjectCollection.class);
        when(oc.getLabel()).thenReturn("SLSA");
        when(oc.getFileKey()).thenReturn("slsa");
        when(oc.getDescription()).thenReturn(null);

        IntellectualEntityFieldData intellectualEntity = mock(IntellectualEntityFieldData.class);
        when(intellectualEntity.getDcType()).thenReturn("Bild");
        when(intellectualEntity.getDcType2()).thenReturn("fotografi");
        when(intellectualEntity.getEntityIdentifier()).thenReturn("HLA:slsa1150_ent.foto.60");

        DigitalObjectFieldData digitalObject = mock(DigitalObjectFieldData.class);
        when(digitalObject.getEntityOrderInt()).thenReturn(2);
        when(digitalObject.getFileTypeAcronym()).thenReturn("tif");

        ObjectCategory oCat = mock(ObjectCategory.class);
        when(oCat.getFileKey()).thenReturn("foto");

        when(mockConfig.getCollectionByLabel("SLSA")).thenReturn(oc);
        when(mockConfig.getCategoryBySubCategoryFuzzy("Bild", "fotografi")).thenReturn(oCat);

        FilenameMetaData result = testable.generate(collection, intellectualEntity, digitalObject);
        assertEquals("slsa/slsa1150/slsa_1150_foto_00060_0002_01.tif", result.getDcIdentifier());
        assertEquals("slsa_1150_foto_00060_0002_01.tif", result.getFilename());
    }

    @Test
    void generate_handlesArchiveCatalogNrCorrectly() {
        CollectionFieldData collection = mock(CollectionFieldData.class);
        when(collection.getCollectionName()).thenReturn("ÖTA");
        when(collection.getArchiveNumber()).thenReturn(112L);

        ObjectCollection oc = mock(ObjectCollection.class);
        when(oc.getLabel()).thenReturn("ÖTA");
        when(oc.getFileKey()).thenReturn("ota");
        when(oc.getDescription()).thenReturn(null);

        IntellectualEntityFieldData intellectualEntity = mock(IntellectualEntityFieldData.class);
        when(intellectualEntity.getDcType()).thenReturn("Bild");
        when(intellectualEntity.getDcType2()).thenReturn("fotografi");
        when(intellectualEntity.getEntityIdentifier()).thenReturn("OTA:ota112-10_ent.foto.00001");

        DigitalObjectFieldData digitalObject = mock(DigitalObjectFieldData.class);
        when(digitalObject.getEntityOrderInt()).thenReturn(1);
        when(digitalObject.getFileTypeAcronym()).thenReturn("tif");
        when(digitalObject.getIdentifier()).thenReturn("OTA:ota112-10_foto.00001.tif");
        when(digitalObject.getDcIdentifier()).thenReturn("OTAfoto/ota112/mapp_10/ota112_10_foto_00001.tif");

        ObjectCategory oCat = mock(ObjectCategory.class);
        when(oCat.getFileKey()).thenReturn("foto");

        when(mockConfig.getCollectionByLabel("ÖTA")).thenReturn(oc);
        when(mockConfig.getCategoryBySubCategoryFuzzy("Bild", "fotografi")).thenReturn(oCat);

        FilenameMetaData result = testable.generate(collection, intellectualEntity, digitalObject);
        assertEquals("ota/ota112/ota_112.10_foto_00001_0001_01.tif", result.getDcIdentifier());
        assertEquals("ota_112.10_foto_00001_0001_01.tif", result.getFilename());


        // catalog 11
        when(intellectualEntity.getEntityIdentifier()).thenReturn("OTA:ota112-11_ent.foto.00001");
        when(digitalObject.getDcIdentifier()).thenReturn("OTAfoto/ota112/mapp_10/ota112_11_foto_00001.tif");
        when(digitalObject.getIdentifier()).thenReturn("OTA:ota112-11_foto.00001.tif");

        result = testable.generate(collection, intellectualEntity, digitalObject);
        assertEquals("ota/ota112/ota_112.11_foto_00001_0001_01.tif", result.getDcIdentifier());
        assertEquals("ota_112.11_foto_00001_0001_01.tif", result.getFilename());

        // catalog 321
        when(intellectualEntity.getEntityIdentifier()).thenReturn("OTA:ota112-321_ent.foto.00001");
        when(digitalObject.getDcIdentifier()).thenReturn("OTAfoto/ota112/mapp_10/ota112_321_foto_00001.tif");
        when(digitalObject.getIdentifier()).thenReturn("OTA:ota112-321_foto.00001.tif");

        result = testable.generate(collection, intellectualEntity, digitalObject);
        assertEquals("ota/ota112/ota_112.321_foto_00001_0001_01.tif", result.getDcIdentifier());
        assertEquals("ota_112.321_foto_00001_0001_01.tif", result.getFilename());
    }

    @Test
    void generate_handles_SLS2269_sound_archiveCatalogNrCorrectly() {
        CollectionFieldData collection = mock(CollectionFieldData.class);
        when(collection.getCollectionName()).thenReturn("SLS");
        when(collection.getArchiveNumber()).thenReturn(2269L);

        ObjectCollection oc = mock(ObjectCollection.class);
        when(oc.getLabel()).thenReturn("SLS");
        when(oc.getFileKey()).thenReturn("sls");
        when(oc.getDescription()).thenReturn(null);

        IntellectualEntityFieldData intellectualEntity = mock(IntellectualEntityFieldData.class);
        when(intellectualEntity.getDcType()).thenReturn("Ljud");
        when(intellectualEntity.getDcType2()).thenReturn("ljudinspelning");
        when(intellectualEntity.getEntityIdentifier()).thenReturn("FKA:fmk196_ent.ljud.1970-69");

        DigitalObjectFieldData digitalObject = mock(DigitalObjectFieldData.class);
        when(digitalObject.getEntityOrderInt()).thenReturn(1);
        when(digitalObject.getFileTypeAcronym()).thenReturn("wav");
        when(digitalObject.getIdentifier()).thenReturn("FKA:fmk196_ljud.1970-69-1.wav");
        when(digitalObject.getDcIdentifier()).thenReturn("fmk196/1030_1970_69_01.wav");

        ObjectCategory oCat = mock(ObjectCategory.class);
        when(oCat.getFileKey()).thenReturn("ljud");

        when(mockConfig.getCollectionByLabel("SLS")).thenReturn(oc);
        when(mockConfig.getCategoryBySubCategoryFuzzy("Ljud", "ljudinspelning")).thenReturn(oCat);

        FilenameMetaData result = testable.generate(collection, intellectualEntity, digitalObject);
        assertEquals("sls/sls2269/sls_2269.1970_ljud_00069_0001_01.wav", result.getDcIdentifier());
        assertEquals("sls_2269.1970_ljud_00069_0001_01.wav", result.getFilename());


        // year 1969
        when(intellectualEntity.getEntityIdentifier()).thenReturn("FKA:fmk196_ent.ljud.1969-69");
        when(digitalObject.getDcIdentifier()).thenReturn("fmk196/1030_1969_69_01.wav");
        when(digitalObject.getIdentifier()).thenReturn("FKA:fmk196_ljud.1969-69-1.wav");

        result = testable.generate(collection, intellectualEntity, digitalObject);
        assertEquals("sls/sls2269/sls_2269.1969_ljud_00069_0001_01.wav", result.getDcIdentifier());
        assertEquals("sls_2269.1969_ljud_00069_0001_01.wav", result.getFilename());

        // year 1970, nr 71 1
        when(intellectualEntity.getEntityIdentifier()).thenReturn("FKA:fmk196_ent.ljud.1970-71");
        when(digitalObject.getDcIdentifier()).thenReturn("fmk196/1033_1970_71_01.wav");
        when(digitalObject.getIdentifier()).thenReturn("FKA:fmk196_ljud.1970-71-1.wav");

        result = testable.generate(collection, intellectualEntity, digitalObject);
        assertEquals("sls/sls2269/sls_2269.1970_ljud_00071_0001_01.wav", result.getDcIdentifier());
        assertEquals("sls_2269.1970_ljud_00071_0001_01.wav", result.getFilename());

        // year 1970, nr 71 2
        when(intellectualEntity.getEntityIdentifier()).thenReturn("FKA:fmk196_ent.ljud.1970-71");
        when(digitalObject.getDcIdentifier()).thenReturn("fmk196/1033_1970_71_02.wav");
        when(digitalObject.getIdentifier()).thenReturn("FKA:fmk196_ljud.1970-71-2.wav");
        when(digitalObject.getEntityOrderInt()).thenReturn(2);

        result = testable.generate(collection, intellectualEntity, digitalObject);
        assertEquals("sls/sls2269/sls_2269.1970_ljud_00071_0002_01.wav", result.getDcIdentifier());
        assertEquals("sls_2269.1970_ljud_00071_0002_01.wav", result.getFilename());
    }

    @Test
    void generate_handles_SLS2269_archiveCatalogNrCorrectly() {
        CollectionFieldData collection = mock(CollectionFieldData.class);
        when(collection.getCollectionName()).thenReturn("SLS");
        when(collection.getArchiveNumber()).thenReturn(2269L);

        ObjectCollection oc = mock(ObjectCollection.class);
        when(oc.getLabel()).thenReturn("SLS");
        when(oc.getFileKey()).thenReturn("sls");
        when(oc.getDescription()).thenReturn(null);

        IntellectualEntityFieldData intellectualEntity = mock(IntellectualEntityFieldData.class);
        when(intellectualEntity.getDcType()).thenReturn("Bild");
        when(intellectualEntity.getDcType2()).thenReturn("fotografi");
        when(intellectualEntity.getEntityIdentifier()).thenReturn("FKA:fmk196_ent.bild.1970-666");

        DigitalObjectFieldData digitalObject = mock(DigitalObjectFieldData.class);
        when(digitalObject.getEntityOrderInt()).thenReturn(1);
        when(digitalObject.getFileTypeAcronym()).thenReturn("tif");
        when(digitalObject.getIdentifier()).thenReturn("FKA:fmk196_bild.1970-666-1.tif");
        when(digitalObject.getDcIdentifier()).thenReturn("fmk196/1030_1970_666_01.tif");

        ObjectCategory oCat = mock(ObjectCategory.class);
        when(oCat.getFileKey()).thenReturn("bild");

        when(mockConfig.getCollectionByLabel("SLS")).thenReturn(oc);
        when(mockConfig.getCategoryBySubCategoryFuzzy("Bild", "fotografi")).thenReturn(oCat);

        FilenameMetaData result = testable.generate(collection, intellectualEntity, digitalObject);
        assertEquals("sls/sls2269/sls_2269_bild_00666_0001_01.tif", result.getDcIdentifier());
        assertEquals("sls_2269_bild_00666_0001_01.tif", result.getFilename());

    }

    @Test
    void generate_handles_SLS1859_archiveCatalogNrCorrectly() {
        CollectionFieldData collection = mock(CollectionFieldData.class);
        when(collection.getCollectionName()).thenReturn("SLS");
        when(collection.getArchiveNumber()).thenReturn(1859L);

        ObjectCollection oc = mock(ObjectCollection.class);
        when(oc.getLabel()).thenReturn("SLS");
        when(oc.getFileKey()).thenReturn("sls");
        when(oc.getDescription()).thenReturn(null);

        IntellectualEntityFieldData intellectualEntity = mock(IntellectualEntityFieldData.class);
        when(intellectualEntity.getDcType()).thenReturn("Ljud");
        when(intellectualEntity.getDcType2()).thenReturn("ljudinspelning");
        when(intellectualEntity.getEntityIdentifier()).thenReturn("FKA:sls1859_ent.ljud.1971-478");

        DigitalObjectFieldData digitalObject = mock(DigitalObjectFieldData.class);
        when(digitalObject.getEntityOrderInt()).thenReturn(1);
        when(digitalObject.getFileTypeAcronym()).thenReturn("wav");
        when(digitalObject.getIdentifier()).thenReturn("FKA:sls1859_ljud.1971-478-1.wav");
        when(digitalObject.getDcIdentifier()).thenReturn("sls1859/1726_1971_478_01.wav");

        ObjectCategory oCat = mock(ObjectCategory.class);
        when(oCat.getFileKey()).thenReturn("ljud");

        when(mockConfig.getCollectionByLabel("SLS")).thenReturn(oc);
        when(mockConfig.getCategoryBySubCategoryFuzzy("Ljud", "ljudinspelning")).thenReturn(oCat);

        FilenameMetaData result = testable.generate(collection, intellectualEntity, digitalObject);
        assertEquals("sls/sls1859/sls_1859.1971_ljud_00478_0001_01.wav", result.getDcIdentifier());
        assertEquals("sls_1859.1971_ljud_00478_0001_01.wav", result.getFilename());
    }

    @Test
    void generate_handles_SLS983_archiveCatalogNrCorrectly() {
        CollectionFieldData collection = mock(CollectionFieldData.class);
        when(collection.getCollectionName()).thenReturn("SLS");
        when(collection.getArchiveNumber()).thenReturn(983L);

        ObjectCollection oc = mock(ObjectCollection.class);
        when(oc.getLabel()).thenReturn("SLS");
        when(oc.getFileKey()).thenReturn("sls");
        when(oc.getDescription()).thenReturn(null);

        IntellectualEntityFieldData intellectualEntity = mock(IntellectualEntityFieldData.class);
        when(intellectualEntity.getDcType()).thenReturn("Ljud");
        when(intellectualEntity.getDcType2()).thenReturn("ljudinspelning");
        when(intellectualEntity.getEntityIdentifier()).thenReturn("FKA:sls983_ent.ljud.1969-48");

        DigitalObjectFieldData digitalObject = mock(DigitalObjectFieldData.class);
        when(digitalObject.getEntityOrderInt()).thenReturn(2);
        when(digitalObject.getFileTypeAcronym()).thenReturn("wav");
        when(digitalObject.getIdentifier()).thenReturn("FKA:sls983_ljud.1969-48-2.wav");
        when(digitalObject.getDcIdentifier()).thenReturn("sls983/0732_1969_48_01.wav");

        ObjectCategory oCat = mock(ObjectCategory.class);
        when(oCat.getFileKey()).thenReturn("ljud");

        when(mockConfig.getCollectionByLabel("SLS")).thenReturn(oc);
        when(mockConfig.getCategoryBySubCategoryFuzzy("Ljud", "ljudinspelning")).thenReturn(oCat);

        FilenameMetaData result = testable.generate(collection, intellectualEntity, digitalObject);
        assertEquals("sls/sls983/sls_983.1969_ljud_00048_0002_01.wav", result.getDcIdentifier());
        assertEquals("sls_983.1969_ljud_00048_0002_01.wav", result.getFilename());
    }



    @Test
    void generate_usesVersionFromDigitalObjectIfExists() {
        CollectionFieldData collection = mock(CollectionFieldData.class);
        when(collection.getCollectionName()).thenReturn("SLSA");
        when(collection.getArchiveNumber()).thenReturn(1150L);

        ObjectCollection oc = mock(ObjectCollection.class);
        when(oc.getLabel()).thenReturn("SLSA");
        when(oc.getFileKey()).thenReturn("slsa");
        when(oc.getDescription()).thenReturn(null);

        IntellectualEntityFieldData intellectualEntity = mock(IntellectualEntityFieldData.class);
        when(intellectualEntity.getDcType()).thenReturn("Bild");
        when(intellectualEntity.getDcType2()).thenReturn("fotografi");
        when(intellectualEntity.getEntityIdentifier()).thenReturn("HLA:slsa1150_ent.foto.60");

        DigitalObjectFieldData digitalObject = mock(DigitalObjectFieldData.class);
        when(digitalObject.getEntityOrderInt()).thenReturn(2);
        when(digitalObject.getFileTypeAcronym()).thenReturn("tif");
        when(digitalObject.getEntityTypeOrderInt()).thenReturn(2);

        ObjectCategory oCat = mock(ObjectCategory.class);
        when(oCat.getFileKey()).thenReturn("foto");

        when(mockConfig.getCollectionByLabel("SLSA")).thenReturn(oc);
        when(mockConfig.getCategoryBySubCategoryFuzzy("Bild", "fotografi")).thenReturn(oCat);

        FilenameMetaData result = testable.generate(collection, intellectualEntity, digitalObject);
        assertEquals("slsa/slsa1150/slsa_1150_foto_00060_0002_02.tif", result.getDcIdentifier());
        assertEquals("slsa_1150_foto_00060_0002_02.tif", result.getFilename());
    }

    @Test
    void generate_usesArchiveCollectionNrStrategyForOTA112() {
        CollectionFieldData collection = mock(CollectionFieldData.class);
        when(collection.getCollectionName()).thenReturn("ÖTA");
        when(collection.getArchiveNumber()).thenReturn(112L);

        ObjectCollection oc = mock(ObjectCollection.class);
        when(oc.getLabel()).thenReturn("ÖTA");
        when(oc.getFileKey()).thenReturn("ota");
        when(oc.getDescription()).thenReturn(null);

        IntellectualEntityFieldData intellectualEntity = mock(IntellectualEntityFieldData.class);
        when(intellectualEntity.getDcType()).thenReturn("Bild");
        when(intellectualEntity.getDcType2()).thenReturn("fotografi");
        when(intellectualEntity.getEntityIdentifier()).thenReturn("OTA:ota112-1_ent.foto.00005");

        DigitalObjectFieldData digitalObject = mock(DigitalObjectFieldData.class);
        when(digitalObject.getEntityOrderInt()).thenReturn(1);
        when(digitalObject.getFileTypeAcronym()).thenReturn("tif");
        when(digitalObject.getEntityTypeOrderInt()).thenReturn(1);
        when(digitalObject.getDcIdentifier()).thenReturn("OTAfoto/ota112/mapp_1/ota112_1_foto_00005.tif");
        when(digitalObject.getIdentifier()).thenReturn("OTA:ota112-1_foto.00005.tif");

        ObjectCategory oCat = mock(ObjectCategory.class);
        when(oCat.getFileKey()).thenReturn("foto");

        when(mockConfig.getCollectionByLabel("ÖTA")).thenReturn(oc);
        when(mockConfig.getCategoryBySubCategoryFuzzy("Bild", "fotografi")).thenReturn(oCat);

        FilenameMetaData result = testable.generate(collection, intellectualEntity, digitalObject);
        assertEquals("ota/ota112/ota_112.1_foto_00005_0001_01.tif", result.getDcIdentifier());
        assertEquals("ota_112.1_foto_00005_0001_01.tif", result.getFilename());
    }

    @Test
    void extractSigumFromIdentifier_returnsCorrectly() {

        assertEquals(60, testable.extractSigumFromIdentifier("HLA:slsa1150_ent.foto.60"));
        assertEquals(460, testable.extractSigumFromIdentifier("HLA:slsa1150_ent.foto.460"));

    }

    @Test
    void extractSigumFromIdentifier_returnsZeroForInvalidInput() {
        assertEquals(0, testable.extractSigumFromIdentifier("HLA:slsa1150_ent.foto.924b"));
        assertEquals(0, testable.extractSigumFromIdentifier("doesnotcompute"));
        assertEquals(0, testable.extractSigumFromIdentifier(null));
    }

}