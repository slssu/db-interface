package fi.sls.ingest.manager.runnable;


import fi.sls.ingest.config.IngestConfig;
import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.proxy.slsfileapi.FileAPIMoveRequestBody;
import fi.sls.ingest.proxy.slsfileapi.SLSFileAPI;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.SocketMessenger;
import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * Calls the SLS File API to re-generate accessfiles for an IngestProcessingItem
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class AccessFilesRunnable extends IngestRunnableBase {

    /**
     * SLS File API wrapper for transferring files from projects to masterfiles
     */
    private SLSFileAPI slsFileAPI;

    /**
     * Configuration for this API
     */
    private IngestConfig ingestConfig;

    @Autowired
    protected AccessFilesRunnable(
            IngestItemRepository ingestItemRepository,
            SocketMessenger socketMessenger,
            SLSFileAPI slsFileAPI,
            IngestConfig ingestConfig

    ) {
        super(ingestItemRepository, socketMessenger);

        this.slsFileAPI = slsFileAPI;
        this.ingestConfig = ingestConfig;

    }


    @Timed("ingestapi.runnable.accessFiles")
    public void run(IngestProcessingItem item) {
        log.info("Requesting re-processing of accessfiles for file with token: {}",
                kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                kv(LogMessageKey.FILE_PATH, item.getItem().getFitsPath())
        );

        // Create file accessfiles request and start the long wait
        // once complete, we re-enter the Arkiva stuff because we now have accessfiles (maybe split this into own update so it could be run separately?)
        if(item.getFileAPIResponse() == null){
            log.warn("Missing FileAPIResponse for item, cannot re-generate accessfiles: {}",
                    kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken()),
                    kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
            );
            throw new IngestProcessingException(String.format("Missing FileAPIResponse for item with token %s, cannot re-generate accessfiles", item.getToken()));
        }
        FileAPIMoveRequestBody request = FileAPIMoveRequestBody.createFrom(item.getFileAPIResponse(), ingestConfig.getHostRelativeAPIUrl("ingest/continue?token="+item.getToken()));
        setProcessingItemStatusAndSend(ProcessingStatus.INIT_PROCESSING_USER_COPIES, item);

        log.debug("Sending accessfiles request to File API: {}",
                kv(LogMessageKey.REQUEST_BODY, request)
        );
        slsFileAPI.accessfiles(request);

        setProcessingItemStatusAndSend(ProcessingStatus.PROCESSING_USER_COPIES, item);
    }
}