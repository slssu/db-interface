package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import fi.sls.ingest.proxy.filemaker.request.RecordPayload;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import fi.sls.ingest.representation.file.FilenameMetaData;
import lombok.Data;

@Data
public class DerivateObjectFieldData implements RecordPayload {

    @JsonProperty("id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Long id;

    @JsonProperty("filePath")
    String filePath;

    @JsonProperty("isGeneratedFrom")
    String isGeneratedFrom;

    @JsonProperty("roleTitle")
    String roleTitle;

    @JsonProperty("roleDescription")
    String roleDescription;

    @JsonProperty("fileType")
    String fileType;

    @JsonProperty("filePath_folder")
    String filePathFolder;

    @JsonProperty("thumbnail")
    String thumbnail;

    @JsonProperty("ei_thumb") // typo intentional
    Long ie_thumb;

    @JsonProperty("databasbild")
    String databasbild;

    @JsonProperty("databasbild_ie")
    Long databasbild_ie;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("c_filePath_total")
    String cFilePathTotal;


    public void populateFromMetadataResults(AccessFile accessFile, FilenameMetaData metaData){

        this.filePath = accessFile.getStrippedFilePath();

        // [sic] same value in two fields in same table
        this.isGeneratedFrom = metaData.getDigitalObjectIdentifier();

        this.roleTitle = accessFile.getRoleTitle();
        this.roleDescription = accessFile.getRoleDescription();
        this.fileType = accessFile.getMimeType();
        this.filePathFolder = accessFile.getFilePathFolder();
    }
}
