package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import fi.sls.ingest.proxy.filemaker.request.RecordPayload;
import lombok.Data;

/**
 * Represents data from the intellectualEntityParts layout view in FileMaker
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IntellectualEntityPartFieldData implements RecordPayload {

    @JsonProperty("nummer")
    Long entityNumber;

    @JsonProperty("title")
    String title;

    @JsonProperty("sortIE")
    String orderNr;

    @JsonProperty("filePath")
    String filePath;

    @JsonProperty("c_databasbild")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String databaseImage;

    @JsonProperty("c_thumbnail")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String databaseThumbnail;

    /**
     * Populates all applicable metadata fields on this object from the provided result objects
     *
     */
    public void populateFromMetadataResults(DigitalObjectFieldData fieldData){
        this.title = fieldData.getEntityLabel();
        this.orderNr = fieldData.getEntityOrder();
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }
}
