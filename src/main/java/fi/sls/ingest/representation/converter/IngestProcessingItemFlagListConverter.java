package fi.sls.ingest.representation.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.manager.IngestProcessingItemFlag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.AttributeConverter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class IngestProcessingItemFlagListConverter extends JsonStringConverter implements AttributeConverter<List<IngestProcessingItemFlag>, String> {
    @Autowired
    public IngestProcessingItemFlagListConverter(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public String convertToDatabaseColumn(List<IngestProcessingItemFlag> ingestProcessingItemFlags) {

        if(ingestProcessingItemFlags == null){
            return null;
        } else {
            String rtnJson = null;

            try{
                rtnJson = objectMapper.writeValueAsString(ingestProcessingItemFlags);
            } catch (JsonProcessingException e) {
                log.error("Error converting from List of IngestProcessingItemFlag to JSON: {}", e.getMessage());
            }
            return rtnJson;
        }
    }

    @Override
    public List<IngestProcessingItemFlag> convertToEntityAttribute(String jsonData) {
        List<IngestProcessingItemFlag> rtnItem = new ArrayList<>();

        if(jsonData != null){
            try{
                rtnItem = objectMapper.readValue(jsonData, objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, IngestProcessingItemFlag.class));
            } catch (IOException e) {
                log.error("Error converting to List of IngestProcessingItemFlag from JSON: {}", e.getMessage());
            }

        }
        return rtnItem;
    }
}
