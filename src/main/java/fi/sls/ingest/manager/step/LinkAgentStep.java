package fi.sls.ingest.manager.step;

import fi.sls.ingest.config.FileMakerConfig;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.Agent;
import fi.sls.ingest.proxy.filemaker.response.record.MTAgentEvents;
import fi.sls.ingest.proxy.filemaker.response.record.PremiseEvent;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;

/**
 * Provides implementation of the ingest processing steps necessary to link
 * an Agent with a PremisEvent.
 */
@Service
@Slf4j
public class LinkAgentStep extends RecordServiceStep {

    FileMakerConfig fileMakerConfig;

    @Autowired
    public LinkAgentStep(RecordService service, FileMakerConfig fileMakerConfig) {
        super(service);

        this.fileMakerConfig = fileMakerConfig;
    }


    /**
     * Performs the tasks of this step
     *
     * @return The Agent that was linked to the given PremiseEvent
     */
    public Agent executeStep(PremiseEvent premiseEvent) throws IOException {

        Agent agent = new Agent();
        agent.getFieldData().setName(fileMakerConfig.getAgentName());
        agent.getFieldData().setVersion(fileMakerConfig.getAgentVersion());


        Optional<? extends FileMakerResponseEntity> existingItem;

//        jw 2022-02-14: remove try-catch as it is handled by runnable
//        try {
            existingItem = service.findRecordByIdentifier(agent);

            if(existingItem.isPresent()){
                agent = (Agent) existingItem.get();

                MTAgentEvents mtAgentEvents = new MTAgentEvents();
                mtAgentEvents.getFieldData().setAgentNr(agent.getFieldData().getEntityNumber());
                mtAgentEvents.getFieldData().setEventNr(premiseEvent.getFieldData().getEntityNumber());


                Optional<? extends FileMakerResponseEntity> existingAgentEvents = service.findRecordByIdentifier(mtAgentEvents);
                MTAgentEvents agentEvents;

                if(existingAgentEvents.isPresent()){
                    agentEvents = (MTAgentEvents) existingAgentEvents.get();
                } else {
                    agentEvents = new MTAgentEvents();
                    agentEvents.getFieldData().setCreator(fileMakerConfig.getAgentName());
                    agentEvents.getFieldData().setDateCreate(LocalDate.now());
                }

                agentEvents.getFieldData().setAgentNr(agent.getFieldData().getEntityNumber());
                agentEvents.getFieldData().setEventNr(premiseEvent.getFieldData().getEntityNumber());


                if(agentEvents.isNewRecord()){
                    agentEvents = (MTAgentEvents) service.createObjectRecord(agentEvents);
                    log.debug("Created new mtAgentsEvents with id: {}", agentEvents.getRecordId());
                } else {
                    agentEvents = (MTAgentEvents) service.updateObjectRecord(agentEvents);
                    log.debug("Updated mtAgentsEvents with id: {}", agentEvents.getRecordId());
                }

                return agent;
            } else {
                // FIXME: this is probably critical, so should throw error that gets sent to UI
                log.error("Could not find agent with name: {}", fileMakerConfig.getAgentName());
                return null;
            }
//        } catch (IOException e) {
//            // FIXME: probably critical, should re-throw instead
//            log.error("Error creating Agent link: {}", e.getMessage());
//            return null;
//        }
    }
}
