package fi.sls.ingest.migration.report;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class MigrationMasterFilesReport implements Serializable {
    /**
     * Name of collection for which files were migrated.
     */
    String collectionName;

    /**
     * The number of the collection/archive
     *
     */
    Long archiveNr;

    /**
     * List of original file paths
     */
    List<String> originalMasterfilePaths;

    public MigrationMasterFilesReport(String collectionName, Long archiveNr){
        this.collectionName = collectionName;
        this.archiveNr = archiveNr;
        originalMasterfilePaths = new ArrayList<>();
    }
}
