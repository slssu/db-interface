package fi.sls.ingest.controller;

import fi.sls.ingest.parser.FilenameMetaParser;
import fi.sls.ingest.representation.file.FilenameMetaData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static net.logstash.logback.argument.StructuredArguments.kv;

@BasePathAwareController
@RestController
@Slf4j
public class MetadataController {

    @Autowired
    protected FilenameMetaParser parser;

    @RequestMapping(method = RequestMethod.GET, value = "/metadata/parse")
    public FilenameMetaData getMetadata(@RequestParam(value="filename") String filename){
        log.debug("Received getMetadata request: {}", kv("filename", filename));
        return this.parser.parse(filename);
    }
}
