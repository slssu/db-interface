package fi.sls.ingest.util;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FITSDateUtilTest {

    @Test
    void parseString_returnsValidForUnixTimestampMillis() {
        LocalDateTime rtn = FITSDateUtil.parseString("1568719610000");
        assertEquals(1568719610, rtn.toEpochSecond(ZoneOffset.UTC) );
    }

    @Test
    void parseString_returnsValidForISO_DATE_TIME() {
        LocalDateTime rtn = FITSDateUtil.parseString("2011-12-03T10:15:30");
        assertEquals("2011-12-03T10:15:30", rtn.format(DateTimeFormatter.ISO_DATE_TIME));

        rtn = FITSDateUtil.parseString("2019-07-05T07:06:41+00:00");
        assertEquals("2019-07-05T07:06:41", rtn.format(DateTimeFormatter.ISO_DATE_TIME));
    }

    @Test
    void parseString_returnsValidForNLNZ() {
        LocalDateTime rtn = FITSDateUtil.parseString("2011:12:03 10:15:30");
        assertEquals("2011-12-03T10:15:30", rtn.format(DateTimeFormatter.ISO_DATE_TIME));
    }

    @Test
    void parseString_returnsValidForDateOnlyString(){

        LocalDateTime rtn = FITSDateUtil.parseString("2011-12-03");
        assertEquals("2011-12-03T00:00:00", rtn.format(DateTimeFormatter.ISO_DATE_TIME));
    }

    @Test
    void parseString_returnsValidForDateTimeString(){
        LocalDateTime rtn = FITSDateUtil.parseString("2011-01-03 12:53:09");
        assertEquals("2011-01-03T12:53:09", rtn.format(DateTimeFormatter.ISO_DATE_TIME));
    }

    @Test
    void parseString_throwsExceptionForUnrecognizedInput() {
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> {
            LocalDateTime rtn = FITSDateUtil.parseString("2011-12/03 10+15:30");
        });

        assertEquals("Input value '2011-12/03 10+15:30' could not be parsed into a valid LocalDateTime object", e.getMessage());

    }
}