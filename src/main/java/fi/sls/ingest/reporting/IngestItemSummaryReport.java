package fi.sls.ingest.reporting;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class IngestItemSummaryReport implements Serializable {

    String legend;
    Long count;
    Long totalBytesProcessed;

    public IngestItemSummaryReport(String legend, Long count, Long totalBytesProcessed){
        this.legend = legend;
        this.count = count;
        this.totalBytesProcessed = totalBytesProcessed;
    }
}
