package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import lombok.Data;
import lombok.Getter;

/**
 * Entity representing API responses for values in the "mt_samlingar_IE" table/view in FileMaker.
 */
@Data
public class MTCollectionIntellectualEntity extends FileMakerResponseEntity {

    @JsonIgnore
    @Getter
    private String layoutName = "ingestapi_mt_samlingar_IE";


    MTCollectionIntellectualEntityFieldData fieldData;

    public MTCollectionIntellectualEntity(){
        this.fieldData = new MTCollectionIntellectualEntityFieldData();
    }


    @Override
    public FindQueryRequest createFindQueryRequest() {
        FindQueryRequest queryRequest = new FindQueryRequest("samling_nummer", "="+getFieldData().getCollectionNumber());
        queryRequest.addToExistingQueryItem(0, "ie_nummer", "="+getFieldData().getIntellectualEntityNumber());

        return queryRequest;
    }

    @Override
    public FindQueryRequest createFindQueryRequest(boolean ignoreNullFields, Long pageSize){
        return createFindQueryRequest(ignoreNullFields, pageSize, 0L);
    }

    @Override
    public FindQueryRequest createFindQueryRequest(boolean ignoreNullFields, Long pageSize, Long pageNr){
        if(ignoreNullFields == false){
            return this.createFindQueryRequest();
        } else {
            FindQueryRequest queryRequest = new FindQueryRequest();

            if(pageSize != null && pageSize > 0){
                queryRequest.setLimit(pageSize.toString());
            }

            if(pageNr != null && pageNr > 0){
                // calculate real offset
                Long offset = Integer.parseInt(queryRequest.getLimit()) * pageNr + 1;
                queryRequest.setOffset(offset.toString());
            }

            if(getFieldData().getCollectionNumber() != null){
                queryRequest.addQueryItem("samling_nummer", "="+getFieldData().getCollectionNumber());
            }

            if(getFieldData().getIntellectualEntityNumber() != null){
                if(queryRequest.getQuery().isEmpty()){
                    queryRequest.addQueryItem("ie_nummer", "="+getFieldData().getIntellectualEntityNumber());
                } else {
                    queryRequest.addToExistingQueryItem(0, "ie_nummer", "="+getFieldData().getIntellectualEntityNumber());
                }
            }

            return queryRequest;
        }
    }

    @Override
    public UpdateRequest createUpdateRequest() {
        return new UpdateRequest(getFieldData());
    }

    @Override
    public CreateRequest createRecordRequest() {
        return new CreateRequest(getFieldData());
    }
}
