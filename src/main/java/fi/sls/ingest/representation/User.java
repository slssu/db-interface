package fi.sls.ingest.representation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.representation.audit.DateAudit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a user stored in the database
 *
 * This entity is used as a proxy for AD users as well, in that
 * an instance of this class is created and stored in the database for each AD user.
 *
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String uuid;

    @NotEmpty
    private String username;

    private String displayName;

    /**
     * Utility method for returning the value of the displayName property, or if that is empty then the username
     * @return
     */
    public String getDisplayOrUsername(){
        if(getDisplayName() != null && !getDisplayName().isEmpty()){
            return getDisplayName();
        } else {
            return getUsername();
        }
    }

    @JsonIgnore
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="user_role", joinColumns=@JoinColumn(name="user_id"),
    inverseJoinColumns=@JoinColumn(name="role_id"))
    private List<Role> roles = new ArrayList<>();
}
