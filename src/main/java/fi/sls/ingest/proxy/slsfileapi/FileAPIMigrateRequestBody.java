package fi.sls.ingest.proxy.slsfileapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.migration.DigitalObjectMigrationResult;
import fi.sls.ingest.representation.projection.IngestProcessingItemMinimal;
import lombok.Data;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotBlank;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents the request body structure for starting the
 * process of migrating a file from an old location to a new
 * and generating access files using the SLS File API
 *
 * See also <a href="https://bitbucket.org/slssu/file_api">https://bitbucket.org/slssu/file_api</a>
 *
 * Example request body
 * <code>
 *     {
 *         "sourcePath": "ingest/foo/bar/file_name.tif",
 *         "targetFilename": "new_file_name.tif",
 *         "mimeType": "image/jpeg",
 *         "accessFiles": [],
 *         "token": "eyJzdWIiOiJ7XCJpZFwiOjEsXCJ1c2VybmFtZVwiOlwiSW5nZXN0b3JOZXN0b",
 *         "callbackUrl": "https://whatever.tld/move_completed/file_id"
 *     }
 * </code>
 *
 */
@Data
public class FileAPIMigrateRequestBody extends FileAPIRequestBody {

    /**
     * The path to the file to process, as read from Arkiva dc_identifier
     */
    @NotBlank
    private String sourcePath;

    /**
     * The new filename based on which the new location should be generated
     */
    @NotBlank
    private String targetFilename;

    /**
     * The mime type of the file as defined in the FileMaker record for the processed item
     */
    @NotBlank
    private String mimeType;

    /**
     * The checksum for the file as defined in FileMaker
     */
    @NotBlank
    private String checksum;

    /**
     * List of the accessfiles to remove from their original location
     */
    @JsonProperty("accessfiles")
    private List<AccessFileMinimal> accessFiles;

    /**
     * Creates a FileAPIMigrateRequestBody based on the given DigitalObjectMigrationResult and callbackUrl
     *
     * @param item
     * @param callbackUrl
     * @return
     */
    public static FileAPIMigrateRequestBody createFrom(DigitalObjectMigrationResult item, String callbackUrl){
        FileAPIMigrateRequestBody rtn = new FileAPIMigrateRequestBody();
        rtn.setCallbackUrl(callbackUrl);
        rtn.setToken(item.getToken());
        rtn.setSourcePath(StringUtils.trimLeadingCharacter(item.getOriginalDcIdentifier(), File.separatorChar));
        rtn.setChecksum(item.getOriginalChecksum());

        // fetch new dcIdentifier part from last / to end and use as targetFilename
        String targetFilename = item.getMigratedDcIdentifier().substring(item.getMigratedDcIdentifier().lastIndexOf("/")+1);
        rtn.setTargetFilename(targetFilename);
        rtn.setMimeType(item.getOriginalMimeType());

        // send original access files to File API so they can be cleaned up as well

        List<AccessFileMinimal> files = new ArrayList<>();

        for(AccessFile f : item.getOriginalAccessFiles()){
            files.add(new AccessFileMinimal(f.getPath(), f.getMimeType()));
        }

        rtn.setAccessFiles(files);

        return rtn;
    }
}
