package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import fi.sls.ingest.proxy.filemaker.request.RecordPayload;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CollectionFieldData implements RecordPayload {

    // nummer = c_nummer2 in FileMaker
    @JsonProperty("nummer")
    Long entityNumber;

    @JsonProperty(value = "slsArkiv")
    String collectionName;

    @JsonProperty(value = "arkivetsNr")
    Long archiveNumber;

    @JsonProperty(value = "arkivetsNr_subsignum")
    String archiveNumberSuffix;
}
