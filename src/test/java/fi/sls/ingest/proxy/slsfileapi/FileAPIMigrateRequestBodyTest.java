package fi.sls.ingest.proxy.slsfileapi;

import fi.sls.ingest.migration.DigitalObjectMigrationResult;
import org.junit.jupiter.api.Test;
import org.springframework.data.projection.ProjectionFactory;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FileAPIMigrateRequestBodyTest {

    @Test
    void createFrom() {
        DigitalObjectMigrationResult mockResult = mock(DigitalObjectMigrationResult.class);
        when(mockResult.getToken()).thenReturn("thisisatoken");
        when(mockResult.getOriginalDcIdentifier()).thenReturn("HLAfoto/tiff/slsa1150/slsa1150_460.tif");
        when(mockResult.getMigratedDcIdentifier()).thenReturn("slsa/slsa1150/slsa_1150_foto_00460_0001_01.tif");
        when(mockResult.getOriginalChecksum()).thenReturn("thisismychecksum");

        FileAPIMigrateRequestBody testable = FileAPIMigrateRequestBody.createFrom(mockResult, "my/callback/url");

        assertEquals("HLAfoto/tiff/slsa1150/slsa1150_460.tif", testable.getSourcePath());
        assertEquals("slsa_1150_foto_00460_0001_01.tif", testable.getTargetFilename());
        assertEquals("my/callback/url", testable.getCallbackUrl());
        assertEquals("thisisatoken", testable.getToken());
        assertEquals("thisismychecksum", testable.getChecksum());


    }
}