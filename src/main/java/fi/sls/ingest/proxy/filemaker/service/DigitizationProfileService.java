package fi.sls.ingest.proxy.filemaker.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.config.FileMakerConfig;
import fi.sls.ingest.proxy.filemaker.AuthTokenStore;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.parameters.SortOrder;
import fi.sls.ingest.proxy.filemaker.response.FileMakerArrayDataResponse;
import fi.sls.ingest.proxy.filemaker.response.record.DigitizationProfile;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Provides methods for accessing records in the digitizationProfiles table in FileMaker.
 */
@Service
public class DigitizationProfileService extends RecordServiceBase {


    public DigitizationProfileService(
            AuthTokenStore authTokenStore,
            @Qualifier("mediumTimeoutRestTemplate") RestTemplate restTemplate,
            FileMakerConfig fileMakerConfig,
            ObjectMapper objectMapper
    ) {
        super(authTokenStore, restTemplate, fileMakerConfig, objectMapper);
    }


    /**
     * Fetches a list of all active digitization profiles in FileMaker.
     *
     * @return
     * @throws IOException If the response cannot be parsed to a list of DigitizationProfiles from JSON.
     */
    public List<DigitizationProfile> fetchDigitizationProfiles() throws IOException {

        FindQueryRequest queryRequest = new FindQueryRequest("active_profile", "=OK");
        queryRequest.addSortItem("profil_id", SortOrder.ASCENDING);

        Optional<String> response = this.findByQuery(queryRequest, DigitizationProfile.LAYOUT_NAME);
        // default to empty response in case we don't find a matching record
        List<DigitizationProfile> rtn = new ArrayList<>();

        if(response.isPresent()){
            FileMakerArrayDataResponse<DigitizationProfile> wrapper = this.objectMapper.readValue(response.get(), new TypeReference<FileMakerArrayDataResponse<DigitizationProfile>>(){});
            rtn = (List<DigitizationProfile>) wrapper.getResponse().getData();
        }

        return rtn;
    }
}
