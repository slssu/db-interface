package fi.sls.ingest.queue.rabbitmq.queue;

import org.springframework.amqp.core.Queue;

public class MigrationItemPostProcessingQueue extends Queue {
    public MigrationItemPostProcessingQueue(String queueName){
        super(queueName);
    }
}
