package fi.sls.ingest.config;

import fi.sls.ingest.repository.DigitalObjectMigrationResultRepository;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.security.CustomUserDetailsService;
import fi.sls.ingest.security.filter.URLTokenAuthFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Authentication config for IngestProcessingItem token authentication. Used only for
 * when an external process (File API) wants to callback in order to proceed with ingest
 */
@Configuration
@Order(2)   // High order so it evaluates before the default in SecurityConfig
public class TokenAuthConfig extends WebSecurityConfigurerAdapter {

    private CustomUserDetailsService customUserDetailsService;
    private IngestItemRepository ingestItemRepository;
    private DigitalObjectMigrationResultRepository digitalObjectMigrationResultRepository;

    @Autowired
    public TokenAuthConfig(
            CustomUserDetailsService customUserDetailsService,
            IngestItemRepository ingestItemRepository,
            DigitalObjectMigrationResultRepository digitalObjectMigrationResultRepository
    ){
        this.customUserDetailsService = customUserDetailsService;
        this.ingestItemRepository = ingestItemRepository;
        this.digitalObjectMigrationResultRepository = digitalObjectMigrationResultRepository;
    }

    /**
     * Base path to the API (added to hostUrl)
     */
    @Value("${spring.data.rest.basePath}")
    private String apiBasePath;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors()
                .and()
                .csrf()
                .disable()
                .exceptionHandling()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .requestMatchers()
                .antMatchers(apiBasePath+"/ingest/continue", apiBasePath+"/migration/continue")
                .and()
                .authorizeRequests().anyRequest().authenticated();

        http.addFilterBefore(urlTokenAuthFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public URLTokenAuthFilter urlTokenAuthFilter(){
        return new URLTokenAuthFilter(
                customUserDetailsService,
                ingestItemRepository,
                digitalObjectMigrationResultRepository
        );
    }
}
