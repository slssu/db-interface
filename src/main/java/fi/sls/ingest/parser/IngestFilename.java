package fi.sls.ingest.parser;

import fi.sls.ingest.validation.ValidProcessableFile;
import lombok.Data;

/**
 * Wrapper class for validating the filename that the FilenameMetaParser should be able to handle
 */
@Data
public class IngestFilename {

    @ValidProcessableFile
    private String name;

    public IngestFilename(String name){
        this.name = name;
    }
}
