package fi.sls.ingest.manager;

import fi.sls.ingest.exception.InvalidProcessingStatusException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ProcessingStatusTransitionHandlerTest {

    ProcessingStatusTransitionHandler testable;

    @BeforeEach
    void setUp(){
        testable = new ProcessingStatusTransitionHandler();
    }

    @Test
    void transitionToState_allowsCorrectTransitionsFor_Complete() {
        IngestProcessingItem item = new IngestProcessingItem();

        item.setProcessingStatus(ProcessingStatus.COMPLETE);
        testable.transitionToState(item, ProcessingStatus.ARCHIVED);
        assertEquals(ProcessingStatus.ARCHIVED, item.getProcessingStatus());

    }

    @Test
    void transitionToState_blocksTransitionForUnmapped(){
        IngestProcessingItem item = new IngestProcessingItem();

        item.setProcessingStatus(ProcessingStatus.ARCHIVED);
        InvalidProcessingStatusException e = assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.ARCHIVED);
        });
        assertEquals("Transition not allowed from ProcessingStatus ARCHIVED to ProcessingStatus ARCHIVED", e.getMessage());
    }

    @Test
    void transitionToState_blocksIncorrectTransitionsFor_Complete() {
        IngestProcessingItem item = new IngestProcessingItem();

        item.setProcessingStatus(ProcessingStatus.COMPLETE);
        InvalidProcessingStatusException e = assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.STAGING);
        });
        assertEquals("Transition not allowed from ProcessingStatus COMPLETE to ProcessingStatus STAGING", e.getMessage());

        item.setProcessingStatus(ProcessingStatus.COMPLETE);
        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.DELETED);
        });

        item.setProcessingStatus(ProcessingStatus.COMPLETE);
        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.REJECTED);
        });
    }

    @Test
    void transitionToState_allowsCorrectTransitionsFor_ProcessingUserCopies() {
        IngestProcessingItem item = new IngestProcessingItem();

        item.setProcessingStatus(ProcessingStatus.PROCESSING_USER_COPIES);
        testable.transitionToState(item, ProcessingStatus.PROCESSING_USER_COPIES_COMPLETE);
        assertEquals(ProcessingStatus.PROCESSING_USER_COPIES_COMPLETE, item.getProcessingStatus());

        item.setProcessingStatus(ProcessingStatus.PROCESSING_USER_COPIES);
        testable.transitionToState(item, ProcessingStatus.DELETED);
        assertEquals(ProcessingStatus.DELETED, item.getProcessingStatus());

        item.setProcessingStatus(ProcessingStatus.PROCESSING_USER_COPIES);
        testable.transitionToState(item, ProcessingStatus.REJECTED);
        assertEquals(ProcessingStatus.REJECTED, item.getProcessingStatus());

    }

    @Test
    void transitionToState_blocksIncorrectTransitionsFor_ProcessingUserCopies() {
        IngestProcessingItem item = new IngestProcessingItem();
        item.setProcessingStatus(ProcessingStatus.PROCESSING_USER_COPIES);

        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.COMPLETE);
        });

        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.ARCHIVED);
        });
    }

    @Test
    void transitionToState_allowsCorrectTransitionsFor_ProcessingMetadata() {
        IngestProcessingItem item = new IngestProcessingItem();

        item.setProcessingStatus(ProcessingStatus.PROCESSING_METADATA);
        testable.transitionToState(item, ProcessingStatus.DELETED);
        assertEquals(ProcessingStatus.DELETED, item.getProcessingStatus());

        item.setProcessingStatus(ProcessingStatus.PROCESSING_METADATA);
        testable.transitionToState(item, ProcessingStatus.REJECTED);
        assertEquals(ProcessingStatus.REJECTED, item.getProcessingStatus());
    }

    @Test
    void transitionToState_blocksIncorrectTransitionsFor_ProcessingMetadata() {
        IngestProcessingItem item = new IngestProcessingItem();
        item.setProcessingStatus(ProcessingStatus.PROCESSING_METADATA);

        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.COMPLETE);
        });

        item.setProcessingStatus(ProcessingStatus.PROCESSING_METADATA);
        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.ARCHIVED);
        });
    }

    @Test
    void transitionToState_allowsCorrectTransitionsFor_Staging() {
        IngestProcessingItem item = new IngestProcessingItem();

        item.setProcessingStatus(ProcessingStatus.STAGING);
        testable.transitionToState(item, ProcessingStatus.DELETED);
        assertEquals(ProcessingStatus.DELETED, item.getProcessingStatus());

        item.setProcessingStatus(ProcessingStatus.STAGING);
        testable.transitionToState(item, ProcessingStatus.REJECTED);
        assertEquals(ProcessingStatus.REJECTED, item.getProcessingStatus());

        item.setProcessingStatus(ProcessingStatus.STAGING);
        testable.transitionToState(item, ProcessingStatus.STAGING_REJECTED);
        assertEquals(ProcessingStatus.STAGING_REJECTED, item.getProcessingStatus());

    }

    @Test
    void transitionToState_blocksIncorrectTransitionsFor_Staging() {
        IngestProcessingItem item = new IngestProcessingItem();
        item.setProcessingStatus(ProcessingStatus.STAGING);

        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.COMPLETE);
        });

        item.setProcessingStatus(ProcessingStatus.STAGING);
        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.ARCHIVED);
        });
    }

    @Test
    void transitionToState_allowsCorrectTransitionsFor_Rejected() {
        IngestProcessingItem item = new IngestProcessingItem();

        item.setProcessingStatus(ProcessingStatus.REJECTED);
        testable.transitionToState(item, ProcessingStatus.DELETED);
        assertEquals(ProcessingStatus.DELETED, item.getProcessingStatus());

    }

    @Test
    void transitionToState_blocksIncorrectTransitionsFor_Rejected() {
        IngestProcessingItem item = new IngestProcessingItem();
        item.setProcessingStatus(ProcessingStatus.REJECTED);

        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.COMPLETE);
        });

        item.setProcessingStatus(ProcessingStatus.REJECTED);
        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.ARCHIVED);
        });
    }

    @Test
    void transitionToState_allowsCorrectTransitionsFor_StagingRejected() {
        IngestProcessingItem item = new IngestProcessingItem();

        item.setProcessingStatus(ProcessingStatus.STAGING_REJECTED);
        testable.transitionToState(item, ProcessingStatus.DELETED);
        assertEquals(ProcessingStatus.DELETED, item.getProcessingStatus());

    }

    @Test
    void transitionToState_blocksIncorrectTransitionsFor_StagingRejected() {
        IngestProcessingItem item = new IngestProcessingItem();
        item.setProcessingStatus(ProcessingStatus.STAGING_REJECTED);

        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.COMPLETE);
        });

        item.setProcessingStatus(ProcessingStatus.REJECTED);
        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.ARCHIVED);
        });
    }

    @Test
    void transitionToState_allowsCorrectTransitionsFor_CreateDatabaseObjects() {
        IngestProcessingItem item = new IngestProcessingItem();

        item.setProcessingStatus(ProcessingStatus.CREATE_DATABASE_OBJECTS);
        testable.transitionToState(item, ProcessingStatus.REJECTED);
        assertEquals(ProcessingStatus.REJECTED, item.getProcessingStatus());

        item.setProcessingStatus(ProcessingStatus.CREATE_DATABASE_OBJECTS);
        testable.transitionToState(item, ProcessingStatus.DELETED);
        assertEquals(ProcessingStatus.DELETED, item.getProcessingStatus());

        item.setProcessingStatus(ProcessingStatus.CREATE_DATABASE_OBJECTS);
        testable.transitionToState(item, ProcessingStatus.COMPLETE);
        assertEquals(ProcessingStatus.COMPLETE, item.getProcessingStatus());
    }
    @Test
    void transitionToState_blocksIncorrectTransitionsFor_CreateDatabaseObjects() {
        IngestProcessingItem item = new IngestProcessingItem();
        item.setProcessingStatus(ProcessingStatus.CREATE_DATABASE_OBJECTS);

        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.ARCHIVED);
        });
    }


    @Test
    void transitionToState_allowsCorrectTransitionsFor_SendToPreProcessingQueue() {
        IngestProcessingItem item = new IngestProcessingItem();

        item.setProcessingStatus(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);
        testable.transitionToState(item, ProcessingStatus.REJECTED);
        assertEquals(ProcessingStatus.REJECTED, item.getProcessingStatus());

        item.setProcessingStatus(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);
        testable.transitionToState(item, ProcessingStatus.DELETED);
        assertEquals(ProcessingStatus.DELETED, item.getProcessingStatus());

        item.setProcessingStatus(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);
        testable.transitionToState(item, ProcessingStatus.PROCESSING_FILENAME_METADATA);
        assertEquals(ProcessingStatus.PROCESSING_FILENAME_METADATA, item.getProcessingStatus());
    }

    @Test
    void transitionToState_blocksIncorrectTransitionsFor_SendToPreProcessingQueue() {
        IngestProcessingItem item = new IngestProcessingItem();
        item.setProcessingStatus(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);

        assertThrows(InvalidProcessingStatusException.class, () -> {
            testable.transitionToState(item, ProcessingStatus.ARCHIVED);
        });
    }

}