package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import fi.sls.ingest.representation.file.FilenameMetaData;
import lombok.Data;
import lombok.Getter;

@Data
public class DerivateObject extends FileMakerResponseEntity {

    @Getter
    @JsonIgnore
    private String layoutName = "ingestapi_derivativeObjects";

    DerivateObjectFieldData fieldData;

    public DerivateObject(){
        this.fieldData = new DerivateObjectFieldData();
    }

    /**
     * Utility method that calls the same method on the fieldData object
     *
     * @param filenameMetaData
     */
    public void populateFromMetadataResults(AccessFile accessFile, FilenameMetaData filenameMetaData){
        this.fieldData.populateFromMetadataResults(accessFile, filenameMetaData);
    }

    @Override
    public FindQueryRequest createFindQueryRequest() {
        FindQueryRequest queryRequest = new FindQueryRequest("filePath", "="+getFieldData().getFilePath());
        queryRequest.addToExistingQueryItem(0, "isGeneratedFrom", getFieldData().getIsGeneratedFrom());
        queryRequest.addToExistingQueryItem(0, "filePath_folder", getFieldData().getFilePathFolder());

        return queryRequest;
    }

    @Override
    public UpdateRequest createUpdateRequest() {
        return new UpdateRequest(getFieldData());
    }

    @Override
    public CreateRequest createRecordRequest() {
        return new CreateRequest(getFieldData());
    }
}
