# SLS Ingest API / Db Interface


An Spring Boot 2 / Java-based API for extracting metadata from filenames and the XML-file created by running the metadata-extractor project, which is then inserted into a database for further processing. Currently the API is built around storing the values in a FileMaker database, but it should be possible to change the target by writing a new IngestPostProcessRunnable


## Table of Contents

1. [Process description for full Ingest stack](#process-desc)
1. [Automated build and deployment](#automated-build)
1. [Building the Docker image](#docker-build)
1. [Deployment](#deployment)
    1. [Starting the Docker based stack locally](#deployment-local)
    1. [Deploy on docker swarm](#deployment-swarm)
    1. [Updating an image on the swarm](#deployment-update)
    1. [Configuration changes](#deployment-config-changes)
1. [Running a Arkiva collection migration](#migration)
1. [Development](#development)
1. [Known issues](#known-issues)
1. [Troubleshooting](#troubleshooting)
    1. [Check service health](#troubleshooting-check-health)
    1. [Logging](#troubleshooting-logging)
    1. [File is stuck in PROCESSING_USER_COPIES status](#troubleshooting-file-stuck)
    1. [Client loads slowly or gateway timeout](#troubleshooting-client-slow)


<a href="process-desc"></a>
## Process description for full Ingest stack

The flow chart below describes the steps a file is moved through once the user as started the ingest process after staging a file or batch of files.

MD5 checksum calculation before transfer to masterfiles was moved from the FITS stack to File API 2020-10-21 because the FITS stack calculator caused problems for large files, and the File API does a checksum verification anyway.

![Ingest process description](./docs/images/Ingest-flow-2020.svg "Ingest process description")

<a href="automated-build"></a>
## Automated build and deployment

A Jenkinsfile exists which links to a pipeline project on jenkins.sls.fi. This handles everything from building a new docker image, tagging it, pushing to docker hub and finally deploying a new release on the target server. It is recommended to deploy new images using this mechanism, however manual build and deployment using the steps described further down is also possible. Note that the build process must be triggered manually, as the code resides in a BitBucket repo that does not have hook access to the SLS Jenkins server, and the code is not updated frequently enough to warrant a polling for changes. In addition, the production release pipeline has a manual approval stage which must be clicked in Jenkins once the build reaches the deployment stage.

When deploying a new image, make sure to update the version number both in the ```pom.xml``` and the ```docker-compose.prod.yml``` file.

<a href="docker-build"></a>
## Building the Docker image

For a one-liner build and publish you can use the build script found in ```scripts/build.sh```. The script must be run from the root folder of the project and it requires ```mvn``` and ```docker``` to be available to the user running the script.

For usage instructions, run
```
scripts/build.sh
```


For new code to appear in the docker image, you must build a jar of the source code and copy it under the docker/ingest.jar path (see step 2-3 below). 

The db-interface docker image can be built manually using the steps below.

1. Change into the project root folder, then build the jar-file by running

    ```
    mvn clean package -Dspring.config.location=classpath:application.yml,classpath:application-test.yml
    ```

    the application-test.yml is needed for unit/integration tests to pass, but will not be copied into the resulting jar file.

1. Copy the resulting ingest-api-<version nr hash here>.jar to the docker folder
    ```
    cp target/ingest-api-<version nr hash here>.jar docker/ingest.jar
    ```

1. Build the Docker image
    ```
    docker build -t slsfinland/dbinterface:local docker/.
    ```

    _note: the ```:local``` tag should never be pushed to the remote registry. It is used to ensure a local docker stack deploy does not use a production build of the image._ 

1. To push the image to a registry, tag it with the registry name + new version nr as well and then push

    ```
    docker image ls 
    docker tag <my_image_id> slsfinland/dbinterface:1.2.3
    docker push slsfinland/dbinterface:1.2.3
    ```
    
    also tag the image with ```:latest``` and push to registry
    
    ```
    docker tag <my_image_id> slsfinland/dbinterface:latest
    docker push slsfinland/dbinterface:latest
    ```    

    you must be logged in to docker hub first before pushing (this should be needed only once):

    ```
    docker login --username=yourhubusername --password-stdin
    ```

You will be asked for your docker hub account password.


<a href="deployment"></a>
## Deployment

If deployed as a docker swarm stack using the configs in the ```ingest-stack``` folder, the following addresses are made available on the server:

1. ```<server_address>/traefik``` - Traefik proxy dashboard, for checking status of docker services. The traefik proxy handles routing of incoming requests.
1. ```<server_address>/ingest-client``` - The main browser client used to start ingest processes
1. ```<server_address>/ingest-api``` - Endpoint for this API 
1. ```<server_address>/fits``` - The FITS metadata extractor tool
1. ```<server_address>:15672/``` - RabbitMQ admin dashboard, for managing or monitoring the work queues.

<a href="deployment-local"></a>
### Starting the Docker based stack locally

Change into the ./ingest-stack folder, then run

```
docker-compose up
```

The default setup, which includes the docker-compose.override.yml, will create a stack aimed at local development, which will use the docker image tagged with ```dbinterface:local```. This is to prevent one from accidentally starting a stack that accesses production systems. 

In general this should not be needed, but to run the production environment, start the stack using

```
docker-compose up -f docker-compose.yml -f docker-compose.prod.yml
```

This will use the image tagged with ```slsfinland/dbinterface:<some_version>```. Note: make sure to modify the docker-compose.prod.yml to match the production environment.


<a href="deployment-swarm"></a>
### Deploy on docker swarm
The production servers (ingest01, ingest02) use a docker swarm based setup. To generate a single docker stack yml configuration file from the docker-compose files for use in the swarm, run

```
docker-compose -f docker-compose.yml -f docker-compose.prod.yml config > ingest-swarm.yml
```

To deploy the ingest-swarm.yml on a server, run

```
docker stack deploy --compose-file ingest-swarm.yml sls_ingest
```

Before the stack can be deploy the docker daemon must be set to swarm mode, and a overlay network created:

```
docker swarm init
docker network create --driver=overlay ingest-net
```

Also the following docker secrets need to be available in the swarm:

* ingest_api_db_password
* ingest_api_db_root
* ingest_api_db_username
* ingest_api_filemaker_rest_pass
* ingest_api_filemaker_rest_user
* ingest_api_key
* ingest_api_ldap_pass
* ingest_api_ldap_user
* ingest_api_rabbitmq_pass
* ingest_api_rabbitmq_user

And the following configurations:

* ingest_api_rabbitmq_plugins (= the contents of the file ./ingest-stack/rabbit_mq_plugins in this project)

<a href="deployment-update"></a>
### Updating an image on the swarm

1. Ensure the new image is available in the Docker registry.
2. ssh into a swarm manager host and ```cd /var/www/ingest_api```
3. Repeat the docker stack deploy command from the previous section, and it should update the stack with new images. Note if you use a new image version nr, you should also update the corresponding service in the ```ingest-stack.yml``` file with the new version tag. Also note that this command will update the entire stack to match the values in the ingest-stack.yml file. This includes scaling containers to the nr that matches the deploy parameter in the yml file, etc. so if any manual modifications have been done to the stack config they will be lost.

<a href="deployment-config-change"></a>
### Configuration changes

Changes to the runtime configuration can be done by passing a new application.yml file to the startup command if running the jar file directly. In the example below the application-dev.yml file is added and thus overrides values in application.yml:

```
java -jar ingest.jar -Dspring.config.location=classpath:application.yml,classpath:application-dev.yml
```

To do this with the docker image, modify the relevat docker-compose file and add environment variables to e.g. the ```dbinterface``` service using CAPS_SNAKE for any value in the application.yml file. E.g. to change the rest path for the spring boot service:

```
services:
    dbinterface:
        environment:
            SPRING_DATA_REST_BASEPATH: my/new/rest/path/1.0
        
        ... <rest of config>
```


For more permament configuration changes such as category additions, it's recommended to create a separate application.yml that contains the modified values, store it as a docker config on the server, and reference the config from the service. See the ZTS config for example.

<a href="migration"></a>
## Running a Arkiva collection migration

The migration tool available through the API converts dc_identifiers to follow the expected format, and also calls the File API to move an existing masterfile to a new location with new filename based on the expected format. The File API will generate new accessfiles and return the new paths to the Ingest API, after which the Ingest API updates the relevant records in FileMaker. 

Note that there is a UI available for these actions in the ingest client as well, so manual calls using curl should not be needed other than for debugging.

Migrations are usually done on a per collection basis. The requests require authentication, see Troubleshooting section for instructions. Migrating a new collection may require code changes and fixes in order to accommodate the peculiarities of that collection.

### Dry run

To see if records in a collection might potentially not pass a migration run, you can do a dry run. This will process each matching record up to the File API request. No destructive updates are made to the records at this time. The dry run outputs a report in Excel format.  

To do a dry run, set the ```isDryRun``` parameter to true in the request (see examples below)

Example 1. Dry run on the collection ÖTA 112, matching entity identifiers starting with ```OTA:ota112-28```

```
curl --request POST \
  --url http://ingest.sls.fi/ingest-api/api/rest/1.0/migration/start \
  --header 'accept: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' \
  --header 'authorization: Bearer <your-auth-token>' \
  --header 'content-type: application/json' \
  --data '    {
        "collectionName": "ÖTA",
				"archiveNr": 112,
				"limit": 0,
				"maxPageNr": 0,
				"isDryRun": true,
				"entityIdentifier": "OTA:ota112-28*"
    } '
```

Example 1. Dry run on the collection ÖTA 112, matching an exact entity identifier ```OTA:ota112-20_ent.foto.02268```
```
curl --request POST \
  --url http://ingest.sls.fi/ingest-api/api/rest/1.0/migration/start \
  --header 'accept: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' \
  --header 'authorization: Bearer <my-auth-token>' \
  --header 'content-type: application/json' \
  --data '    {
        "collectionName": "ÖTA",
				"archiveNr": 112,
				"limit": 0,
				"maxPageNr": 0,
				"isDryRun": true,
				"entityIdentifier": "OTA:ota112-20_ent.foto.02268"
    } '
```    
    
### Production run

To do a production run, simply change the ```isDryRun``` parameter to ```false``` once the dry run produces a satisfactory output

### Check intermediate status

A simple metrics endpoint exists that outputs the current nr of COMPLETE migrations for a given collection. This can be used to gauge whether or not the migration is complete by checking to count before starting the migration, then repeating the request until it seems like the value is not changing anymore and the (new value - start value) matches the expected nr of records to process. 

```
curl --request GET \
  --url 'http://ingest.sls.fi/ingest-api/actuator/metrics/ingestapi.migration_items.count?tag=collection%3A%C3%B6ta_112%2Cstatus%3ACOMPLETE' \
  --header 'authorization: Bearer <my-auth-token>'
```
 

### Report

A final report can be generated in Excel format, which will show whether or not each record was successfully migrated.

E.g. to get the report for the collection SLSA 1150, make the following request

```
curl --request POST \
  --url http://ingest.sls.fi/ingest-api/api/rest/1.0/migration/report \
  --header 'accept: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' \
  --header 'authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ7XCJpZFwiOjIsXCJ1c2VybmFtZVwiOlwiamVud2VnXCIsXCJhdXRob3JpdGllc1wiOltdLFwiZW5hYmxlZFwiOnRydWUsXCJhY2NvdW50Tm9uTG9ja2VkXCI6dHJ1ZSxcImFjY291bnROb25FeHBpcmVkXCI6dHJ1ZSxcImNyZWRlbnRpYWxzTm9uRXhwaXJlZFwiOnRydWV9IiwiaWF0IjoxNTg5OTYxMjA0LCJleHAiOjE1ODk5NzU2MDR9.gqnDVrC7zxpej5cq3RugUNcHadR5qeRIwT3aLQNuJe8' \
  --header 'content-type: application/json' \
  --data '    {
        "collectionName": "SLSA",
				"archiveNr": 1150
    }'
```

<a href="development"></a>
## Development


### Running unit tests and generating coverage reports

To run unit tests change to the project root directory and run
```
mvn clean test
```

To generate code coverage reports after a test run, call
```
mvn jacoco:report
```
The coverage report is available as static html files under target/site/jacoco

<a href="known-issues"></a>
## Known issues

- The metadata extractor (FITS stack) has trouble handling large video files. It seems when the file size closes in on 100GB it silently fails at some point of the analysis. Possibly this has to do with the cifs mount used for mounting the Isilon share, but this is not confirmed. Currently no proper known solution. Suggest ingesting these types of video files via other means.


<a href="troubleshooting"></a>
## Troubleshooting

Note all API requests below require authentication in the form of a bearer token. You can authenticate yourself by making a request to the auth endpoint using your AD credentials:

```
curl --request POST \
  --url http://ingest.sls.fi/ingest-api/api/rest/1.0/auth/signin \
  --header 'content-type: application/json' \
  --data '{
	"username": "<AD username>",
	"password": "<AD password>"
}'
```

<a href="troubleshooting-check-health"></a>
## Check service health

You can make a call to the health endpoint to check the current status of all services that the Ingest API depends on:
```
curl --request GET \
  --url http://ingest.sls.fi/ingest-api/actuator/health 
```

This request does not require authentication.

<a href="troubleshooting-logging"></a>
## Logging

Logs from the ingest stack are streamed to the Logstash/Kibana service at log.sls.fi.

Logging is done using the logback framework and spring boot actuators, which allow for granular adjustment of log levels on the fly. 

To see all available loggers, make a GET request to the actuator/loggers endpoint:

```
curl --request GET \
  --url http://ingest.sls.fi/ingest-api/actuator/loggers \
  --header 'authorization: Bearer <some-bearer-token>' 
```

To adjust the log level either on the package or class level, make a POST request appending the package name to the end of the endpoint, e.g.:

```
 curl --request POST \
   --url http://ingest.sls.fi/ingest-api/actuator/loggers/fi.sls.ingest \
   --header 'authorization: Bearer <some-bearer-token>' \
   --header 'content-type: application/json' \
   --data '{
     "configuredLevel": "DEBUG"
 }'
```

<a href="troubleshooting-file-stuck-metadata"></a>
### File is stuck in PROCESSING_METADATA status

This seems to happen when file sizes close in on 100GB and thus typically concerns video files (see known issues). 

1. First ensure the files are not still being processed by the metadata extractor. Log in to both ```ingest01.sls.fi``` and ```ingest02.sls.fi``` servers using SSH, and run ```docker stats```. Verify that the CPU usage of the metadata-extractor video containers is at or close to 0%, meaning they are not doing anything right now.
1. If the extractors are not doing anything, but video files are stuck in the processing_metadata state according to the UI, shut down the metadata extractor services and the db-interface by scaling them down. This needs to be done on a swarm manager node. Note! first ensure nobody is ingesting other items at the same time, as scaling down the db-interface will interrupt this process!

    ```
    docker service scale sls_ingest_fits_metadata-extractor-video=0 sls_ingest_ingest-db-interface=0
    ```
1. Once the services have scaled down, log into the RabbitMQ manager, find the ```ingest-pre-processing-video-work-queue``` and verify that it has 1-2 files in the "Ready" state. 
    * if the nr of items in the queue in the "ready" state matches the nr of files stuck in "PROCESSING_METADATA" state, these are the same items, so it should be safe to just purge the queue.
    * if there are more items in the "ready" than stuck items, one needs to clear the stuck items one by one. Open the "Get messages" section of the queue interface, change the Ack mode to "Ack message requeue false", then click "Get messages" once for each file that is stuck. This should allow items that are queued after the stuck items to continue processing once the services run again.
1. Once the RabbitMQ items have been removed, start up the docker service again
    ```
    docker service scale sls_ingest_fits_metadata-extractor-video=2 sls_ingest_ingest-db-interface=1
    ```
1. Once the services are running again, you can either retry the failed files, or remove them from the ingest and process them another way.

<a href="troubleshooting-file-stuck"></a>
### File is stuck in PROCESSING_USER_COPIES status

1. First ensure that the file is not actually still being processed. Search the logs for the filename or ingest token to determine if the File API has made a callback request to the ```<ingest-api-basepath>/ingest/continue``` endpoint.
1. If the File API has made the request, ensure that the expected files actually exist on masterfiles/accessfiles. See the callback payload for expected file paths.
1. If masterfiles seems ok, then for some reason the callback request did not pass all the way back to the Ingest API. You can safely do a manual continuation of the process by repeating the request using e.g. curl (though you should try to investigate why the callback did not succeed, as this is not normal behavior):
   ```
    curl --request POST \
     --url 'http://ingest.sls.fi/ingest-api/api/rest/1.0/ingest/continue?token=DE527A03017B367F973C5E9306FDA2B8' \
     --header 'content-type: application/json' \
     --data '{
	   "status": "ok",
	   "path": "/masterfiles/slsa/slsa1070/slsa_1070.97_dbok_00001_0042_01.tif",
	   "metadataPath": "/mnt/masterfiles/slsa/slsa1070/slsa_1070.97_dbok_00001_0042_01.tif.fits.xml",
	   "md5": "9499428dd001eb16bddd5c76699caee7",
	   "accessfiles": [
   		{
   			"path": "accessfiles/kundkopior/slsa/slsa1070/slsa_1070.97_dbok_00001_0042_01.jpg",
   			"mimeType": "image/jpeg"
   		},
   		{
   			"path": "accessfiles/databasbilder/slsa/slsa1070/slsa_1070.97_dbok_00001_0042_01.jpg",
   			"mimeType": "image/jpeg"
   		},
   		{
   			"path": "accessfiles/thumbnails/slsa/slsa1070/slsa_1070.97_dbok_00001_0042_01.jpg",
   			"mimeType": "image/jpeg"
   		}
   	],
   	"token": "DE527A03017B367F973C5E9306FDA2B8"
   }'
   ```

    In the example above we manually continue the processing for a file that has the token DE527A03017B367F973C5E9306FDA2B8. Note that for this endpoint, a Bearer token is not necessary, but a valid item token is.


<a href="troubleshooting-client-slow"></a>
### Client loads slowly or gateway timeout

Log in to a docker swarm manager node, and try reloading the Traefik proxy configuration by scaling the service down to 0, then back up to 1
```
docker service scale sls_ingest_reverse-proxy=0
docker service scale sls_ingest_reverse-proxy=1
```
Wait for the service to stabilise, then try to load the client again.
