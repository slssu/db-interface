package fi.sls.ingest.proxy.filemaker.response.record;

import lombok.Getter;

public enum PremisEventType {
    CREATION("creation"),
    INGESTION("ingestion"),
    DIGEST_CALCULATION("digest calculation"),
    MIGRATION("migration");

    @Getter
    private String code;

    PremisEventType(String code){
        this.code = code;
    }

}
