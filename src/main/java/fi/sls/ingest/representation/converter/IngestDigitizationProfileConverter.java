package fi.sls.ingest.representation.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.manager.IngestDigitizationProfile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.AttributeConverter;
import java.io.IOException;

@Slf4j
public class IngestDigitizationProfileConverter extends JsonStringConverter implements AttributeConverter<IngestDigitizationProfile, String> {

    @Autowired
    public IngestDigitizationProfileConverter(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public String convertToDatabaseColumn(IngestDigitizationProfile ingestDigitizationProfile) {
        String rtnJson = null;

        if(ingestDigitizationProfile == null){
            return null;
        } else {
            try{
                rtnJson = objectMapper.writeValueAsString(ingestDigitizationProfile);
            } catch (JsonProcessingException e) {
                log.error("Error converting from IngestDigitizationProfile to JSON: {}", e.getMessage());
            }
            return rtnJson;
        }
    }

    @Override
    public IngestDigitizationProfile convertToEntityAttribute(String jsonData) {

        if(jsonData == null){
            return null;
        } else {
            IngestDigitizationProfile rtnItem = null;
            try{
                rtnItem = objectMapper.readValue(jsonData, IngestDigitizationProfile.class);
            } catch (IOException e) {
                log.error("Error converting to IngestDigitizationProfile from JSON: {}", e.getMessage());
            }
            return rtnItem;
        }


    }
}
