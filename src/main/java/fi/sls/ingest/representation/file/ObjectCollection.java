package fi.sls.ingest.representation.file;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents a single SLS collection as defined by the sls.metadata.collections configuration
 * after the configuration has been initialized.
 */
@Data
@NoArgsConstructor
public class ObjectCollection {
    protected String fileKey;

    protected String label;

    protected String description;

    public ObjectCollection(String fileKey, String label){
        this.fileKey = fileKey;
        this.label = label;
    }

    public ObjectCollection(String fileKey, String label, String description){
        this.fileKey = fileKey;
        this.label = label;
        this.description = description;
    }
}
