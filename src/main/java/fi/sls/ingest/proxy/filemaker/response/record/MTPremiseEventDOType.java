package fi.sls.ingest.proxy.filemaker.response.record;

import lombok.Getter;

public enum MTPremiseEventDOType {

    CREATED("cr"),
    INGESTION("ing"),
    DIGEST_CALCULATION("mdc"),
    MIGRATION("migr");

    @Getter
    private String code;

    MTPremiseEventDOType(String code){
        this.code = code;
    }

}
