package fi.sls.ingest.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.lang.annotation.*;

/**
 * This class defines the validation that a string must pass in order to be considered a valid
 * filename for the SLS ingest process.
 */
@Target({ ElementType.ANNOTATION_TYPE, ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { })
@Documented
@NotEmpty
@Pattern(regexp = "^[a-z]+_[a-z0-9]+([\\.][a-z0-9\\.]+)?_[a-z0-9]+_[0-9]+_[0-9]+(_[0-9]+)?(_[a-z]+)?\\.[a-zA-Z0-9]+$", message = "File name must follow the SLS naming convention.")
public @interface ValidProcessableFile {

    String message() default "File name must follow the SLS naming convention.";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
