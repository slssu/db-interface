package fi.sls.ingest.exception;

/**
 * Exception thrown when an attempt is made to transition a IngestProcessingItem to a ProcessingStatus
 * that it should not be allowed in
 */
public class InvalidProcessingStatusException extends RuntimeException{
    public InvalidProcessingStatusException(String message) {
        super(message);
    }
}
