package fi.sls.ingest.manager;

import lombok.Data;

/**
 * Wrapper class for staging area validation errors on IngestProcessingItems
 *
 * Flags can have different levels of severity: warnings allow the process to continue, whereas errors block the
 * processing of this file
 */
@Data
public class IngestProcessingItemFlag {

    public IngestProcessingItemFlag(Severity severity, String message) {
        this.severity = severity;
        this.message = message;
    }

    /**
     * Defines the levels of Flags we can have.
     *
     * - A warning is shown to the user but does not stop the processing of the item.
     * - A error means the item is rejected and not processed.
     */
    public enum Severity{WARNING, ERROR}

    /**
     * The severity level of the flag
     */
    private Severity severity;

    /**
     * A string explaining the reason for the Flag
     */
    private String message;
}
