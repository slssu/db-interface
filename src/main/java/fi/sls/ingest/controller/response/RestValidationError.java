package fi.sls.ingest.controller.response;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * A wrapper class for returning errors to a REST client in a parsable form
 *
 * One instance of this class can contain validation errors for multiple fields
 */
public class RestValidationError {

    /**
     * The application internal error code
     */
    @Getter
    @Setter
    private String errorCode;

    /**
     * The short error message to return
     *
     * This message should be formatted so it can be shown to the end user in the client.
     */
    @Getter
    @Setter
    private String errorMessage;

    @Getter
    private List<FieldError> fieldErrors = new ArrayList<>();

    public RestValidationError(String errorCode, String errorMessage){
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    /**
     * Adds an error for a specific json field
     *
     * @param path
     *      The json path to the field with the error
     *
     *      e.g. digitizationProfile.profileId
     *
     * @param message
     *      The error message for that specific field
     */
    public void addFieldError(String path, String message){
        this.fieldErrors.add(new FieldError(path, message));
    }

    /**
     * Creates an instance of this class with the error code set to SLS-001 and matching message
     *
     * @return A new instance of this class
     */
    public static RestValidationError SLS001(){
        return new RestValidationError("SLS-001", "Parameter validation failed for one or more fields");
    }

}
