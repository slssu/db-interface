package fi.sls.ingest.actuator.metrics;

import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.MetricsMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service that provides an aggregate of the processing metrics wrapped in a MetricsMessage
 *
 */
@Service
public class ProcessingMetricsService {

    /**
     * Source for item metrics
     */
    protected IngestItemRepository ingestItemRepository;

    @Autowired
    public ProcessingMetricsService(IngestItemRepository ingestItemRepository){
        this.ingestItemRepository = ingestItemRepository;
    }

    public MetricsMessage loadMetrics(){
        MetricsMessage message = new MetricsMessage();

        // Create an aggregate stats object to broadcast
        // The metrics framework would otherwise be nice, but maybe in future
        List<IngestProcessingStatusCount> counts = ingestItemRepository.countAllProcessingStatus();
        message.setStatusCounts(counts);

        return message;

    }
}
