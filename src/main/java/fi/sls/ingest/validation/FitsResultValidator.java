package fi.sls.ingest.validation;

import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.fits.exception.FitsResultException;
import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.generated.StatusType;
import org.springframework.stereotype.Service;

/**
 * Validates that the FITS result returned from the service contains sane result items
 * i.e. it contains MD5 sums, no tools have failed, the result is not a partial identification
 */
@Service
public class FitsResultValidator {


    /**
     * Validates fields of the FitsResult and throws exception if the field check fails
     *
     * @param result
     * @throws FitsResultException
     */
    public void validate(FitsResult result) throws FitsResultException {

// disabled 2020-10-21 Jens: Due to video calculation problems, decided to let File API handle MD5 completely
        // check that basic data was returned (MD5 not null and has 32 char length)
//        if(result.getFileMD5() == null || !result.getFileMD5().matches("(?i:[0-9a-z]){32}")){
//            throw new FitsResultException(String.format("FITS did not return valid MD5 checksum, value was %s", result.getFileMD5()));
//        }

        if(result.getRawResult() == null){
            throw new FitsResultException("FITS did not return a raw FITS result XML.");
        }

        // check that the result is not a partial result
        if(result.getFitsResult().getIdentification().getStatus() == StatusType.PARTIAL){
            throw new FitsResultException("FITS returned a partial result. This might be because one of the analysis tools failed to run.");
        }
    }
}
