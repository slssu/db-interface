package fi.sls.ingest.security.filter;

import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.migration.DigitalObjectMigrationResult;
import fi.sls.ingest.repository.DigitalObjectMigrationResultRepository;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.security.CustomUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * Checks if the request url contains a valid IngestProcessingItem token string.
 *
 * Authenticates the request if a matching token is found in the database.
 * This class depends on there being a generic user with the ID 1 in the user database.
 */
@Slf4j
public class URLTokenAuthFilter extends OncePerRequestFilter {

    private CustomUserDetailsService customUserDetailsService;
    private IngestItemRepository ingestItemRepository;
    private DigitalObjectMigrationResultRepository digitalObjectMigrationResultRepository;

    public URLTokenAuthFilter(
            CustomUserDetailsService customUserDetailsService,
            IngestItemRepository ingestItemRepository,
            DigitalObjectMigrationResultRepository digitalObjectMigrationResultRepository
    ){
        this.customUserDetailsService = customUserDetailsService;
        this.ingestItemRepository = ingestItemRepository;
        this.digitalObjectMigrationResultRepository = digitalObjectMigrationResultRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        try {
            // token should be the last part of the URL if it's valid, the url should match the route to ingest/continue
            String token = getTokenFromRequest(request);

            if (StringUtils.hasText(token)) {

                boolean tokenWasFound;

                // which repository should be used for checking the token string
                if(request.getRequestURI().contains("migration/continue")){
                    Optional<DigitalObjectMigrationResult> item = digitalObjectMigrationResultRepository.findByToken(token);
                    tokenWasFound = item.isPresent();
                } else {
                    Optional<IngestProcessingItem> item = ingestItemRepository.findByToken(token);
                    tokenWasFound = item.isPresent();
                }

                if(tokenWasFound){
                    // we found an existing item matching the token, so accept as authenticated
                    UserDetails userDetails = customUserDetailsService.loadUserById(1L);
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }

            }
        } catch (Exception ex) {
            log.error("Could not set user authentication in security context", ex);
        }

        filterChain.doFilter(request, response);
    }

    /**
     * Fetches the token parameter from the request object
     *
     * @param request
     * @return The token value if the parameter is found
     */
    private String getTokenFromRequest(HttpServletRequest request) {
        return request.getParameter("token");
    }
}
