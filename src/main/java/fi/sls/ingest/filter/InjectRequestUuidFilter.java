package fi.sls.ingest.filter;

import org.slf4j.MDC;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;

/**
 * Servlet filter that adds a uuid to each request in the MDC for
 * logging purposes
 */
@Component
@Order(1)
public class InjectRequestUuidFilter implements Filter {

    private final String LOG_KEY = "request_uuid";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;

        MDC.put(LOG_KEY, String.valueOf(UUID.randomUUID()));

        try {
            filterChain.doFilter(request, response);
        } finally {
            MDC.remove(LOG_KEY);
        }
    }
}
