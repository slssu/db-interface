package fi.sls.ingest.proxy.filemaker.response;

import lombok.Data;

@Data
public class RequestError {

    /**
     * FileMaker internal error code
     *
     * @see <a href="https://fmhelp.filemaker.com/help/17/fmp/en/index.html#page/FMP_Help/error-codes.html">FileMaker error codes</a>
     */
    private int errorCode;

    /**
     * Error message returned from FileMaker
     */
    private String errorMessage;

    public RequestError(){

    }

    public RequestError(int errorCode, String errorMessage){
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }


}
