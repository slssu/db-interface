package fi.sls.ingest.queue.rabbitmq.sender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.queue.rabbitmq.queue.IngestItemPostProcessingQueue;
import fi.sls.ingest.queue.rabbitmq.sender.IngestPostProcessingItemSender;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.SocketMessenger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class IngestPostProcessingItemSenderTest {

    RabbitTemplate template;
    IngestItemPostProcessingQueue queue;
    ObjectMapper objectMapper;
    SocketMessenger socketMessenger;
    IngestItemRepository ingestItemRepository;

    IngestPostProcessingItemSender testable;

    @BeforeEach
    void setUp(){

        template = Mockito.mock(RabbitTemplate.class);
        queue = Mockito.mock(IngestItemPostProcessingQueue.class);
        objectMapper = Mockito.mock(ObjectMapper.class);
        socketMessenger = Mockito.mock(SocketMessenger.class);
        ingestItemRepository = Mockito.mock(IngestItemRepository.class);

        testable = new IngestPostProcessingItemSender(template, queue, objectMapper, socketMessenger, ingestItemRepository);

        when(queue.getName()).thenReturn("testQueue");
    }

    @Test
    void send_callsOverloadWithCorrectParam() {

        IngestProcessingItem item = Mockito.mock(IngestProcessingItem.class);
        when(item.getProcessingStatus()).thenReturn(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);

        IngestProcessingItem item2 = Mockito.mock(IngestProcessingItem.class);

        when(item.getToken()).thenReturn("itemToken");
        when(item2.getToken()).thenReturn("itemToken");

        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.of(item));
        IngestPostProcessingItemSender spyTestable = Mockito.spy(testable);

        spyTestable.send(item2);

        verify(spyTestable).send(item2, ProcessingStatus.SEND_TO_POST_PROCESSING_QUEUE);
    }

    @Test
    void send_throwsExceptionIfItemAlreadyQueued(){
        IngestProcessingItem item = Mockito.mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");
        when(item.getProcessingStatus()).thenReturn(ProcessingStatus.SEND_TO_POST_PROCESSING_QUEUE);

        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.of(item));



        IngestProcessingException exception = assertThrows(IngestProcessingException.class, () -> {
            testable.send(item);
        });

        assertThat(exception.getMessage(), containsString("status SEND_TO_POST_PROCESSING_QUEUE"));
        assertThat(exception.getMessage(), containsString("will not add again"));

    }

    @Test
    void send_failsIfItemNotPresentInRepository() throws JsonProcessingException {
        IngestProcessingItem item = Mockito.mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");
        when(item.getProcessingStatus()).thenReturn(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);

        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.empty());

        String mockJson = "jsonSerializedItem";
        when(objectMapper.writeValueAsString(item)).thenReturn(mockJson);

        IngestProcessingException e = assertThrows(IngestProcessingException.class, () -> {
            testable.send(item);
        });

        assertEquals("Could not find stored item with token: itemToken", e.getMessage());
    }

    @Test
    void send_sendsItemToQueueAndSavesToRepository() throws JsonProcessingException {
        IngestProcessingItem item = Mockito.mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");
        when(item.getProcessingStatus())
                .thenReturn(ProcessingStatus.STAGING)
                .thenReturn(ProcessingStatus.REJECTED);

        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.of(item));

        String mockJson = "jsonSerializedItem";
        when(objectMapper.writeValueAsString(item)).thenReturn(mockJson);

        testable.send(item);

        verify(template).convertAndSend(queue.getName(), mockJson);
        verify(item).setProcessingStatus(ProcessingStatus.SEND_TO_POST_PROCESSING_QUEUE);
        verify(ingestItemRepository).save(item);
    }

    @Test
    void send_trowsExceptionIfItemNotSerializable() throws JsonProcessingException {
        IngestProcessingItem item = Mockito.mock(IngestProcessingItem.class);
        when(item.getToken()).thenReturn("itemToken");

        when(ingestItemRepository.findByToken("itemToken")).thenReturn(Optional.empty());

        String mockJson = "jsonSerializedItem";
        when(objectMapper.writeValueAsString(item)).thenThrow(JsonProcessingException.class);

        assertThrows(IngestProcessingException.class, () -> {
            testable.send(item);
        });


    }

}