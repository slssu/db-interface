package fi.sls.ingest.proxy.slsfileapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A minimal projection of an AccessFile, containing only the
 * properties the File API expects.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccessFileMinimal {

    /**
     * The path to the access file as returned by the File API
     */
    String path;

    /**
     * The mimetype of the access file as returned by the File API
     */
    String mimeType;

}
