package fi.sls.ingest.security;

import org.springframework.security.core.Authentication;

public interface AuthenticationFacade {

    /**
     * Provides access to an Authentication object containing user details
     */
    Authentication getAuthentication();
}
