package fi.sls.ingest.reporting;

import fi.sls.ingest.manager.IngestProcessingItem;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class StagingItemsReport implements Serializable {
    List<IngestProcessingItem> items = new ArrayList<>();
}
