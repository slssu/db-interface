package fi.sls.ingest.representation.converter;

import fi.sls.ingest.manager.IngestDigitizationProfile;
import org.junit.jupiter.api.BeforeEach;

class IngestDigitizationProfileConverterTest extends JsonStringConverterTestBase<IngestDigitizationProfile> {

    @BeforeEach
    void setUp() {
        testable = new IngestDigitizationProfileConverter(objectMapper);
    }
}