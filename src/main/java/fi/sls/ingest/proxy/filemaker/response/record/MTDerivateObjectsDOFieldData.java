package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import fi.sls.ingest.proxy.filemaker.request.RecordPayload;
import lombok.Data;

@Data
public class MTDerivateObjectsDOFieldData implements RecordPayload {

    @JsonProperty("nummer")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Long id;

    @JsonProperty("der_nummer")
    String derNumber;

    @JsonProperty("do_identifier")
    String doIdentifier;

    @JsonProperty("do_nummer")
    Long doNumber;

}
