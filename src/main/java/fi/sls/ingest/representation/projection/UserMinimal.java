package fi.sls.ingest.representation.projection;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import fi.sls.ingest.representation.User;
import org.springframework.data.rest.core.config.Projection;

@Projection(name="minimal", types={User.class})
@JsonPropertyOrder(alphabetic=true)
public interface UserMinimal {

    Long getId();
    String getUsername();
    String getDisplayOrUsername();
}
