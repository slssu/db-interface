package fi.sls.ingest.manager.step;

import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.DigitalObject;
import fi.sls.ingest.proxy.filemaker.response.record.MTPremiseEventDOType;
import fi.sls.ingest.proxy.filemaker.response.record.MtPremiseEventsDO;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Service
@Slf4j
public class LinkPremisEventDigitalObjectStep extends RecordServiceStep {

    @Autowired
    public LinkPremisEventDigitalObjectStep(RecordService service) {
        super(service);
    }


    /**
     * Performs the tasks of this step
     *
     * @return The MtPremiseEventsDO that was linked to the given digitalObject and premiseEventDOType
     */
    public MtPremiseEventsDO executeStep(DigitalObject digitalObject, MTPremiseEventDOType mtPremisEventDOType, String eventDate) throws IOException {
        return executeStep(digitalObject, mtPremisEventDOType, eventDate, null);
    }

    /**
     * Performs the tasks of this step
     *
     * Provides option to override the createDate string which is used for event identifier
     *
     * @return The MtPremiseEventsDO that was linked to the given digitalObject and premiseEventDOType
     */
    public MtPremiseEventsDO executeStep(DigitalObject digitalObject, MTPremiseEventDOType mtPremisEventDOType, String eventDate, String createDate) throws IOException {
        MtPremiseEventsDO mtPremiseEventsDO = new MtPremiseEventsDO();
        mtPremiseEventsDO.getFieldData().setObjectNumber(digitalObject.getFieldData().getEntityNumber());
        mtPremiseEventsDO.getFieldData().setEventId(digitalObject.getPremiseEventIdForType(mtPremisEventDOType.getCode(), createDate));

        Optional<? extends FileMakerResponseEntity> existingItem;
//        jw 2022-02-14: remove try-catch as it is handled by runnable
//        try {
            existingItem = service.findRecordByIdentifier(mtPremiseEventsDO);

            if(existingItem.isPresent()){
                mtPremiseEventsDO = (MtPremiseEventsDO) existingItem.get();
            } else {
                mtPremiseEventsDO = new MtPremiseEventsDO();
                mtPremiseEventsDO.getFieldData().setEventId(digitalObject.getPremiseEventIdForType(mtPremisEventDOType.getCode(), createDate));
            }

            mtPremiseEventsDO.getFieldData().setObjectNumber(digitalObject.getFieldData().getEntityNumber());
            mtPremiseEventsDO.getFieldData().setTempDigitizationProfileId(digitalObject.getFieldData().getDigitizationProfileId());
            mtPremiseEventsDO.getFieldData().setTempEventDate(eventDate);

            if(mtPremiseEventsDO.isNewRecord()){
                mtPremiseEventsDO = (MtPremiseEventsDO) service.createObjectRecord(mtPremiseEventsDO);
                log.debug("Created new mtPremiseEventDO with id: {}", mtPremiseEventsDO.getRecordId());
            } else {
                mtPremiseEventsDO = (MtPremiseEventsDO) service.updateObjectRecord(mtPremiseEventsDO);
                log.debug("Updated mtPremiseEventDO with id: {}", mtPremiseEventsDO.getRecordId());
            }

            return mtPremiseEventsDO;

//        } catch (IOException e) {
//            // FIXME: probably critical, should re-throw instead
//            log.error("Error creating MtPremiseEventsDO link: {}", e.getMessage());
//            return null;
//        }
    }
}
