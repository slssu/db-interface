package fi.sls.ingest.fits.model.tool;

import fi.sls.ingest.fits.model.generated.Fits;

/**
 * Wrapper class that provides easier access to the result structure that the Droid
 * tool produces for the fields that we are interested in.
 *
 */
public class FitsDroidResult extends FitsToolResultBase {

    public FitsDroidResult(Fits xml){
        super(xml, "Droid");
    }

    /**
     * Searches for and returns the filePuid field from the Droid xml result
     *
     * @return
     */
    public String getFilePuid(){
        if(hasToolResult() && toolResult.getElementsByTagName("filePuid").getLength() > 0){
            return toolResult.getElementsByTagName("filePuid").item(0).getTextContent();
        } else {
            return null;
        }
    }
}
