package fi.sls.ingest.proxy.slsfileapi;

import fi.sls.ingest.fits.FitsService;
import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.manager.IngestProcessingItem;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotBlank;
import java.io.File;

/**
 * Base class for the request body structure for SLS File API requests
 *
 * See also <a href="https://bitbucket.org/slssu/file_api">https://bitbucket.org/slssu/file_api</a>
 *
 * See subclasses for example request body
 */
@Data
public abstract class FileAPIRequestBody {

    /**
     * The processing token used to identify this item in the Ingest db interface tool chain
     */
    @NotBlank
    private String token;

    /**
     * Absolute url to the API endpoint that the File API should call once results for the
     * file move are ready.
     */
    @NotBlank
    private String callbackUrl;

}
