package fi.sls.ingest.actuator.metrics;

import fi.sls.ingest.actuator.metrics.supplier.MigrationResultCountSupplier;
import fi.sls.ingest.actuator.metrics.supplier.ProcessingItemAverageDurationSupplier;
import fi.sls.ingest.actuator.metrics.supplier.ProcessingItemBytesSupplier;
import fi.sls.ingest.actuator.metrics.supplier.ProcessingItemCountSupplier;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.migration.MigrationResult;
import fi.sls.ingest.repository.DigitalObjectMigrationResultRepository;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.representation.projection.MigrationResultCount;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Publishes a metric for the processing status for migration items
 */
@Component
public class MigrationResultMetric {
    public static final String NAME_PREFIX = "ingestapi.migration_items";

    @Autowired
    public MigrationResultMetric(MeterRegistry registry, DigitalObjectMigrationResultRepository itemRepository){
        registerMonitors(registry, itemRepository);
    }

    protected void registerMonitors(MeterRegistry registry, DigitalObjectMigrationResultRepository itemRepository){

        // fetch list of all migration collections in the database

        List<MigrationResultCount> rows = itemRepository.countMigrationResults();

        for(MigrationResultCount m : rows){

            String collectionTag = m.getCollectionName().concat("_").concat(m.getArchiveNr().toString()).toLowerCase();

            Gauge
                    .builder(NAME_PREFIX + ".count", new MigrationResultCountSupplier(itemRepository, m))
                    .tag("collection", collectionTag)
                    .tag("status", m.getProcessingStatus().name())
                    .description("The nr of items currently having the given status in the given collection")
                    .register(registry);
        }

        Gauge
                .builder(NAME_PREFIX + ".totalItems", new MigrationResultCountSupplier(itemRepository))
                .description("The total nr of items in the migration table)")
                .register(registry);

    }
}
