package fi.sls.ingest.fits.model.fieldresolver.identification;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;

import java.util.Optional;

public class FitsIdentityMetadataMimetype extends AbstractFieldValueResolver {
    public FitsIdentityMetadataMimetype(FieldValueResolver resolver) {
        super("mimetype", resolver);
    }

    public FitsIdentityMetadataMimetype(FitsResult fitsResult) {
        super("mimetype", fitsResult);
    }

    public FitsIdentityMetadataMimetype(FitsResult fitsResult, FieldValueResolver resolver) {
        super("mimetype", fitsResult, resolver);
    }

    @Override
    public Optional<String> getValue() {
        if(fitsResult.getFitsResult().getIdentification().getIdentity().size() > 0){
            String rtn = fitsResult.getFitsResult().getIdentification().getIdentity().get(0).getMimetype();

            if(rtn != null && !rtn.isEmpty()){
                return Optional.of(rtn);
            }
        }

        return executeNextResolver();
    }
}
