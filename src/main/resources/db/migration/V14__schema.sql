-- ingest_api.`role` definition
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ingest_api.user_role definition
CREATE TABLE `user_role` (
  `role_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE ingest_api.user_role ADD CONSTRAINT user_role_FK FOREIGN KEY (user_id) REFERENCES ingest_api.`user`(id);
ALTER TABLE ingest_api.user_role ADD CONSTRAINT user_role_FK_1 FOREIGN KEY (role_id) REFERENCES ingest_api.`role`(id);
