package fi.sls.ingest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import fi.sls.ingest.controller.request.AuthenticationRequest;
import fi.sls.ingest.controller.response.JwtAuthenticationResponse;
import fi.sls.ingest.repository.UserRepository;
import fi.sls.ingest.security.jwt.JwtTokenProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

import static net.logstash.logback.argument.StructuredArguments.kv;

@BasePathAwareController
@Slf4j
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    UserRepository userRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/auth/signin")
    public ResponseEntity signin(@Valid @RequestBody AuthenticationRequest data) throws JsonProcessingException {
        log.debug("Received signin request for {}", kv("username", data.getUsername()));
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        data.getNoSuffixUsername(),
                        data.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }
}
