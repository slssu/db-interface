package fi.sls.ingest.controller;

import fi.sls.ingest.actuator.metrics.ProcessingMetricsService;
import fi.sls.ingest.config.SecurityRoleConfig;
import fi.sls.ingest.exception.ProcessingItemNotFoundException;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.manager.*;
import fi.sls.ingest.proxy.slsfileapi.FileAPIResponseBody;
import fi.sls.ingest.representation.User;
import fi.sls.ingest.representation.projection.IngestProcessingItemMinimal;
import fi.sls.ingest.rest.BatchDeleteWithStatusRequest;
import fi.sls.ingest.rest.BatchStatusChangeRequest;
import fi.sls.ingest.rest.IngestRequestBody;
import fi.sls.ingest.rest.ListTokensRequestBody;
import fi.sls.ingest.security.AuthenticationFacade;
import fi.sls.ingest.security.jwt.UserPrincipal;
import fi.sls.ingest.ws.MetricsMessage;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * Provides endpoint methods for handling various aspects of the Ingest process.
 */
@BasePathAwareController
@RestController
@Slf4j
public class IngestController {

    /**
     * Reference to manager class that processes the requests coming through this controller.
     */
    IngestManager ingestManager;

    /**
     * Reference to service that provides statistics about the current processing status
     */
    ProcessingMetricsService processingMetricsService;

    /**
     * Reference to a projectionFactory instance for modifying the response structure in some of the methods.
     */
    ProjectionFactory projectionFactory;

    /**
     * Reference to source of currently authenticated user
     */
    AuthenticationFacade authenticationFacade;

    /**
     * Reference to user roles available within system
     */
    SecurityRoleConfig securityRoleConfig;

    @Autowired
    public IngestController(
            IngestManager ingestManager,
            ProcessingMetricsService processingMetricsService,
            ProjectionFactory projectionFactory,
            AuthenticationFacade authenticationFacade,
            SecurityRoleConfig securityRoleConfig
    ){
        this.ingestManager = ingestManager;
        this.projectionFactory = projectionFactory;
        this.authenticationFacade = authenticationFacade;
        this.processingMetricsService = processingMetricsService;
        this.securityRoleConfig = securityRoleConfig;
    }

    /**
     * @param projection Either the string "minimal" or "default" is supported.
     * @return A list of all IngestProcessingItems currently stored in the queue for processing
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.GET, value = "/ingest/items")
    public Page<?> getItems(HttpServletRequest request,
                            @RequestParam(value = "projection", required = false, defaultValue = "default") String projection,
                            @RequestParam(value = "filter", required = false, defaultValue = "") String filterBy,
                            @RequestParam(value = "user", required = false, defaultValue = "") String userId,
                            @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                            @RequestParam(value = "pageSize", required = false, defaultValue = "100") Integer pageSize
    ) throws IOException{
        log.debug("Received getItems request: {} {} {} {} {}",
                kv("projection", projection),
                kv("filterBy", filterBy),
                kv("user", userId),
                kv("page", page),
                kv("pageSize", pageSize)
        );

        Pageable pageable = PageRequest.of(page, pageSize);

        List<Long> userIdList = getUserIdFromString(request.isUserInRole(securityRoleConfig.getAdmin()), userId);

        if (projection.equals("minimal")) {
            return ingestManager.getProcessingItems(filterBy, userIdList, pageable)
                    .map(item -> projectionFactory.createProjection(IngestProcessingItemMinimal.class, item));
        } else {
            return ingestManager.getProcessingItems(filterBy, userIdList, pageable);
        }
    }

    /**
     * Helper for extracting a list of comma separated id strings into a list of Longs
     *
     * @param userIdList
     * @return
     */
    protected List<Long> getUserIdFromString(boolean isAdmin, String userIdList){

        List<Long> filterByUserId = new ArrayList<>();
        // if nothing provided, use authenticated user
        if(userIdList.isEmpty()){
            Authentication currentAuth = authenticationFacade.getAuthentication();
            User u = ((UserPrincipal) currentAuth.getPrincipal()).getUser();

            // if user is admin, return empty list, which equals to matching all users
            if(isAdmin) {
                log.debug("User has role {}, fetching for all users", securityRoleConfig.getAdmin());

                return filterByUserId;
            }

            userIdList = u.getId().toString();
        }

        for(String uid : userIdList.split(",")){
            try{
                filterByUserId.add(Long.parseLong(uid));
            } catch (NumberFormatException e){
                // noop: we don't care, only use valid inputs
            }
        }
        return filterByUserId;
    }

    /**
     * @param token
     * @return Details about a specific IngestProcessingItem identified by the given token
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.GET, value = "/ingest/items/{token}")
    public IngestProcessingItem getProcessingItem(@PathVariable(name = "token") String token) throws IOException{
        MDC.put(LogMessageKey.INGEST_ITEM_TOKEN, token);
        try{
            log.debug("Received getProcessingItem request: {}", kv(LogMessageKey.INGEST_ITEM_TOKEN, token));
            Optional<IngestProcessingItem> item = ingestManager.getProcessingItem(token);
            if (item.isPresent()) {
                return item.get();
            } else {
                throw new ProcessingItemNotFoundException(String.format("Item with token '%s' not found", token));
            }
        } finally {
            MDC.remove(LogMessageKey.INGEST_ITEM_TOKEN);
        }
    }

    /**
     * Remove a single processing item from the queue
     * @param token
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/ingest/items/{token}")
    public void deleteProcessingItem(@PathVariable(name = "token") String token) throws IOException{
        MDC.put(LogMessageKey.INGEST_ITEM_TOKEN, token);
        try {
            log.debug("Received deleteProcessingItem request: {}",
                    kv(LogMessageKey.INGEST_ITEM_TOKEN, token)
            );
            ingestManager.deleteProcessingItem(token);
        } finally {
            MDC.remove(LogMessageKey.INGEST_ITEM_TOKEN);
        }
    }

    /**
     * Remove all processing items from the queue
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/ingest/clearQueue")
    public void clearQueue() throws IOException{
        log.debug("Received clearQueue request");
        ingestManager.clearQueue();
    }

    /**
     * Delete a batch of processing items completely
     *
     * @param tokensRequestBody
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.PATCH, value = "/ingest/items-delete")
    public void deleteProcessingItems(@Valid @RequestBody ListTokensRequestBody tokensRequestBody) throws IOException{
        log.debug("Received deleteProcessingItems request",
                kv(LogMessageKey.REQUEST_BODY, tokensRequestBody)
        );
        ingestManager.deleteProcessingItems(tokensRequestBody.getTokens());
    }

    /**
     * Archive a single processing item in the queue
     * @param token
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.PATCH, value = "/ingest/items/{token}/archive")
    public void archiveProcessingItem(@PathVariable(name = "token") String token) throws IOException{
        MDC.put(LogMessageKey.INGEST_ITEM_TOKEN, token);
        try {
            log.debug("Received archiveProcessingItem request: {}",
                    kv(LogMessageKey.INGEST_ITEM_TOKEN, token)
            );
            ingestManager.changeProcessingItemStatus(token, ProcessingStatus.ARCHIVED);
        } finally {
            MDC.remove(LogMessageKey.INGEST_ITEM_TOKEN);
        }
    }

    /**
     * Archive a batch of processing items
     *
     * @param tokensRequestBody
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.PATCH, value = "/ingest/items-archive")
    public void archiveProcessingItems(@Valid @RequestBody ListTokensRequestBody tokensRequestBody) throws IOException{
        log.debug("Received archiveProcessingItems request",
                kv(LogMessageKey.REQUEST_BODY, tokensRequestBody)
        );
        ingestManager.changeProcessingItemStatus(tokensRequestBody.getTokens(), ProcessingStatus.ARCHIVED);
    }

    /**
     * Change status for all items with a given ProcessingStatus belonging to one or more users (TODO: admin only)
     *
     * @param batchStatusChangeRequest
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.PATCH, value = "/ingest/items-status")
    public void batchChangeProcessingStatus(
            HttpServletRequest request,
            @Valid @RequestBody BatchStatusChangeRequest batchStatusChangeRequest
    ) throws IOException{
        log.debug("Received batchChangeProcessingStatus request: {} {} {}",
                kv("user_id_list", batchStatusChangeRequest.getUserIds()),
                kv("from_processing_status", batchStatusChangeRequest.getFromStatus()),
                kv("to_processing_status", batchStatusChangeRequest.getToStatus())
        );

        String userId = batchStatusChangeRequest.getUserIds();

        ingestManager.changeProcessingItemStatus(
                batchStatusChangeRequest.getFromStatus(),
                batchStatusChangeRequest.getToStatus(),
                getUserIdFromString(request.isUserInRole(securityRoleConfig.getAdmin()), userId)
        );
    }

    /**
     * Delete items having the given status and belonging to given user(s)
     *
     * @param batchDeleteWithStatusRequest
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/ingest/items-status")
    public void batchDeleteWithProcessingStatus(
            HttpServletRequest request,
            @Valid @RequestBody BatchDeleteWithStatusRequest batchDeleteWithStatusRequest){
        log.debug("Received batchDeleteWithProcessingStatus request: {} {}",
                kv("user_id_list", batchDeleteWithStatusRequest.getUserIds()),
                kv("from_processing_status", batchDeleteWithStatusRequest.getStatus())
        );

        String userId = batchDeleteWithStatusRequest.getUserIds();

        ingestManager.deleteItemsWithProcessingStatus(
                batchDeleteWithStatusRequest.getStatus(),
                getUserIdFromString(request.isUserInRole(securityRoleConfig.getAdmin()), userId)
        );

    }

    /**
     * Fetch the nr of items that would change status if a batch status change request was made
     */
    @RequestMapping(method = RequestMethod.GET, value = "/ingest/items-status")
    public long getNumStatusChangeableProcessingItems(
            HttpServletRequest request,
            @RequestParam(value = "fromStatus") String fromStatus,
            @RequestParam(value = "toStatus") String toStatus,
            @RequestParam(
                    value = "userIds",
                    defaultValue = "",
                    required = false
            ) String userIds
    ){
        log.debug("Received getNumStatusChangeableProcessingItems request: {} {}",
                kv("user_id_list", userIds),
                kv("from_processing_status", fromStatus),
                kv("to_processing_status", toStatus)
        );

        return ingestManager.getNumStatusChangeableProcessingItems(
                ProcessingStatus.fromStatusCodeString(fromStatus),
                ProcessingStatus.fromStatusCodeString(toStatus),
                getUserIdFromString(request.isUserInRole(securityRoleConfig.getAdmin()), userIds)
        );
    }

    /**
     * Add the given set of items to the staging area
     *
     * @param ingest
     * @return
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.POST, value = "/ingest/staging")
    public Page<?> ingestStaging(
            HttpServletRequest request,
            @Valid @RequestBody IngestRequestBody ingest,
            @RequestParam(value = "projection", required = false, defaultValue = "default") String projection,
            @RequestParam(value = "filter", required = false, defaultValue = "") String filterBy,
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(value = "pageSize", required = false, defaultValue = "100") Integer pageSize
            ) throws IOException{
        log.debug("Received staging ingest request", kv(LogMessageKey.REQUEST_BODY, ingest));
        // convert request body items to something our manager can handle
        List<IngestItem> items = ingest.getItems().stream().collect(Collectors.toList());
        IngestDigitizationProfile profile = ingest.getDigitizationProfile();

        ingestManager.processItems(items, profile);

        Authentication currentAuth = authenticationFacade.getAuthentication();
        User u = ((UserPrincipal) currentAuth.getPrincipal()).getUser();
        return getItems(request, projection, filterBy, u.getId().toString(), page, pageSize);
    }

    /**
     * Start the ingest process for all items with the state ProcessingStatus.STAGING
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/ingest/start")
    public void startIngest(
            HttpServletRequest request,
            @RequestParam(value = "user", required = false, defaultValue = "") String userId){
        log.debug("Received start ingest request: {}", kv("user", userId));

        // only allow user to start their own ingests, even if admin.
        ingestManager.preProcessItems(getUserIdFromString(false, userId));
    }

    /**
     * Continue processing the IngestProcessingItem identified by the token in the response body
     *
     * This method should called by the File API to continue ingesting of an item after files have been copied etc.
     *
     * @param authToken
     * @param processingResult
     */
    @RequestMapping(method = RequestMethod.POST, value = "/ingest/continue")
    public void masterFilesCallback(
            @PathParam("authToken") String authToken,
            @Valid @RequestBody FileAPIResponseBody processingResult)
    {

        log.debug("Received masterFilesCallback request: ",
                kv(LogMessageKey.REQUEST_BODY, processingResult)
        );
        // Check that auth is valid
        if (processingResult.getStatus().equalsIgnoreCase("ok")) {
            // Call manager to continue processing of item
            // This should be async and respond with an accepted httpd status
            ingestManager.postProcessItem(processingResult.getToken(), processingResult);
        } else if (processingResult.getStatus().equalsIgnoreCase("error")) {
            // mark item as having been skipped due to error
            ingestManager.rejectProcessingItem(processingResult.getToken(), processingResult.getReason());
        } else {
            log.error("NOT YET IMPLEMENTED");
            // TODO: Call manager to end processing of original item and queue new item in case file processing resulted in
            // a new file that needs handling
        }
    }

    /**
     * Continue processing a list of IngestProcessingItems identified by their tokens
     *
     * @param tokensRequestBody
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.POST, value = "/ingest/items-continue")
    public void continueProcessingItems(@Valid @RequestBody ListTokensRequestBody tokensRequestBody) throws IOException{
        log.debug("Received continueProcessingItems request",
                kv(LogMessageKey.REQUEST_BODY, tokensRequestBody));
        ingestManager.postProcessItems(tokensRequestBody.getTokens());
    }

    /**
     * Retry processing of an item identified by the items token
     * @param item
     */
    @RequestMapping(method = RequestMethod.POST, value = "/ingest/retry")
    public void retryItem(@Valid @RequestBody IngestProcessingItem item){
        MDC.put("ingest_item_token", item.getToken());
        log.debug("Received retryItem request",
                kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
        );
        ingestManager.retryProcessItem(item.getToken());
    }

    /**
     * Retry a list of files identified by their tokens
     * @param tokensRequestBody
     */
    @RequestMapping(method = RequestMethod.POST, value = "/ingest/items-retry")
    public void retryItems(@Valid @RequestBody ListTokensRequestBody tokensRequestBody){
        log.debug("Received retryItems request");
        ingestManager.retryProcessItems(tokensRequestBody.getTokens());
    }

    /**
     * Retry generating access files in the File API
     *
     * @param item
     */
    @RequestMapping(method = RequestMethod.POST, value = "/ingest/accessfiles/retry")
    public void retryAccessFiles(@Valid @RequestBody IngestProcessingItem item){
        MDC.put("ingest_item_token", item.getToken());
        log.debug("Received retryAccessFiles request",
                kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
        );
        ingestManager.retryAccessFiles(item.getToken());
    }

    /**
     * Fetch current stats for the processing queue
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/ingest/stats")
    public MetricsMessage loadProcessingMetrics(){
        return processingMetricsService.loadMetrics();
    }
}
