package fi.sls.ingest.fits.model.fieldresolver.tool;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.*;
import java.util.Iterator;
import java.util.Optional;

public class MediaInfoMetadata extends AbstractFieldValueResolver {

    public final static String TRACK_TYPE_AUDIO = "Audio";
    public final static String TRACK_TYPE_GENERAL = "General";
    public final static String TRACK_TYPE_VIDEO = "Video";

    public final static String FIELD_CHROMA_SUBSAMPLING = "Chroma_subsampling";
    public final static String FIELD_BIT_DEPTH = "Bit_depth";
    public final static String FIELD_PIXEL_ASPECT_RATIO = "Pixel_aspect_ratio";
    public final static String FIELD_STANDARD = "Standard";
    public final static String FIELD_WIDTH = "Width";
    public final static String FIELD_HEIGHT = "Height";
    public final static String FIELD_FRAME_RATE = "Frame_rate";
    public final static String FIELD_FRAME_RATE_MODE = "Frame_rate_mode";
    public final static String FIELD_BIT_RATE_MODE = "Bit_rate_mode";
    public final static String FIELD_BIT_RATE = "Bit_rate";
    public final static String FIELD_CODEC_ID = "Codec_ID";
    public final static String FIELD_CODEC_FAMILY = "Codec_Family";

    public final static String FIELD_COUNT_OF_STREAMS = "Count_of_stream_of_this_kind";

    public final static String FIELD_SAMPLING_RATE = "Sampling_rate";
    public final static String FIELD_CHANNELS = "Channel_s_";
    public final static String FIELD_FORMAT = "Format";

    public final static String FIELD_DURATION = "Duration";





    protected String subSectionType;

    public MediaInfoMetadata(String fieldName, String subSectionType, FieldValueResolver resolver) {
        super(fieldName, resolver);
        this.subSectionType = subSectionType;
    }

    public MediaInfoMetadata(String fieldName, String subSectionType, FitsResult fitsResult) {
        super(fieldName, fitsResult);
        this.subSectionType = subSectionType;
    }

    public MediaInfoMetadata(String fieldName, String subSectionType, FitsResult fitsResult, FieldValueResolver resolver) {
        super(fieldName, fitsResult, resolver);
        this.subSectionType = subSectionType;
    }

    @Override
    public Optional<String> getValue() {
        XPath xPath = XPathFactory.newInstance().newXPath();

        try {

            xPath.setNamespaceContext(new MediaInfoMetadata.MediaInfoNamespaceContext(fitsResult.getXmlDocument()));

            String expStr = "/fits:fits/fits:toolOutput/fits:tool[@name = 'MediaInfo']/File/track[@type='" + subSectionType + "']/" + fieldName;

            XPathExpression expression = xPath.compile(expStr);

            NodeList nodes = (NodeList) expression.evaluate(fitsResult.getXmlDocument(), XPathConstants.NODESET);
            if(nodes.getLength() > 0){
                Node n = nodes.item(0);
                String val = n.getTextContent();

                if(val != null && !val.isEmpty()){
                    return Optional.of(processValue(val));
                }
            }

            return executeNextResolver();

        } catch (XPathExpressionException e) {
            return executeNextResolver();
        }
    }

    /**
     * Helper for additional processing of a result value before it is returned
     *
     * @param val
     * @return
     */
    protected String processValue(String val){
        switch(fieldName){
            case FIELD_PIXEL_ASPECT_RATIO:
                return val != null ? val.replace(".", ",") : null;
            default:
                return val;
        }
    }

    class MediaInfoNamespaceContext implements NamespaceContext {

        private Document sourceDocument;

        public MediaInfoNamespaceContext(Document document){
            sourceDocument = document;
        }

        @Override
        public String getNamespaceURI(String prefix) {
            if (prefix.equals(XMLConstants.DEFAULT_NS_PREFIX)) {
                return sourceDocument.lookupNamespaceURI(null);
            } else if(prefix.equals("fits")) {
                return "http://hul.harvard.edu/ois/xml/ns/fits/fits_output";
            } else {
                return sourceDocument.lookupNamespaceURI(prefix);
            }
        }

        @Override
        public String getPrefix(String namespaceURI) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Iterator<String> getPrefixes(String namespaceURI) {
            throw new UnsupportedOperationException();
        }
    }
}
