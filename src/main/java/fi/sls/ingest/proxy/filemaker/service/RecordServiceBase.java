package fi.sls.ingest.proxy.filemaker.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.config.FileMakerConfig;
import fi.sls.ingest.proxy.filemaker.AuthToken;
import fi.sls.ingest.proxy.filemaker.AuthTokenStore;
import fi.sls.ingest.proxy.filemaker.FileMakerInternalResponseCode;
import fi.sls.ingest.proxy.filemaker.exception.FileMakerAPIException;
import fi.sls.ingest.proxy.filemaker.exception.FileMakerDataMismatchException;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.*;
import fi.sls.ingest.proxy.filemaker.response.record.DerivateObject;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Optional;

/**
 * Base class for use by services that handle FileMaker records.
 */
public abstract class RecordServiceBase extends AuthenticationService {

    protected ObjectMapper objectMapper;

    public RecordServiceBase(AuthTokenStore authTokenStore, RestTemplate restTemplate, FileMakerConfig fileMakerConfig, ObjectMapper objectMapper) {
        super(authTokenStore, restTemplate, fileMakerConfig);

        this.objectMapper = objectMapper;
    }

    /**
     * Find a single item from a FileMaker layout based on the createFindQueryRequest return value of the given responseEntity.
     *
     * @param responseEntity
     * @return
     * @throws IOException
     */
    public Optional<? extends FileMakerResponseEntity> findRecordByIdentifier(FileMakerResponseEntity responseEntity) throws IOException {
        JavaType type = objectMapper.getTypeFactory().constructParametricType(FileMakerArrayDataResponse.class, responseEntity.getClass());
        return this.typeCastResponse(type, this.findByQuery(responseEntity.createFindQueryRequest(), responseEntity.getLayoutName()));
    }

    public Optional<? extends FileMakerResponseEntity> findRecordByQuery(FileMakerResponseEntity responseEntity, FindQueryRequest request) throws IOException {
        JavaType type = objectMapper.getTypeFactory().constructParametricType(FileMakerArrayDataResponse.class, responseEntity.getClass());
        return this.typeCastResponse(type, this.findByQuery(request, responseEntity.getLayoutName()));
    }

    /**
     * Find a list of items from a FileMaker layout based on the createFindQueryRequest(true) return value of the given responseEntity.
     *
     * @param responseEntity
     * @return
     * @throws IOException
     */
    public FileMakerArrayDataResponse<?> findRecords(FileMakerResponseEntity responseEntity, Long pageSize) throws IOException {
        JavaType type = objectMapper.getTypeFactory().constructParametricType(FileMakerArrayDataResponse.class, responseEntity.getClass());
        return this.getResponseWrapper(type, this.findByQuery(responseEntity.createFindQueryRequest(true, pageSize), responseEntity.getLayoutName()));
    }

    /**
     * Find a list of items from a FileMaker layout based on the createFindQueryRequest(true) return value of the given responseEntity.
     *
     * @param responseEntity
     * @return
     * @throws IOException
     */
    public FileMakerArrayDataResponse<?> findRecords(FileMakerResponseEntity responseEntity, Long pageSize, Long pageNr) throws IOException {
        JavaType type = objectMapper.getTypeFactory().constructParametricType(FileMakerArrayDataResponse.class, responseEntity.getClass());
        return this.getResponseWrapper(type, this.findByQuery(responseEntity.createFindQueryRequest(true, pageSize, pageNr), responseEntity.getLayoutName()));
    }

    public FileMakerResponseEntity getObjectRecord(FileMakerResponseEntity responseEntity, long recordId) throws IOException {
        JavaType type = objectMapper.getTypeFactory().constructParametricType(FileMakerArrayDataResponse.class, responseEntity.getClass());
        return this.getObjectRecord(type, recordId, responseEntity.getLayoutName());
    }

    /**
     * Fetches a record from FileMaker based on it's internal recordId
     *
     * @param recordId
     * @return
     * @throws IOException
     */
    public FileMakerResponseEntity getObjectRecord(JavaType type, long recordId, String layoutName) throws IOException {
        Optional<String> record = getRecord(recordId, layoutName);
        if(record.isPresent()){
            // FIXME: this feels a bit weird, we assume the optional exists after typeCastResponse
            return typeCastResponse(type, record).orElse(null);
        } else {
            return null;
        }
    }

    /**
     * Creates a new record in FileMaker based on the given record entity
     *
     * @param record
     * @return
     * @throws IOException
     */
    public FileMakerResponseEntity createObjectRecord(FileMakerResponseEntity record) throws IOException, FileMakerDataMismatchException {

        Optional<String> response = createRecord(record.createRecordRequest(), record.getLayoutName());

        if(response.isPresent()){
            FileMakerCreateRecordResponse rtn = this.objectMapper.readValue(response.get(), FileMakerCreateRecordResponse.class);

            FileMakerInternalResponseCode code = FileMakerInternalResponseCode.fromValue(rtn.getFirstResponseCode().get().getCode());
            if (code == FileMakerInternalResponseCode.OK) {
                // re-fetch to make sure we're up to date locally
                // this is perhaps not the correct pattern, but let's see...
                JavaType type = objectMapper.getTypeFactory().constructParametricType(FileMakerArrayDataResponse.class, record.getClass());
                return getObjectRecord(type, rtn.getResponse().getRecordId(), record.getLayoutName());
            }
        }

        throw new FileMakerDataMismatchException("createRecord response was empty, did not expect that");
    }

    /**
     * Updates the fields of the given record in FileMaker.
     *
     * @param record
     * @return
     * @throws IOException
     */
    public FileMakerResponseEntity updateObjectRecord(FileMakerResponseEntity record) throws IOException {

        JavaType type = objectMapper.getTypeFactory().constructParametricType(FileMakerArrayDataResponse.class, record.getClass());
        FileMakerArrayDataResponse rtn = getResponseWrapper(type, updateRecord(record.createUpdateRequest(), record.getRecordId(), record.getLayoutName()));

        if(rtn != null){
            FileMakerInternalResponseCode code = FileMakerInternalResponseCode.fromValue(rtn.getFirstResponseCode().get().getCode());
            if(code == FileMakerInternalResponseCode.OK){
                // re-fetch to make sure we're up to date locally
                // this is perhaps not the correct pattern, but let's see...
                return getObjectRecord(type, record.getRecordId(), record.getLayoutName());
            }
        }

        throw new FileMakerDataMismatchException("updateRecord response was empty, did not expect that");
    }


    public boolean deleteObjectRecord(FileMakerResponseEntity objectRecord) throws IOException {
        Optional<String> record = deleteRecord(objectRecord.getRecordId(), objectRecord.getLayoutName());

        if(record.isPresent()){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Makes an authentication request and adds the given token to the header object
     *
     * Sets other default headers for requests against the FileMaker API
     *
     * @return
     */
    protected HttpHeaders createAPIHeaders(){
        AuthToken authToken = this.authenticate();

        return new HttpHeaders() {{
            set( "Authorization", fileMakerConfig.getAuthHeader() + " "+ authToken.getToken() );
            setContentType(MediaType.APPLICATION_JSON);
        }};
    }

    /**
     * Performs a _find query against the FileMaker API using the parameters defined in queryRequest
     * and the layout defined in layoutName.
     *
     * @param queryRequest The search query parameters to use for the _find request.
     * @param layoutName The name of the FileMaker table or layout to search in.
     * @return
     * @throws IOException If a server response could not be parsed into a Java object from JSON.
     * @throws FileMakerAPIException If the server responds with a HttpServerErrorException and it was not a "record not found" exception.
     */
    protected Optional<String> findByQuery(FindQueryRequest queryRequest, String layoutName) throws IOException {

        HttpEntity<FindQueryRequest> request = new HttpEntity<>(queryRequest, createAPIHeaders());

        try {
            ResponseEntity<String> response = restTemplate.exchange(
                    getEndpointUrl("_find", layoutName),
                    HttpMethod.POST, request, String.class);

            if(response.getStatusCode() == HttpStatus.OK){
                return Optional.of(response.getBody());
            } else {
                return Optional.empty();
            }

        } catch(HttpServerErrorException e){
            return this.processFileMakerError(e);
        } catch(HttpClientErrorException e){
            if(e.getStatusCode() == HttpStatus.UNAUTHORIZED && numAuthRetries++ < maxAuthRetries){
                resetAuthToken();
                return findByQuery(queryRequest, layoutName);
            } else {
                // we don't handle this after all so re-throw
                throw e;
            }
        }
    }

    /**
     * Fetch a single record using HTTP.GET from a FileMaker layout or table based on the FileMaker record id.
     *
     * @param recordId The FileMaker internal recordId who's related data should be fetched.
     * @param layoutName The name of the FileMaker layout or table to target.
     * @return
     * @throws IOException
     */
    protected Optional<String> getRecord(Long recordId, String layoutName) throws IOException {

        HttpEntity<String> request = new HttpEntity<>("", createAPIHeaders());

        try {
            ResponseEntity<String> response = restTemplate.exchange(
                    getEndpointUrl("records", layoutName, recordId),
                    HttpMethod.GET, request, String.class);

            if(response.getStatusCode() == HttpStatus.OK){
                return Optional.of(response.getBody());
            } else {
                return Optional.empty();
            }
        } catch(HttpServerErrorException e){
            // try to extract a FileMaker internal error from the exception.
            return this.processFileMakerError(e);
        } catch(HttpClientErrorException e){

            if(e.getStatusCode() == HttpStatus.UNAUTHORIZED && numAuthRetries++ < maxAuthRetries){
                resetAuthToken();
                return getRecord(recordId, layoutName);
            } else {
                // we don't handle this after all so re-throw
                throw e;
            }
        }
    }

    protected Optional<String> createRecord(CreateRequest requestBody, String layoutName) throws IOException {
        HttpEntity<CreateRequest> request = new HttpEntity<>(requestBody, this.createAPIHeaders());

        try {
            ResponseEntity<String> response = restTemplate.exchange(
                    getEndpointUrl("records", layoutName),
                    HttpMethod.POST, request, String.class);

            if(response.getStatusCode() == HttpStatus.OK){
                return Optional.of(response.getBody());
            } else {
                return Optional.empty();
            }
        } catch(HttpServerErrorException e){
            // try to extract a FileMaker internal error from the exception.
            return this.processFileMakerError(e);
        } catch(HttpClientErrorException e){

            if(e.getStatusCode() == HttpStatus.UNAUTHORIZED && numAuthRetries++ < maxAuthRetries){
                resetAuthToken();
                return createRecord(requestBody, layoutName);
            } else {
                // we don't handle this after all so re-throw
                throw e;
            }
        }
    }

    protected Optional<String> updateRecord(UpdateRequest requestBody, Long recordId, String layoutName) throws IOException {
        HttpEntity<UpdateRequest> request = new HttpEntity<>(requestBody, this.createAPIHeaders());

        try {
            ResponseEntity<String> response = restTemplate.exchange(
                    getEndpointUrl("records", layoutName, recordId),
                    HttpMethod.PATCH, request, String.class);

            if(response.getStatusCode() == HttpStatus.OK){
                return Optional.of(response.getBody());
            } else {
                return Optional.empty();
            }
        } catch(HttpServerErrorException e){
            // try to extract a FileMaker internal error from the exception.
            return this.processFileMakerError(e);
        } catch(HttpClientErrorException e){

            if(e.getStatusCode() == HttpStatus.UNAUTHORIZED && numAuthRetries++ < maxAuthRetries){
                resetAuthToken();
                return updateRecord(requestBody, recordId, layoutName);
            } else {
                // we don't handle this after all so re-throw
                throw e;
            }
        }
    }

    /**
     * Delete a single record using HTTP.DEL from a FileMaker layout or table based on the FileMaker record id.
     *
     * @param recordId The FileMaker internal recordId who's related data should be deleted.
     * @param layoutName The name of the FileMaker layout or table to target.
     * @return
     * @throws IOException
     */
    protected Optional<String> deleteRecord(Long recordId, String layoutName) throws IOException {

        HttpEntity<String> request = new HttpEntity<>("", createAPIHeaders());

        try {
            ResponseEntity<String> response = restTemplate.exchange(
                    getEndpointUrl("records", layoutName, recordId),
                    HttpMethod.DELETE, request, String.class);

            if(response.getStatusCode() == HttpStatus.OK){
                return Optional.of(response.getBody());
            } else {
                return Optional.empty();
            }
        } catch(HttpServerErrorException e){
            // try to extract a FileMaker internal error from the exception.
            return this.processFileMakerError(e);
        } catch(HttpClientErrorException e){

            if(e.getStatusCode() == HttpStatus.UNAUTHORIZED && numAuthRetries++ < maxAuthRetries){
                resetAuthToken();
                return deleteRecord(recordId, layoutName);
            } else {
                // we don't handle this after all so re-throw
                throw e;
            }
        }
    }

    /**
     * Attempts to handle a server exception to see if it contains a FileMaker internal error code that
     * we want to handle gracefully.
     *
     * @param ex The exception returned from the rest call
     * @return
     * @throws IOException If the exception response body could not be converted from JSON to an FileMakerErrorResponse.
     * @throws FileMakerAPIException If the original exception could not be processed by this method.
     */
    protected Optional<String> processFileMakerError(HttpServerErrorException ex) throws IOException, FileMakerAPIException {
        // try to parse response as json in case it contains an error code we can handle [sic]
        FileMakerErrorResponse err = this.objectMapper.readValue(ex.getResponseBodyAsString(), new TypeReference<FileMakerErrorResponse>(){});

        if(err.getFirstResponseCode().isPresent()){
            try {
                FileMakerInternalResponseCode code = FileMakerInternalResponseCode.fromValue(err.getFirstResponseCode().get().getCode());
                switch(code){
                    case RECORD_IS_MISSING:
                    case NO_RECORDS_MATCH_REQUEST:
                        // filemaker returns internal 101 or 401 if records not found in a get or find request!
                        return Optional.empty();
                    default:
                        throw new FileMakerAPIException(ex, code);
                }
            } catch (IllegalArgumentException e){
                throw new FileMakerAPIException(e);
            }
        } else {
            throw new FileMakerAPIException(ex);
        }
    }

    /**
     * Helper that converts the response from a api request to the java type
     * we want to return.
     *
     * The calling method needs to type cast the return value from this method to what it expects.
     *
     * @param type
     * @param response
     * @return
     * @throws IOException
     */
    protected Optional<? extends FileMakerResponseEntity> typeCastResponse(JavaType type, Optional<String> response) throws IOException {

        FileMakerArrayDataResponse<?> wrapper = getResponseWrapper(type, response);

        // default to empty response in case we don't find a matching record
        Optional<? extends FileMakerResponseEntity> rtn = Optional.empty();

        //if(response.isPresent()){
        if(wrapper != null){
            // FileMakerArrayDataResponse<?> wrapper = this.objectMapper.readValue(response.get(), type);
            if(wrapper.getResponse().getData().size() > 0){
                rtn = Optional.of((FileMakerResponseEntity) wrapper.getResponse().getData().get(0));
            }
        }

        return rtn;
    }

    /**
     * Helper for returning just an FileMakerArrayDataResponse of the given JavaType and response
     *
     * @param type
     * @param response
     * @return
     * @throws IOException
     */
    protected FileMakerArrayDataResponse<?> getResponseWrapper(JavaType type, Optional<String> response) throws IOException {
        if (response.isPresent()) {
            return this.objectMapper.readValue(response.get(), type);
        } else {
            return null;
        }
    }
}
