package fi.sls.ingest.manager.step;

import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntity;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntityFieldData;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntityPartFieldData;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class LinkThumbnailStepTest {

    LinkThumbnailStep testable;
    RecordService service;

    @BeforeEach
    void setUp() {
        service = mock(RecordService.class);
        testable = new LinkThumbnailStep(service);
    }

    @Test
    void executeStep_updatesRecordWithExpectedValues() throws IOException {
        IntellectualEntityFieldData iefd = mock(IntellectualEntityFieldData.class);

        IntellectualEntity ie = mock(IntellectualEntity.class);
        when(ie.getFieldData()).thenReturn(iefd);

        IntellectualEntityPartFieldData iep = mock(IntellectualEntityPartFieldData.class);
        when(iep.getDatabaseThumbnail()).thenReturn("databasethumb");
        when(iep.getDatabaseImage()).thenReturn("databaseimg");
        when(iep.getOrderNr()).thenReturn("12");

        when(service.updateObjectRecord(ie)).thenReturn(ie);

        IntellectualEntity rtn = testable.executeStep(ie, iep);

        assertNotNull(rtn);

        verify(iefd).setThumbnail(iep.getDatabaseThumbnail());
        verify(iefd).setDatabasbild(iep.getDatabaseImage());
        verify(iefd).setShowPageNr(iep.getOrderNr());
    }

    @Test
    void executeStep_resetsValueNotResendable() throws IOException {
        IntellectualEntityFieldData iefd = mock(IntellectualEntityFieldData.class);
        when(iefd.getEntityNumber()).thenReturn(0L);

        IntellectualEntity ie = mock(IntellectualEntity.class);
        when(ie.getFieldData()).thenReturn(iefd);

        IntellectualEntityPartFieldData iep = mock(IntellectualEntityPartFieldData.class);

        when(service.updateObjectRecord(ie)).thenReturn(ie);

        IntellectualEntity rtn = testable.executeStep(ie, iep);
        assertNotNull(rtn);
        verify(iefd, times(0)).setEntityNumber(null);

        when(iefd.getEntityNumber()).thenReturn(12L);
        rtn = testable.executeStep(ie, iep);
        assertNotNull(rtn);
        verify(iefd).setEntityNumber(null);
    }
}