package fi.sls.ingest.representation.converter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.manager.IngestDigitizationProfile;
import fi.sls.ingest.rest.IngestFile;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.AttributeConverter;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class IngestFileConverterTest {

    AttributeConverter<IngestFile, byte[]> testable;
    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = mock(ObjectMapper.class);
        testable = new IngestFileConverter(objectMapper);
    }

    @Test
    void convertToDatabaseColumn_returnsNullForNullInput() {
        byte[] rtn = testable.convertToDatabaseColumn(null);
        assertNull(rtn);
    }

    @Test
    void convertToDatabaseColumn_returnsNullForJsonProcessingException() throws JsonProcessingException {
        IngestFile mock = mock(IngestFile.class);

        when(objectMapper.writeValueAsString(mock)).thenThrow(JsonProcessingException.class);
        byte[] rtn = testable.convertToDatabaseColumn(mock);
        assertNull(rtn);
    }

    @Test
    void convertToDatabaseColumn_returnsJsonBytesForValidInput() throws JsonProcessingException {
        IngestFile mock = mock(IngestFile.class);

        when(objectMapper.writeValueAsString(mock)).thenReturn("hello");
        byte[] rtn = testable.convertToDatabaseColumn(mock);
        assertEquals("hello", new String(rtn));
    }

    @Test
    void convertToEntityAttribute_returnsNullForNullInput() {
        IngestFile data = testable.convertToEntityAttribute(null);
        assertNull(data);
    }

    @Test
    void convertToEntityAttribute_returnsNullForIOException() throws IOException {
        byte[] bytes = "hello".getBytes();

        when(objectMapper.readValue(bytes, IngestFile.class)).thenThrow(IOException.class);
        IngestFile rtn = testable.convertToEntityAttribute(bytes);
        assertNull(rtn);
    }

    @Test
    void convertToEntityAttribute_deserializesFromJsonBytes() throws IOException {
        byte[] bytes = "someValidIngestFileJson".getBytes();
        IngestFile mock = mock(IngestFile.class);

        when(objectMapper.readValue(bytes, IngestFile.class)).thenReturn(mock);
        IngestFile rtn = testable.convertToEntityAttribute(bytes);
        assertEquals(mock, rtn);
    }

    @Test
    void convertToEntityAttribute_returnNullForInvalidDeserializedClass() throws IOException {
        byte[] bytes = "hello".getBytes();

        when(objectMapper.readValue(bytes, IngestFile.class)).thenThrow(JsonProcessingException.class);
        IngestFile rtn = testable.convertToEntityAttribute(bytes);
        assertNull(rtn);
    }

    @Test
    void convertToEntityAttribute_deserializesFromValidSerializedClass() throws IOException {

        IngestFile file = new IngestFile();
        file.setName("myname");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(file);
        oos.flush();

        byte[] json = bos.toByteArray();
        when(objectMapper.readValue(json, IngestFile.class)).thenThrow(JsonParseException.class);

        IngestFile rtn = testable.convertToEntityAttribute(json);
        assertNotNull(rtn);
        assertEquals(file.getName(), rtn.getName());
    }

}