package fi.sls.ingest.representation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserTest {

    @Test
    void getDisplayOrUsername_returnsDisplayNameIfSet() {
        User u = new User();
        u.setDisplayName("Marky");

        assertEquals("Marky", u.getDisplayOrUsername());
    }

    @Test
    void getDisplayOrUsername_returnsUsernameIfNoDisplayName(){
        User u = new User();
        u.setUsername("myuser");

        assertEquals("myuser", u.getDisplayOrUsername());

    }

    @Test
    void getDisplayOrUsername_returnsUsernameIfEmptyDisplayName(){
        User u = new User();
        u.setDisplayName("");
        u.setUsername("myuser");

        assertEquals("myuser", u.getDisplayOrUsername());
    }

}