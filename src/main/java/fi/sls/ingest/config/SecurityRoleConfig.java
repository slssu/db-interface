package fi.sls.ingest.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotBlank;

@Configuration
@ConfigurationProperties(prefix = "sls.security.role")
@Data
public class SecurityRoleConfig {

    /**
     * Role with access to all data within the interface
     */
    @NotBlank
    protected String admin;

    /**
     * Custom getter to verify value has proper prefix and is upper cased
     * @return
     */
    public String getAdmin() {
        String rtn = admin;

        if(!admin.startsWith("ROLE_")){
            rtn = "ROLE_".concat(admin);
        }

        return rtn.toUpperCase();
    }
}
