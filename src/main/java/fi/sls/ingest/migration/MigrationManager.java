package fi.sls.ingest.migration;

import fi.sls.ingest.config.IngestConfig;
import fi.sls.ingest.controller.request.IngestMigrationRequest;
import fi.sls.ingest.exception.EntityNotFoundException;
import fi.sls.ingest.exception.MigrationProcessingCompletedException;
import fi.sls.ingest.exception.MigrationProcessingException;
import fi.sls.ingest.exception.ProcessingItemNotFoundException;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.migration.generator.DcIdentifierGenerator;
import fi.sls.ingest.migration.report.MigrationMasterFilesReport;
import fi.sls.ingest.migration.runnable.MigrationPostProcessRunnable;
import fi.sls.ingest.parser.StrictIngestFilename;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerArrayDataResponse;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.*;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import fi.sls.ingest.proxy.slsfileapi.FileAPIMigrateRequestBody;
import fi.sls.ingest.proxy.slsfileapi.FileAPIMigrateResponseBody;
import fi.sls.ingest.proxy.slsfileapi.SLSFileAPI;
import fi.sls.ingest.queue.rabbitmq.sender.MigrationItemSender;
import fi.sls.ingest.repository.DigitalObjectMigrationResultRepository;
import fi.sls.ingest.representation.User;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.security.AuthenticationFacade;
import fi.sls.ingest.security.jwt.UserPrincipal;
import fi.sls.ingest.ws.SocketMessenger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.net.Socket;
import java.security.MessageDigest;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Service
@Slf4j
public class MigrationManager {

    /**
     * Service for accessing records in FileMaker
     */
    protected RecordService recordService;

    /**
     * Service for generating new dcIdentifier strings
     */
    protected DcIdentifierGenerator dcIdentifierGenerator;

    protected DigitalObjectMigrationResultRepository migrationResultRepository;

    /**
     * Access to the currently authenticated user so we can track who started the migration
     */
    protected AuthenticationFacade authenticationFacade;

    /**
     * Configuration for this API
     */
    protected IngestConfig ingestConfig;

    /**
     * SLS File API wrapper for transferring files from projects to masterfiles
     */
    protected SLSFileAPI slsFileAPI;

    /**
     * Queue sender used for queuing up the items to be processed after the File API move
     */
    protected MigrationItemSender migrationItemSender;

    /**
     * Post process runnable that will be used after the File API call.
     *
     * Injected here to re-use method calls against FileMaker
     */
    protected MigrationPostProcessRunnable migrationPostProcessRunnable;

    /**
     * Validator used to validate the expected new filename of the dc identifier
     */
    protected Validator validator;

    /**
     * Messenger used to send out updates on processed items
     */
    protected SocketMessenger socketMessenger;

    @Autowired
    public MigrationManager(
            RecordService recordService,
            DcIdentifierGenerator dcIdentifierGenerator,
            DigitalObjectMigrationResultRepository migrationResultRepository,
            AuthenticationFacade authenticationFacade,
            IngestConfig ingestConfig,
            SLSFileAPI slsFileAPI,
            MigrationItemSender migrationItemSender,
            MigrationPostProcessRunnable migrationPostProcessRunnable,
            Validator validator,
            SocketMessenger socketMessenger){
        this.recordService = recordService;
        this.dcIdentifierGenerator = dcIdentifierGenerator;
        this.migrationResultRepository = migrationResultRepository;
        this.authenticationFacade = authenticationFacade;
        this.ingestConfig = ingestConfig;
        this.slsFileAPI = slsFileAPI;
        this.migrationItemSender = migrationItemSender;
        this.migrationPostProcessRunnable = migrationPostProcessRunnable;
        this.validator = validator;
        this.socketMessenger = socketMessenger;
    }

    /**
     * Migration path that prioritizes the entity_identifier as a filter for which records to process
     * @param migrationRequest
     * @return
     * @throws IOException
     */
    public MigrationResult migrate(IngestMigrationRequest migrationRequest) throws IOException {
        log.debug("running migration on: {} {} {}",
                kv("collection_name", migrationRequest.getCollectionName()),
                kv("archive_nr", migrationRequest.getArchiveNr()),
                kv("is_dry_run", migrationRequest.isDryRun()),
                kv("migration_request", migrationRequest)
        );

        // link the current logged in user to the new item as the creator
        Authentication currentAuth = authenticationFacade.getAuthentication();
        User currentUser = ((UserPrincipal) currentAuth.getPrincipal()).getUser();

        boolean isDryRun = migrationRequest.isDryRun();

        MigrationResult migrationResult = new MigrationResult(migrationRequest.getCollectionName(), migrationRequest.getArchiveNr(), isDryRun);

        Collection existingCollection = loadCollection(migrationRequest.getCollectionName(), migrationRequest.getArchiveNr());

        // collection verified, load intellectual entities based on identifier
        IntellectualEntity intellectualEntity = new IntellectualEntity();
        intellectualEntity.getFieldData().setEntityIdentifier(migrationRequest.getEntityIdentifier());

        // marks if we should continue to search for more collection entities
        boolean doCarryOn = false;
        Long pageNr = migrationRequest.getPageNr();
        Long maxPageNr = migrationRequest.getMaxPageNr();

        do {
            FileMakerArrayDataResponse intellectualEntityResponse = recordService.findRecords(intellectualEntity, migrationRequest.getLimit(), pageNr);

            if(intellectualEntityResponse == null){
                // no results with the given query, cancel the show
                break;
            }

            // loop over each result, ensure link to collection + list of digitalObjects through a semla event
            Iterator i = intellectualEntityResponse.getResponse().getData().iterator();

            if(intellectualEntityResponse.getResponse().getData().size() == 0){
                // no more results found, we are done
                break;
            }

            while(i.hasNext()) {

                IntellectualEntity intellectualEntityRow = (IntellectualEntity) i.next();

                // we have a verified collection, verify intellectualEntity belongs to it by searching the link in MT
                MTCollectionIntellectualEntity linkCollectionEntities = new MTCollectionIntellectualEntity();
                linkCollectionEntities.getFieldData().setCollectionNumber(existingCollection.getFieldData().getEntityNumber());
                linkCollectionEntities.getFieldData().setIntellectualEntityNumber(intellectualEntityRow.getFieldData().getEntityNumber());

                Optional<? extends FileMakerResponseEntity> mtCollectionIntellectualEntityResponse = recordService.findRecordByIdentifier(linkCollectionEntities);

                if(mtCollectionIntellectualEntityResponse.isPresent()){
                    // verified, we can continue
                    processSemlaEventEntities(migrationResult, intellectualEntityRow, existingCollection, currentUser, isDryRun);
                } else {
                    // not verified, log the problem but continue since this should mean the item is not accessible from a collection
                    log.warn("Could not verify link from IntellectualEntity to Collection for entity_identifier: {}",
                            kv("entity_identifier", intellectualEntityRow.getFieldData().getEntityIdentifier()),
                            kv("collection_entity_number", existingCollection.getFieldData().getEntityNumber()),
                            kv("ie_entity_number", intellectualEntityRow.getFieldData().getEntityNumber())
                    );
                }
            }

            pageNr++;
            if(maxPageNr == 0 || (maxPageNr > 0 && pageNr < maxPageNr)){
                doCarryOn = true;
            } else {
                doCarryOn = false;
            }
        } while (doCarryOn == true);


        return migrationResult;
    }

    public MigrationResult migrate(String collectionName, Long archiveNr, Boolean isDryRun, Long limit, Long pageNr, Long maxPageNr, String entity_identifier) throws IOException {
        log.debug("running migration on: {} {} {}",
                kv("collection_name", collectionName),
                kv("archive_nr", archiveNr),
                kv("is_dry_run", isDryRun)
        );

        // link the current logged in user to the new item as the creator
        Authentication currentAuth = authenticationFacade.getAuthentication();
        User currentUser = ((UserPrincipal) currentAuth.getPrincipal()).getUser();

        MigrationResult migrationResult = new MigrationResult(collectionName, archiveNr, isDryRun);

        // verify that collection exists in FileMaker by doing a fetch
        Collection existingCollection = loadCollection(collectionName, archiveNr);

        // we have a verified collection, see what intellectual entities are linked to them
        MTCollectionIntellectualEntity linkCollectionEntities = new MTCollectionIntellectualEntity();
        linkCollectionEntities.getFieldData().setCollectionNumber(existingCollection.getFieldData().getEntityNumber());

        // marks if we should continue to search for more collection entities
        boolean doCarryOn = false;

        do {
            FileMakerArrayDataResponse collectionEntities = recordService.findRecords(linkCollectionEntities, limit, pageNr);

            // This is not a very efficient algorithm as we are making new requests for each found MT ie row
            // but FileMaker portals did not provide a good enough support without refactoring the database
            // and since this is planned as a once off migration tool there is little point in optimizing very much

            // loop over each result, fetch related intellectualEntity + list of digitalObjects through a semla event
            Iterator i = collectionEntities.getResponse().getData().iterator();

            if(collectionEntities.getResponse().getData().size() == 0){
                // no more results found, we are done
                break;
            }

            while(i.hasNext()){
                MTCollectionIntellectualEntity e = (MTCollectionIntellectualEntity) i.next();
                log.debug("processing record {}",
                        kv("ie_number", e.getFieldData().getIntellectualEntityNumber())
                );

                IntellectualEntity intellectualEntity = new IntellectualEntity();
                intellectualEntity.getFieldData().setEntityNumber(e.getFieldData().getIntellectualEntityNumber());

                FindQueryRequest request = new FindQueryRequest("nummer", "="+intellectualEntity.getFieldData().getEntityNumber());

                Optional<? extends FileMakerResponseEntity> intellectualEntityResponse = recordService.findRecordByQuery(intellectualEntity, request);

                if(intellectualEntityResponse.isPresent()){
                    intellectualEntity = (IntellectualEntity) intellectualEntityResponse.get();
                    log.debug("Using intellectualEntity {}",
                            kv("intellectualEntity", intellectualEntity.getFieldData())
                    );

                    processSemlaEventEntities(migrationResult, intellectualEntity, existingCollection, currentUser, isDryRun);
                }
            }

            pageNr++;
            if(maxPageNr == 0 || (maxPageNr > 0 && pageNr < maxPageNr)){
                doCarryOn = true;
            } else {
                doCarryOn = false;
            }
        } while (doCarryOn == true);

        return migrationResult;
    }


    protected Collection loadCollection(String collectionName, Long archiveNr) throws IOException {
        // verify that collection exists in FileMaker by doing a fetch
        Collection collection = new Collection();
        collection.getFieldData().setCollectionName(collectionName);
        collection.getFieldData().setArchiveNumber(archiveNr);

        // collection.getFieldData().setArchiveNumberSuffix();

        Optional<? extends FileMakerResponseEntity> existingCollectionResult = recordService.findRecordByIdentifier(collection);

        if(existingCollectionResult.isEmpty()){
            // if the collection does not exist, we cannot continue our work
            throw new EntityNotFoundException(
                    String.format("Collection with name '%s' and nr '%s' not found", collectionName, archiveNr)
            );
        }

        return (Collection) existingCollectionResult.get();
    }

    void processSemlaEventEntities(MigrationResult migrationResult, IntellectualEntity intellectualEntity, Collection existingCollection, User currentUser, boolean isDryRun) throws IOException {
        // load the digitalObjects, first need to find SemlaEvent related to the object
        SemlaEvent semlaEventQuery = new SemlaEvent();
        semlaEventQuery.getFieldData().setIntellectualEntityNumber(intellectualEntity.getFieldData().getEntityNumber());

        FileMakerArrayDataResponse semlaEventEntities = recordService.findRecords(semlaEventQuery, 500L);

        if(semlaEventEntities == null){
            // this entity is missing a link to digital objects, need to be created instead of just migrated?
            log.warn("Could not find link from IntellectualEntity to DigitalObject, SemlaEvents missing for entity_number: {}",
                    kv("entity_number", intellectualEntity.getFieldData().getEntityNumber())
            );
            return;
        }

        // loop over each result, fetch related digitalObject if digitalObjects_nr is set
        Iterator doIterator = semlaEventEntities.getResponse().getData().iterator();

        while(doIterator.hasNext()){
            SemlaEvent se = (SemlaEvent) doIterator.next();
            DigitalObjectMigrationResult rowResult = new DigitalObjectMigrationResult();
            rowResult.setCollectionName(migrationResult.getCollectionName());
            rowResult.setArchiveNr(migrationResult.getArchiveNr());

            try{
                // calculate token
                MessageDigest md = MessageDigest.getInstance("MD5");
                String tokenParts = intellectualEntity.getRecordId().toString() + "_" + se.getRecordId().toString();
                md.update(tokenParts.getBytes());
                String token = DatatypeConverter.printHexBinary(md.digest()).toUpperCase();

                // attempt to fetch existing rowResult based on token
                Optional<DigitalObjectMigrationResult> existingRowResult = migrationResultRepository.findByToken(token);
                if(existingRowResult.isPresent()){
                    rowResult = existingRowResult.get();
                }

                rowResult.setDryRun(isDryRun);
                rowResult.setToken(token);
                rowResult.setCreatedBy(currentUser);
                rowResult.setProcessingStatus(ProcessingStatus.MIGRATION_PRE_PROCESSING);
                rowResult.setError(null);
                rowResult.setProcessingStartedAt(Instant.now());
                rowResult.setIntellectualEntityRecordId(intellectualEntity.getRecordId());

                rowResult.setCollectionFieldData(existingCollection.getFieldData());
                rowResult.setIntellectualEntityFieldData(intellectualEntity.getFieldData());
                rowResult.setSemlaEventFieldData(se.getFieldData());

                if(se.getFieldData().getDigitalObjectsNumber() != null && se.getFieldData().getDigitalObjectsNumber() > 0L){
                    log.debug("Using SemlaEvent {}",
                            kv("semlaEvent", se.getFieldData())
                    );

                    // Load the actual digital object
                    DigitalObject digitalObject = new DigitalObject();

                    FindQueryRequest digitalObjectQR = new FindQueryRequest();
                    digitalObjectQR.addQueryItem("nummer", se.getFieldData().getDigitalObjectsNr());

                    Optional<? extends FileMakerResponseEntity> digitalObjectResponse = recordService.findRecordByQuery(digitalObject, digitalObjectQR);

                    if(digitalObjectResponse.isPresent()){
                        digitalObject = (DigitalObject) digitalObjectResponse.get();

                        log.debug("Using DigitalObject {}",
                                kv("digitalObject", digitalObject.getFieldData())
                        );
                        rowResult.setDigitalObjectRecordId(digitalObject.getRecordId());
                        rowResult.setOriginal(digitalObject);

                        rowResult.setOriginalChecksum(digitalObject.getFieldData().getMd5Checksum());

                        // new data population
                        DigitalObject migratedDigitalObject = new DigitalObject();
                        migratedDigitalObject.setRecordId(digitalObject.getRecordId());

                        FilenameMetaData filenameMetaData = dcIdentifierGenerator.generate(
                                rowResult.getCollectionFieldData(),
                                intellectualEntity.getFieldData(),
                                digitalObject.getFieldData()
                        );
                        rowResult.setFilenameMetaData(filenameMetaData);
                        migratedDigitalObject.getFieldData().setDcIdentifier(
                                filenameMetaData.getDcIdentifier()
                        );

                        rowResult.setMigrated(migratedDigitalObject);

                        if(rowResult.isCompleted()){
                            // this item has already been marked as migrated, no need to do more about it
                            throw new MigrationProcessingCompletedException(
                                    String.format("Item has been marked as completed at %s",
                                            DateTimeFormatter.ISO_INSTANT.format(rowResult.getProcessingCompletedAt())
                                    )
                            );
                        }


                        // if new dc_identifier ends up having a row of zeros, mark as error because we should not allow that
                        if(filenameMetaData.getSigumNr() == 0
                                || filenameMetaData.getPageNr() == 0
                                || filenameMetaData.getVersionNr() == 0){
                            throw new MigrationProcessingException(
                                    String.format("One of signumNr, pageNr or versionNr is zero: %s, %s, %s",
                                            kv("signum_nr", filenameMetaData.getSigumNr()),
                                            kv("page_nr", filenameMetaData.getPageNr()),
                                            kv("version_nr", filenameMetaData.getVersionNr())
                                    ));
                        }

                        // validate the new filename based on part from DC identifier after last /
                        Set<ConstraintViolation<StrictIngestFilename>> constraintViolations = validator.validate(new StrictIngestFilename(filenameMetaData.getFilename()));

                        if(!constraintViolations.isEmpty()){
                            throw new MigrationProcessingException(
                                    String.format("New dc_identifier did not produce a valid filename, was %s", filenameMetaData.getFilename()));

                        }

                        // if original dc_identifier is the same as the new one,
                        // skip processing because this item has already been migrated
                        if(migratedDigitalObject.getFieldData().getDcIdentifier().equalsIgnoreCase(digitalObject.getFieldData().getDcIdentifier())){
                            throw new MigrationProcessingException(
                                    String.format("Original and migrated dc_identifier are the same, will not process"));
                        }

                        rowResult.setOriginalDcIdentifier(digitalObject.getFieldData().getDcIdentifier());
                        rowResult.setMigratedDcIdentifier(migratedDigitalObject.getFieldData().getDcIdentifier());



                        // set the collection_order on intellectual entity
                        if(intellectualEntity.getFieldData().getCollectionOrder() == null || intellectualEntity.getFieldData().getCollectionOrder() == 0L) {
                            // create new instance and save that, because not all fields in FM necessarily validate anymore due to old data
                            IntellectualEntity migrationIE = new IntellectualEntity();
                            migrationIE.setRecordId(intellectualEntity.getRecordId());
                            migrationIE.getFieldData().setCollectionOrder(
                                    Long.valueOf(dcIdentifierGenerator.extractSigumFromIdentifier(intellectualEntity.getFieldData().getEntityIdentifier()))
                            );
                            if(!isDryRun){
                                // re-store result of update to local var so we have the new values for the next iteration
                                intellectualEntity = (IntellectualEntity) recordService.updateObjectRecord(migrationIE);
                                log.debug("Updated collection order for intellectual entity with record id {} to {}",
                                        kv("record_id", intellectualEntity.getRecordId()),
                                        kv("collection_order", intellectualEntity.getFieldData().getCollectionOrder())
                                );
                            } else {
                                // store the would be collection order locally so it goes as close as possible to what would have happened
                                intellectualEntity.getFieldData().setCollectionOrder(migrationIE.getFieldData().getCollectionOrder());
                                log.debug("Dry run: skipped updating collection order for intellectual entity with record id {} to {}",
                                        kv("record_id", intellectualEntity.getRecordId()),
                                        kv("collection_order", intellectualEntity.getFieldData().getCollectionOrder())
                                );

                            }
                        }

                        // pick out original accessfilepaths for the digitalObject, send with migration request
                        rowResult.setOriginalAccessFiles(migrationPostProcessRunnable.findAccessFilesForDigitalObject(rowResult.getOriginal()));

                        if(!isDryRun){
                            FileAPIMigrateRequestBody fileAPIRequest = FileAPIMigrateRequestBody.createFrom(
                                    rowResult,
                                    ingestConfig.getHostRelativeAPIUrl("migration/continue?token="+rowResult.getToken())
                            );
                            rowResult.setFileAPICallbackURL(fileAPIRequest.getCallbackUrl());
                            rowResult.setProcessingStatus(ProcessingStatus.MIGRATION_FILE_API);

                            log.debug("Sending migrate request to File API: {}", kv("request", fileAPIRequest));
                            slsFileAPI.migrate(fileAPIRequest);

                        } else {
                            rowResult.setProcessingStatus(ProcessingStatus.DRY_RUN_MIGRATION_FILE_API);
                            log.debug("Dry run: skipped calling File API with migrate request");
                        }
                    } else {
                        log.debug("Did not find valid DigitalObject {}",
                                kv("digitalObjectQuery", digitalObjectQR)
                        );
                        rowResult.setProcessingStatus(ProcessingStatus.ERROR);
                        rowResult.setError(
                                String.format("Could not find DigitalObject with nummer field = %d ",
                                        se.getFieldData().getDigitalObjectsNr()
                                )
                        );
                    }

                } else {
                    log.debug("SemlaEvent is missing digital object nr {}",
                            kv("semlaEvent", se.getFieldData())
                    );
                    rowResult.setProcessingStatus(ProcessingStatus.ERROR);
                    rowResult.setError(
                            String.format("SemlaEvent with recordId %d and intellectualEntityNr %d is missing digital object nr ",
                                    se.getRecordId(),
                                    se.getFieldData().getIntellectualEntityNumber()
                            )
                    );
                }
            } catch (MigrationProcessingCompletedException ex) {
                rowResult.setProcessingStatus(ProcessingStatus.MIGRATION_REJECTED);
                rowResult.setError(ex.getMessage());
            } catch (Exception ex){
                rowResult.setProcessingStatus(ProcessingStatus.ERROR);
                rowResult.setError(ex.getMessage());
            }

            // regardless of outcome, we save the record to the DB and update composite migration result
            migrationResultRepository.save(rowResult);
            migrationResult.getItemResults().add(rowResult);
            socketMessenger.broadcast(rowResult);
        }
    }

    /**
     * Continue processing of item
     *
     * @param token
     * @param processingResult
     */
    public void postProcessItem(String token, FileAPIMigrateResponseBody processingResult) {

        Optional<DigitalObjectMigrationResult> row = migrationResultRepository.findByToken(token);
        if(row.isPresent()){
            DigitalObjectMigrationResult item = row.get();
            item.setFileAPIResponse(processingResult);
            item.setMigratedAccessFiles(processingResult.getAccessFiles());
            migrationResultRepository.save(item);
            migrationItemSender.send(item);
        } else {
            throwItemNotFoundException(token);
        }
    }

    /**
     * Record the error for the item being rejected
     *
     * @param token
     * @param reason
     */
    public void rejectProcessingItem(String token, String reason) {
        Optional<DigitalObjectMigrationResult> row = migrationResultRepository.findByToken(token);

        if(row.isPresent()){
            DigitalObjectMigrationResult item = row.get();
            item.setError(reason);
            migrationResultRepository.save(item);
        } else {
            throwItemNotFoundException(token);
        }
    }

    /**
     * Generate a report of the current status for the items in the migration database
     * @param collectionName
     * @param archiveNr
     * @return
     */
    public MigrationResult generateReport(String collectionName, Long archiveNr) {
        MigrationResult result = new MigrationResult(collectionName, archiveNr, true);

        List<DigitalObjectMigrationResult> rows = migrationResultRepository.findAllByCollectionNameAndArchiveNr(collectionName, archiveNr);

        result.setItemResults(rows);

        return result;
    }

    /**
     * Generate a report of original paths from where files where moved to a
     * new location in masterfiles during the migration of a given collection.
     *
     * Before using this list to remove anything, some sort of sanity check should
     * be done to ensure the file is not removed from a location where it should actually be.
     * E.g. if the file path corresponds to the location where the file should be according to
     * new folder structure, then a removal should not be done.
     *
     *
     * @param collectionName
     * @param archiveNr
     * @return
     */
    public MigrationMasterFilesReport generateMasterfilesReport(String collectionName, Long archiveNr) {
        MigrationMasterFilesReport report = new MigrationMasterFilesReport(collectionName, archiveNr);

        List<DigitalObjectMigrationResult> rows = migrationResultRepository.findAllByCollectionNameAndArchiveNr(collectionName, archiveNr);

        rows.stream().forEach((item) -> {
            if(item.getOriginalDcIdentifier() != null && item.getProcessingStatus() == ProcessingStatus.COMPLETE){
                report.getOriginalMasterfilePaths().add(item.getOriginalDcIdentifier());
            }
        });

        return report;
    }
    /**
     * Helper to throw an exception for when an item with a given token cannot be found in the processing store
     *
     * @param token
     */
    protected void throwItemNotFoundException(String token){
        log.warn("Item not found in migration store: {}", kv("migration_item_token", token));
        String err = String.format("Item with token %s could not be found in migration store.", token);
        throw new ProcessingItemNotFoundException(err);
    }
}
