package fi.sls.ingest.actuator.metrics.supplier;

import fi.sls.ingest.repository.IngestItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ProcessingItemAverageDurationSupplierTest {

    ProcessingItemAverageDurationSupplier testable;
    IngestItemRepository repository;

    @BeforeEach
    void setUp(){
        repository = mock(IngestItemRepository.class);

        testable = new ProcessingItemAverageDurationSupplier(repository);
    }

    @Test
    void get_returnsAvgIfAvailable() {
        when(repository.getAverageProcessingDuration()).thenReturn(Optional.of(Double.valueOf(132)));
        assertEquals(132.0, testable.get());
    }

    @Test
    void get_returnsZeroIfNotAvailable() {
        when(repository.getAverageProcessingDuration()).thenReturn(Optional.empty());
        assertEquals(0.0, testable.get());
    }
}