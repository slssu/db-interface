package fi.sls.ingest.fits.model;

import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.fileinfo.FitsFileinfoDateMetadata;
import fi.sls.ingest.fits.model.fieldresolver.fileinfo.FitsFileinfoMetadata;
import fi.sls.ingest.fits.model.fieldresolver.identification.FitsIdentityMetadataExternalIdentifier;
import fi.sls.ingest.fits.model.fieldresolver.identification.FitsIdentityMetadataFormat;
import fi.sls.ingest.fits.model.fieldresolver.identification.FitsIdentityMetadataFormatVersion;
import fi.sls.ingest.fits.model.fieldresolver.identification.FitsIdentityMetadataMimetype;
import fi.sls.ingest.fits.model.fieldresolver.tool.DroidMetadata;
import fi.sls.ingest.fits.model.fieldresolver.tool.JHoveMetadata;
import fi.sls.ingest.fits.model.fieldresolver.tool.TikaMetadata;
import fi.sls.ingest.fits.model.generated.Fits;
import fi.sls.ingest.fits.model.generated.FitsMetadataType;
import fi.sls.ingest.fits.model.generated.StatisticsType;
import fi.sls.ingest.fits.model.tool.FitsDroidResult;
import fi.sls.ingest.fits.model.tool.FitsJHoveResult;
import fi.sls.ingest.fits.model.tool.FitsTikaResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.bind.JAXBElement;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Provides an abstraction against the Fits XML result structure for easier access to the metadata that
 * we are interested in.
 */
public class FitsResult implements Serializable {

    protected Fits generatedResult;
    protected String rawResult;
    protected Document xmlDocument;

    @Deprecated
    protected FitsDroidResult droidResult;
    @Deprecated
    protected FitsTikaResult tikaResult;
    @Deprecated
    protected FitsJHoveResult jHoveResult;

    public FitsResult(Fits generatedResult){
        this.generatedResult = generatedResult;

        // setup result wrappers for various specific tools whose results we are interested in
        droidResult = new FitsDroidResult(generatedResult);
        tikaResult = new FitsTikaResult(generatedResult);
        jHoveResult = new FitsJHoveResult(generatedResult);
    }

    public FitsResult(Fits generatedResult, String rawResult) {
        this(generatedResult);
        this.rawResult = rawResult;

    }

    /**
     * Factory method for creating a FitsResult object corresponding to the main file format identified in the XML data.
     *
     * Main file formats are: audio, document, image, text, video
     *
     * Not all formats unfortunately generate a structure in the metadata result tag, so for some we also check the identification tag
     * for specific keywords.
     *
     * @param xml
     * @return
     */
    public static FitsResult createFromFits(Fits xml) {
        if(xml.getMetadata().getImage() != null || xml.getIdentification().getIdentity().get(0).getMimetype().toLowerCase().contains("image")){
            return new FitsImageResult(xml);
        } else if(xml.getMetadata().getAudio() != null || xml.getIdentification().getIdentity().get(0).getMimetype().toLowerCase().contains("audio")) {
            return new FitsAudioResult(xml);
        } else if(xml.getMetadata().getVideo() != null || xml.getIdentification().getIdentity().get(0).getMimetype().toLowerCase().contains("video")) {
            return new FitsVideoResult(xml);
        } else {
            return new FitsResult(xml);
        }
    }

    public static FitsResult createFromFits(Fits xml, String rawResult){
        FitsResult res = createFromFits(xml);
        res.rawResult = rawResult;

        return res;
    }

    public static FitsResult createFromFits(Fits xml, String rawResult, Document xmlDocument){
        FitsResult res = createFromFits(xml, rawResult);
        res.xmlDocument = xmlDocument;

        return res;
    }

    /**
     * @return The generated XML structure as returned by FITS service and converted to a Fits object.
     */
    public Fits getFitsResult(){
        return generatedResult;
    }

    public Document getXmlDocument(){
        return xmlDocument;
    }

    /**
     * @return The raw XML string as returned by FITS service.
     */
    public String getRawResult(){
        return rawResult;
    }

    /**
     * Target field in FileMaker: teknisk_data -> originalMD5
     *
     * @return
     */
    public String getFileMD5(){

        FieldValueResolver resolver = new FitsFileinfoMetadata(FitsFileinfoMetadata.FIELD_MD5_CHECKSUM, this);

        return resolver.getValue().orElse(null);
    }

    /**
     * Target field in FileMaker: teknisk_data -> file_size
     * @return
     */
    public Long getFileSize(){

        FieldValueResolver resolver = new FitsFileinfoMetadata(FitsFileinfoMetadata.FIELD_FILESIZE, this);

        String size = resolver.getValue().orElse(null);

        if(size != null){
            return Long.parseLong(size);
        } else {
            return null;
        }
    }

    /**
     * Target field in FileMaker: teknisk_data -> originalFileName
     * @return
     */
    public String getFileName(){
        FieldValueResolver resolver = new FitsFileinfoMetadata(FitsFileinfoMetadata.FIELD_FILENAME, this);

        return resolver.getValue().orElse(null);
    }

    /**
     * Target field in FileMaker: teknisk_data -> created
     *
     * @return
     */
    public String getCreated(){
        FieldValueResolver resolver = new FitsFileinfoDateMetadata(FitsFileinfoDateMetadata.FIELD_CREATED, this,
                new TikaMetadata(TikaMetadata.FIELD_DATE_CREATED, this)
        );

        String val = resolver.getValue().orElse(null);

        if(val == null || val.isEmpty()){
            // use modified date instead if a real creation date cannot be found
            return getModified();
        } else {
            return val;
        }
    }

    /**
     * Target field in FileMaker: teknisk_data -> modified
     *
     * @return
     */
    public String getModified(){
        FieldValueResolver resolver = new FitsFileinfoDateMetadata(FitsFileinfoDateMetadata.FIELD_LAST_MODIFIED, this,
                new FitsFileinfoDateMetadata(FitsFileinfoDateMetadata.FIELD_FS_LAST_MODIFIED, this,
                        new TikaMetadata(TikaMetadata.FIELD_DATE_CREATED, this,
                                new JHoveMetadata(JHoveMetadata.FIELD_LAST_MODIFIED, this)
                    )
                )
        );

        return resolver.getValue().orElse(null);
    }

    /**
     * Target field in FileMaker: teknisk_data -> originalCaptureDevice
     *
     * @return
     */
    public String getOriginalCaptureDevice(){
        // TODO: JHove might also have this info, add if we notice that tika value often is not set or correct

        FieldValueResolver resolver = new TikaMetadata(TikaMetadata.FIELD_MODEL, this);

        return resolver.getValue().orElse(null);


    }


    /**
     * Target field in FileMaker: teknisk_data -> filetype_name
     *
     * @return
     */
    public String getIdentityFormat(){

        FieldValueResolver resolver = new FitsIdentityMetadataFormat(this);

        return resolver.getValue().orElse(null);
    }

    /**
     * Target field in FileMaker: teknisk_data -> filetype_MIME
     *
     * @return
     */
    public String getIdentityMimetype(){

        FieldValueResolver resolver = new FitsIdentityMetadataMimetype(this);

        return resolver.getValue().orElse(null);

    }

    /**
     * Target field in FileMaker: teknisk_data -> filetype_pronom
     *
     * @return
     */
    public String getExternalIdentifier(){
        FieldValueResolver resolver = new FitsIdentityMetadataExternalIdentifier(this,
                new DroidMetadata(DroidMetadata.FIELD_FILE_PUID, this)
        );

        return resolver.getValue().orElse(null);

    }

    /**
     * Target field in FileMaker: teknisk_data -> filetype_formatVersion
     *
     * @return
     */
    public String getFileTypeFormatVersion(){

        FieldValueResolver resolver = new FitsIdentityMetadataFormatVersion(this,
                new JHoveMetadata(JHoveMetadata.FIELD_VERSION, this)
        );

        return resolver.getValue().orElse(null);
    }

    /**
     *
     * @return The FITS version used to generate this result
     */
    public String getFitsVersion(){
        return generatedResult.getVersion();
    }

    /**
     * Helper for running a FieldValueResolver and extracting the result as an Integer if it exists,
     * or null if the resolver could not find a matching value.
     *
     * @param parser
     * @return
     */
    protected Integer parseIntegerOrNull(FieldValueResolver parser){
        String val = parser.getValue().orElse(null);

        if(val != null){
            return Integer.parseInt(val);
        } else {
            return null;
        }
    }

    /**
     * Helper for running a FieldValueResolver and extracting the result as an Long if it exists,
     * or null if the resolver could not find a matching value.
     *
     * @param parser
     * @return
     */
    protected Long parseLongOrNull(FieldValueResolver parser){
        String val = parser.getValue().orElse(null);

        if(val != null){
            return Long.parseLong(val);
        } else {
            return null;
        }
    }

    /**
     * Helper for running a FieldValueResolver and extracting the result as an String if it exists,
     * or null if the resolver could not find a matching value.
     *
     * @param parser
     * @return
     */
    protected String parseStringOrNull(FieldValueResolver parser) {
        return parser.getValue().orElse(null);
    }


    /**
     * Returns true if any of the tools in the FitsResult have been marked as "failed" in their status.
     *
     * This indicates that the tool ran into an error that it wasn't able to recover from, which might indicate
     * that not all necessary metadata has been extracted.
     *
     * @return
     */
    public boolean hasFailedToolResults() {

        List<StatisticsType.Tool> tools = generatedResult.getStatistics()
                .getTool().stream()
                .filter(
                        item -> item.getStatus() != null && item.getStatus().equalsIgnoreCase("failed")
                ).collect(Collectors.toList());

        return tools.size() > 0;
    }

    @Deprecated
    protected String findFileInfoElementValue(String elementName){
        FitsMetadataType t = findFileInfoElementByName(elementName);
        if(t!= null){
            return t.getContent().get(0).toString();
        } else {
            return null;
        }
    }

    @Deprecated
    protected FitsMetadataType findFileInfoElementByName(String name){

        Optional<JAXBElement<FitsMetadataType>> item = generatedResult.
                getFileinfo().
                getFileInfoElements().
                stream().
                filter(
                        fitsMetadataTypeJAXBElement ->
                                fitsMetadataTypeJAXBElement.
                                        getName().getLocalPart().equals(name)
                ).findFirst();

        if(item.isPresent()){
            return item.get().getValue();
        } else {
            return null;
        }
    }


    /**
     * This method is expected to be overridden in a subclass,
     * but not marking as abstract because we need to be able to create
     * instances of this class as well.
     *
     * @return
     */
    @Deprecated
    protected Element findMetadataElementByName(String name){
        return null;
    }

    @Deprecated
    protected String findStringMetadataByElementName(String name ){
        Element val = this.findMetadataElementByName(name);
        if(val != null){
            return val.getFirstChild().getNodeValue();
        }
        return null;
    }

    @Deprecated
    protected Integer findIntegerMetadataByElementName(String name){
        Element val = this.findMetadataElementByName(name);
        if(val != null){
            return Integer.parseInt(val.getFirstChild().getNodeValue());
        }
        return null;
    }
}
