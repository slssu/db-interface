package db.migration;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.representation.converter.IngestFileConverter;
import fi.sls.ingest.representation.converter.StringCompressionConverter;
import fi.sls.ingest.rest.IngestFile;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Convert the item column of ingest_processing_item table to be json string bytes instead of serialized java class.
 *
 * Avoids serialization issues in case the java class changes
 *
 * migrate rows 0-40000 because open source flyway does not support streaming of migration,
 * and data amounts are too large to allow smooth opeartion without
 */
public class V12__IngestProcessingItemJsonConvert extends BaseJavaMigration {

    @Override
    public boolean canExecuteInTransaction(){
        return false;
    }

    @Override
    public void migrate(Context context) throws Exception {

        int offset = 0;
        int pageSize = 20;
        int maxRows = 40000;

        runMigrationWithParams(context, pageSize, offset, maxRows);

    }

    protected void runMigrationWithParams(Context context, int pageSize, int offset, int maxRows) throws SQLException {

        ObjectMapper objectMapper = new ObjectMapper();
        IngestFileConverter converter = new IngestFileConverter(objectMapper);


        // figure out total nr of posts to convert
        JdbcTemplate jdbcTemplate = new JdbcTemplate(new SingleConnectionDataSource(context.getConnection(), true));
        Map<String, Object> rowCount = jdbcTemplate.queryForMap("SELECT count(id) as numRows FROM ingest_processing_item");

        long numRows = (long) rowCount.get("numRows");

        // limit nr of rows to process in one migration so we don't run into memory issues on server
        if(numRows > maxRows){
            numRows = maxRows;
        }

        System.out.println(String.format("Processing %d rows in ingest_processing_item table", numRows));

        DataSource ds = new SingleConnectionDataSource(context.getConnection(), true);
        Connection conn = ds.getConnection();
        conn.setAutoCommit(false);

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(ds);

        do{

            System.out.println(String.format("Processing offset %d, with pageSize %d", offset, pageSize));

            List<Map<String, Object>> resultSet = namedParameterJdbcTemplate.queryForList("SELECT id, item FROM ingest_processing_item ORDER BY id LIMIT :offset, :limit",
                    new MapSqlParameterSource()
                            .addValue("offset", offset)
                            .addValue("limit", pageSize)
            );

            System.out.println(String.format("Found %d rows in ingest_processing_item table to process in batch", resultSet.size()));

            Iterator<Map<String, Object>> iter = resultSet.iterator();

            String updateSql = "UPDATE ingest_processing_item SET item=:item WHERE id=:id";

            List<V12__IngestProcessingItemJsonConvert.RowObject> updateList = new ArrayList<>();

            while(iter.hasNext()){
                Map<String, Object> row = iter.next();

                long rowId = (long) row.get("id");
                byte[] byteResult = (byte[]) row.get("item");

                // run through converter to ensure value is a json byte array
                IngestFile file = converter.convertToEntityAttribute(byteResult);
                byte[] reConverted = converter.convertToDatabaseColumn(file);

                V12__IngestProcessingItemJsonConvert.RowObject ro = new V12__IngestProcessingItemJsonConvert.RowObject(rowId, reConverted);
                updateList.add(ro);
            }

            SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(updateList);

            namedParameterJdbcTemplate.batchUpdate(updateSql, batch);

            conn.commit();
            System.out.println(String.format("Batch offset %d, with pageSize %d commited", offset, pageSize));

            offset += pageSize;

        } while(offset < numRows);
    }

    protected class RowObject {

        public RowObject(long id, byte[] item){
            this.id = id;
            this.item = item;
        }

        public long id;
        public byte[] item;

        public long getId(){
            return id;
        }

        public byte[] getItem(){
            return item;
        }

    }
}
