package fi.sls.ingest.representation.file;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class FilenameMetaDataTests {


    @Test
    public void setsPropsThroughConstructor(){
        String fn = "myfile.txt";
        FilenameMetaData testable = new FilenameMetaData(fn);
        assertEquals(fn, testable.getFilename());
    }

    @Test
    public void getArchiveNrDigit(){
        FilenameMetaData testable = new FilenameMetaData();
        testable.setArchiveNr("555");
        assertEquals(Long.valueOf(555), testable.getArchiveNrDigit());

        testable.setArchiveNr("555j");
        assertEquals(Long.valueOf(555), testable.getArchiveNrDigit());
    }

    @Test
    public void getArchiveNrSuffix(){
        FilenameMetaData testable = new FilenameMetaData();
        testable.setArchiveNr("555");
        assertNull(testable.getArchiveNrSuffix());

        testable.setArchiveNr("555j");
        assertEquals("j", testable.getArchiveNrSuffix());

    }

    @Test
    public void getDigitalObjectIdentifierNoSuffix(){
        ObjectCategory c = new ObjectCategory();
        c.setFileKey("brev");

        FilenameMetaData testable = new FilenameMetaData();
        testable.setCollectionKey("slsa");
        testable.setArchiveNr("555");
        testable.setCategory(c);
        testable.setSigumNr(1);
        testable.setPageNr(2);
        testable.setVersionNr(2);


        assertEquals("SLS:slsa555_brev.1-2-2", testable.getDigitalObjectIdentifierNoSuffix());

        testable.setDigitalOriginal(true);
        assertEquals("SLS:slsa555_brev.1-2-2_orig", testable.getDigitalObjectIdentifierNoSuffix());

    }

    @Test
    public void getDigitalObjectIdentifier(){
        ObjectCategory c = new ObjectCategory();
        c.setFileKey("brev");

        FilenameMetaData testable = new FilenameMetaData();
        testable.setCollectionKey("slsa");
        testable.setArchiveNr("555");
        testable.setCategory(c);
        testable.setSigumNr(1);
        testable.setPageNr(2);
        testable.setVersionNr(1);
        testable.setFileSuffix("tif");

        assertEquals("SLS:slsa555_brev.1-2-1.tif", testable.getDigitalObjectIdentifier());
    }

    @Test
    public void constructor_defaultConstructorUsesSLSIdentityPrefix(){
        ObjectCategory c = new ObjectCategory();
        c.setFileKey("brev");

        FilenameMetaData testable = new FilenameMetaData("somefile.tif");
        testable.setCollectionKey("slsa");
        testable.setArchiveNr("555");
        testable.setCategory(c);
        testable.setSigumNr(1);
        testable.setPageNr(2);
        testable.setVersionNr(3);
        testable.setFileSuffix("tif");

        assertEquals("SLS:slsa555_brev.1-2-3.tif", testable.getDigitalObjectIdentifier());
    }

    @Test
    public void constructor_identityPrefixInjectable(){
        ObjectCategory c = new ObjectCategory();
        c.setFileKey("brev");

        FilenameMetaData testable = new FilenameMetaData("somefile.tif", "ANOTHER");
        testable.setCollectionKey("slsa");
        testable.setArchiveNr("555");
        testable.setCategory(c);
        testable.setSigumNr(1);
        testable.setPageNr(2);
        testable.setVersionNr(1);
        testable.setFileSuffix("tif");

        assertEquals("ANOTHER:slsa555_brev.1-2-1.tif", testable.getDigitalObjectIdentifier());
    }

    @Test
    public void getCollectionNameOrDesc_returnsCollectionNameIfNoDescription(){
        FilenameMetaData testable = new FilenameMetaData("somefile.tif", "ANOTHER");
        testable.setCollectionName("TA");

        assertEquals("TA", testable.getCollectionDescriptionOrName());
    }

    @Test
    public void getCollectionNameOrDesc_returnsDescriptionIfSet(){
        FilenameMetaData testable = new FilenameMetaData("somefile.tif", "ANOTHER");
        testable.setCollectionName("TA");
        testable.setCollectionDescription("Tjänstearkivet");

        assertEquals("Tjänstearkivet", testable.getCollectionDescriptionOrName());
    }

    // TODO: test getArchiveSignum
    // TODO: test getArchiveCollectionName
    // TODO: test getEntityIdentifier
    // TODO: test getEntityIdentifier

}