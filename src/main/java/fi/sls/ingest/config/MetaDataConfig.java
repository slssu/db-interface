package fi.sls.ingest.config;

import fi.sls.ingest.exception.IngestFilenameException;
import fi.sls.ingest.representation.file.ObjectCategory;
import fi.sls.ingest.representation.file.ObjectCollection;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
@ConfigurationProperties(prefix="sls.metadata")
@Data
public class MetaDataConfig {

    protected String identityPrefix = "SLS";

    protected List<ObjectCategory> categories = new ArrayList<>();

    protected List<ObjectCollection> collections = new ArrayList<>();


    /**
     * Helper for fetching a collection config of a SLS collection
     * from the configuration based on the given label.
     *
     * @param label The label to look for in the configuration
     * @return The defined collection fileKey or null if none is found
     */
    public ObjectCollection getCollectionByLabel(String label){

        Iterator<ObjectCollection> it = this.getCollections().iterator();

        while(it.hasNext()){
            ObjectCollection oc = it.next();
            if(oc.getLabel().equalsIgnoreCase(label)){
                return oc;
            }
        }

        throw new IngestFilenameException(String.format("There is not a collection configured using label: %s", label));
    }

    /**
     * Attempt to find ObjectCategory using the category label and a subcategory
     *
     * If no exact match is found on the label and subLabel, we recursively shorten
     * the subLabel to attempt a match until the length is 2 (?)
     *
     * @param label The main category label to match
     * @param subLabel the subcategory label to attempt matching
     * @return
     */
    public ObjectCategory getCategoryBySubCategoryFuzzy(String label, String subLabel){

        Iterator<ObjectCategory> it = this.getCategories().iterator();

        while(it.hasNext()){
            ObjectCategory oc = it.next();
            ObjectCategory matched = recursiveMatchCategory(label, subLabel, oc);

            if(matched != null){
                return matched;
            }
        }

        throw new IngestFilenameException(String.format("There is not a category configured using label and subLabel: %s, %s", label, subLabel));
    }

    protected ObjectCategory recursiveMatchCategory(String label, String subLabel, ObjectCategory oc) {
        if(oc.getCategoryLabel().equalsIgnoreCase(label) && oc.getSubCategory().equalsIgnoreCase(subLabel)){
            return oc;
        } else if(subLabel.length() > 2){
            // remove one char from the end of subLabel and try again
            return recursiveMatchCategory(label, subLabel.substring(0, subLabel.length() - 2), oc);
        } else {
            return null;
        }
    }

}
