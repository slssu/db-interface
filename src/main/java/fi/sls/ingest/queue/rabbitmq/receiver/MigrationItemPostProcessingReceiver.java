package fi.sls.ingest.queue.rabbitmq.receiver;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.exception.MigrationProcessingException;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.migration.DigitalObjectMigrationResult;
import fi.sls.ingest.migration.runnable.MigrationPostProcessRunnable;
import fi.sls.ingest.repository.DigitalObjectMigrationResultRepository;
import fi.sls.ingest.repository.PagingAndSortingTokenStorageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

import java.io.IOException;
import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * Handles IngestProcessingItems that are sent from the post-processing queue
 *
 * This means items that have been processed by the masterfiles service (File API)
 */
@Slf4j
//@RabbitListener(queues = "${sls.queue.migrationPostProcessing}", errorHandler = "rabbitMigrationRetryHandler")
@RabbitListener(queues = "${sls.queue.migrationPostProcessing}", containerFactory = "singlePrefetchDefaultRabbitListenerContainerFactory", concurrency = "${sls.concurrency.migrationPostProcessing:1}")
public class MigrationItemPostProcessingReceiver {

    /**
     * ObjectMapper used for converting the MQ message body back to a class instance
     */
    protected ObjectMapper objectMapper;
    protected MigrationPostProcessRunnable runnable;
    protected PagingAndSortingTokenStorageRepository tokenRepository;

    public MigrationItemPostProcessingReceiver(ObjectMapper objectMapper,
                                               MigrationPostProcessRunnable runnable,
                                               DigitalObjectMigrationResultRepository tokenRepository) {
        this.objectMapper = objectMapper;
        this.runnable = runnable;
        this.tokenRepository = tokenRepository;
    }

    @RabbitHandler
    public void receive(String json) throws AmqpRejectAndDontRequeueException {

        try {
            DigitalObjectMigrationResult item = objectMapper.readValue(json, DigitalObjectMigrationResult.class);

            // verify that the item we got from the queue is in the repo
            Optional<DigitalObjectMigrationResult> checkItemExists = tokenRepository.findByToken(item.getToken());
            if(checkItemExists.isPresent()) {
                log.info("Start processing migration item with token: {} ",
                        kv(LogMessageKey.MIGRATION_ITEM_TOKEN, item.getToken()),
                        kv("item_status", item.getProcessingStatus()),
                        kv(LogMessageKey.MIGRATION_PROCESSING_ITEM, item)
                );
                runnable.setItem(item);
                runnable.run();
                log.info("Processing complete for migration item with token: {} ",
                        kv(LogMessageKey.MIGRATION_ITEM_TOKEN, item.getToken()),
                        kv("item_status", item.getProcessingStatus()),
                        kv(LogMessageKey.MIGRATION_PROCESSING_ITEM, item)
                );
            } else {
                log.warn("Could not start processing item because an item with the given token does not exist in the tokenRepository: {}",
                        kv(LogMessageKey.MIGRATION_ITEM_TOKEN, item.getToken()),
                        kv(LogMessageKey.MIGRATION_PROCESSING_ITEM, item)
                );
                throw new MigrationProcessingException(
                        String.format("Could not start processing item because an item with the given token does not exist: token=%s",
                                item.getToken()
                        )
                );
            }
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new AmqpRejectAndDontRequeueException(e);
        }
    }
}