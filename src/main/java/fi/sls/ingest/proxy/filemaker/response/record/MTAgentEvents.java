package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.sls.ingest.proxy.filemaker.request.CreateRequest;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.request.UpdateRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import lombok.Data;
import lombok.Getter;

/**
 * Entity representing API responses for values in the "mt_agents_events" table/view in FileMaker.
 */

@Data
public class MTAgentEvents extends FileMakerResponseEntity {

    @JsonIgnore
    @Getter
    private String layoutName = "ingestapi_mt_agents_events";


    MTAgentEventsFieldData fieldData;

    public MTAgentEvents(){
        this.fieldData = new MTAgentEventsFieldData();
    }

    @Override
    public FindQueryRequest createFindQueryRequest() {
        FindQueryRequest queryRequest = new FindQueryRequest("ag_nr", "="+getFieldData().getAgentNr());
        queryRequest.addToExistingQueryItem(0, "ev_nr", "="+getFieldData().getEventNr());
        return queryRequest;
    }

    @Override
    public UpdateRequest createUpdateRequest() {
        return new UpdateRequest(getFieldData());
    }

    @Override
    public CreateRequest createRecordRequest() {
        return new CreateRequest(getFieldData());
    }
}
