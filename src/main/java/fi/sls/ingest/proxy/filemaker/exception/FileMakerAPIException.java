package fi.sls.ingest.proxy.filemaker.exception;

import fi.sls.ingest.proxy.filemaker.FileMakerInternalResponseCode;
import lombok.Getter;

/**
 * Wrapper for exceptions coming from the FileMaker API.
 *
 * Errors that are internal to the FileMaker API can return with a 500 HTTP status code
 * even though they are recoverable (e.g. record not found), so they need to be handled separately.
 *
 * <a href="https://fmhelp.filemaker.com/help/17/fmp/en/index.html#page/FMP_Help/error-codes.html"
 *
 */
public class FileMakerAPIException extends RuntimeException {

    @Getter
    FileMakerInternalResponseCode code;

    public FileMakerAPIException(RuntimeException e){
        super(e);
    }

    public FileMakerAPIException(RuntimeException e, FileMakerInternalResponseCode code){
        super(e);
        this.code = code;
    }
}
