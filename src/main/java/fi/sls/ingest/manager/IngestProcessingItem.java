package fi.sls.ingest.manager;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import fi.sls.ingest.proxy.slsfileapi.FileAPIResponseBody;
import fi.sls.ingest.representation.User;
import fi.sls.ingest.representation.audit.DateAudit;
import fi.sls.ingest.representation.converter.*;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.rest.IngestFile;
import fi.sls.ingest.security.TokenAuthenticable;
import lombok.Data;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a unit of work during the ingest process.
 *
 * An IngestProcessingItem instance contains metadata about the process,
 * including which file is being processed, which digitization profile has been used, what the status is of the
 * processing of this file, checksum hashes for before and after the process.
 */
@Data
@Entity
public class IngestProcessingItem extends DateAudit implements MD5ComparisonItem, TokenAuthenticable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private User createdBy;

    /**
     * Timestamp for when this item was sent to the first processor after the staging area
     */
    private Instant processingStartedAt;

    /**
     * Timestamp for when this item has been marked as COMPLETE, ie has been completely processed
     */
    private Instant processingCompletedAt;

    /**
     * A unique string token for this item.
     *
     * Can be anything as long as there is a low risk that the same token is used for multiple files.
     */
    @Column(unique = true)
    private String token;

    /**
     * Metadata about the actual file being processed
     *
     * E.g. file path, if it's a directory or not, etc
     */
    @Convert(converter = IngestFileConverter.class)
    @Lob
    @JsonDeserialize(as = IngestFile.class)
    private IngestItem item;


    /**
     * The digitization profile used for creating the digital file being processed
     *
     * This is SLS metadata about the actual archiving work being done, which will be stored to the archive along the file.
     */
    @Convert(converter = IngestDigitizationProfileConverter.class)
    @Lob
    private IngestDigitizationProfile digitizationProfile;

    /**
     * The current status of this item within the ingest processing chain
     */
    private ProcessingStatus processingStatus;

    /**
     * The MD5 hash of the file content before it is transferred to final storage
     */
    @Column(length = 32)
    private String md5ChecksumBefore;

    /**
     * The MD5 hash of the file content after it has been moved to final storage
     *
     * Final storage transfer is handled by a separate service.
     */
    @Column(length = 32)
    private String md5ChecksumAfter;

    /**
     * The size of the file in bytes
     */
    private Long fileSize;

    /**
     * The raw result XML string of a request to the FITS examine service.
     *
     * Used for further processing after the masterfiles client has completed it's work and called back.
     *
     * The data is stored as a gzipped binary value.
     */
    @Lob
    @Convert(converter = StringCompressionConverter.class)
    private String rawFitsResult;

    /**
     * The metadata extracted from the filename of this processing item
     */
    @Lob
    @Convert(converter = FilenameMetaDataConverter.class)
    private FilenameMetaData filenameMetaData;

    /**
     * The response that the SLS file API returned regarding processing of this item
     */
    @Lob
    @Convert(converter = FileAPIResponseBodyConverter.class)
    private FileAPIResponseBody fileAPIResponse;


    /**
     * Create a new processing error flag and set it's status to ERROR.
     *
     *
     */
    @JsonIgnore
    public void setMessage(String message){
        IngestProcessingItemFlag flag = new IngestProcessingItemFlag(IngestProcessingItemFlag.Severity.ERROR, message);
        addErrorFlag(flag);
    }

    /**
     * Contains a list of errors or warnings for this item once it has been processed in the staging area.
     */
    @Convert(converter = IngestProcessingItemFlagListConverter.class)
    @Lob
    private List<IngestProcessingItemFlag> errors = new ArrayList<>();

    /**
     * URL that is used by the SLS File API to continue the processing of this item
     */
    @Column(length = 512)
    private String fileAPICallbackURL;

    public IngestProcessingItem(){}

    public IngestProcessingItem(IngestItem item, String token, ProcessingStatus processingStatus, IngestDigitizationProfile digitizationProfile){
        this(item, token, processingStatus);
        this.digitizationProfile = digitizationProfile;
    }

    public IngestProcessingItem(IngestItem item, String token, ProcessingStatus processingStatus){
        this.token = token;
        this.item = item;
        this.processingStatus = processingStatus;
    }

    public IngestProcessingItem(ProcessingStatus processingStatus, String message){
        this.processingStatus = processingStatus;
        this.setMessage(message);
    }

    @PrePersist
    protected void prePersistHandler(){
        updateProcessingTimestamps();
    }

    @PreUpdate
    protected void preUpdateHandler(){
        updateProcessingTimestamps();
        clearRawFitsResultsOnArchived();
    }

    /**
     * Clears the rawFitsResults data if the items processingStatus is ARCHIVED
     */
    protected void clearRawFitsResultsOnArchived() {
        if(processingStatus.equals(ProcessingStatus.ARCHIVED)){
            setRawFitsResult(null);
        }
    }

    /**
     * Update the timestamps for when the processing of this item has started and finished
     */
    protected void updateProcessingTimestamps(){
        if(this.getProcessingStatus().equals(ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE)){
            setProcessingStartedAt(Instant.now());
        } else if(this.getProcessingStatus().equals(ProcessingStatus.COMPLETE)){
            setProcessingCompletedAt(Instant.now());
        }
    }

    /**
     * Return the file path to the customer copy in a format useable by
     * filemaker when linking IntellectualEntityPart
     *
     * @return
     */
    @JsonIgnore
    public String getCustomerCopyFilePath() {
        String customerCopyFilePath = "";

        // if accessfiles has a value, we use that even if it's an empty array
        if(getFileAPIResponse() != null && getFileAPIResponse().getAccessFiles() != null && !getFileAPIResponse().getAccessFiles().isEmpty()){
            for(AccessFile accessFile : getFileAPIResponse().getAccessFiles()){
                if(accessFile.getFilePathFolder().equals(AccessFile.FOLDER_CUSTOMER_COPY)){
                    return accessFile.getNoAccessFilesFilePath();
                }
            }
        }
        // test the deprecated accessfilePaths property, in case we're dealing with an old ingest item
        else if(getFileAPIResponse() != null && getFileAPIResponse().getAccessfilePaths() != null && !getFileAPIResponse().getAccessfilePaths().isEmpty()){
            for(String filePath : getFileAPIResponse().getAccessfilePaths()){
                AccessFile accessFile = new AccessFile(filePath);
                if(accessFile.getFilePathFolder().equals(AccessFile.FOLDER_CUSTOMER_COPY)){
                    customerCopyFilePath = accessFile.getNoAccessFilesFilePath();
                    break;
                }
            }
        }

        return customerCopyFilePath;
    }

    /**
     * @return A new LoggableIngestProcessingItem populated with values from this IngestProcessingItem
     */
    @JsonIgnore
    public LoggableIngestProcessingItem getLoggableItem(){
        LoggableIngestProcessingItem rtn = new LoggableIngestProcessingItem();

        rtn.setFilenameMetaData(getFilenameMetaData());
        rtn.setMd5ChecksumAfter(getMd5ChecksumAfter());
        rtn.setMd5ChecksumBefore(getMd5ChecksumBefore());
        rtn.setProcessingStatus(getProcessingStatus());
        rtn.setToken(getToken());
        rtn.setItem(getItem());

        return rtn;
    }


    /**
     * Adds a flag to the list of errors if it is unique.
     *
     * Uniqueness is checked by comparing the error message string
     *
     * @param flag
     */
    @JsonIgnore
    public void addErrorFlag(IngestProcessingItemFlag flag){


        boolean hasDupes = false;

        for(IngestProcessingItemFlag f : errors){
            if(f.getMessage().equalsIgnoreCase(flag.getMessage())){
                hasDupes = true;
                break;
            }
        }

        if(!hasDupes){
            errors.add(flag);
        }

    }

    /**
     * Clears any existing error flags from this item
     */
    public void resetErrorFlags() {
        errors.clear();
    }
}
