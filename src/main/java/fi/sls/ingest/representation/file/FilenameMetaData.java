package fi.sls.ingest.representation.file;

import lombok.Data;
import org.springframework.util.StringUtils;

import java.io.Serializable;

@Data
public class FilenameMetaData implements Serializable {

    /**
     * The original filename that was parsed
     */
    protected String filename;

    /**
     * The file suffix for the parsed file
     */
    protected String fileSuffix;


    /**
     * The collection fileKey (Samling) that is extracted from the filename
     */
    protected String collectionKey;

    /**
     * The resolved collection name as defined in archive system based on collectionKey
     */
    protected String collectionName;

    /**
     * The resolved optional collection description as defined in archive system based on collectionKey
     *
     * This value is optional in the config, and only defined for some collections. It is needed
     * because the collection table in FileMaker defines some collection names with long text,
     * e.g. Tjänstearkivet instead of TA like the other collections. The signum again needs the TA
     * abbreviation, and not the long text collection name.
     */
    protected String collectionDescription;

    /**
     * The category of objects that this item belongs to, e.g. ljud, bild, etc
     */
    protected ObjectCategory category;

    /**
     * The archive id number as extracted from the filename
     */
    protected String archiveNr;

    /**
     * The optional archive catalog number as extracted from the filename
     */
    protected String archiveCatalogNr;

    /**
     * The archive signum number as extracted from the filename
     */
    protected int sigumNr;

    /**
     * The document page number as extracted from the filename
     */
    protected int pageNr;

    /**
     * The file version as extracted from the filename.
     *
     * If the version nr is missing from the filename, we assume 1.
     */
    protected int versionNr = 1;

    /**
     * Indicates if the versionNr given is a default one, or actually extracted from file name
     */
    protected boolean defaultVersionNr = false;

    /**
     * Marks if the file represents a Digital original (digitalt original) or not
     */
    protected boolean digitalOriginal = false;

    /**
     * The prefix used for generating the objectIdentifier and digitalObjectIdentifier
     */
    protected String identityPrefix = "SLS";

    /**
     * If a collection description has been set the method returns it,
     * or as a fallback returns the collection name.
     *
     * @return
     */
    public String getCollectionDescriptionOrName(){
        if(collectionDescription == null || collectionDescription.isEmpty()){
            return collectionName;
        } else {
            return collectionDescription;
        }
    }

    /**
     * Generates the Archive signum string based on the fields of this meta data object (intellectualEntities->dc_source in FileMaker).
     *
     * <p>Example 1: ÖTA 555 j:1:4 brev 3
     * <p>Example 2: ÖTA 582 av 30
     *
     * @return The generated archive signum string
     */
    public String getArchiveSignum(){

        String archiveCatalogNr = "";
        if(!StringUtils.isEmpty(this.getArchiveCatalogNr())){
            archiveCatalogNr = this.getArchiveCatalogNr().replace(".", ":");
            archiveCatalogNr = ":"+archiveCatalogNr;
        }

        return this.getCollectionName()+" "+this.getArchiveNrWithFormat()+archiveCatalogNr+" "+this.getCategory().getArchiveSignumKey() + " "+this.getSigumNr();
    }

    /**
     * Generates an Archive collection string based on fields of this metadata object (intellectualEntities->dcterms_isPartOf in FileMaker)
     *
     * <p>Example 1: ÖTA 555 j
     * <p>Example 2: FMI 563
     *
     * @return The generated Archive collection string
     */
    public String getArchiveCollectionName(){
        return this.getCollectionName()+" "+this.getArchiveNrWithFormat();
    }

    /**
     * If the archiveNr is in the format 123x where x is any alpha character,
     * this method adds a space between the nr and the character.
     *
     * <p>Example: 555j -> 555 j
     *
     * @return
     */
    protected String getArchiveNrWithFormat(){
        String[] parts = getArchiveNrParts();
        return String.join(" ", parts );
    }

    /**
     * Returns the digit part of the archiveNr
     *
     * <p>Example: 555j -> 555
     * @return
     */
    public Long getArchiveNrDigit(){
        String[] parts = getArchiveNrParts();
        return Long.parseLong(parts[0]);
    }

    /**
     * Returns the suffix part of the archiveNr
     * <p>Example: 555j -> j
     * @return
     */
    public String getArchiveNrSuffix() {
        String[] parts = getArchiveNrParts();
        if(parts.length > 1){
            return parts[1];
        } else {
            return null;
        }
    }


    private String[] getArchiveNrParts(){
        String source = this.getArchiveNr();
        return source.split("(?<=\\d)(?=\\D)|(?<=\\D)(?=\\d)");
    }


    /**
     * Returns the archiveCatalogNr with a given separator instead of default dot
     *
     * if archiveCatalogNr is not set, returns empty string
     *
     * @param separator The separator character to use
     * @return The formatted catalog nr, or empty string
     */
    protected String getArchiveCatalogNrWithSeparator(String separator, String prefix){
        String archiveCatalogNr = "";
        if(!StringUtils.isEmpty(this.getArchiveCatalogNr())){
            archiveCatalogNr = this.getArchiveCatalogNr().replace(".", separator);
            archiveCatalogNr = prefix+archiveCatalogNr;
        }
        return archiveCatalogNr;
    }

    protected String getArchiveCatalogNrWithSeparator(String separator) {
        return this.getArchiveCatalogNrWithSeparator(separator, "-");
    }

    /**
     * Generates the Archive entity identifier string based on the fields of this meta data object (entity_identifier in FileMaker)
     *
     * <p>Example 1: SLS:ota555j-1-4_ent.brev.3
     * <p>Example 2: SLS:ota579_ent.textil.27
     *
     * @return The Archive entity identifier
     */
    public String getEntityIdentifier(){
        return identityPrefix+":"+this.getCollectionKey()+this.getArchiveNr()+this.getArchiveCatalogNrWithSeparator("-")+"_ent."+this.getCategory().getFileKey() + "."+this.getSigumNr();
    }

    /**
     * Generates a Digital Object identifier based on the fields of this meta data object (digitalObjects->identifier in FileMaker)
     *
     * <p>Example 1: SLS:ota582_av.30-1-1.mp4
     * <p>Example 2: SLS:ota555j-1-4_brev.3-1-2_orig.tif
     *
     * @return The Digital Object identifier
     */
    public String getDigitalObjectIdentifier(){
        return getDigitalObjectIdentifierNoSuffix()+"."+this.getFileSuffix();
    }

    /**
     * Returns the digitalObjectIdentifier without file suffix.
     *
     * This is used for validation to ensure two processing items do not share the same identifier sans suffix
     * which would cause them to clash later in the process
     *
     * @return
     */
    public String getDigitalObjectIdentifierNoSuffix(){
        String doIdentifier = identityPrefix+":"+this.getCollectionKey()+this.getArchiveNr()+this.getArchiveCatalogNrWithSeparator("-")+"_"+this.getCategory().getFileKey() + "."+this.getSigumNr()+"-"+this.getPageNr()+"-"+this.getVersionNr();

        if(this.isDigitalOriginal()){
            doIdentifier = doIdentifier+"_orig";
        }

        return doIdentifier;
    }


    /**
     * Generates a dc_identifier string based on the fields in this metadata object (digitalObjects->dc_identifier in FileMaker)
     *
     * <p>Example 1: ota/ota555j/ota_555j.1.4_brev_00003_0002_01.tif
     * <p>Example 2: original/ota/ota555j/ota_555j.1.4_brev_00003_0001_02_orig.tif
     *
     * @return
     */
    public String getDcIdentifier(){
        String dcIdentifier = this.getCollectionKey()
                +"/"+this.getCollectionKey()+this.getArchiveNr()+"/"
                +this.getCollectionKey()+"_"+this.getArchiveNr()+this.getArchiveCatalogNrWithSeparator(".", ".")
                +"_"+this.getCategory().getFileKey()+"_"
                +String.format("%05d", this.getSigumNr())+"_"
                +String.format("%04d", this.getPageNr());

        if(!defaultVersionNr){
            dcIdentifier = dcIdentifier+"_"
            +String.format("%02d", this.getVersionNr());
        }

        if(this.isDigitalOriginal()){
            dcIdentifier = "original/"+dcIdentifier+"_orig";
        }

        dcIdentifier = dcIdentifier+"."+this.getFileSuffix();

        return dcIdentifier;
    }

    /**
     *
     * @return Always either "masterfil" or "digitalt original" depending on isDigitalOriginal value (true/false)
     */
    public String getEntityType(){
        return this.isDigitalOriginal() ? "digitalt original" : "masterfil";
    }

    public FilenameMetaData(){
    }

    public FilenameMetaData(String filename){
        this.setFilename(filename);
    }

    public FilenameMetaData(String filename, String identityPrefix) {
        this(filename);
        this.identityPrefix = identityPrefix;
    }
}
