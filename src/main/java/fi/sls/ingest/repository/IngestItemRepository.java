package fi.sls.ingest.repository;

import fi.sls.ingest.actuator.metrics.IngestProcessingStatusCount;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface IngestItemRepository extends PagingAndSortingRepository<IngestProcessingItem, Long> {

    void deleteByTokenIn(ArrayList<String> tokens);

    List<IngestProcessingItem> findByProcessingStatus(ProcessingStatus status);
    //List<IngestProcessingItem> findByProcessingStatusNot(ProcessingStatus status);
    Page<IngestProcessingItem> findByProcessingStatusNot(ProcessingStatus status, Pageable pageable);
    //List<IngestProcessingItem> findByProcessingStatusIn(List<ProcessingStatus> status);
    Page<IngestProcessingItem> findByProcessingStatusIn(List<ProcessingStatus> status, Pageable pageable);
    List<IngestProcessingItem> findByProcessingStatusIn(List<ProcessingStatus> status);
    List<IngestProcessingItem> findByCreatedByIdIn(List<Long> filterByUserId);
    List<IngestProcessingItem> findByProcessingStatusNotAndCreatedByIdIn(ProcessingStatus archived, List<Long> userIdList);
    //List<IngestProcessingItem> findByProcessingStatusNotAndCreatedByIdIn(ProcessingStatus archived, List<Long> userIdList, Pageable pageable);
    Page<IngestProcessingItem> findByProcessingStatusNotAndCreatedByIdIn(ProcessingStatus archived, List<Long> userIdList, Pageable pageable);
    List<IngestProcessingItem> findByProcessingStatusInAndCreatedByIdIn(List<ProcessingStatus> filterByStatus, List<Long> filterByUserId);
    //List<IngestProcessingItem> findByProcessingStatusInAndCreatedByIdIn(List<ProcessingStatus> filterByStatus, List<Long> filterByUserId, Pageable pageable);
    Page<IngestProcessingItem> findByProcessingStatusInAndCreatedByIdIn(List<ProcessingStatus> filterByStatus, List<Long> filterByUserId, Pageable pageable);

    Optional<IngestProcessingItem> findByToken(String token);

    @Query(value = "SELECT COUNT(item) FROM IngestProcessingItem item WHERE item.processingStatus = :filterOnStatus")
    long countByProcessingStatus(ProcessingStatus filterOnStatus);

    @Query(value = "SELECT COUNT(item) FROM IngestProcessingItem item WHERE item.processingStatus IN (:filterOnStatus)")
    Optional<Long> countByProcessingStatusIn(List<ProcessingStatus> filterOnStatus);



    @Query(value = "SELECT COUNT(item) FROM IngestProcessingItem item WHERE item.processingStatus = :filterOnStatus AND item.createdBy.id IN (:userIdList)")
    long countByProcessingStatusAndUser(ProcessingStatus filterOnStatus, List<Long> userIdList);


    @Query(value = "SELECT new fi.sls.ingest.actuator.metrics.IngestProcessingStatusCount(item.processingStatus, COUNT(item)) FROM IngestProcessingItem item GROUP BY item.processingStatus")
    List<IngestProcessingStatusCount> countAllProcessingStatus();

    @Query(value = "SELECT SUM(item.fileSize) as totalBytesProcessed FROM IngestProcessingItem WHERE item.processingStatus = 200 OR item.processingStatus = 999")
    Optional<Long> getTotalBytesProcessed();

    @Query(value = "SELECT AVG(TIME_TO_SEC(TIMEDIFF(item.processingCompletedAt, item.processingStartedAt))) as average FROM IngestProcessingItem WHERE item.processingStatus = 200 OR item.processingStatus = 999")
    Optional<Double> getAverageProcessingDuration();


}
