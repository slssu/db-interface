package fi.sls.ingest.migration.generator.fieldstrategy;

import lombok.Getter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Strategy for extracting a String value from the given input based on the regex passed into the constructor
 */
public class StringRegexStrategy implements FieldGeneratorStrategy<String, String> {

    protected Pattern regexPattern;

    @Getter
    protected String sourceObjectMethodName;

    public StringRegexStrategy(String regex){
        this(regex, null);
    }

    public StringRegexStrategy(String regex, String sourceObjectMethodName){
        regexPattern = Pattern.compile(regex);
        this.sourceObjectMethodName = sourceObjectMethodName;
    }

    @Override
    public String generate(String source) {

        if(source == null){
            return null;
        }

        Matcher m = regexPattern.matcher(source);

        if(m.find() && m.group(1).length() > 0){
            String res = m.group(1);
            return res;
        }

        return null;
    }
}
