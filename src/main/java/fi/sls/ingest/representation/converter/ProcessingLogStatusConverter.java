package fi.sls.ingest.representation.converter;

import fi.sls.ingest.manager.ProcessingStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ProcessingLogStatusConverter implements AttributeConverter<ProcessingStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(ProcessingStatus status) {
        return status.getStatusCode();
    }

    @Override
    public ProcessingStatus convertToEntityAttribute(Integer dbData) {
        return ProcessingStatus.fromStatusCode(dbData);
    }
}
