package fi.sls.ingest.fits.model.fieldresolver.image;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import org.w3c.dom.Element;

import java.util.Optional;

/**
 * Resolver that fetches the image data from the fits->metadata->image structure of the FITS result
 */
public class FitsImageMetadata extends AbstractFieldValueResolver {

    public final static String FIELD_IMAGE_WIDTH = "imageWidth";
    public final static String FIELD_IMAGE_HEIGHT = "imageHeight";
    public final static String FIELD_BITS_PER_SAMPLE = "bitsPerSample";
    public final static String FIELD_X_SAMPLING_FREQUENCY = "xSamplingFrequency";
    public final static String FIELD_COLOR_SPACE = "colorSpace";
    public final static String FIELD_ICC_PROFILE_NAME = "iccProfileName";
    public final static String FIELD_SCANNING_SOFTWARE_NAME = "scanningSoftwareName";


    public FitsImageMetadata(String fieldName, FitsResult fitsResult){
        super(fieldName, fitsResult);
    }

    public FitsImageMetadata(String fieldName, FieldValueResolver resolver){
        this(fieldName, resolver.getFitsResult(), resolver);
    }

    /**
     *
     * @param fitsResult
     * @param resolver
     */
    public FitsImageMetadata(String fieldName, FitsResult fitsResult, FieldValueResolver resolver){
        super(fieldName, fitsResult, resolver);
    }

    @Override
    public Optional<String> getValue() {
        if(fitsResult.getFitsResult().getMetadata() != null && fitsResult.getFitsResult().getMetadata().getImage() != null){

            Optional<Element> item = fitsResult.getFitsResult().
                    getMetadata().getImage().
                    getAny().
                    stream().
                    filter(
                            element ->
                                    element.getTagName() == fieldName

                    ).findFirst();

            if(item.isPresent() && !item.get().getTextContent().isEmpty()){
                return Optional.of(item.get().getTextContent());
            }
        }

        return executeNextResolver();

    }

}
