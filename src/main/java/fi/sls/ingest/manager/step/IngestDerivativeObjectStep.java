package fi.sls.ingest.manager.step;

import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.DerivateObject;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntity;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import fi.sls.ingest.representation.file.FilenameMetaData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Slf4j
@Service
public class IngestDerivativeObjectStep extends RecordServiceStep {
    public IngestDerivativeObjectStep(RecordService service) {
        super(service);
    }

    /**
     * Performs the tasks of this step
     *
     * @return The created or updated MTDerivateObjectsDO
     */
    public DerivateObject executeStep(AccessFile accessFile, FilenameMetaData metaData, IntellectualEntity intellectualEntity) throws IOException {

        DerivateObject record = new DerivateObject();
        record.getFieldData().setFilePath(accessFile.getStrippedFilePath());
        record.getFieldData().setIsGeneratedFrom(metaData.getDigitalObjectIdentifier());
        record.getFieldData().setFilePathFolder(accessFile.getFilePathFolder());


        Optional<? extends FileMakerResponseEntity> existingItem;
//        jw 2022-02-14: remove try-catch as runnable will handle all exceptions
//        try {
            existingItem = service.findRecordByIdentifier(record);
            if(existingItem.isPresent()){
                record = (DerivateObject) existingItem.get();
            }

            record.populateFromMetadataResults(accessFile, metaData);
            setIntellectualEntityNumber(accessFile, record, intellectualEntity.getFieldData().getEntityNumber());

            // clear the cFilePathFolder because it's calculated and can't be sent back
            record.getFieldData().setCFilePathTotal(null);

            if(record.isNewRecord()){
                record = (DerivateObject) service.createObjectRecord(record);
                log.info("Created new DerivateObject with identifier {} having id {}",
                        metaData.getDigitalObjectIdentifier(),
                        record.getFieldData().getId()
                );

            } else {
                record = (DerivateObject) service.updateObjectRecord(record);
                log.info("DerivativeObject with identifier {} already exists with id {}, updated",
                        metaData.getDigitalObjectIdentifier(),
                        record.getFieldData().getId()
                );
            }

            // need to do a second update here if thumbnail or database, because a value needs to be copied from one field
            // to another in the same table [sic]
            if(accessFile.getFilePathFolder().equals(AccessFile.FOLDER_DATABASE)
                    || accessFile.getFilePathFolder().equals(AccessFile.FOLDER_THUMBNAIL)
            ){
                // set either thumbnail or database pic link
                if(accessFile.getFilePathFolder().equals(AccessFile.FOLDER_DATABASE)) {
                    record.getFieldData().setDatabasbild(record.getFieldData().getCFilePathTotal());
                } else if(accessFile.getFilePathFolder().equals(AccessFile.FOLDER_THUMBNAIL)) {
                    record.getFieldData().setThumbnail(record.getFieldData().getCFilePathTotal());
                }
                // clear the cFilePathFolder because it's calculated and can't be sent back
                record.getFieldData().setCFilePathTotal(null);
                record = (DerivateObject) service.updateObjectRecord(record);
                log.info("DerivativeObject updated with new image {}",
                        kv("derivative_object_id", record.getFieldData().getId())
                );

                // also need to update the IE to complete the link
                // (do this with new instance so only fields we actually want to update get sent)
                IntellectualEntity updateEntity = new IntellectualEntity();
                updateEntity.setRecordId(intellectualEntity.getRecordId());
                // doing the if check again because we want to be sure we're using data from the most recent version of "record"
                if(accessFile.getFilePathFolder().equals(AccessFile.FOLDER_DATABASE)) {
                    updateEntity.getFieldData().setDatabasbild(record.getFieldData().getDatabasbild());
                } else if(accessFile.getFilePathFolder().equals(AccessFile.FOLDER_THUMBNAIL)) {
                    updateEntity.getFieldData().setThumbnail(record.getFieldData().getThumbnail());
                }

                intellectualEntity = (IntellectualEntity) service.updateObjectRecord(updateEntity);
                log.debug("Updated IntellectualEntity image with id: {}",
                        kv("intellectual_entity_id", intellectualEntity.getRecordId())
                );

            }

            return record;
//        } catch (IOException e) {
//            log.error(e.toString());
//            // FIXME should we throw instead?
//            return null;
//        }
    }

    protected void setIntellectualEntityNumber(AccessFile accessFile, DerivateObject record, Long ieEntityNumber){
        if(accessFile.getFilePathFolder().equals(AccessFile.FOLDER_DATABASE)){
            record.getFieldData().setDatabasbild_ie(ieEntityNumber);
        } else if(accessFile.getFilePathFolder().equals(AccessFile.FOLDER_THUMBNAIL)){
            record.getFieldData().setIe_thumb(ieEntityNumber);
        }
    }

}
