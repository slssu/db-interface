package fi.sls.ingest.proxy.slsfileapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.config.SLSFileAPIConfig;
import fi.sls.ingest.proxy.slsfileapi.exception.FileAPIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Implements methods for calling SLS file API
 */
@Service
@Primary
public class SLSFileAPI {

    RestTemplate restTemplate;
    SLSFileAPIConfig fileAPIConfig;
    ObjectMapper objectMapper;

    @Autowired
    public SLSFileAPI(
        RestTemplate restTemplate,
        SLSFileAPIConfig fileAPIConfig,
        ObjectMapper objectMapper
    ){
        this.restTemplate = restTemplate;
        this.fileAPIConfig = fileAPIConfig;
        this.objectMapper = objectMapper;
    }


    /**
     * Initiates a file move process on the API
     *
     * @param requestBody
     * @return
     */
    public FileAPIQueueResponseBody move(FileAPIMoveRequestBody requestBody){
        return makePostRequest(requestBody, fileAPIConfig.getEndpointURL("/move/"));
    }

    /**
     * Initiates a file migrate process on the API
     *
     * @param requestBody
     * @return
     */
    public FileAPIQueueResponseBody migrate(FileAPIMigrateRequestBody requestBody){
        return makePostRequest(requestBody, fileAPIConfig.getEndpointURL("/migrate/"));
    }

    /**
     * Initiates re-generating access files for a given file on the API
     *
     * @param requestBody
     * @return
     */
    public FileAPIQueueResponseBody accessfiles(FileAPIRequestBody requestBody){
        return makePostRequest(requestBody, fileAPIConfig.getEndpointURL("/accessfiles/"));
    }

    public String healthcheck(){
        return makeGetRequest(fileAPIConfig.getEndpointURL("/healthcheck/"));
    }

    protected String makeGetRequest(String endpoint) {
        HttpEntity<String> request = new HttpEntity<>("", createAPIHeaders());

        try {
            ResponseEntity<String> response = restTemplate.exchange(
                    endpoint,
                    HttpMethod.GET, request, String.class
            );

            if(response.getStatusCode() == HttpStatus.OK){
                return response.getBody();
            } else {
                throw new FileAPIException(response.getBody().toString());
            }
        } catch(RestClientException e){
            throw new FileAPIException(e.getMessage());
        }
    }

    /**
     * Makes the actual request to the API using given request body and endpoint URL
     *
     * @param requestBody
     * @param endpoint
     * @return
     */
    protected FileAPIQueueResponseBody makePostRequest(FileAPIRequestBody requestBody, String endpoint){
        HttpEntity<FileAPIRequestBody> request = new HttpEntity<>(requestBody, createAPIHeaders());

        try {
            ResponseEntity<FileAPIQueueResponseBody> response = restTemplate.exchange(
                    endpoint,
                    HttpMethod.POST, request, FileAPIQueueResponseBody.class
            );

            if(response.getStatusCode() == HttpStatus.OK){
                return response.getBody();
            } else {
                throw new FileAPIException(response.getBody().toString());
            }
        } catch(RestClientException e){
            throw new FileAPIException(e.getMessage());
        }
    }

    /**
     * Makes an authentication request and adds the given token to the header object
     *
     * Sets other default headers for requests against the FileMaker API
     *
     * @return
     */
    protected HttpHeaders createAPIHeaders(){
        return new HttpHeaders() {{
            setContentType(MediaType.APPLICATION_JSON);
        }};
    }
}
