package fi.sls.ingest.representation.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringCompressionConverterTest {

    StringCompressionConverter testable;

    @BeforeEach
    void setUp() {
        testable = new StringCompressionConverter();
    }

    @Test
    void convertToDatabaseColumn_returnsNullForNullOrEmpty() {
        assertNull(testable.convertToDatabaseColumn(null));
        assertNull(testable.convertToDatabaseColumn(""));
    }

    @Test
    void convertToEntityAttribute_returnsNullForNullInput() {
        assertNull(testable.convertToEntityAttribute(null));
    }
}