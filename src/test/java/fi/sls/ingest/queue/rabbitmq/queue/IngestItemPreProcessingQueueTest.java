package fi.sls.ingest.queue.rabbitmq.queue;

import fi.sls.ingest.queue.rabbitmq.queue.IngestItemPreProcessingQueue;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IngestItemPreProcessingQueueTest {

    @Test
    void constructor_createsQueueWithCorrectName(){
        IngestItemPreProcessingQueue testable = new IngestItemPreProcessingQueue("test");

        assertEquals("test", testable.getName());
    }
}