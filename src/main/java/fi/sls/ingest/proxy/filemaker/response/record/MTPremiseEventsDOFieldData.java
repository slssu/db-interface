package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.sls.ingest.proxy.filemaker.request.RecordPayload;
import lombok.Data;

@Data
public class MTPremiseEventsDOFieldData implements RecordPayload {

    /**
     * A created as a combination of
     *
     *  the string "cr:" + date part of DigitalObjectFieldData#created + DigitalObjectFieldData#digitizationProfileId
     */
    @JsonProperty("event_ID")
    private String eventId;

    /**
     * Helper to generate the correct eventId field value
     * @param data
     */
    public void setEventIdFrom(DigitalObjectFieldData data){
        this.eventId = "cr:" + data.getDateCreated() + data.getDigitizationProfileId();
    }


    /**
     * Foreign key to the DigitalObject that this event relates to
     *
     * @see DigitalObjectFieldData#entityNumber
     */
    @JsonProperty("object_nummer")
    private Long objectNumber;

    /**
     * This field seems to be used by scripts to track which MTPremiseEventDO objects have not been linked
     * to a premisEvent yet.
     *
     * Seems to always be the value "x" if in use, but reset immediately once linked. We can handle this a bit cleaner
     * since we have the object we work with in memory already.
     */
    private String temp;

    /**
     * The value of the related DigitalObjectFieldData#digitizationProfileId field
     */
    @JsonProperty("temp_digiprofil")
    private String tempDigitizationProfileId;

    /**
     * The value of the related DigitalObjectFieldData#calculatedDateCreated field
     */
    @JsonProperty("temp_ev_date")
    private String tempEventDate;
}
