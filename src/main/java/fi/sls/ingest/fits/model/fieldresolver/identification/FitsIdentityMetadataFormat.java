package fi.sls.ingest.fits.model.fieldresolver.identification;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.AbstractFieldValueResolver;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;

import java.util.Optional;

public class FitsIdentityMetadataFormat extends AbstractFieldValueResolver {

    public FitsIdentityMetadataFormat(FieldValueResolver resolver) {
        super("format", resolver);
    }

    public FitsIdentityMetadataFormat(FitsResult fitsResult) {
        super("format", fitsResult);
    }

    public FitsIdentityMetadataFormat(FitsResult fitsResult, FieldValueResolver resolver) {
        super("format", fitsResult, resolver);
    }

    @Override
    public Optional<String> getValue() {

        if(fitsResult.getFitsResult().getIdentification().getIdentity().size() > 0){
            String rtn = fitsResult.getFitsResult().getIdentification().getIdentity().get(0).getFormat();

            if(rtn != null && !rtn.isEmpty()){
                return Optional.of(rtn);
            }
        }

        return executeNextResolver();
    }
}
