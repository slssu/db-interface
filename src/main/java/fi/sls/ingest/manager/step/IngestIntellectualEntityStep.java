package fi.sls.ingest.manager.step;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.proxy.filemaker.response.FileMakerResponseEntity;
import fi.sls.ingest.proxy.filemaker.response.record.IntellectualEntity;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import fi.sls.ingest.representation.file.FilenameMetaData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@Service
public class IngestIntellectualEntityStep extends RecordServiceStep {

    @Autowired
    public IngestIntellectualEntityStep(RecordService service) {
        super(service);
    }

    /**
     * Performs the tasks of this step
     *
     * @return The created or updated IntellectualEntity
     */
    public IntellectualEntity executeStep(FilenameMetaData metaData, FitsResult fitsResult) throws IOException {

        IntellectualEntity ie = new IntellectualEntity();
        ie.getFieldData().setEntityIdentifier(metaData.getEntityIdentifier());

        Optional<? extends FileMakerResponseEntity> existingItem;
        // jw 2022-02-14: remove try-catch as runnable will handle all exceptions
//        try {
            existingItem = service.findRecordByIdentifier(ie);
            if(existingItem.isPresent()){
                ie = (IntellectualEntity) existingItem.get();

                // removed updating fields as of discussion w. Rasmus on Slack 2020-09-23: Archive specifies no updates should happen to existing intellectual entities at any time.
//                ie.populateFromMetadataResults(metaData, fitsResult);
//
//                // value is calculated field in FileMaker, sending it back causes error so nullify
//                if(ie.getFieldData().getEntityNumber() != null && ie.getFieldData().getEntityNumber() > 0){
//                    ie.getFieldData().setEntityNumber(null);
//                }
//
//                ie = (IntellectualEntity) service.updateObjectRecord(ie);

                log.info("IntellectualEntity with identifier {} already exists with number {}",
                        ie.getFieldData().getEntityIdentifier(),
                        ie.getFieldData().getEntityNumber()
                );
            } else {
                ie.populateFromMetadataResults(metaData, fitsResult);

                ie = (IntellectualEntity) service.createObjectRecord(ie);

                log.info("Created new IntellectualEntity with identifier {} having number {}",
                        ie.getFieldData().getEntityIdentifier(),
                        ie.getFieldData().getEntityNumber()
                );
            }
            return ie;
//        } catch (IOException e) {
//            log.error(e.toString());
//            // FIXME should we throw instead?
//            return null;
//        }
    }
}