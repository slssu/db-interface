package fi.sls.ingest.migration.runnable;

import fi.sls.ingest.exception.MigrationProcessingException;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.manager.step.*;
import fi.sls.ingest.migration.DigitalObjectMigrationResult;
import fi.sls.ingest.proxy.filemaker.request.FindQueryRequest;
import fi.sls.ingest.proxy.filemaker.response.FileMakerArrayDataResponse;
import fi.sls.ingest.proxy.filemaker.response.record.*;
import fi.sls.ingest.proxy.filemaker.service.RecordService;
import fi.sls.ingest.proxy.slsfileapi.AccessFile;
import fi.sls.ingest.repository.DigitalObjectMigrationResultRepository;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.ws.SocketMessenger;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.Socket;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
@Data
public class MigrationPostProcessRunnable implements Runnable {

    /**
     * Service for accessing records in FileMaker
     */
    protected RecordService recordService;

    /**
     * The repository for storing results for each migration item
     */
    protected DigitalObjectMigrationResultRepository migrationResultRepository;

    protected IngestDerivativeObjectStep ingestDerivativeObjectStep;
    protected LinkDerivativeObjectDigitalObjectStep linkDerivativeObjectDigitalObjectStep;
    protected LinkPremisEventDigitalObjectStep linkPremisEventDigitalObjectStep;
    protected IngestPremiseEventStep ingestPremiseEventStep;
    protected LinkAgentStep linkAgentStep;

    /**
     * Clock instance to use for timestamps of events (injected for testability)
     */
    private Clock clock;

    /**
     * Sends out updates on migration items during processing
     */
    protected SocketMessenger socketMessenger;

    @Autowired
    public MigrationPostProcessRunnable(
            SocketMessenger socketMessenger,
            RecordService recordService,
            DigitalObjectMigrationResultRepository migrationResultRepository,
            IngestDerivativeObjectStep ingestDerivativeObjectStep,
            LinkDerivativeObjectDigitalObjectStep linkDerivativeObjectDigitalObjectStep,
            LinkPremisEventDigitalObjectStep linkPremisEventDigitalObjectStep,
            IngestPremiseEventStep ingestPremiseEventStep,
            LinkAgentStep linkAgentStep,
            Clock clock

    ){
        this.socketMessenger = socketMessenger;
        this.recordService = recordService;
        this.migrationResultRepository = migrationResultRepository;
        this.ingestDerivativeObjectStep = ingestDerivativeObjectStep;
        this.linkDerivativeObjectDigitalObjectStep = linkDerivativeObjectDigitalObjectStep;
        this.linkPremisEventDigitalObjectStep = linkPremisEventDigitalObjectStep;
        this.ingestPremiseEventStep = ingestPremiseEventStep;
        this.linkAgentStep = linkAgentStep;
        this.clock = clock;
    }

    /**
     * The processing item that contains the data to store back into FileMaker
     */
    protected DigitalObjectMigrationResult item;

    @Override
    public void run() {

        log.info("Starting post-processing of migration item {}",
                kv(LogMessageKey.MIGRATION_PROCESSING_ITEM, item)
        );

        DigitalObject digitalObject = new DigitalObject();
        IntellectualEntity intellectualEntity = new IntellectualEntity();
        // load digitalObject and IntellectualEntity from recordService using layout and recordId to ensure they exists
        try {
            digitalObject = (DigitalObject) recordService.getObjectRecord(digitalObject, item.getDigitalObjectRecordId());
            intellectualEntity = (IntellectualEntity) recordService.getObjectRecord(intellectualEntity, item.getIntellectualEntityRecordId());

            if(digitalObject != null && intellectualEntity != null){
                // set the new value and store back
                digitalObject.getFieldData().setDcIdentifier(item.getMigratedDcIdentifier());
                digitalObject = (DigitalObject) recordService.updateObjectRecord(digitalObject);

                // remove existing derivativeObjects + derivativeObjectLinks
                unlinkAccessFiles(digitalObject);

                // connect the access files to the digital object
                linkAccessFiles(digitalObject, intellectualEntity, item.getFilenameMetaData());

                // add migration PremisEvent
                linkPremisEvent(
                        digitalObject,
                        MTPremiseEventDOType.MIGRATION,
                        PremisEventType.MIGRATION,
                        LocalDate.now(clock).format(DateTimeFormatter.ofPattern("MM/dd/yyyy")),
                        LocalDate.now(clock).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                        String.format("dc_identifier ändrad från %s till %s, samt filen flyttad till motsvarande plats på masterfiles. Genererat nya användarkopior.",
                                item.getOriginalDcIdentifier(),
                                item.getMigratedDcIdentifier()
                        )
                );

                // mark item as complete
                item.setProcessingStatus(ProcessingStatus.COMPLETE);
                item.setError(null);
                item.setProcessingCompletedAt(Instant.now());
                migrationResultRepository.save(item);
                socketMessenger.broadcast(item);

            } else if(digitalObject == null){
                throw new MigrationProcessingException(String.format("Could not find digitalObject with recordId %d", item.getDigitalObjectRecordId()));
            } else {
                throw new MigrationProcessingException(String.format("Could not find intellectualEntity with recordId %d", item.getIntellectualEntityRecordId()));
            }
        } catch (IOException e) {
            log.warn("Migration failed for item with token {}",
                    kv(LogMessageKey.MIGRATION_ITEM_TOKEN, item.getToken()));
            item.setProcessingStatus(ProcessingStatus.ERROR);
            item.setError(e.getMessage());
            migrationResultRepository.save(item);
            socketMessenger.broadcast(item);
        }
    }

    /**
     * Helper for linking the access files to the digital object and intellectual entity
     *
     * @param digitalObject
     * @param intellectualEntity
     * @param metaData
     */
    protected void linkAccessFiles(DigitalObject digitalObject, IntellectualEntity intellectualEntity, FilenameMetaData metaData) throws IOException {

        AtomicReference<IntellectualEntity> wrappedIntellectualEntity = new AtomicReference<>();
        wrappedIntellectualEntity.set(intellectualEntity);

        for(AccessFile file: item.getFileAPIResponse().getAccessFiles()) {
        //item.getFileAPIResponse().getAccessFiles().forEach((AccessFile file) -> {

            DerivateObject derivateObject = ingestDerivativeObjectStep.executeStep(file, metaData, intellectualEntity);

            // link DerivativeObject (anvandarkopior) to DigitalObject (teknisk_data) through mt_derivativeObjects_DO
            linkDerivativeObjectDigitalObjectStep.executeStep(derivateObject, digitalObject);

        }
    }

    public FileMakerArrayDataResponse<MTDerivateObjectsDO> findDerivativesForDigitalObject(DigitalObject digitalObject) throws IOException {
        //FindQueryRequest queryRequest = new FindQueryRequest("do_nummer", digitalObject.getFieldData().getEntityNumber().toString());
        MTDerivateObjectsDO query = new MTDerivateObjectsDO();
        query.getFieldData().setDoNumber(digitalObject.getFieldData().getEntityNumber());

        FileMakerArrayDataResponse<MTDerivateObjectsDO> result = (FileMakerArrayDataResponse<MTDerivateObjectsDO>) recordService.findRecords(query, 0L);

        return result;
    }


    public Optional<DerivateObject> findDerivativeObjectForMTObject(MTDerivateObjectsDO mtDerivateObjectsDO) throws IOException {
        FindQueryRequest findQuery = new FindQueryRequest("id", mtDerivateObjectsDO.getFieldData().getDerNumber());
        DerivateObject derivateObject = new DerivateObject();

        Optional<DerivateObject> obj = (Optional<DerivateObject>) recordService.findRecordByQuery(derivateObject, findQuery);

        return obj;
    }

    public List<AccessFile> findAccessFilesForDigitalObject(DigitalObject digitalObject) throws IOException {
        MTDerivateObjectsDO query = new MTDerivateObjectsDO();
        query.getFieldData().setDoNumber(digitalObject.getFieldData().getEntityNumber());

        FileMakerArrayDataResponse<MTDerivateObjectsDO> result = (FileMakerArrayDataResponse<MTDerivateObjectsDO>) recordService.findRecords(query, 0L);

        List<AccessFile> rtn = new ArrayList<>();

        if(result != null){
            Iterable<MTDerivateObjectsDO> i = result.getResponse().getData();

            i.forEach(item -> {
                try {
                    Optional<DerivateObject> obj = findDerivativeObjectForMTObject(item);
                    if(obj.isPresent()){
                        String realPath = obj.get().getFieldData().getFilePathFolder().concat("/").concat(obj.get().getFieldData().getFilePath());
                        AccessFile f = new AccessFile(realPath, obj.get().getFieldData().getFileType());
                        rtn.add(f);
                    }
                } catch (IOException e) {
                    log.error(e.getMessage());
                    return;
                }
            });
        }

        return rtn;

    }

    /**
     * Helper for removing the existing accessfiles for a digital object before adding new ones
     * @param digitalObject
     */
    protected void unlinkAccessFiles(DigitalObject digitalObject) throws IOException {

        FileMakerArrayDataResponse<MTDerivateObjectsDO> result = findDerivativesForDigitalObject(digitalObject);

        if(result != null){
            Iterable<MTDerivateObjectsDO> i = result.getResponse().getData();

            i.forEach(item -> {
                // fetch the derivative object to verify it still exists
                //FindQueryRequest findQuery = new FindQueryRequest("id", item.getFieldData().getDerNumber());
                try {
                    //DerivateObject derivateObject = new DerivateObject();

                    //Optional<DerivateObject> obj = (Optional<DerivateObject>) recordService.findRecordByQuery(derivateObject, findQuery);
                    Optional<DerivateObject> obj = findDerivativeObjectForMTObject(item);

                    if(obj.isPresent()){
                        // delete the related derivativeObject
                        recordService.deleteObjectRecord(obj.get());
                        log.info("Deleted derivativeObject with recordId {} {}",
                                kv("record_id", obj.get().getRecordId()),
                                kv("der_id", obj.get().getFieldData().getId())
                        );

                        // delete the link
                        recordService.deleteObjectRecord(item);
                        log.info("Deleted MTDerivateObjectsDO with recordId {} {} {}",
                                kv("record_id", item.getRecordId()),
                                kv("mtNummer", item.getFieldData().getId()),
                                kv("mtDerObj_id", item.getFieldData().getDerNumber())
                        );
                    }
                } catch (IOException e) {
                    log.error(e.getMessage());
                    return;
                }
            });
        } else {
            log.debug("Did not find a MTDerivateObjectsDO entry for digitalObject {}",
                    kv("digitalObject_entityNumber", digitalObject.getFieldData().getEntityNumber())
            );
        }


    }

    /**
     * Helper for adding a PremisEvent to the digital object
     * @param digitalObject
     * @param mtPremisEventDOType
     * @param premisEventType
     * @param eventDate
     * @param eventOutcomeNote
     * @throws IOException
     */
    protected void linkPremisEvent(DigitalObject digitalObject,
                                   MTPremiseEventDOType mtPremisEventDOType,
                                   PremisEventType premisEventType,
                                   String eventDate,
                                   String createDate,
                                   String eventOutcomeNote) throws IOException
    {

        // link PremiseEvent to DigitalObject
        MtPremiseEventsDO mtPremiseEventsDO = linkPremisEventDigitalObjectStep.executeStep(digitalObject, mtPremisEventDOType, eventDate, createDate);
        // Create event for the link
        PremiseEvent premiseEvent = ingestPremiseEventStep.executeStep(digitalObject, mtPremiseEventsDO, mtPremisEventDOType, premisEventType, eventDate, createDate, eventOutcomeNote);
        // Link the API Agent to the event
        Agent existingAgent = linkAgentStep.executeStep(premiseEvent);
    }
}
