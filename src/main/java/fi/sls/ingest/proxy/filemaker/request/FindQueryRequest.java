package fi.sls.ingest.proxy.filemaker.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import fi.sls.ingest.proxy.filemaker.request.parameters.SortOrder;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Data
public class FindQueryRequest {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<Map<String, String>> query = new ArrayList<Map<String, String>>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<Map<String, String>> sort = new ArrayList<>();

    /**
     * Nr of records to return for a find request
     */
    private String limit = "500";

    /**
     * Offset from which to start returning results
     */
    private String offset = "1";


    public FindQueryRequest(){}

    /**
     * Shortcut constructor to create a query with a single field and value
     *
     * @param fieldName The field on which to search
     * @param value The value to find. It should include the FileMaker operator as well, e.g. =1234
     */
    public FindQueryRequest(String fieldName, String value){
        addQueryItem(fieldName, value);
    }


    public void addQueryItem(String fieldName, String value){
        Map<String, String> item = new HashMap<String, String>();

        item.put(fieldName, value);

        this.query.add(item);
    }

    /**
     * Add a filter to an existing query item.
     *
     * This effectively creates a AND query against FileMaker.
     *
     * @param atIndex
     * @param fieldName
     * @param value
     */
    public void addToExistingQueryItem(int atIndex, String fieldName, String value){
        query.get(atIndex).put(fieldName, value);
    }

    public void addSortItem(String fieldName, SortOrder sortOrder){

        Map<String, String> fieldItem = new HashMap<>();
        fieldItem.put("fieldName", fieldName);
        fieldItem.put("sortOrder", sortOrder.toString());

        this.sort.add(fieldItem);

    }
}
