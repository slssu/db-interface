package fi.sls.ingest.controller.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationRequest implements Serializable {

    @NotBlank
    private String username;
    @NotBlank
    private String password;

    /**
     * Returns the part of the username up to the first @-sign
     *
     * E.g. mister@sls.fi -> mister
     *
     * @return
     */
    public String getNoSuffixUsername(){
        if(getUsername() == null){
            return null;
        }

        if(getUsername().indexOf("@") > -1){
            return getUsername().substring(0, getUsername().indexOf("@"));
        } else {
            return getUsername();
        }
    }
}
