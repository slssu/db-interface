#
# Add table for handling migration processing
#

CREATE TABLE `digital_object_migration_result` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `archive_nr` bigint(20) DEFAULT NULL,
  `digital_object_record_id` bigint(20) DEFAULT NULL,
  `dry_run` bit(1) NOT NULL,
  `error` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileapicallbackurl` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileapiresponse` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filename_meta_data` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `intellectual_entity_record_id` bigint(20) DEFAULT NULL,
  `migrated_dc_identifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_dc_identifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `processing_completed_at` datetime DEFAULT NULL,
  `processing_started_at` datetime DEFAULT NULL,
  `processing_status` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5gkkamrouomg84psiafja8ckc` (`token`),
  KEY `FK9kqop33po3gk34vixnednngwq` (`created_by_id`),
  CONSTRAINT `FK9kqop33po3gk34vixnednngwq` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

# Add index on processing_status and digital_object_record_id
CREATE INDEX `idx_processing_status` ON `digital_object_migration_result` (`processing_status`);
CREATE INDEX `idx_digital_object_record_id` ON `digital_object_migration_result` (`digital_object_record_id`);
CREATE INDEX `idx_sls_collection` ON `digital_object_migration_result` (`collection_name`, `archive_nr`);
