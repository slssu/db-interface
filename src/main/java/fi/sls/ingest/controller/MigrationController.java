package fi.sls.ingest.controller;

import fi.sls.ingest.controller.request.IngestMigrationRequest;
import fi.sls.ingest.controller.response.BinaryDataResponse;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.migration.MigrationManager;
import fi.sls.ingest.migration.MigrationResult;
import fi.sls.ingest.migration.report.MigrationMasterFilesReport;
import fi.sls.ingest.migration.report.XLSXMigrationStatusReport;
import fi.sls.ingest.migration.report.XLSXReport;
import fi.sls.ingest.proxy.slsfileapi.FileAPIMigrateResponseBody;
import fi.sls.ingest.proxy.slsfileapi.FileAPIResponseBody;
import fi.sls.ingest.reporting.StagingItemsReport;
import fi.sls.ingest.reporting.xlsx.XLSXStagingItemsReport;
import fi.sls.ingest.security.jwt.UserPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static net.logstash.logback.argument.StructuredArguments.kv;

@BasePathAwareController
@RestController
@Slf4j
public class MigrationController {

    MigrationManager migrationManager;

    @Autowired
    public MigrationController(MigrationManager migrationManager){
        this.migrationManager = migrationManager;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "/migration/start", produces = {MediaType.APPLICATION_JSON_VALUE})
    public MigrationResult start(@Valid @RequestBody IngestMigrationRequest migrationRequest) throws IOException {

        log.debug("Received migration/start request: {}",
                kv(LogMessageKey.REQUEST_BODY, migrationRequest)
        );
        MigrationResult result;
        if(migrationRequest.getEntityIdentifier() != null){
            // use entity identifier first filtering for results
            result = migrationManager.migrate(migrationRequest);
        } else {
            result = migrationManager.migrate(
                    migrationRequest.getCollectionName(),
                    migrationRequest.getArchiveNr(),
                    migrationRequest.isDryRun(),
                    migrationRequest.getLimit(),
                    migrationRequest.getPageNr(),
                    migrationRequest.getMaxPageNr(),
                    migrationRequest.getEntityIdentifier()
            );
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "/migration/start", produces = {"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    public void startWithReport(@Valid @RequestBody IngestMigrationRequest migrationRequest, HttpServletResponse response) throws IOException {

        log.debug("Received migration/start request: {}",
                kv(LogMessageKey.REQUEST_BODY, migrationRequest)
        );

        MigrationResult result;
        if(migrationRequest.getEntityIdentifier() != null){
            // use entity identifier first filtering for results
            result = migrationManager.migrate(migrationRequest);
        } else {
            result = migrationManager.migrate(
                    migrationRequest.getCollectionName(),
                    migrationRequest.getArchiveNr(),
                    migrationRequest.isDryRun(),
                    migrationRequest.getLimit(),
                    migrationRequest.getPageNr(),
                    migrationRequest.getMaxPageNr(),
                    migrationRequest.getEntityIdentifier()
            );
        }

        XLSXReport report = new XLSXReport(result);
        Workbook book = report.generate();

        LocalDate now = LocalDate.now();
        String filename = result.getCollectionName()+"_"+result.getArchiveNr()+"_migration-"+now.format(DateTimeFormatter.ISO_LOCAL_DATE)+".xlsx";

        populateReportResponse(book, filename, response);
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "/migration/start/xlsx")
    public BinaryDataResponse startWithReportXlsx(@Valid @RequestBody IngestMigrationRequest migrationRequest, HttpServletResponse response) throws IOException {

        log.debug("Received migration/start/xlsx request: {}",
                kv(LogMessageKey.REQUEST_BODY, migrationRequest)
        );

        MigrationResult result;
        if(migrationRequest.getEntityIdentifier() != null){
            // use entity identifier first filtering for results
            result = migrationManager.migrate(migrationRequest);
        } else {
            result = migrationManager.migrate(
                    migrationRequest.getCollectionName(),
                    migrationRequest.getArchiveNr(),
                    migrationRequest.isDryRun(),
                    migrationRequest.getLimit(),
                    migrationRequest.getPageNr(),
                    migrationRequest.getMaxPageNr(),
                    migrationRequest.getEntityIdentifier()
            );
        }

        XLSXReport report = new XLSXReport(result);
        Workbook book = report.generate();

        LocalDate now = LocalDate.now();
        String filename = result.getCollectionName()+"_"+result.getArchiveNr()+"_migration-"+now.format(DateTimeFormatter.ISO_LOCAL_DATE)+".xlsx";

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        book.write(out);
        book.close();

        return new BinaryDataResponse(out, filename);
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "/migration/report", produces = {"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    public void report(@Valid @RequestBody IngestMigrationRequest migrationRequest, HttpServletResponse response) throws IOException {

        log.debug("Received migration/report request: {}",
                kv(LogMessageKey.REQUEST_BODY, migrationRequest)
        );

        MigrationResult result = migrationManager.generateReport(
                migrationRequest.getCollectionName(),
                migrationRequest.getArchiveNr()
        );

        XLSXMigrationStatusReport report = new XLSXMigrationStatusReport(result);
        Workbook book = report.generate();

        LocalDate now = LocalDate.now();
        String filename = result.getCollectionName()+"_"+result.getArchiveNr()+"_migration_report-"+now.format(DateTimeFormatter.ISO_LOCAL_DATE)+".xlsx";

        populateReportResponse(book, filename, response);
    }

    /**
     * Export an report on a collection for the authenticated user in XLSX format
     *
     * Response is returned in a JSON structure as base64 encoded so the client can decide how to present
     * it to user.
     */
    @RequestMapping(method = RequestMethod.POST, value = "/migration/report/xlsx")
    public BinaryDataResponse exportReport(@Valid @RequestBody IngestMigrationRequest migrationRequest, HttpServletResponse response) throws IOException {
        log.debug("Received migration/report/xlsx request: {}",
                kv(LogMessageKey.REQUEST_BODY, migrationRequest)
        );

        MigrationResult result = migrationManager.generateReport(
                migrationRequest.getCollectionName(),
                migrationRequest.getArchiveNr()
        );

        XLSXMigrationStatusReport report = new XLSXMigrationStatusReport(result);
        Workbook book = report.generate();

        LocalDate now = LocalDate.now();
        String filename = result.getCollectionName()+"_"+result.getArchiveNr()+"_migration_report-"+now.format(DateTimeFormatter.ISO_LOCAL_DATE)+".xlsx";

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        book.write(out);
        book.close();

        return new BinaryDataResponse(out, filename);
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "/migration/masterfiles-report", produces = {MediaType.APPLICATION_JSON_VALUE})
    public MigrationMasterFilesReport migrationMasterFilesReport(@Valid @RequestBody IngestMigrationRequest migrationRequest) {

        MigrationMasterFilesReport report = migrationManager.generateMasterfilesReport(migrationRequest.getCollectionName(), migrationRequest.getArchiveNr());

        return report;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/migration/continue")
    public void continueMigration(
            @PathParam("authToken") String authToken,
            @Valid @RequestBody FileAPIMigrateResponseBody processingResult)
    {
        log.debug("Received migration/continue request: {}",
                kv(LogMessageKey.REQUEST_BODY, processingResult)
        );

        if (processingResult.getStatus().equalsIgnoreCase("ok")) {
            // Call manager to continue processing of item
            // This should be async and respond with an accepted httpd status
            migrationManager.postProcessItem(processingResult.getToken(), processingResult);
        } else if (processingResult.getStatus().equalsIgnoreCase("error")) {
            // mark item as having been skipped due to error
            migrationManager.rejectProcessingItem(processingResult.getToken(), processingResult.getReason());
        } else {
            // should not happen so lets log in case we end up here
            log.error("File API did not respond with 'ok' or 'error' {}",
                    kv("file_api_response", processingResult)
            );
        }
    }

    /**
     * Helper for generating a downloadable XLSX response for the response body
     *
     * @param book
     * @param response
     * @throws IOException
     */
    protected void populateReportResponse(Workbook book, String filename, HttpServletResponse response) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        book.write(out);
        book.close();

        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());

        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+ URLEncoder.encode(filename, "UTF-8")+"\"");
        response.setContentLength(out.size()); // if you know size of the file in advance

        IOUtils.copy(in, response.getOutputStream());
    }
}
