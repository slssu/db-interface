package fi.sls.ingest.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Utility for attempting to parse various date strings that might occur in
 * the FITS XML file to a LocalDateTime
 */
public class FITSDateUtil {

    private FITSDateUtil(){
        // noop constructor
    }

    /**
     * List of parsers that we attempt to use in order to extract a LocalDateTime object from a input string
     *
     * The parser are attempted in the order they are defined.
     */
    protected static List<DateTimeStringParser> parsers = Arrays.asList(
            (String val) -> {
                Long parsed = Long.parseLong(val);
                return LocalDateTime.ofInstant(Instant.ofEpochMilli(parsed), ZoneId.of("UTC"));
            },
            (String val) -> {
                DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
                return LocalDateTime.parse(val, formatter);
            },
            (String val) -> {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                return LocalDateTime.parse(val, formatter);
            },
            (String val) -> {
                // NLNZ Metadata Extractor creates weird format date, attempt convert to ISO
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy:MM:dd HH:mm:ss");
                return LocalDateTime.parse(val, formatter);
            },
            (String val) -> {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate d = LocalDate.parse(val, formatter);
                return d.atStartOfDay();
            }
    );

    public static LocalDateTime parseString(String val){

        Iterator<DateTimeStringParser> i = parsers.iterator();
        LocalDateTime dateTime = null;
        do{
            DateTimeStringParser p = i.next();

            try{
                dateTime = p.parse(val);
                // parser worked, so exit loop
                break;
            } catch (NumberFormatException | DateTimeParseException e){
                // failed but noop, so try next
            }

        } while(i.hasNext());

        if(dateTime == null){
            throw new IllegalArgumentException(String.format("Input value '%s' could not be parsed into a valid LocalDateTime object", val));
        }

        return dateTime;
    }

    @FunctionalInterface
    protected interface DateTimeStringParser {
        LocalDateTime parse(String val);
    }
}
