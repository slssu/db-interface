package fi.sls.ingest.manager;

import java.io.File;
import java.io.Serializable;

public interface IngestItem extends Serializable {

    String getPath();
    String getFitsPath();
    String getName();
    Boolean getIsDirectory();

    /**
     * A hash of the path to the file being processed.
     *
     * The path hash should be unique to the file.
     *
     * If one creates a new file with exactly the
     * same name at the same location it will not be detected currently.
     */
    String getPathHash();

    /**
     * Returns a File object based on the path value
     *
     * @return
     */
    File getFileCopy();
}
