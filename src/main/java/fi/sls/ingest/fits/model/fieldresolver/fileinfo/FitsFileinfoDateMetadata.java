package fi.sls.ingest.fits.model.fieldresolver.fileinfo;

import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.fieldresolver.FieldValueResolver;
import fi.sls.ingest.util.FITSDateUtil;

import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class FitsFileinfoDateMetadata extends FitsFileinfoMetadata {

    public static final String FIELD_CREATED = "created";
    public static final String FIELD_LAST_MODIFIED = "lastmodified";
    public static final String FIELD_FS_LAST_MODIFIED = "fslastmodified";


    public FitsFileinfoDateMetadata(String fieldName, FieldValueResolver resolver) {
        super(fieldName, resolver);
    }

    public FitsFileinfoDateMetadata(String fieldName, FitsResult fitsResult) {
        super(fieldName, fitsResult);
    }

    public FitsFileinfoDateMetadata(String fieldName, FitsResult fitsResult, FieldValueResolver resolver) {
        super(fieldName, fitsResult, resolver);
    }

    @Override
    public Optional<String> getValue(){

        // disable super class resolver call so we can
        // handle it after this class has done it's work instead
        disableNextResolver = true;

        Optional<String> superVal = super.getValue();

        if(superVal.isPresent() && superVal.get() != null && !superVal.get().isEmpty()){
            String val = superVal.get();

            try{
                return Optional.of(FITSDateUtil.parseString(val).format(DateTimeFormatter.ISO_DATE_TIME));
            } catch (IllegalArgumentException e){
                // this parser failed because input was not recognized, so try next in chain
            }
        }

        disableNextResolver = false;
        return executeNextResolver();
    }

}
