package fi.sls.ingest.validation;

import fi.sls.ingest.fits.exception.FitsResultException;
import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.fits.model.generated.Fits;
import fi.sls.ingest.fits.model.generated.IdentificationType;
import fi.sls.ingest.fits.model.generated.StatusType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FitsResultValidatorTest {

    FitsResultValidator testable;

    FitsResult mockResult;


    @BeforeEach
    void setUp() {
        testable = new FitsResultValidator();

        mockResult = mock(FitsResult.class);

        // set the defaults to valid, individual tests set the failing values
        when(mockResult.getFileMD5()).thenReturn("a8f5f167f44f4964e6c998dee827110c");
        when(mockResult.getRawResult()).thenReturn("therawresultxml");

        Fits fits = mock(Fits.class);
        IdentificationType identificationType = mock(IdentificationType.class);
        when(identificationType.getStatus()).thenReturn(StatusType.SINGLE_RESULT);

        when(fits.getIdentification()).thenReturn(identificationType);

        when(mockResult.getFitsResult()).thenReturn(fits);
    }

// removed 2020-10-21 Jens: File API handles md5 checksums completely
//    @Test
//    void validate_throwsExceptionOnMissingChecksum() {
//
//        when(mockResult.getFileMD5()).thenReturn(null);
//        FitsResultException e = assertThrows(FitsResultException.class, () -> {
//           testable.validate(mockResult);
//        });
//
//        assertEquals("FITS did not return valid MD5 checksum, value was null", e.getMessage());
//
//
//        when(mockResult.getFileMD5()).thenReturn("notarealchecksum");
//
//        e = assertThrows(FitsResultException.class, () -> {
//            testable.validate(mockResult);
//        });
//
//        assertEquals("FITS did not return valid MD5 checksum, value was notarealchecksum", e.getMessage());
//
//    }

    @Test
    void validate_doesNotThrowOnValidChecksum() {

        when(mockResult.getFileMD5()).thenReturn("a8f5f167f44f4964e6c998dee827110c");

        testable.validate(mockResult);

    }

    @Test
    void validate_throwsExceptionOnPartialResult() {

        when(mockResult.getFitsResult().getIdentification().getStatus()).thenReturn(StatusType.PARTIAL);

        FitsResultException e = assertThrows(FitsResultException.class, () -> {
            testable.validate(mockResult);
        });

        assertEquals("FITS returned a partial result. This might be because one of the analysis tools failed to run.", e.getMessage());

    }


    @Test
    void validate_throwsExceptionOnNoRawResult() {

        when(mockResult.getRawResult()).thenReturn(null);

        FitsResultException e = assertThrows(FitsResultException.class, () -> {
            testable.validate(mockResult);
        });

        assertEquals("FITS did not return a raw FITS result XML.", e.getMessage());

    }
}