package fi.sls.ingest.fits.model.fieldresolver;

import fi.sls.ingest.fits.model.FitsResult;

import java.util.Optional;

public interface FieldValueResolver {

    /**
     * Returns the field value from an FITS XML field as a string.
     *
     * It is up to the calling code to typecast the value to the required type.
     *
     * @return
     */
    Optional<String> getValue();


    /**
     * Returns the FitsResult object from which the value should be extracted
     *
     * This is mainly useful so that composed resolvers can provide a constructor that
     * only accepts another FieldValueResolver, without having to request the FitsResult
     * object as a separate argument.
     *
     * @return
     */
    FitsResult getFitsResult();
}
