package fi.sls.ingest.actuator.metrics;

import fi.sls.ingest.actuator.metrics.supplier.ProcessingItemAverageDurationSupplier;
import fi.sls.ingest.actuator.metrics.supplier.ProcessingItemBytesSupplier;
import fi.sls.ingest.actuator.metrics.supplier.ProcessingItemCountSupplier;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.repository.IngestItemRepository;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Publishes a metric for the ProcessingStatus of the IngestProcessingItems currently in the database
 */
@Component
public class IngestProcessingMetric {

    public static final String NAME_PREFIX = "ingestapi.items";

    @Autowired
    public IngestProcessingMetric(MeterRegistry registry, IngestItemRepository ingestItemRepository){
        registerMonitors(registry, ingestItemRepository);
    }

    protected void registerMonitors(MeterRegistry registry, IngestItemRepository ingestItemRepository){

        for(ProcessingStatus s : ProcessingStatus.values()){
            Gauge
                    .builder(NAME_PREFIX + ".count", new ProcessingItemCountSupplier(ingestItemRepository, s))
                    .tag("status", s.name())
                    .description("The total nr of processing items currently having the given status")
                    .register(registry);

        }

        Gauge
                .builder(NAME_PREFIX + ".totalBytesProcessed", new ProcessingItemBytesSupplier(ingestItemRepository))
                .description("The total nr of bytes processed by the system (having status COMPLETE or ARCHIVED)")
                .register(registry);

        Gauge
                .builder(NAME_PREFIX + ".averageProcessingDuration", new ProcessingItemAverageDurationSupplier(ingestItemRepository))
                .description("The average time in seconds it takes for an item to be processed after the STAGING phase until the data has been stored in Arkiva")
                .register(registry);


    }
}
