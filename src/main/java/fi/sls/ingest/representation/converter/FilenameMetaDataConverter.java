package fi.sls.ingest.representation.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.representation.file.FilenameMetaData;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import java.io.IOException;

@Slf4j
public class FilenameMetaDataConverter extends JsonStringConverter implements AttributeConverter<FilenameMetaData, String> {
    public FilenameMetaDataConverter(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    public String convertToDatabaseColumn(FilenameMetaData filenameMetaData) {
        String rtnJson = null;

        if(filenameMetaData == null){
            return null;
        } else {
            try{
                rtnJson = objectMapper.writeValueAsString(filenameMetaData);
            } catch (JsonProcessingException e) {
                log.error("Error converting from List of FilenameMetaData to JSON: {}", e.getMessage());
            }
            return rtnJson;
        }
    }

    @Override
    public FilenameMetaData convertToEntityAttribute(String jsonData) {

        if(jsonData == null){
            return null;
        } else {
            FilenameMetaData rtnItem = null;
            try{
                rtnItem = objectMapper.readValue(jsonData, FilenameMetaData.class);
            } catch (IOException e) {
                log.error("Error converting to List of FilenameMetaData from JSON: {}", e.getMessage());
            }
            return rtnItem;
        }


    }
}
