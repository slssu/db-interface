package fi.sls.ingest.proxy.filemaker.response.record;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import fi.sls.ingest.proxy.filemaker.request.RecordPayload;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MTCollectionIntellectualEntityFieldData implements RecordPayload {

    @JsonProperty("samling_nummer")
    Long collectionNumber;

    @JsonProperty("ie_nummer")
    Long intellectualEntityNumber;
}
