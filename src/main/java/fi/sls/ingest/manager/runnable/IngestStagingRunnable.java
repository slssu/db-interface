package fi.sls.ingest.manager.runnable;

import fi.sls.ingest.exception.IngestFilenameException;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.manager.IngestFileRenamer;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.parser.FilenameMetaParser;
import fi.sls.ingest.proxy.slsfileapi.exception.FileAPIException;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.validation.StagingItemsValidator;
import fi.sls.ingest.ws.SocketMessenger;
import io.micrometer.core.annotation.Timed;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * A runnable class that processes an Ingestable file to prepare it for the staging phase
 *
 *
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class IngestStagingRunnable extends IngestRunnableBase {

    /**
     * Parser used for extracting metadata from the file name being processed
     */
    @Getter
    private FilenameMetaParser filenameMetaParser;

    /**
     * Validator used to ensure the item does not clash with other items already in the queue
     */
    private StagingItemsValidator stagingItemsValidator;

    @Autowired
    protected IngestStagingRunnable(
            IngestItemRepository ingestItemRepository,
            SocketMessenger socketMessenger,
            FilenameMetaParser filenameMetaParser,
            StagingItemsValidator stagingItemsValidator
    ) {
        super(ingestItemRepository, socketMessenger);

        this.filenameMetaParser = filenameMetaParser;
        this.stagingItemsValidator = stagingItemsValidator;
    }

    @Timed("ingestapi.runnable.staging")
    public void run(IngestProcessingItem item) {

        String filename = item.getItem().getName();

        log.info("Processing file at path: {}",
                kv(LogMessageKey.FILE_PATH, item.getItem().getFitsPath()),
                kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken())
        );

        setProcessingItemStatusAndSend(ProcessingStatus.PROCESSING_FILENAME_METADATA_STAGING, item);

        FilenameMetaData metaData = filenameMetaParser.parse(filename);
        item.setFilenameMetaData(metaData);
        log.debug("FilenameMetaData extracted from file: {}",
                kv(LogMessageKey.FILE_PATH, item.getItem().getFitsPath()),
                kv(LogMessageKey.FILENAME_METADATA, metaData),
                kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
        );

        // validate item against rest of queue
        stagingItemsValidator.validate(item);

        // if the item status changed to rejected due to staging validator, change status to staging_rejected
        if(item.getProcessingStatus() == ProcessingStatus.REJECTED){
            setProcessingItemStatusAndSend(ProcessingStatus.STAGING_REJECTED, item);
        } else {
            setProcessingItemStatusAndSend(ProcessingStatus.STAGING, item);
        }
    }
}
