package fi.sls.ingest.controller;

import fi.sls.ingest.comparator.WindowsExplorerComparator;
import fi.sls.ingest.config.IngestConfig;
import fi.sls.ingest.filesystem.filter.NoHiddenFilesFilter;
import fi.sls.ingest.rest.IngestFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static net.logstash.logback.argument.StructuredArguments.kv;


/**
 * Provides REST methods for listing and navigating the content of the inboxPath defined in configuration
 */
@BasePathAwareController
@RestController
@Slf4j
public class FileBrowserController {

    private IngestConfig metaDataConfig;

    @Autowired
    public FileBrowserController(IngestConfig metaDataConfig){
        this.metaDataConfig = metaDataConfig;
    }


    @RequestMapping(method = RequestMethod.GET, value = "/files/inbox")
    public List<IngestFile> listInboxFiles(@RequestParam(value = "startPath", required = false) String startPath) throws IOException, InvalidPathException {
        log.debug("Received listInboxFiles request: {}", kv("start_path", startPath));
        File f;
        if (startPath != null) {
            f = new File(metaDataConfig.getInboxPath().concat(startPath));
        } else {
            f = new File(metaDataConfig.getInboxPath());
        }

        if (!f.isFile() && !f.isDirectory()) {
            throw new InvalidPathException(f.getAbsolutePath(), "Path is not a valid file or directory in the ingest inbox directory");
        }


        // convert files to return item type
        List<IngestFile> items = new ArrayList<>();

        NoHiddenFilesFilter ff = new NoHiddenFilesFilter();

        File[] files = f.listFiles(ff);

        Comparator<File> comp = new Comparator<File>() {
            private final Comparator<String> NATURAL_SORT = new WindowsExplorerComparator();

            @Override
            public int compare(File o1, File o2) {;
                return NATURAL_SORT.compare(o1.getName(), o2.getName());
            }
        };

        List<File> sortedFiles = Arrays.stream(files).sorted(comp).collect(Collectors.toList());

        for (File file : sortedFiles) {
            items.add(IngestFile.fromFile(file, metaDataConfig.getInboxPath()));
        }

        return items;
    }

}
