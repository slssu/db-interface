package fi.sls.ingest.manager.runnable;

import fi.sls.ingest.config.IngestConfig;
import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.fits.FitsService;
import fi.sls.ingest.fits.exception.FitsResultException;
import fi.sls.ingest.fits.model.FitsResult;
import fi.sls.ingest.log.LogMessageKey;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.parser.FilenameMetaParser;
import fi.sls.ingest.proxy.slsfileapi.FileAPIMoveRequestBody;
import fi.sls.ingest.proxy.slsfileapi.SLSFileAPI;
import fi.sls.ingest.proxy.slsfileapi.exception.FileAPIException;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.representation.file.FilenameMetaData;
import fi.sls.ingest.ws.SocketMessenger;
import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static net.logstash.logback.argument.StructuredArguments.kv;


/**
 * A Runnable class that processes an ingestable file.
 *
 * The class first validates the given filename before proceeding with
 * steps to extract metadata from both the filename and the file itself.
 *
 * Once an MD5 sum has been calculated and metadata extracted, a request is made to an external service
 * to move the file to permanent storage. After the external service calls back to a callback endpoint,
 * the process continues in the IngestPostProcessRunnable
 *
 * @see IngestPostProcessRunnable
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class IngestPreProcessRunnable extends IngestRunnableBase {

    /**
     * Parser used for extracting metadata from the file name being processed
     */
    private FilenameMetaParser filenameMetaParser;

    /**
     * FITS stack service wrapper for extracting file metadata
     */
    private FitsService fitsService;


    /**
     * SLS File API wrapper for transferring files from projects to masterfiles
     */
    private SLSFileAPI slsFileAPI;

    /**
     * Configuration for this API
     */
    private IngestConfig ingestConfig;

    @Autowired
    public IngestPreProcessRunnable(
            IngestItemRepository ingestItemRepository,
            SocketMessenger socketMessenger,
            FilenameMetaParser filenameMetaParser,
            FitsService fitsService,
            SLSFileAPI slsFileAPI,
            IngestConfig ingestConfig
    ){
        super(ingestItemRepository, socketMessenger);

        this.filenameMetaParser = filenameMetaParser;
        this.fitsService = fitsService;
        this.slsFileAPI = slsFileAPI;
        this.ingestConfig = ingestConfig;
    }

    @Timed("ingestapi.runnable.preProcessing")
    public void run(IngestProcessingItem item) {

        log.info("Processing file at path: {}",
                kv(LogMessageKey.FILE_PATH, item.getItem().getFitsPath()),
                kv(LogMessageKey.INGEST_ITEM_TOKEN, item.getToken())
        );

        try {
            // clear any previous errors before re-running the steps
            item.resetErrorFlags();

            // extract metadata from the actual file using FITS
            log.debug("Attempting to parse metadata from file content using FITS: {}",
                    kv(LogMessageKey.FILE_PATH, item.getItem().getFitsPath()),
                    kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
            );
            setProcessingItemStatusAndSend(ProcessingStatus.PROCESSING_METADATA, item);

            CompletableFuture<FitsResult> fitsResult = fitsService.examine(item.getItem().getFitsPath(), item.getItem().getPath());
            FitsResult fitsResultData = fitsResult.get();
            log.debug("Using FITS version: {}",
                    kv("fits_version", fitsResultData.getFitsVersion()),
                    kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
            );
            // removed 2020-10-21 File API handles MD5 checksums
//            log.debug("Calculated MD5 checksum for file: {} {}",
//                    kv(LogMessageKey.FILE_PATH, item.getItem().getFitsPath()),
//                    kv("fits_md5", fitsResultData.getFileMD5()),
//                    kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
//            );

            // moved to fits examine service
//            // check that basic data was returned (MD5 not null and has 32 char length)
//            if(fitsResultData.getFileMD5() == null || !fitsResultData.getFileMD5().matches("(?i:[0-9a-z]){32}")){
//                throw new IngestProcessingException("FITS did not return valid MD5 checksum");
//            }
//            if(fitsResultData.getRawResult() == null){
//                throw new IngestProcessingException("FITS did not return the raw FITS result XML");
//            }

            // removed 2020-10-21 File API handles MD5 checksums.
            // Ensure this by setting before checksum to null
            //item.setMd5ChecksumBefore(fitsResultData.getFileMD5());
            item.setMd5ChecksumBefore(null);
            item.setFileSize(fitsResultData.getFileSize());
            item.setRawFitsResult(fitsResultData.getRawResult());

            // Create file MOVE request and start the long wait
            FileAPIMoveRequestBody request = FileAPIMoveRequestBody.createFrom(item, fitsResultData, ingestConfig.getHostRelativeAPIUrl("ingest/continue?token="+item.getToken()));
            item.setFileAPICallbackURL(request.getCallbackUrl());
            setProcessingItemStatusAndSend(ProcessingStatus.INIT_PROCESSING_USER_COPIES, item);

            log.debug("Sending move request to File API: {}", kv("request", request));
            slsFileAPI.move(request);

            setProcessingItemStatusAndSend(ProcessingStatus.PROCESSING_USER_COPIES, item);

        } catch(IllegalArgumentException | FileAPIException | FitsResultException | HttpClientErrorException | HttpServerErrorException | IngestProcessingException e){
            item.setMessage(e.getMessage());
            setProcessingItemStatusAndSend(ProcessingStatus.RETRY, item);
            log.warn("Rejected file at path: {} {}",
                    kv(LogMessageKey.FILE_PATH, item.getItem().getFitsPath()),
                    kv("reject_reason", e.getMessage()),
                    kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
            );

            // re-throw so queue gets notified as well
            throw e;
        } catch(InterruptedException | ExecutionException | IOException | SAXException e){
            item.setMessage(e.getMessage());
            setProcessingItemStatusAndSend(ProcessingStatus.RETRY, item);
            log.warn("Service call failed: {} {}",
                    kv(LogMessageKey.FILE_PATH, item.getItem().getFitsPath()),
                    kv("reject_reason", e.getMessage()),
                    kv(LogMessageKey.INGEST_PROCESSING_ITEM, item.getLoggableItem())
            );

            // re-throw so queue gets notified as well
            throw new IngestProcessingException(e);
        }
    }
}
