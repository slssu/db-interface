package fi.sls.ingest.queue.rabbitmq.sender;


import com.fasterxml.jackson.databind.ObjectMapper;
import fi.sls.ingest.exception.IngestProcessingException;
import fi.sls.ingest.manager.IngestProcessingItem;
import fi.sls.ingest.manager.ProcessingStatus;
import fi.sls.ingest.queue.rabbitmq.sender.IngestItemSenderBase;
import fi.sls.ingest.repository.IngestItemRepository;
import fi.sls.ingest.ws.SocketMessenger;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.Tika;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * Handles sending a processing item to a pre-processing queue,
 * from where it is eventually picked up by a available receiver.
 */
@Service
@Slf4j
public class IngestPreProcessingItemSender extends IngestItemSenderBase {

    Queue videoQueue;
    protected Tika tika;

    @Autowired
    public IngestPreProcessingItemSender(RabbitTemplate template,
                                         @Qualifier(value = "ingestPreProcessingQueue") Queue queue,
                                         @Qualifier(value = "ingestPreProcessingVideoQueue") Queue videoQueue,
                                         ObjectMapper objectMapper,
                                         SocketMessenger socketMessenger,
                                         IngestItemRepository ingestItemRepository
    ){
        super(template, queue, objectMapper, socketMessenger, ingestItemRepository);

        this.videoQueue = videoQueue;
        this.tika = new Tika();
    }

    /**
     * Sends our ingest item to the queue for processing.
     *
     * @param item The IngestProcessingItem to send to the queue.
     * @return The item with updated processing status field.
     */
    public IngestProcessingItem send(IngestProcessingItem item) throws IngestProcessingException {
        return send(item, ProcessingStatus.SEND_TO_PRE_PROCESSING_QUEUE);
    }

    @Override
    protected void doSendToQueue(String msg, IngestProcessingItem item){
        String examinedFilePath = item.getItem().getPath();
        // make quick guess on type of file so we can direct video files to separate queue
        File file = new File(examinedFilePath);
        String mimeType = null;

        try {
            mimeType = tika.detect(file);
            log.debug("detected mimetype on file {} {} {}",
                    kv("mime_type", mimeType),
                    kv("filename", item.getFilenameMetaData().getFilename()),
                    kv("examined_file_path", examinedFilePath)
            );

            if(mimeType.contains("video")){
                log.debug("will send to queue {} {} {} {}",
                        kv("queue_name", videoQueue.getName()),
                        kv("mime_type", mimeType),
                        kv("filename", item.getFilenameMetaData().getFilename()),
                        kv("examined_file_path", examinedFilePath)
                );
                template.convertAndSend(videoQueue.getName(), msg);
            } else {
                log.debug("will send to queue {} {} {} {}",
                        kv("queue_name", queue.getName()),
                        kv("mime_type", mimeType),
                        kv("filename", item.getFilenameMetaData().getFilename()),
                        kv("examined_file_path", examinedFilePath)
                );
                super.doSendToQueue(msg, item);
            }

        } catch (IOException e) {
            throw new IngestProcessingException(e.getMessage());
        }
    }
}
