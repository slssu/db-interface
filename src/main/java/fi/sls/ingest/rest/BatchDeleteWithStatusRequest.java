package fi.sls.ingest.rest;

import fi.sls.ingest.manager.ProcessingStatus;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
public class BatchDeleteWithStatusRequest {

    @NotNull
    @Valid
    ProcessingStatus status;

    @Valid
    String userIds = "";
}
