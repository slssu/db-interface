package fi.sls.ingest.config;

import fi.sls.ingest.log.RequestResponseLoggingInterceptor;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Collections;

@Configuration
public class RestTemplateConfig {

    /**
     * Used for requests that might take a long time
     *
     * E.g. the FITS examine request can take several minutes depending on the size of the
     * file being examined.
     *
     * @param builder
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws KeyManagementException
     */
    @Bean
    @Primary
    public RestTemplate restTemplate(RestTemplateBuilder builder)
            throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        CloseableHttpClient httpClient = createClientBuilder().build();

        return createRestTemplateWithClient(httpClient);

    }

    /**
     * Used for requests that should timeout quickly in case data is not returned.
     *
     * For example health checks should not wait for a response for several minutes.
     *
     * @param builder
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws KeyManagementException
     */
    @Bean(name = "shortTimeoutRestTemplate")
    public RestTemplate shortTimeoutRestTemplate(RestTemplateBuilder builder) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        return createRestTemplateWithTimeout(5000, 5000, 5000);
    }

    /**
     * Used for requests that usually timeout quickly but might sometimes delay due to network issues.
     *
     * For example Filemaker requests should normally complete within a second or two.
     *
     * @param builder
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws KeyManagementException
     */
    @Bean(name = "mediumTimeoutRestTemplate")
    public RestTemplate mediumTimeoutRestTemplate(RestTemplateBuilder builder) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        return createRestTemplateWithTimeout(30000, 30000, 30000);
    }

    protected RestTemplate createRestTemplateWithTimeout(int connectTimeout, int connectionRequestTimeout, int socketTimeout) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        RequestConfig requestConfig = RequestConfig.custom().
                setConnectTimeout(connectTimeout).
                setConnectionRequestTimeout(connectionRequestTimeout).
                setSocketTimeout(socketTimeout).build();

        HttpClientBuilder clientBuilder = createClientBuilder();

        clientBuilder.setDefaultRequestConfig(requestConfig);

        CloseableHttpClient httpClient = clientBuilder.build();

        return createRestTemplateWithClient(httpClient);
    }

    protected HttpClientBuilder createClientBuilder() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        // FIXME: This disables all SSL due to test api server certificate error, so we need this to be a configurable thing from the yaml files
        // Default should be to just return an instance of RestTemplate default setup
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

        SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build();

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

        return HttpClients.custom()
                .setSSLSocketFactory(csf);
    }

    protected RestTemplate createRestTemplateWithClient(CloseableHttpClient httpClient){
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);

        BufferingClientHttpRequestFactory bfFactory = new BufferingClientHttpRequestFactory(requestFactory);
        RestTemplate restTemplate = new RestTemplate(bfFactory);
//        restTemplate.setErrorHandler(new RestErrorHandler());
        restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));

        return restTemplate;
    }
}
