package fi.sls.ingest.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="sls.filemaker")
public class FileMakerConfig {

    @Getter
    @Setter
    private String url;

    @Getter
    @Setter
    private String database;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    @Value("${sls.filemaker.api.authHeaderPrefix:Bearer}")
    private String authHeader;

    @Getter
    @Setter
    private String agentName;

    @Getter
    @Setter
    private String agentVersion;

}
