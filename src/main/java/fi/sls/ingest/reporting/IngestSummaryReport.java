package fi.sls.ingest.reporting;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
public class IngestSummaryReport implements Serializable {

    LocalDate fromDate;
    LocalDate toDate;

    IngestItemSummaryReport itemSummary;
    List<IngestItemSummaryReport> itemSummaryByUser;


    public IngestSummaryReport(LocalDate fromDate, LocalDate toDate){
        this.fromDate = fromDate;
        this.toDate = toDate;
    }
}
