package fi.sls.ingest.proxy.filemaker.exception;

/**
 * Exception thrown for unexpected data returned from FileMaker API.
 *
 * This can happen for instance if a record is created, but
 * the record was not found after all on the subsequent fetch.
 *
 * Or if the data fetched from FileMaker does not match what was
 * sent in a previous update/create request.
 *
 */
public class FileMakerDataMismatchException extends RuntimeException {
    public FileMakerDataMismatchException(String message){
        super(message);
    }
}
