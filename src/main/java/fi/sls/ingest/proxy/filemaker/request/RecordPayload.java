package fi.sls.ingest.proxy.filemaker.request;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public interface RecordPayload {
}
